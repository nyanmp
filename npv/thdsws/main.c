#ifndef THDSWS_MAIN_C
#define THDSWS_MAIN_C
/*
 * code protected with a GNU affero GPLv3 license 
 * copyright (C) 2020 Sylvain BERTRAND
 */
#include <stdlib.h>
#include <stdarg.h>
#include <signal.h>
#include <pthread.h>
#include <errno.h>
#if __GNUC__ > 4
	#include <stdatomic.h>
#endif
#include <unistd.h>
#include <time.h>
#include "npv/c_fixing.h"
#include "npv/global.h"
#include "npv/public.h"
#include "npv/thdsws/public.h"
/*----------------------------------------------------------------------------*/
#include "npv/namespace/ffmpeg.h"
#include "npv/thdsws/namespace/public.h"
#include "npv/thdsws/namespace/main.c"
/*----------------------------------------------------------------------------*/
#include "npv/thdsws/local/code.frag.c"
#include "npv/thdsws/public/code.frag.c"
/*----------------------------------------------------------------------------*/
#define CLEANUP
#include "npv/namespace/ffmpeg.h"
#include "npv/thdsws/namespace/public.h"
#include "npv/thdsws/namespace/main.c"
#undef CLEANUP
/*----------------------------------------------------------------------------*/
#endif
