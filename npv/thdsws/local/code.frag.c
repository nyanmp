struct thdsws_ctx_private_t {
	pthread_mutex_t mutex;
	u8 state;
	pthread_cond_t have_fr_to_scale;
	struct SwsContext *sws;
};
STATIC void fatal(u8 *fmt, ...)
{
	va_list ap;

	npv_perr("scaler:");
	va_start(ap, fmt);
	npv_vfatal(fmt, ap);
	va_end(ap); /* unreachable */
}
STATIC void fatalw(u8 *fmt, ...)
{
	va_list ap;

	npv_perr("scaler worker:");
	va_start(ap, fmt);
	npv_vfatal(fmt, ap);
	va_end(ap); /* unreachable */
}
STATIC void state_lock(struct thdsws_ctx_private_t *ctx_priv)
{
	int r;

	r = pthread_mutex_lock(&ctx_priv->mutex);
	if (r != 0)
		fatal("unable to lock the state\n");
}
STATIC void state_unlock(struct thdsws_ctx_private_t *ctx_priv)
{
	int r;

	r = pthread_mutex_unlock(&ctx_priv->mutex);
	if (r != 0)
		fatal("unable to unlock the state\n");
}
STATIC void do_work(struct thdsws_ctx_t *ctx, struct thdsws_ctx_private_t *ctx_priv)
{
	ctx_priv->sws = sws_get_cached_ctx(ctx_priv->sws,
		(int)ctx->cfg.width, (int)ctx->cfg.height, ctx->cfg.src_fmt,
		(int)ctx->cfg.width, (int)ctx->cfg.height, ctx->cfg.dst_fmt,
						(int)ctx->cfg.flags, 0, 0, 0);
	if (ctx_priv->sws == 0)
		fatalw("unable to get a ffmpeg context\n");

	/* XXX: this is the hotspot */
	(void)sws_scale(ctx_priv->sws, 
				(uint8_t const*const*)ctx->scale.src_slices,
				(int*)ctx->scale.src_strides,
				0, (int)ctx->cfg.height,
				(uint8_t*const*)&ctx->scale.dst_slice,
				(int*)&ctx->scale.dst_stride);
}
#define IDLE	0
#define RUNNING	1
STATIC void worker(struct thdsws_ctx_t *ctx,
					struct thdsws_ctx_private_t *ctx_priv)
{
	/* on entry the state must be locked */
	if (ctx_priv->state == RUNNING) {
		state_unlock(ctx_priv);
		do_work(ctx, ctx_priv);
		state_lock(ctx_priv);
		ctx_priv->state = IDLE;
	}
	/* state == IDLE */
	loop { /* spurious IDLE protection */
		int r;

		/* while waiting the mutex is unlocked */
		r = pthread_cond_wait(&ctx_priv->have_fr_to_scale,
							&ctx_priv->mutex);
		/* on exit the mutex is locked */
		if (r != 0)
			fatalw("an error occured while waiting for a frame\n");
		if (ctx_priv->state != IDLE)
			break;
		/* state == IDLE */
	}
}
#undef IDLE
#undef RUNNING
STATIC void *worker_entry(void *arg)
{
	int r;
	sigset_t sset;
	struct thdsws_ctx_t *ctx;
	struct thdsws_ctx_private_t *ctx_priv;

	r = sigfillset(&sset);
	if (r == -1)
		fatalw("unable to get a full signal mask\n");

	r = pthread_sigmask(SIG_SETMASK, &sset, 0);
	if (r != 0)
		fatalw("unable to \"block\" \"all\" signals\n");

	ctx = arg;
	ctx_priv = ctx->private;

	state_lock(ctx_priv);
	loop worker(ctx, ctx_priv); /* state must be locked on entry */
}
