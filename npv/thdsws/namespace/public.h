#ifndef CLEANUP
#define thdsws_ctx_t		npv_thdsws_ctx_t
#define thdsws_init_once	npv_thdsws_init_once
#define thdsws_is_busy		npv_thdsws_is_busy
#define thdsws_run		npv_thdsws_run
#define thdsws_wait_for_idle	npv_thdsws_wait_for_idle
#else
#undef thdsws_ctx_t
#undef thdsws_init_once
#undef thdsws_is_busy
#undef thdsws_run
#undef thdsws_wait_for_idle
#endif
