#ifndef CLEANUP
#define fatal		npv_thdsws_fatal
#define fatalw		npv_thdsws_fatalw
#define worker_entry	npv_thdsws_worker_entry
#define worker		npv_thdsws_worker
#define do_work		npv_thdsws_do_work
#define state_lock	npv_thdsws_state_lock
#define state_unlock	npv_thdsws_state_unlock
#else
#undef fatal
#undef fatalw
#undef worker_entry
#undef worker
#undef do_work
#undef state_lock
#undef state_unlock
#endif
