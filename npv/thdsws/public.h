#ifndef THDSWS_PUBLIC_H
#define THDSWS_PUBLIC_H
/*
 * code protected with a GNU affero GPLv3 license 
 * copyright (C) 2020 Sylvain BERTRAND
 */
#include <stdint.h>
#include <stdbool.h>
#include <libswscale/swscale.h>
#include <libavutil/pixfmt.h>
#include "npv/c_fixing.h"
/*----------------------------------------------------------------------------*/
#include "npv/namespace/ffmpeg.h"
#include "npv/thdsws/namespace/public.h"
/*----------------------------------------------------------------------------*/
struct thdsws_ctx_t {
	/* XXX: we use the scaler only for pix fmt conv */
	struct  {
		u32 width;
		u32 height;
		enum avutil_pixel_fmt_t src_fmt;
		enum avutil_pixel_fmt_t dst_fmt;
		u32 flags;
	} cfg;

	struct {
		/* care the ptrs and their content is stable */
		void *src_slices;
		u32 *src_strides;
		/* only one slice here */
		void *dst_slice;
		u32 dst_stride;
	} scale;

	void *private;
};
STATIC struct thdsws_ctx_t *thdsws_init_once(void);
STATIC bool thdsws_is_busy(struct thdsws_ctx_t *ctx);
STATIC void thdsws_run(struct thdsws_ctx_t *ctx);
STATIC void thdsws_wait_for_idle(struct thdsws_ctx_t *ctx);
/*----------------------------------------------------------------------------*/
#define CLEANUP
#include "npv/namespace/ffmpeg.h"
#include "npv/thdsws/namespace/public.h"
#undef CLEANUP
/*----------------------------------------------------------------------------*/
#endif
