#define RUNNING	1
STATIC void thdsws_run(struct thdsws_ctx_t *ctx)
{
	int r;
	struct thdsws_ctx_private_t *ctx_priv;

	ctx_priv = ctx->private;

	state_lock(ctx_priv);
	if (ctx_priv->state == RUNNING)
		fatal("the worker thread is already running, did you forget to wait for it to idle\n");
	ctx_priv->state = RUNNING;
	state_unlock(ctx_priv);
	r = pthread_cond_signal(&ctx_priv->have_fr_to_scale);
	if (r != 0)
		fatal("unable to signal the worker thread a frame is to be scaled\n");
}
#undef RUNNING
#define RUNNING	1
STATIC bool thdsws_is_busy(struct thdsws_ctx_t *ctx)
{
	struct thdsws_ctx_private_t *ctx_priv;
	bool r;

	ctx_priv = ctx->private;
	state_lock(ctx_priv);
	if (ctx_priv->state == RUNNING)
		r = true;
	else
		r = false;
	state_unlock(ctx_priv);
	return r;
}
#undef RUNNING
#define IDLE 0
STATIC struct thdsws_ctx_t *thdsws_init_once(void)
{
	int r;
	pthread_t worker;
	pthread_attr_t attr;
	struct thdsws_ctx_t *ctx;
	struct thdsws_ctx_private_t *ctx_priv;

	ctx = calloc(1, sizeof(*ctx));
	if (ctx == 0)
		fatal("unable to allocate memory for context\n");
	ctx_priv = calloc(1, sizeof(*ctx_priv));
	if (ctx_priv == 0)
		fatal("unable to allocate memory for private context\n");
	ctx->private = ctx_priv;

	r = pthread_mutex_init(&ctx_priv->mutex, 0);
	if (r != 0)
		fatal("unable to create the state mutex\n");
	r = pthread_cond_init(&ctx_priv->have_fr_to_scale, 0);
	if (r != 0)
		fatal("unable to create waiting condition\n");

	r = pthread_attr_init(&attr);
	if (r != 0)
		fatal("unable to initialize a worker thread attribute\n");

	r = pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_DETACHED);
	if (r != 0)
		fatal("unable to set the worker thread attribute to detach mode\n");

	/* be really sure the entry state is correct _before_ thd creation */
	ctx_priv->state = IDLE;
	r = pthread_create(&worker, &attr, &worker_entry, ctx);
	if (r != 0)
		fatal("unable to create the worker thread\n");
	pthread_attr_destroy(&attr);
	return ctx;
}
#undef IDLE
#define TIME_UNIT_NS	1000000 /* 1ms */
#define TIMEOUT_UNITS_N	100 /* 100 * 1 ms = 100 ms */
STATIC void thdsws_wait_for_idle(struct thdsws_ctx_t *ctx)
{
	u8 nanoloops_n;

	nanoloops_n = 0;
	loop {
		struct timespec ts;

		if (!thdsws_is_busy(ctx))
			break;
		ts.tv_sec = 0;
		ts.tv_nsec = TIME_UNIT_NS;
		loop {
			struct timespec rem;
			int r;

			memset(&rem, 0, sizeof(rem));
			errno = 0;
			r = nanosleep(&ts, &rem);
			if (r == 0)
				break;
			/* r == -1 */
			if (errno == EINTR) {
				memcpy(&ts, &rem, sizeof(ts));
				continue;
			}
			fatal("unable to sleep to wait for idle\n");
		}
		++nanoloops_n;
		if (nanoloops_n == TIMEOUT_UNITS_N)
			fatal("wait for idle timeout\n");
	}
}
#undef TIME_UNIT_NS
#undef TIMEOUT_UNITS_N
