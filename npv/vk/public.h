#ifndef NPV_VK_H
#define NPV_VK_H
/*
 * code protected with a GNU affero GPLv3 license 
 * copyright (C) 2020 Sylvain BERTRAND
 */
#include <stdbool.h>
#include <stddef.h>
#include <xcb/xcb.h>
#include "npv/c_fixing.h"
#include "npv/nyanvk/types.h"
#include "npv/nyanvk/syms_dev.h"
#include "npv/global.h"
/*----------------------------------------------------------------------------*/
#include "npv/vk/namespace/public.h"
/*----------------------------------------------------------------------------*/
#include "npv/vk/public/state.frag.h"
/*----------------------------------------------------------------------------*/
STATIC void init_once(void);
STATIC void surf_init_once(xcb_connection_t *c, u32 wind_id);
STATIC void swpchn_update(void);
STATIC void fatal(u8 *fmt, ...);
STATIC void warning(u8 *fmt, ...);
STATIC void vfatal(u8 *fmt, va_list ap);
STATIC void vwarning(u8 *fmt, va_list ap);
/*----------------------------------------------------------------------------*/
#define CLEANUP
#include "npv/vk/namespace/public.h"
#undef CLEANUP
/*----------------------------------------------------------------------------*/
#endif
