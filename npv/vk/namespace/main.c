#ifndef CLEANUP
#define check_vk_version			npv_vk_check_vk_version
#define cp_create				npv_vk_cp_create
#define dev_create				npv_vk_dev_create
#define dev_init				npv_vk_dev_init
#define dev_syms				npv_vk_devs_syms
#define dev_type_str				npv_vk_dev_type_str
#define fences_create				npv_vk_fences_create
#define instance_create				npv_vk_instance_create
#define instance_exts_dump			npv_vk_instance_exts_dump
#define instance_l				npv_vk_instance_l
#define instance_layers_dump			npv_vk_instance_layers_dump
#define instance_static_syms			npv_vk_instance_static_syms
#define instance_syms				npv_vk_instance_syms
#define loader_l				npv_vk_loader_l
#define loader_syms				npv_vk_loader_syms
#define load_vk_loader				npv_vk_load_vk_loader
#define misc_cbs_init_once			npv_vk_misc_cbs_init_once
#define phydev_exts_dump			npv_vk_phydev_exts_dump
#define phydev_init				npv_vk_phydev_init
#define phydev_mem_type_dump			npv_vk_phydev_mem_type_dump
#define phydev_mem_heap_dump			npv_vk_phydev_mem_heap_dump
#define phydev_surf_b8g8r8a8_srgb_setup		npv_vk_phydev_surf_b8g8r8a8_srgb_setup
#define phydev_surf_caps_dump			npv_vk_phydev_surf_caps_dump
#define phydev_surf_caps_get			npv_vk_phydev_surf_caps_get
#define phydev_surf_texel_mem_blk_fmts_dump	npv_vk_phydev_surf_texel_mem_blk_fmts_dump
#define pout					npv_vk_pout
#define present_mode_to_str			npv_vk_present_mode_to_str
#define q_get					npv_vk_q_get
#define sems_create				npv_vk_sems_create
#define surf_create				npv_vk_surf_create
#define swpchn_imgs_cbs_init_once		npv_vk_swpchn_imgs_cbs_init_once
#define swpchn_imgs_get				npv_vk_swpchn_imgs_get
#define swpchn_init_once			npv_vk_swpchn_init_once
#define swpchn_reinit				npv_vk_swpchn_reinit
#define texel_mem_blk_confs_dump		npv_vk_texel_mem_blk_confs_dump
#define tmp_phydev_and_q_fam_select		npv_vk_tmp_phydev_and_q_fam_select
#define tmp_phydev_mem_heaps_dump		npv_vk_tmp_phydev_mem_heaps_dump
#define tmp_phydev_mem_props_dump		npv_vk_tmp_phydev_mem_props_dump
#define tmp_phydev_mem_props_get		npv_vk_tmp_phydev_mem_props_get
#define tmp_phydev_mem_types_dump		npv_vk_tmp_phydev_mem_types_dump
#define tmp_phydev_q_fams_dump			npv_vk_tmp_phydev_q_fams_dump
#define tmp_phydev_q_fams_get			npv_vk_tmp_phydev_q_fams_get
#define tmp_phydev_q_fams_n_max			npv_vk_tmp_phydev_q_fams_n_max
#define tmp_phydev_surf_present_modes_dump	npv_vk_tmp_phydev_surf_present_modes_dump
#define tmp_phydev_surf_present_modes_get	npv_vk_tmp_phydev_surf_present_modes_get
#define tmp_phydev_t				npv_vk_tmp_phydev_t
#define tmp_phydevs_exts_dump			npv_vk_tmp_phydevs_exts_dump
#define tmp_phydevs_get				npv_vk_tmp_phydevs_get
#define tmp_phydevs_l				npv_vk_tmp_phydevs_l
#define tmp_phydevs_mem_props_dump		npv_vk_tmp_phydevs_mem_props_dump
#define tmp_phydevs_mem_props_get		npv_vk_tmp_phydevs_mem_props_get
#define tmp_phydevs_n_l				npv_vk_tmp_phydevs_n_l
#define tmp_phydevs_n_max			npv_vk_tmp_phydevs_n_max
#define tmp_phydevs_props_dump			npv_vk_tmp_phydevs_props_dump
#define tmp_phydevs_q_fams_dump			npv_vk_tmp_phydevs_q_fams_dump
#define tmp_phydevs_q_fams_get			npv_vk_tmp_phydevs_q_fams_get
#define tmp_phydevs_q_fams_surf_support_get	npv_vk_tmp_phydevs_q_fams_surf_support_get
#define tmp_present_modes_l			npv_vk_tmp_present_modes_l
#define tmp_present_modes_n_l			npv_vk_tmp_present_modes_n_l
#define tmp_present_modes_n_max			npv_vk_tmp_present_modes_n_max
#define tmp_selected_phydev_cherry_pick		npv_vk_tmp_selected_phydev_cherry_pick
#define uuid_str				npv_vk_uuid_str
/******************************************************************************/
#else
#undef check_vk_version
#undef cp_create
#undef dev_create
#undef dev_init
#undef dev_syms
#undef dev_type_str
#undef fences_create
#undef instance_create
#undef instance_exts_dump
#undef instance_l
#undef instance_layers_dump
#undef instance_static_syms
#undef instance_syms
#undef loader_l
#undef loader_syms
#undef load_vk_loader
#undef misc_cbs_init_once
#undef phydev_exts_dump
#undef phydev_init
#undef phydev_mem_type_dump
#undef phydev_mem_heap_dump
#undef phydev_surf_b8g8r8a8_srgb_setup
#undef phydev_surf_caps_dump
#undef phydev_surf_caps_get
#undef phydev_surf_texel_mem_blk_fmts_dump
#undef pout
#undef present_mode_to_str
#undef q_get
#undef sems_create
#undef surf_create
#undef swpchn_imgs_cbs_init_once
#undef swpchn_imgs_get
#undef swpchn_init_once
#undef swpchn_reinit
#undef texel_mem_blk_confs_dump
#undef tmp_phydev_and_q_fam_select
#undef tmp_phydev_mem_heaps_dump
#undef tmp_phydev_mem_props_dump
#undef tmp_phydev_mem_props_get
#undef tmp_phydev_mem_types_dump
#undef tmp_phydev_q_fams_dump
#undef tmp_phydev_q_fams_get
#undef tmp_phydev_q_fams_n_max
#undef tmp_phydev_surf_present_modes_dump
#undef tmp_phydev_surf_present_modes_get
#undef tmp_phydev_t
#undef tmp_phydevs_exts_dump
#undef tmp_phydevs_get
#undef tmp_phydevs_l
#undef tmp_phydevs_mem_props_dump
#undef tmp_phydevs_mem_props_get
#undef tmp_phydevs_n_l
#undef tmp_phydevs_n_max
#undef tmp_phydevs_props_dump
#undef tmp_phydevs_q_fams_dump
#undef tmp_phydevs_q_fams_get
#undef tmp_phydevs_q_fams_surf_support_get
#undef tmp_present_modes_l
#undef tmp_present_modes_n_l
#undef tmp_present_modes_n_max
#undef tmp_selected_phydev_cherry_pick
#undef uuid_str
#endif
