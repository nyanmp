#ifndef CLEANUP
#define color_space_layout_transition	npv_vk_color_space_layout_transition
#define dev_t				npv_vk_dev_t
#define fatal				npv_vk_fatal
#define init_once			npv_vk_init_once
#define phydev_t			npv_vk_phydev_t
#define sem_acquire_img_done		npv_vk_sem_acquire_img_done
#define	sem_blit_done			npv_vk_sem_blit_done
#define	sems_n				npv_vk_sems_n 
#define surf_p				npv_vk_surf_p
#define surf_init_once			npv_vk_surf_init_once
#define swpchn_update			npv_vk_swpchn_update
#define swpchn_imgs_n_max		npv_vk_swpchn_imgs_n_max
#define swpchn_t			npv_vk_swpchn_t
#define texel_mem_blk_fmt_t		npv_vk_texel_mem_blk_fmt_t
#define vfatal				npv_vk_vfatal
#define vwarning			npv_vk_vwarning
#define warning				npv_vk_warning
/*============================================================================*/
#else
#undef color_space_layout_transition
#undef dev_t
#undef fatal
#undef init_once
#undef phydev_t
#undef sem_acquire_img_done
#undef sem_blit_done
#undef sems_n 
#undef surf_p
#undef surf_init_once
#undef swpchn_update
#undef swpchn_imgs_n_max
#undef swpchn_t
#undef texel_mem_blk_fmt_t
#undef vfatal
#undef vwarning
#undef warning
#endif

