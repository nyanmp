#ifndef NPV_VK_C
#define NPV_VK_C
/*
 * code protected with a GNU affero GPLv3 license 
 * copyright (C) 2020 Sylvain BERTRAND
 */
#include <stdbool.h>
#include <stdarg.h>
#include <stdio.h>
#include <dlfcn.h>
#include <stdlib.h>
#include <string.h>
#include <xcb/xcb.h>
#include "npv/c_fixing.h"
#include "npv/public.h"
#include "npv/nyanvk/consts.h"
#include "npv/nyanvk/types.h"
#include "npv/global.h"
#include "npv/xcb/public.h"
#include "npv/vk/public.h"
/*----------------------------------------------------------------------------*/
/* this defines the npv vk api use, hence the use of the vk_* prefix */
#include "npv/vk/api_usage.h"
/*----------------------------------------------------------------------------*/
#include "npv/vk/namespace/public.h"
#include "npv/vk/namespace/main.c"
/*----------------------------------------------------------------------------*/
/* from api_usage.h" */
VK_GLOBAL_SYMS
/*----------------------------------------------------------------------------*/
#include "npv/vk/local/state.frag.c"
/*----------------------------------------------------------------------------*/
#include "npv/vk/local/code.frag.c"
#include "npv/vk/public/code.frag.c"
/*----------------------------------------------------------------------------*/
#define CLEANUP
#include "npv/vk/namespace/public.h"
#include "npv/vk/namespace/main.c"
#undef CLEANUP
/*----------------------------------------------------------------------------*/
#endif
