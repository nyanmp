#ifndef NPV_VK_API_USAGE_H
#define NPV_VK_API_USAGE_H
/*
 * this is public domain without any warranties of any kind
 * Sylvain BERTRAND
 */
/*
 * this is the simplification and taylorization of vk api for the specific
 * uses of npv
 */
#define IF_FATALVK(fmt, ...) \
if (r < 0) \
	npv_vk_fatal(fmt, __VA_ARGS__)
/*----------------------------------------------------------------------------*/
#define vk_get_dev_q() \
npv_vk_surf_p.dev.dl_vk_get_dev_q(npv_vk_surf_p.dev.vk, npv_vk_surf_p.dev.phydev.q_fam, 0, &npv_vk_surf_p.dev.q)

#define vk_create_cp(info) \
r = npv_vk_surf_p.dev.dl_vk_create_cp(npv_vk_surf_p.dev.vk, info, 0, &npv_vk_surf_p.dev.cp)

#define vk_create_swpchn(info) \
r = npv_vk_surf_p.dev.dl_vk_create_swpchn(npv_vk_surf_p.dev.vk, info, 0, &npv_vk_surf_p.dev.swpchn.vk)

#define vk_destroy_swpchn(swpchn) \
npv_vk_surf_p.dev.dl_vk_destroy_swpchn(npv_vk_surf_p.dev.vk, swpchn, 0)

#define vk_get_swpchn_imgs() \
r = npv_vk_surf_p.dev.dl_vk_get_swpchn_imgs(npv_vk_surf_p.dev.vk, npv_vk_surf_p.dev.swpchn.vk, &npv_vk_surf_p.dev.swpchn.imgs_n, npv_vk_surf_p.dev.swpchn.imgs)

#define vk_create_img(info, img) \
r = npv_vk_surf_p.dev.dl_vk_create_img(npv_vk_surf_p.dev.vk, info, 0, img)

#define vk_destroy_img(img) \
npv_vk_surf_p.dev.dl_vk_destroy_img(npv_vk_surf_p.dev.vk, img, 0)

#define vk_get_img_mem_rqmts(...) \
r = npv_vk_surf_p.dev.dl_vk_get_img_mem_rqmts(npv_vk_surf_p.dev.vk,##__VA_ARGS__)

#define vk_alloc_mem(info, dev_mem) \
r = npv_vk_surf_p.dev.dl_vk_alloc_mem(npv_vk_surf_p.dev.vk, info, 0, dev_mem)

#define vk_free_mem(dev_mem) \
npv_vk_surf_p.dev.dl_vk_free_mem(npv_vk_surf_p.dev.vk, dev_mem, 0)

#define vk_bind_img_mem(infos) \
r = npv_vk_surf_p.dev.dl_vk_bind_img_mem(npv_vk_surf_p.dev.vk, 1, infos)

#define vk_map_mem(dev_mem, data) \
r = npv_vk_surf_p.dev.dl_vk_map_mem(npv_vk_surf_p.dev.vk, dev_mem, 0, vk_whole_sz, 0, data)

#define vk_unmap_mem(dev_mem) \
npv_vk_surf_p.dev.dl_vk_unmap_mem(npv_vk_surf_p.dev.vk, dev_mem)

#define vk_alloc_cbs(info, cbs) \
r = npv_vk_surf_p.dev.dl_vk_alloc_cbs(npv_vk_surf_p.dev.vk, info, cbs)

#define vk_begin_cb(...) \
r = npv_vk_surf_p.dev.dl_vk_begin_cb(__VA_ARGS__)

#define vk_end_cb(...) \
r = npv_vk_surf_p.dev.dl_vk_end_cb(__VA_ARGS__)

#define vk_cmd_pl_barrier(cb, src_stage_msk, dst_stage_msk, mbs_n, mbs, img_bs_n, img_bs) \
npv_vk_surf_p.dev.dl_vk_cmd_pl_barrier(cb, src_stage_msk, dst_stage_msk, 0, mbs_n, mbs, 0, 0, img_bs_n, img_bs)

#define vk_q_submit(info, fence) \
r = npv_vk_surf_p.dev.dl_vk_q_submit(npv_vk_surf_p.dev.q, 1, info, fence)

#define vk_q_wait_idle() \
r = npv_vk_surf_p.dev.dl_vk_q_wait_idle(npv_vk_surf_p.dev.q)

#define vk_get_img_subrsrc_layout(...) \
npv_vk_surf_p.dev.dl_vk_get_img_subrsrc_layout(npv_vk_surf_p.dev.vk, __VA_ARGS__)

#define vk_acquire_next_img(...) \
r = npv_vk_surf_p.dev.dl_vk_acquire_next_img(npv_vk_surf_p.dev.vk, __VA_ARGS__)

#define vk_reset_cb(cb) \
r = npv_vk_surf_p.dev.dl_vk_reset_cb(cb, 0)

#define vk_cmd_blit_img(cb, src_img, dst_img, region, filter) \
npv_vk_surf_p.dev.dl_vk_cmd_blit_img(cb, src_img, vk_img_layout_general, dst_img, vk_img_layout_general, 1, region, filter)

#define vk_q_present(info) \
r = npv_vk_surf_p.dev.dl_vk_q_present(npv_vk_surf_p.dev.q, info)

#define vk_create_sem(info, sem) \
r = npv_vk_surf_p.dev.dl_vk_create_sem(npv_vk_surf_p.dev.vk, info, 0, sem)

#define vk_cmd_clr_color_img npv_vk_surf_p.dev.dl_vk_cmd_clr_color_img

#define vk_create_fence(info,fencep) \
r = npv_vk_surf_p.dev.dl_vk_create_fence(npv_vk_surf_p.dev.vk, info, 0, fencep)

#define vk_get_fence_status(fence) \
r = npv_vk_surf_p.dev.dl_vk_get_fence_status(npv_vk_surf_p.dev.vk, fence)

#define vk_reset_fences(fences) \
r = npv_vk_surf_p.dev.dl_vk_reset_fences(npv_vk_surf_p.dev.vk, 1, fences)

/* 100 ms timeout, any fence signaled */
#define vk_wait_for_fences(fences) \
r = npv_vk_surf_p.dev.dl_vk_wait_for_fences(npv_vk_surf_p.dev.vk, 1, fences, 0, 100000000)
/******************************************************************************/
/* cherry picked and namespaced from nyanvk/syms_global.h */
#define VK_GLOBAL_SYMS \
	STATIC void *(*npv_dl_vk_get_instance_proc_addr)(\
			struct vk_instance_t *instance,\
			u8 *name);\
	STATIC void *(*npv_dl_vk_get_dev_proc_addr)(\
			struct vk_dev_t *dev,\
			u8 *name);\
	STATIC s32 (*npv_dl_vk_enumerate_instance_version)(u32 *version); \
	STATIC s32 (*npv_dl_vk_enumerate_instance_layer_props)( \
			u32 *props_n, \
			struct vk_layer_props_t *props); \
	STATIC s32 (*npv_dl_vk_enumerate_instance_ext_props)( \
			u8 *layer_name, \
			u32 *props_n, \
			struct vk_ext_props_t *props); \
	STATIC s32 (*npv_dl_vk_create_instance)( \
			struct vk_instance_create_info_t *info, \
			void *allocator, \
			struct vk_instance_t **instance); \
	STATIC s32 (*npv_dl_vk_enumerate_phydevs)( \
			struct vk_instance_t *instance, \
			u32 *phydevs_n, \
			struct vk_phydev_t **phydevs); \
	STATIC s32 (*npv_dl_vk_enumerate_dev_ext_props)( \
			struct vk_phydev_t *phydev, \
			u8 *layer_name, \
			u32 *props_n, \
			struct vk_ext_props_t *props); \
	STATIC void (*npv_dl_vk_get_phydev_props)( \
			struct vk_phydev_t *phydev, \
			struct vk_phydev_props_t *props); \
	STATIC s32 (*npv_dl_vk_create_dev)( \
			struct vk_phydev_t *phydev, \
			struct vk_dev_create_info_t *create_info, \
			void *allocator, \
			struct vk_dev_t **dev); \
	STATIC void (*npv_dl_vk_get_phydev_q_fam_props)( \
			struct vk_phydev_t *phydev, \
			u32 *q_fam_props_n, \
			struct vk_q_fam_props_t *props); \
	STATIC s32 (*npv_dl_vk_create_xcb_surf)( \
			struct vk_instance_t *instance, \
			struct vk_xcb_surf_create_info_t *info, \
			void *allocator, \
			struct vk_surf_t **surf); \
	STATIC void (*npv_dl_vk_destroy_surf)(\
			struct vk_instance_t *instance,\
			struct vk_surf_t *surf,\
			void *allocator); \
	STATIC s32 (*npv_dl_vk_get_phydev_surf_support)( \
			struct vk_phydev_t *phydev, \
			u32 q_fam, \
			struct vk_surf_t *surf, \
			u32 *supported); \
	STATIC s32  (*npv_dl_vk_get_phydev_surf_texel_mem_blk_confs)( \
			struct vk_phydev_t *phydev, \
			struct vk_phydev_surf_info_t *info, \
			u32 *confs_n, \
			struct vk_surf_texel_mem_blk_conf_t *confs); \
	STATIC void (*npv_dl_vk_get_phydev_mem_props)( \
			struct vk_phydev_t *phydev, \
			struct vk_phydev_mem_props_t *props); \
	STATIC s32 (*npv_dl_vk_get_phydev_surf_caps)( \
			struct vk_phydev_t *phydev, \
			struct vk_phydev_surf_info_t *info, \
			struct vk_surf_caps_t *caps); \
	STATIC s32 (*npv_dl_vk_get_phydev_surf_present_modes)( \
			struct vk_phydev_t *phydev, \
			struct vk_surf_t *surf, \
			u32 *modes_n, \
			u32 *modes); \
	STATIC void (*npv_dl_vk_get_phydev_texel_mem_blk_fmt_props)(\
				struct vk_phydev_t *phydev,\
				u32 fmt,\
				struct vk_texel_mem_blk_fmt_props_t *props);
/******************************************************************************/
#define vk_get_instance_proc_addr npv_dl_vk_get_instance_proc_addr

#define vk_get_dev_proc_addr npv_dl_vk_get_dev_proc_addr

#define vk_enumerate_instance_version \
r = npv_dl_vk_enumerate_instance_version

#define vk_enumerate_instance_layer_props \
r = npv_dl_vk_enumerate_instance_layer_props

#define vk_enumerate_instance_ext_props(...) \
r = npv_dl_vk_enumerate_instance_ext_props(0, __VA_ARGS__)

#define vk_create_instance(info) \
r = npv_dl_vk_create_instance(info, 0, &npv_vk_instance_l)

#define vk_enumerate_phydevs(...) \
r = npv_dl_vk_enumerate_phydevs(npv_vk_instance_l, __VA_ARGS__)

#define vk_enumerate_dev_ext_props(phydev, props_n, props) \
r = npv_dl_vk_enumerate_dev_ext_props(phydev, 0, props_n, props)

#define vk_get_phydev_props npv_dl_vk_get_phydev_props

#define vk_create_dev(info) \
r = npv_dl_vk_create_dev(npv_vk_surf_p.dev.phydev.vk, info, 0, &npv_vk_surf_p.dev.vk)

#define vk_get_phydev_q_fam_props npv_dl_vk_get_phydev_q_fam_props

#define vk_create_xcb_surf(info) \
r = npv_dl_vk_create_xcb_surf(npv_vk_instance_l, info, 0, &npv_vk_surf_p.vk)

#define vk_get_phydev_surf_support(phydev, q_fam, supported) \
r = npv_dl_vk_get_phydev_surf_support(phydev, q_fam, npv_vk_surf_p.vk, supported)

#define vk_get_phydev_surf_texel_mem_blk_confs(...) \
r = npv_dl_vk_get_phydev_surf_texel_mem_blk_confs(npv_vk_surf_p.dev.phydev.vk, __VA_ARGS__)

#define vk_get_phydev_mem_props npv_dl_vk_get_phydev_mem_props

#define vk_get_phydev_texel_mem_blk_fmt_props(...) \
npv_dl_vk_get_phydev_texel_mem_blk_fmt_props(npv_vk_surf_p.dev.phydev.vk, __VA_ARGS__)

#define vk_get_phydev_surf_caps(info, caps) \
r = npv_dl_vk_get_phydev_surf_caps(npv_vk_surf_p.dev.phydev.vk, info, caps)

#define vk_get_phydev_surf_present_modes() \
r = npv_dl_vk_get_phydev_surf_present_modes(npv_vk_surf_p.dev.phydev.vk, npv_vk_surf_p.vk, &npv_vk_tmp_present_modes_n_l, npv_vk_tmp_present_modes_l)
#endif
