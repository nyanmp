STATIC void fatal(u8 *fmt, ...)
{
	va_list ap;

	npv_perr("vulkan:");
	va_start(ap, fmt);
	npv_vfatal(fmt, ap);
	va_end(ap); /* unreachable */
}
STATIC void vfatal(u8 *fmt, va_list ap)
{
	va_list aq;

	npv_perr("vulkan:");
	va_copy(aq, ap);
	npv_vfatal(fmt, aq);
	va_end(aq);
}
STATIC void warning(u8 *fmt, ...)
{
	va_list ap;

	npv_perr("vulkan:");
	va_start(ap, fmt);
	npv_vwarning(fmt, ap);
	va_end(ap);
}
STATIC void vwarning(u8 *fmt, va_list ap)
{
	va_list aq;

	npv_perr("vulkan:");
	va_copy(aq, ap);
	npv_vwarning(fmt, aq);
	va_end(aq);
}
STATIC void swpchn_update(void)
{
	phydev_surf_caps_get();
	swpchn_reinit(); /* handle any previous swpchn */
	swpchn_imgs_get();
}
STATIC void surf_init_once(xcb_connection_t *c, u32 win_id)
{
	surf_create(c, win_id);
	dev_init();
	sems_create();
	fences_create();
	misc_cbs_init_once();
	/*====================================================================*/
	swpchn_init_once();
	swpchn_imgs_cbs_init_once();
	swpchn_update();
}
STATIC void init_once(void)
{
	load_vk_loader();
	loader_syms();
	instance_static_syms();
	check_vk_version();
	instance_exts_dump();
	instance_layers_dump();
	/*--------------------------------------------------------------------*/
	instance_create();
	instance_syms();
}
