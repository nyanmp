constant_u32 {
	swpchn_imgs_n_max = 3,
};
struct swpchn_t {
	struct vk_swpchn_t *vk;

	u32 imgs_n;
	struct vk_img_t *imgs[swpchn_imgs_n_max];
};
struct texel_mem_blk_fmt_t {
	struct vk_surf_texel_mem_blk_conf_core_t conf_core;
	bool linear_tiling_has_linear_filt;
};
struct phydev_t {
	struct vk_phydev_t  *vk;
	u8 q_fam;
	bool is_discret_gpu;
	struct texel_mem_blk_fmt_t b8g8r8a8_srgb;
	u32 mem_types_n;
	struct vk_mem_type_t mem_types[VK_MEM_TYPES_N_MAX];
	struct vk_surf_caps_t surf_caps;
};
constant_u32 {
	sem_acquire_img_done = 0,
	sem_blit_done = 1,
	sems_n = 2,
	color_space_layout_transition = 0
};
struct dev_t {
	struct vk_dev_t *vk;
	struct phydev_t phydev;
	struct swpchn_t swpchn;
	struct vk_fence_t *fence;
	struct vk_fence_t *fence_color_space_layout;
	struct vk_q_t *q;
	struct vk_cp_t *cp;
	struct vk_cb_t *cbs[swpchn_imgs_n_max];
	struct vk_cb_t *misc_cbs[1]; /* only one for now */
	struct vk_sem_t *sems[sems_n];
	VK_DEV_SYMS_FULL /* TODO:should cherry pick the syms we use */
};
/*============================================================================*/
STATIC struct {
	struct vk_surf_t *vk;
	struct dev_t dev;
} surf_p;
