STATIC void pout(u8 *fmt, ...)
{
	va_list ap;

	npv_pout("vulkan:");
	va_start(ap, fmt);
	npv_vpout(fmt, ap);
	va_end(ap);
}
#define INSTANCE_STATIC_SYM(x,y)						\
	npv_dl_##y = vk_get_instance_proc_addr(0, #x);				\
	if (npv_dl_##y == 0)							\
		fatal("unable to find vulkan " #x "\n");
STATIC void instance_static_syms(void)
{
	INSTANCE_STATIC_SYM(vkEnumerateInstanceVersion,
						vk_enumerate_instance_version);
	INSTANCE_STATIC_SYM(vkEnumerateInstanceExtensionProperties,
					vk_enumerate_instance_ext_props);
	INSTANCE_STATIC_SYM(vkEnumerateInstanceLayerProperties,
					vk_enumerate_instance_layer_props);
	INSTANCE_STATIC_SYM(vkCreateInstance, vk_create_instance);
}
#undef INSTANCE_STATIC_SYM
/*----------------------------------------------------------------------------*/
#define INSTANCE_SYM(x,y)							\
	npv_dl_##y = vk_get_instance_proc_addr(instance_l, #x);			\
	if (npv_dl_##y == 0)							\
		fatal("unable to find vulkan " #x "\n");
STATIC void instance_syms(void)
{
	INSTANCE_SYM(vkEnumeratePhysicalDevices, vk_enumerate_phydevs);
	INSTANCE_SYM(vkEnumerateDeviceExtensionProperties,
						vk_enumerate_dev_ext_props);
	INSTANCE_SYM(vkGetPhysicalDeviceProperties2, vk_get_phydev_props);
	INSTANCE_SYM(vkGetPhysicalDeviceQueueFamilyProperties2,
						vk_get_phydev_q_fam_props);
	INSTANCE_SYM(vkCreateDevice, vk_create_dev);
	/* wsi related -------------------------------------------------------*/
	INSTANCE_SYM(vkGetPhysicalDeviceSurfaceSupportKHR,
					vk_get_phydev_surf_support);
	INSTANCE_SYM(vkGetPhysicalDeviceSurfaceFormats2KHR,
				vk_get_phydev_surf_texel_mem_blk_confs);
	INSTANCE_SYM(vkCreateXcbSurfaceKHR, vk_create_xcb_surf);
	INSTANCE_SYM(vkGetPhysicalDeviceFormatProperties2,
					vk_get_phydev_texel_mem_blk_fmt_props);
	INSTANCE_SYM(vkGetPhysicalDeviceMemoryProperties2,
						vk_get_phydev_mem_props);
	INSTANCE_SYM(vkGetPhysicalDeviceSurfaceCapabilities2KHR,
						vk_get_phydev_surf_caps);
	INSTANCE_SYM(vkGetPhysicalDeviceSurfacePresentModesKHR,
					vk_get_phydev_surf_present_modes);
	/*--------------------------------------------------------------------*/
}
#undef INSTANCE_SYM
/*----------------------------------------------------------------------------*/
#define DEV_SYM(x,y) \
	surf_p.dev.dl_##y = vk_get_dev_proc_addr(surf_p.dev.vk, #x); \
	if (surf_p.dev.dl_##y == 0) \
		fatal("unable to find vulkan device " #x "\n");
STATIC void dev_syms(void)
{
	DEV_SYM(vkGetDeviceQueue, vk_get_dev_q);
	DEV_SYM(vkCreateCommandPool, vk_create_cp);
	DEV_SYM(vkCreateSwapchainKHR, vk_create_swpchn);
	DEV_SYM(vkDestroySwapchainKHR, vk_destroy_swpchn);
	DEV_SYM(vkGetSwapchainImagesKHR, vk_get_swpchn_imgs);
	DEV_SYM(vkCreateImage, vk_create_img);
	DEV_SYM(vkDestroyImage, vk_destroy_img);
	DEV_SYM(vkGetImageMemoryRequirements2KHR, vk_get_img_mem_rqmts);
	DEV_SYM(vkAllocateMemory, vk_alloc_mem);
	DEV_SYM(vkFreeMemory, vk_free_mem);
	DEV_SYM(vkBindImageMemory2KHR, vk_bind_img_mem);
	DEV_SYM(vkMapMemory, vk_map_mem);
	DEV_SYM(vkUnmapMemory, vk_unmap_mem);
	DEV_SYM(vkAllocateCommandBuffers, vk_alloc_cbs);
	DEV_SYM(vkBeginCommandBuffer, vk_begin_cb);
	DEV_SYM(vkEndCommandBuffer, vk_end_cb);
	DEV_SYM(vkCmdPipelineBarrier, vk_cmd_pl_barrier);
	DEV_SYM(vkQueueSubmit, vk_q_submit);
	DEV_SYM(vkQueueWaitIdle, vk_q_wait_idle);
	DEV_SYM(vkGetImageSubresourceLayout, vk_get_img_subrsrc_layout);
	DEV_SYM(vkAcquireNextImage2KHR, vk_acquire_next_img);
	DEV_SYM(vkResetCommandBuffer, vk_reset_cb);
	DEV_SYM(vkCmdBlitImage, vk_cmd_blit_img);
	DEV_SYM(vkQueuePresentKHR, vk_q_present);
	DEV_SYM(vkCreateSemaphore, vk_create_sem);
	DEV_SYM(vkCmdClearColorImage, vk_cmd_clr_color_img);
	DEV_SYM(vkCreateFence, vk_create_fence);
	DEV_SYM(vkGetFenceStatus, vk_get_fence_status);
	DEV_SYM(vkResetFences, vk_reset_fences);
	DEV_SYM(vkWaitForFences, vk_wait_for_fences);
}
#undef DEVICE_SYM
/*----------------------------------------------------------------------------*/
#define DLSYM(x, y)								\
	npv_dl_##y = dlsym(loader_l, #x);						\
	if (npv_dl_##y == 0)							\
		fatal("%s:unable to find " #x "\n", dlerror());
STATIC void loader_syms(void)
{
	DLSYM(vkGetInstanceProcAddr, vk_get_instance_proc_addr);
	DLSYM(vkGetDeviceProcAddr, vk_get_dev_proc_addr);
}
#undef DLSYM
/*----------------------------------------------------------------------------*/
STATIC void load_vk_loader(void)
{
	/* no '/' in the shared dynamic lib path name, then standard lookup */
	loader_l = dlopen("libvulkan.so.1", RTLD_LAZY);
	if (loader_l == 0)
		fatal("%s:unable to load the vulkan loader dynamic shared library\n", dlerror());
}
STATIC void check_vk_version(void)
{
	u32 api_version;
	s32 r;

	vk_enumerate_instance_version(&api_version);
	if (r != vk_success)
		fatal("%d:unable to enumerate instance version\n", r);
	pout("vulkan instance version %#x = %u.%u.%u\n", api_version, VK_VERSION_MAJOR(api_version), VK_VERSION_MINOR(api_version), VK_VERSION_PATCH(api_version));
	if (VK_VERSION_MAJOR(api_version) == 1
					&& VK_VERSION_MINOR(api_version) == 0)
		fatal("instance version too old\n");
}
#define EXTS_N_MAX 256
/* in theory, this could change on the fly */
STATIC void instance_exts_dump(void)
{
	struct vk_ext_props_t exts[EXTS_N_MAX];
	u32 n;
	s32 r;

	memset(exts, 0, sizeof(exts));
	n = EXTS_N_MAX;
	vk_enumerate_instance_ext_props(&n, exts);
	if (r != vk_success && r != vk_incomplete) {
		warning("%d:unable to enumerate instance extension(s)\n", r);
		return;
	} 
	if (r == vk_incomplete) {
		warning("too many extensions (%u/%u), dumping disabled", n, EXTS_N_MAX);
		return;
	}
	/* vk_success */
	pout("have %u instance extension(s)\n", n);
	loop {
		if (n == 0)
			break;
		pout("instance extension:name=%s:specification version=%u\n", exts[n - 1].name, exts[n - 1].spec_version);
		n--;
	}
}
#undef EXTS_N_MAX
#define LAYERS_N_MAX 32
/* in theory, this could change on the fly */
STATIC void instance_layers_dump(void)
{
	struct vk_layer_props_t layers[LAYERS_N_MAX];
	u32 n;
	s32 r;

	memset(layers, 0, sizeof(layers));
	n = LAYERS_N_MAX;
	vk_enumerate_instance_layer_props(&n, layers);
	if (r != vk_success && r != vk_incomplete) {
		warning("%d:unable to enumerate instance layer(s)\n", r);
		return;
	}
	if (r == vk_incomplete) {
		warning("too many layers (%u/%u), dumping disabled", n, LAYERS_N_MAX);
		return;
	}
	/* vk_success */
	pout("have %u instance layer(s)\n", n);
	loop {
		if (n == 0)
			break;
		pout("instance layer:%u:name=%s:specification version=%u:implementation version=%u:description=%s\n", n, layers[n].name, layers[n].spec_version, layers[n].implementation_version, layers[n].desc);
		n--;
	}
}
#undef LAYERS_N_MAX
STATIC void instance_create(void)
{
	s32 r;
	struct vk_instance_create_info_t info;
	static u8 *exts[] = {
		/*
		 * TODO: there is a shabby (coze mess of pixel fmts),
		 * "expensive", promoted YUV extension
		 */
		/*
		 * XXX: not 1.1 promoted, should not use it, but it is fixing
		 * some non-consistency from 1.0
		 */
		"VK_KHR_get_surface_capabilities2",
		/* 1.1 promoted */
		"VK_KHR_get_physical_device_properties2",
		"VK_KHR_xcb_surface",
		"VK_KHR_surface"};
	u32 i;

	i = 0;
	loop {
		if (i == ARRAY_N(exts))
			break;
		pout("will use instance extension %s\n", exts[i]);
		++i;
	}
	memset(&info, 0, sizeof(info));
	info.type = vk_struct_type_instance_create_info;
	info.enabled_exts_n = ARRAY_N(exts);
	info.enabled_ext_names = exts;
	vk_create_instance(&info);
	IF_FATALVK("%d:unable to create an instance\n", r);
	pout("instance handle %p\n", instance_l);
}
STATIC void tmp_phydevs_get(void)
{
	struct vk_phydev_t *phydevs[tmp_phydevs_n_max];
	u32 n;
	s32 r;

	memset(phydevs, 0, sizeof(phydevs));
	n = tmp_phydevs_n_max;
	vk_enumerate_phydevs(&n, phydevs);
	if (r != vk_success && r != vk_incomplete)
		fatal("%ld:unable to enumerate physical devices\n", r);
	if (r == vk_incomplete)
		fatal("too many vulkan physical devices %u/%u for our temporary storage\n", n, tmp_phydevs_n_max);
	/* vk_success */
	pout("detected %u physical devices\n", n);
	if (n == 0)
		fatal("no vulkan physical devices, exiting\n");
	tmp_phydevs_n_l = n;
	memset(tmp_phydevs_l, 0, sizeof(tmp_phydevs_l));
	n = 0;	
	loop {
		if (n == tmp_phydevs_n_l)
			break;
		tmp_phydevs_l[n].vk = phydevs[n];
		++n;
	};
}
#define EXTS_N_MAX 512
STATIC void phydev_exts_dump(void *phydev)
{
	struct vk_ext_props_t exts[EXTS_N_MAX];
	u32 n;
	s32 r;

	memset(exts, 0, sizeof(exts));
	n = EXTS_N_MAX;
	vk_enumerate_dev_ext_props(phydev, &n, exts);
	if (r != vk_success && r != vk_incomplete) {
		warning("physical device:%p:%d:unable to enumerate device extension(s)\n", phydev, r);
		return;
	} 
	if (r == vk_incomplete) {
		warning("physical device:%p:too many extensions (%u/%u), dumping disabled", phydev, n, EXTS_N_MAX);
		return;
	}
	/* vk_success */
	pout("physical device:%p:have %u device extension(s)\n", phydev, n);
	loop {
		if (n == 0)
			break;
		pout("physical device:%p:device extension:name=%s:specification version=%u\n", phydev, exts[n - 1].name, exts[n - 1].spec_version);
		n--;
	}
}
#undef EXTS_N_MAX
STATIC void tmp_phydevs_exts_dump(void)
{
	u8 i;

	i = 0;
	loop {
		if (i == tmp_phydevs_n_l)
			break;
		phydev_exts_dump(tmp_phydevs_l[i].vk);
		++i;
	}
}
STATIC u8 *dev_type_str(u32 type)
{
	switch (type) {
	case vk_phydev_type_other:
		return "other";
	case vk_phydev_type_integrated_gpu:
		return "integrated gpu";
	case vk_phydev_type_discrete_gpu:
		return "discrete gpu";
	case vk_phydev_type_virtual_gpu:
		return "virtual gpu";
	case vk_phydev_type_cpu:
		return "cpu";
	default:
		return "UNKNOWN";
	}
}
STATIC u8 *uuid_str(u8 *uuid)
{
	static u8 uuid_str_buf[VK_UUID_SZ * 2 + 1];
	u8 i;

	memset(uuid_str_buf, 0, sizeof(uuid_str_buf));
	i = 0;
	loop {
		if (i == VK_UUID_SZ)
			break;
		/* XXX: always write a terminating  0, truncated or not */
		snprintf(uuid_str_buf + i * 2, 3, "%02x", uuid[i]);
		++i;
	}	
	return uuid_str_buf;
}
STATIC void tmp_phydevs_props_dump(void)
{
	u32 i;

	i = 0;
	loop {
		struct vk_phydev_props_t props;
		struct tmp_phydev_t *p;

		if (i == tmp_phydevs_n_l)
			break;
		p = &tmp_phydevs_l[i];
		memset(&props, 0, sizeof(props));
		props.type = vk_struct_type_phydev_props;
		vk_get_phydev_props(p->vk, &props);
		pout("physical device:%p:properties:api version=%#x=%u.%u.%u\n", p->vk, props.core.api_version, VK_VERSION_MAJOR(props.core.api_version), VK_VERSION_MINOR(props.core.api_version), VK_VERSION_PATCH(props.core.api_version));
		pout("physical device:%p:properties:driver version=%#x=%u.%u.%u\n", p->vk, props.core.driver_version, VK_VERSION_MAJOR(props.core.driver_version), VK_VERSION_MINOR(props.core.driver_version), VK_VERSION_PATCH(props.core.driver_version));
		pout("physical device:%p:properties:vendor id=%#x\n", p->vk, props.core.vendor_id);
		pout("physical device:%p:properties:device id=%#x\n", p->vk, props.core.dev_id);
		pout("physical device:%p:properties:type=%s\n", p->vk, dev_type_str(props.core.dev_type));
		if (props.core.dev_type == vk_phydev_type_discrete_gpu)
			p->is_discret_gpu = true;
		else
			p->is_discret_gpu = false;
		pout("physical device:%p:properties:name=%s\n", p->vk, props.core.name);
		pout("physical device:%p:properties:pipeline cache uuid=%s\n", p->vk, uuid_str(props.core.pl_cache_uuid));
		/* disp the limits and sparse props at "higher log lvl", if needed in the end */
		++i;
	}
}
STATIC void tmp_phydev_mem_props_get(struct tmp_phydev_t *p)
{
	memset(&p->mem_props, 0, sizeof(p->mem_props));
	p->mem_props.type = vk_struct_type_phydev_mem_props;
	vk_get_phydev_mem_props(p->vk, &p->mem_props);
}
STATIC void tmp_phydevs_mem_props_get(void)
{
	u8 i;

	i = 0;
	loop {
		if (i == tmp_phydevs_n_l)
			break;
		tmp_phydev_mem_props_get(&tmp_phydevs_l[i]);
		++i;
	}
}
STATIC void phydev_mem_type_dump(void *phydev, u8 i,
						struct vk_mem_type_t *type)
{
	pout("physical device:%p:memory type:%u:heap:%u\n", phydev, i, type->heap);
	pout("physical device:%p:memory type:%u:flags:%#08x\n", phydev, i, type->prop_flags);
	if ((type->prop_flags & vk_mem_prop_dev_local_bit) != 0)
		pout("physical device:%p:memory type:%u:device local\n", phydev, i);
	if ((type->prop_flags & vk_mem_prop_host_visible_bit) != 0)
		pout("physical device:%p:memory type:%u:host visible\n", phydev, i);
	if ((type->prop_flags & vk_mem_prop_host_cached_bit) != 0)
		pout("physical device:%p:memory type:%u:host cached\n", phydev, i);
	if ((type->prop_flags & vk_mem_prop_host_coherent_bit) != 0)
		pout("physical device:%p:memory type:%u:host coherent\n", phydev, i);
}
STATIC void tmp_phydev_mem_types_dump(struct tmp_phydev_t *p)
{
	u8 i;

	pout("physical device:%p:%u memory types\n", p->vk, p->mem_props.core.mem_types_n);
	i = 0;
	loop {
		if (i == p->mem_props.core.mem_types_n)
			break;
		phydev_mem_type_dump(p->vk, i,
					&p->mem_props.core.mem_types[i]);
		++i;
	}
}
STATIC void phydev_mem_heap_dump(void *phydev, u8 i,
						struct vk_mem_heap_t *heap)
{
	pout("physical device:%p:memory heap:%u:size:%u bytes\n", phydev, i, heap->sz);
	pout("physical device:%p:memory heap:%u:flags:%#08x\n", phydev, i, heap->flags);
	if ((heap->flags & vk_mem_heap_dev_local_bit) != 0)
		pout("physical device:%p:memory heap:%u:device local\n", phydev, i);
	if ((heap->flags & vk_mem_heap_multi_instance_bit) != 0)
		pout("physical device:%p:memory type:%u:multi instance\n", phydev, i);
}
STATIC void tmp_phydev_mem_heaps_dump(struct tmp_phydev_t *p)
{
	u8 i;

	pout("physical device:%p:%u memory heaps\n", p->vk, p->mem_props.core.mem_heaps_n);
	i = 0;
	loop {
		if (i == p->mem_props.core.mem_heaps_n)
			break;
		phydev_mem_heap_dump(p->vk, i,
					&p->mem_props.core.mem_heaps[i]);
		++i;
	}

}
STATIC void tmp_phydev_mem_props_dump(struct tmp_phydev_t *p)
{
	tmp_phydev_mem_types_dump(p);
	tmp_phydev_mem_heaps_dump(p);
}
STATIC void tmp_phydevs_mem_props_dump(void)
{
	u8 i;

	i = 0;
	loop {
		if (i == tmp_phydevs_n_l)
			break;
		tmp_phydev_mem_props_dump(&tmp_phydevs_l[i]);
		++i;
	}
}
STATIC void tmp_phydev_q_fams_get(struct tmp_phydev_t *p)
{
	u8 i;
	u32 n;

	n = 0;
	vk_get_phydev_q_fam_props(p->vk, &n, 0);
	if (n > tmp_phydev_q_fams_n_max)
		fatal("physical device:%p:too many queue families %u/%u\n", p->vk, n, tmp_phydev_q_fams_n_max);
	memset(p->q_fams, 0, sizeof(p->q_fams));
	i = 0;
	loop {
		if (i == tmp_phydev_q_fams_n_max)
			break;
		p->q_fams[i].type = vk_struct_type_q_fam_props;
		++i;
	}
	vk_get_phydev_q_fam_props(p->vk, &n, p->q_fams);
	p->q_fams_n = n;
	pout("physical device:%p:have %u queue families\n", p->vk, p->q_fams_n);
}
STATIC void tmp_phydevs_q_fams_get(void)
{
	u8 i;

	i = 0;
	loop {
		if (i == tmp_phydevs_n_l)
			break;
		tmp_phydev_q_fams_get(&tmp_phydevs_l[i]);
		++i;
	}
}
STATIC void tmp_phydev_q_fams_dump(struct tmp_phydev_t *p)
{
	u8 i;

	i = 0;
	loop {
		if (i == p->q_fams_n)
			break;
		if ((p->q_fams[i].core.flags & vk_q_gfx_bit) != 0)
			pout("physical device:%p:queue family:%u:flags:graphics\n", p->vk, i);
		if ((p->q_fams[i].core.flags & vk_q_compute_bit) != 0)
			pout("physical device:%p:queue family:%u:flags:compute\n", p->vk, i);
		if ((p->q_fams[i].core.flags & vk_q_transfer_bit) != 0)
			pout("physical device:%p:queue family:%u:flags:transfer\n", p->vk, i);
		if ((p->q_fams[i].core.flags & vk_q_sparse_binding_bit) != 0)
			pout("physical device:%p:queue family:%u:flags:sparse binding\n", p->vk, i);
		if ((p->q_fams[i].core.flags & vk_q_protected_bit) != 0)
			pout("physical device:%p:queue family:%u:flags:protected\n", p->vk, i);
		pout("physical device:%p:queue family:%u:%u queues\n", p->vk, i, p->q_fams[i].core.qs_n);
		pout("physical device:%p:queue family:%u:%u bits timestamps\n", p->vk, i, p->q_fams[i].core.timestamp_valid_bits);
		pout("physical device:%p:queue family:%u:(width=%u,height=%u,depth=%u) minimum image transfer granularity\n", p->vk, i, p->q_fams[i].core.min_img_transfer_granularity.width, p->q_fams[i].core.min_img_transfer_granularity.height, p->q_fams[i].core.min_img_transfer_granularity.depth);
		++i;
	}
}
STATIC void tmp_phydevs_q_fams_dump(void)
{
	u8 i;

	i = 0;
	loop {
		if (i == tmp_phydevs_n_l)
			break;
		tmp_phydev_q_fams_dump(&tmp_phydevs_l[i]);
		++i;
	}
}
/*
 * the major obj to use in vk abstraction of gfx hardware is the q. In this
 * abstraction, many core objs like bufs/imgs are "own" by a specific q, and
 * transfer of such ownership to other qs can be expensive. we know it's not
 * really the case on AMD hardware, but if vk abstraction insists on this, it
 * probably means it is important on some hardware of other vendors.
 */
STATIC void tmp_phydevs_q_fams_surf_support_get(void)
{
	u8 i;

	i = 0;
	loop {
		struct tmp_phydev_t *p;
		u8 j;

		if (i == tmp_phydevs_n_l)
			break;
		p = &tmp_phydevs_l[i];
		j = 0;
		loop {
			s32 r;
			u32 supported;

			if (j == p->q_fams_n)
				break;
			supported = vk_false;
			vk_get_phydev_surf_support(p->vk, j, &supported);
			IF_FATALVK("%d:physical device:%p:queue family:%u:surface:%p:unable to query queue family wsi/(image presentation to our surface) support\n", r, p->vk, j, surf_p.vk);
			if (supported == vk_true) {
				pout("physical device:%p:queue family:%u:surface:%p:does support wsi/(image presentation to our surface) \n", p->vk, j, surf_p.vk);
				p->q_fams_surf_support[j] = true;
			} else {
				pout("physical device:%p:queue family:%u:surface:%p:does not support wsi/(image presentation to our surface)\n", p->vk, j, surf_p.vk);
				p->q_fams_surf_support[j] = false; 
			}
			++j;
		}
		++i;
	}
}
STATIC void tmp_selected_phydev_cherry_pick(u8 i)
{
	struct tmp_phydev_t *p;

	p = &tmp_phydevs_l[i];
	surf_p.dev.phydev.vk = p->vk;
	surf_p.dev.phydev.is_discret_gpu = p->is_discret_gpu;
	surf_p.dev.phydev.mem_types_n = p->mem_props.core.mem_types_n;
	memcpy(surf_p.dev.phydev.mem_types, p->mem_props.core.mem_types,
					sizeof(surf_p.dev.phydev.mem_types));
}
/*
 * we ask qs of phydevs which one is able to present imgs to the
 * external pe surf. Additionally we require this q to support gfx. we
 * select basically the first q from the first phydev fitting what we are
 * looking for.
 */
STATIC void tmp_phydev_and_q_fam_select(void)
{
	u8 i;
		
	i = 0;
	loop {
		u8 j;
		struct tmp_phydev_t *p;

		if (i == tmp_phydevs_n_l)
			break;
		p = &tmp_phydevs_l[i];
		j = 0;
		loop {
			if (j == p->q_fams_n)
				break;
			/*
			 * we are looking for a q fam with:
			 *  - img presentation to our surf
			 *  - gfx
			 *  - transfer (implicit with gfx)
			 */
			if (p->q_fams_surf_support[j]
				&& (p->q_fams[j].core.flags & vk_q_gfx_bit)
									!= 0) {
				surf_p.dev.phydev.q_fam = j;
				tmp_selected_phydev_cherry_pick(i);
				pout("physical device %p selected for (wsi/image presentation to our surface %p) using its queue family %u\n", surf_p.dev.phydev.vk, surf_p.vk, surf_p.dev.phydev.q_fam);
				return;
			}
			++j;
		}
		++i;
	}
}
STATIC void texel_mem_blk_confs_dump(u32 confs_n,
				struct vk_surf_texel_mem_blk_conf_t *confs)
{
	u32 i;

	i = 0;
	loop {
		if (i == confs_n)
			break;
		pout("physical device:%p:surface:%p:texel memory block configuration:format=%u color_space=%u\n", surf_p.dev.phydev.vk, surf_p.vk, confs[i].core.fmt, confs[i].core.color_space);
		++i;
	}
}
#define CONFS_N_MAX 1024
STATIC void phydev_surf_texel_mem_blk_fmts_dump(void)
{
	struct vk_phydev_surf_info_t info;
	struct vk_surf_texel_mem_blk_conf_t confs[CONFS_N_MAX];
	s32 r;
	u32 confs_n;
	u32 i;

	memset(&info, 0, sizeof(info));
	info.type = vk_struct_type_phydev_surf_info;
	info.surf = surf_p.vk;
	vk_get_phydev_surf_texel_mem_blk_confs(&info, &confs_n, 0);
	IF_FATALVK("%d:physical device:%p:surface:%p:unable get the count of valid surface texel memory block configurations\n", r, surf_p.dev.phydev.vk, surf_p.vk);
	if (confs_n > CONFS_N_MAX)
		fatal("physical device:%p:surface:%p:too many surface texel memory block configurations %u/%u\n", surf_p.dev.phydev.vk, surf_p.vk, confs_n, CONFS_N_MAX);

	memset(confs, 0, sizeof(confs[0]) * confs_n);
	i = 0;
	loop {
		if (i == confs_n)
			break;
		confs[i].type = vk_struct_type_surf_texel_mem_blk_conf;
		++i;
	}	
	vk_get_phydev_surf_texel_mem_blk_confs(&info, &confs_n, confs);
	IF_FATALVK("%d:physical device:%p:surface:%p:unable get the valid surface texel memory block configurations\n", r, surf_p.dev.phydev.vk, surf_p.vk);
	if (confs_n == 0)
		fatal("physical device:%p:surface:%p:no valid surface texel memory block configuration\n", surf_p.dev.phydev.vk, surf_p.vk);
	texel_mem_blk_confs_dump(confs_n, confs);
}
#undef CONFS_N_MAX
STATIC void phydev_surf_b8g8r8a8_srgb_setup(void)
{
	struct vk_surf_texel_mem_blk_conf_core_t *cc;
	struct texel_mem_blk_fmt_t *fmt;
	struct vk_texel_mem_blk_fmt_props_t b8g8r8a8_srgb_props;
	/*
	 * the b8g8r8a8_srgb texel cfg is guaranteed to exist, and this is what
	 * we get from ff scaler
	 */
	fmt = &surf_p.dev.phydev.b8g8r8a8_srgb;
	cc = &fmt->conf_core;

	cc->fmt = vk_texel_mem_blk_fmt_b8g8r8a8_srgb;
	pout("physical device:%p:surface:%p:using our surface texel memory block format %u\n", surf_p.dev.phydev.vk, surf_p.vk, cc->fmt);
	cc->color_space = vk_color_space_srgb_nonlinear;
	pout("physical device:%p:surface:%p:using prefered surface texel memory block color space %u\n", surf_p.dev.phydev.vk, surf_p.vk, cc->color_space);
	/*--------------------------------------------------------------------*/
	/* do we have linear filt on b8g8r8a8 srgb linear tiling? */
	memset(&b8g8r8a8_srgb_props, 0, sizeof(b8g8r8a8_srgb_props));
	b8g8r8a8_srgb_props.type = vk_struct_type_texel_mem_blk_fmt_props;
	vk_get_phydev_texel_mem_blk_fmt_props(
		vk_texel_mem_blk_fmt_b8g8r8a8_srgb, &b8g8r8a8_srgb_props);
	if (b8g8r8a8_srgb_props.core.linear_tiling_features &
		vk_texel_mem_blk_fmt_feature_sampled_img_filt_linear_bit != 0) {
		fmt->linear_tiling_has_linear_filt = true;
		pout("physical device:%p:linear tiling of b8g8r8a8 sRGB texel memory blocks has linear filtering\n", surf_p.dev.phydev.vk);
	} else {
		fmt->linear_tiling_has_linear_filt = false;
		pout("physical device:%p:linear tiling of b8g8r8a8 sRGB texel memory blocks does not have linear filtering\n", surf_p.dev.phydev.vk);
	}
}
STATIC void phydev_surf_caps_get(void)
{
	s32 r;
	struct vk_phydev_surf_info_t info;

	memset(&info, 0, sizeof(info));
	info.type = vk_struct_type_phydev_surf_info;
	info.surf = surf_p.vk;
	memset(&surf_p.dev.phydev.surf_caps, 0,
					sizeof(surf_p.dev.phydev.surf_caps));
	surf_p.dev.phydev.surf_caps.type = vk_struct_type_surf_caps;
	vk_get_phydev_surf_caps(&info, &surf_p.dev.phydev.surf_caps);
	IF_FATALVK("%d:physical device:%p:surface:%p:unable to get our surface capabilities in the context of the selected physical device\n", r, surf_p.dev.phydev.vk, surf_p.vk);
	/* we have room for a maximum of 3 images per swapchain */
	if (surf_p.dev.phydev.surf_caps.core.imgs_n_min > swpchn_imgs_n_max)
		fatal("physical device:%p:surface:%p:we have room for %u images per swapchain, but this swapchain requires a minimum of %u images\n", surf_p.dev.phydev.vk, surf_p.vk, swpchn_imgs_n_max, surf_p.dev.phydev.surf_caps.core.imgs_n_min);
}
STATIC void phydev_surf_caps_dump(void)
{
	pout("physical device:%p:surface:%p:imgs_n_min=%u\n", surf_p.dev.phydev.vk, surf_p.vk, surf_p.dev.phydev.surf_caps.core.imgs_n_min);
	pout("physical device:%p:surface:%p:imgs_n_max=%u\n", surf_p.dev.phydev.vk, surf_p.vk, surf_p.dev.phydev.surf_caps.core.imgs_n_max);
	pout("physical device:%p:surface:%p:current extent=(width=%u, height=%u)\n", surf_p.dev.phydev.vk, surf_p.vk, surf_p.dev.phydev.surf_caps.core.current_extent.width, surf_p.dev.phydev.surf_caps.core.current_extent.height);
	pout("physical device:%p:surface:%p:minimal extent=(width=%u, height=%u)\n", surf_p.dev.phydev.vk, surf_p.vk, surf_p.dev.phydev.surf_caps.core.img_extent_min.width, surf_p.dev.phydev.surf_caps.core.img_extent_min.height);
	pout("physical device:%p:surface:%p:maximal extent=(width=%u, height=%u)\n", surf_p.dev.phydev.vk, surf_p.vk, surf_p.dev.phydev.surf_caps.core.img_extent_max.width, surf_p.dev.phydev.surf_caps.core.img_extent_max.height);
	pout("physical device:%p:surface:%p:img_array_layers_n_max=%u\n", surf_p.dev.phydev.vk, surf_p.vk, surf_p.dev.phydev.surf_caps.core.img_array_layers_n_max);
	pout("physical device:%p:surface:%p:supported_transforms=%#08x\n", surf_p.dev.phydev.vk, surf_p.vk, surf_p.dev.phydev.surf_caps.core.supported_transforms);
	pout("physical device:%p:surface:%p:current_transform=%#08x\n", surf_p.dev.phydev.vk, surf_p.vk, surf_p.dev.phydev.surf_caps.core.current_transform);
	pout("physical device:%p:surface:%p:supported_composite_alpha=%#08x\n", surf_p.dev.phydev.vk, surf_p.vk, surf_p.dev.phydev.surf_caps.core.supported_composite_alpha);
	pout("physical device:%p:surface:%p:supported_img_usage_flags=%#08x\n", surf_p.dev.phydev.vk, surf_p.vk, surf_p.dev.phydev.surf_caps.core.supported_img_usage_flags);
}
STATIC void tmp_phydev_surf_present_modes_get(void)
{
	s32 r;

	tmp_present_modes_n_l = tmp_present_modes_n_max;
	vk_get_phydev_surf_present_modes();
	IF_FATALVK("%d:physical device:%p:surface:%p:unable to get the physical device present mode for our surface\n", r, surf_p.dev.phydev.vk, surf_p.vk);
}
STATIC u8 *present_mode_to_str(u32 mode)
{
	switch (mode) {
	case vk_present_mode_immediate:
		return "immediate";
	case vk_present_mode_mailbox:
		return "mailbox";
	case vk_present_mode_fifo:
		return "fifo";
	case vk_present_mode_fifo_relaxed:
		return "fifo relaxed";
	default:
		return "unknown";
	}
}
STATIC void tmp_phydev_surf_present_modes_dump(void)
{
	u8 i;

	i = 0;
	pout("physical device:%p:surface:%p:%u present modes\n", surf_p.dev.phydev.vk, surf_p.vk, tmp_present_modes_n_l);
	loop {
		if (i == (u8)tmp_present_modes_n_l)
			break;
		pout("physical device:%p:surface:%p:present mode=%s\n", surf_p.dev.phydev.vk, surf_p.vk, present_mode_to_str(tmp_present_modes_l[i]));
		++i;
	}
}
STATIC void phydev_init(void)
{
	tmp_phydevs_get();
	/*--------------------------------------------------------------------*/
	tmp_phydevs_exts_dump();
	tmp_phydevs_props_dump();
	tmp_phydevs_mem_props_get();
	tmp_phydevs_mem_props_dump();
	/*--------------------------------------------------------------------*/
	tmp_phydevs_q_fams_get();
	tmp_phydevs_q_fams_dump();
	/*====================================================================*/
	/* from here our surf is involved */
	/*--------------------------------------------------------------------*/
	/* select the phydev and its q family which can work with our surf */
	tmp_phydevs_q_fams_surf_support_get();
	tmp_phydev_and_q_fam_select();
	/*--------------------------------------------------------------------*/
	phydev_surf_texel_mem_blk_fmts_dump();
	phydev_surf_b8g8r8a8_srgb_setup();
	/*--------------------------------------------------------------------*/
	phydev_surf_caps_get();
	phydev_surf_caps_dump();
	/*--------------------------------------------------------------------*/
	tmp_phydev_surf_present_modes_get();
	tmp_phydev_surf_present_modes_dump();
}
/* the phydev q fam selected */
STATIC void dev_create(void)
{
	struct vk_dev_create_info_t info;
	struct vk_dev_q_create_info_t q_info;
	float q_prio;
	STATIC u8 *exts[] = {
		/* 1.1 promoted */
		"VK_KHR_bind_memory2",
		/* 1.1 promoted */
		"VK_KHR_get_memory_requirements2",
		"VK_KHR_swapchain"};

	s32 r;

	memset(&info, 0, sizeof(info));
	memset(&q_info, 0, sizeof(q_info));
	/*--------------------------------------------------------------------*/
	q_info.type = vk_struct_type_dev_q_create_info;
	q_info.q_fam = surf_p.dev.phydev.q_fam;
	q_info.qs_n = 1;
	q_info.q_prios = &q_prio;
	q_prio = 1.0f;
	/*--------------------------------------------------------------------*/
	info.type = vk_struct_type_dev_create_info;
	info.q_create_infos_n = 1;
	info.q_create_infos = &q_info;
	info.enabled_exts_n = ARRAY_N(exts);
	info.enabled_ext_names = exts;
	vk_create_dev(&info);
	IF_FATALVK("%d:physical device:%p:unable to create a vulkan device\n", r, surf_p.dev.phydev.vk);
	pout("physical device:%p:vulkan device created with one proper queue:%p\n", surf_p.dev.phydev.vk, surf_p.dev.vk);
}
STATIC void q_get(void)
{
	pout("device:%p:getting queue:family=%u queue=0\n", surf_p.dev.vk, surf_p.dev.phydev.q_fam);
	vk_get_dev_q();
	pout("device:%p:got queue:%p\n", surf_p.dev.vk, surf_p.dev.q);
}
STATIC void cp_create(void)
{
	s32 r;
	struct vk_cp_create_info_t info;

	memset(&info, 0, sizeof(info));
	info.type = vk_struct_type_cp_create_info;
	info.flags = vk_cp_create_reset_cb_bit;
	info.q_fam = surf_p.dev.phydev.q_fam;
	vk_create_cp(&info);
	IF_FATALVK("%d:unable to create the commmand pool with supported command buffer reset\n", r);
	pout("device:%p:queue family:%u:created command pool %p with supported command buffer reset\n", surf_p.dev.vk, surf_p.dev.phydev.q_fam, surf_p.dev.cp);
}
STATIC void dev_init(void)
{
	phydev_init();
	/*--------------------------------------------------------------------*/
	dev_create();
	dev_syms();
	q_get();
	cp_create();
}
/* XXX: the surf is an obj at the instance lvl, NOT THE [PHYSICAL] * DEV LVL */
STATIC void surf_create(xcb_connection_t *c, u32 win_id)
{
	struct vk_xcb_surf_create_info_t vk_xcb_info;
	s32 r;

	memset(&surf_p, 0, sizeof(surf_p));
	memset(&vk_xcb_info, 0, sizeof(vk_xcb_info));
	vk_xcb_info.type = vk_struct_type_xcb_surf_create_info;
	vk_xcb_info.c = c;
	vk_xcb_info.win = win_id;
	vk_create_xcb_surf(&vk_xcb_info);
	IF_FATALVK("%d:xcb:%p:window id:%#x:unable to create a vulkan surface from this x11 window\n", r, c, win_id);
	pout("xcb:%p:window id:%#x:created vk_surface=%p\n", c, win_id, surf_p.vk);
}
STATIC void swpchn_init_once(void)
{
	memset(&surf_p.dev.swpchn, 0, sizeof(surf_p.dev.swpchn));	
}
STATIC void swpchn_reinit(void)
{
	struct vk_swpchn_t *old_swpchn;
	struct vk_swpchn_create_info_t info;
	struct phydev_t *p;
	s32 r;
	/* first, deal with the previous swpchn, if any */
	old_swpchn = surf_p.dev.swpchn.vk;
	surf_p.dev.swpchn.vk = 0;
	/* lifetime of swpchn imgs is handled by the pe, not us */
	surf_p.dev.swpchn.imgs_n = 0;
	memset(surf_p.dev.swpchn.imgs, 0, sizeof(surf_p.dev.swpchn.imgs));
	/*--------------------------------------------------------------------*/
	memset(&info, 0, sizeof(info));
	p = &surf_p.dev.phydev;
	info.type = vk_struct_type_swpchn_create_info;
	info.surf = surf_p.vk;
	info.imgs_n_min = surf_p.dev.phydev.surf_caps.core.imgs_n_min;
	info.img_texel_mem_blk_fmt = p->b8g8r8a8_srgb.conf_core.fmt;
	info.img_color_space = p->b8g8r8a8_srgb.conf_core.color_space;
	memcpy(&info.img_extent,
			&surf_p.dev.phydev.surf_caps.core.current_extent,
						sizeof(info.img_extent));
	info.img_layers_n = 1;
	info.img_usage = vk_img_usage_color_attachment_bit
					| vk_img_usage_transfer_dst_bit;
	info.img_sharing_mode = vk_sharing_mode_exclusive;
	info.pre_transform = vk_surf_transform_identity_bit;
	info.composite_alpha = vk_composite_alpha_opaque_bit;
	info.present_mode = vk_present_mode_fifo;
	info.clipped = vk_true;
	if (old_swpchn != 0)
		info.old_swpchn = old_swpchn;
	vk_create_swpchn(&info);
	IF_FATALVK("%d:device:%p:surface:%p:unable to create the swapchain\n", r, surf_p.dev.vk, surf_p.vk);
	if (old_swpchn != 0)
		vk_destroy_swpchn(old_swpchn);
}
STATIC void swpchn_imgs_get(void)
{
	s32 r;
	surf_p.dev.swpchn.imgs_n = swpchn_imgs_n_max;
	/*
	 * we queried the minimum image count already, then vk_incomplete is not
	 * supposed to happen
	 */
	vk_get_swpchn_imgs();
	IF_FATALVK("%d:device:%p:surface:%p:swapchain:%p:unable to get the swapchain images\n", r, surf_p.dev.vk, surf_p.vk, surf_p.dev.swpchn.vk);
}
STATIC void sems_create(void)
{
	s32 r;
	struct vk_sem_create_info_t info;
	u8 sem;

	sem = 0;
	loop {
		if (sem == sems_n)
			break;
		memset(&info, 0, sizeof(info));
		info.type = vk_struct_type_sem_create_info;
		vk_create_sem(&info, &surf_p.dev.sems[sem]);
		IF_FATALVK("%d:device:%p:unable to create a semaphore %u for the synchronization of the swapchain\n", r, surf_p.dev.vk, sem);
		pout("device:%p:semaphore %u for the synchronization of the swapchain created %p\n", surf_p.dev.vk, sem, surf_p.dev.sems[sem]);
		++sem;
	}
}
STATIC void fences_create(void)
{
	s32 r;
	struct vk_fence_create_info_t info;

	memset(&info, 0, sizeof(info));
	info.type = vk_struct_type_fence_create_info;
	info.flags = vk_fence_create_signaled_bit;
	vk_create_fence(&info, &surf_p.dev.fence);
	IF_FATALVK("%d:device:%p:unable to create the synchronization fence\n", r, surf_p.dev.vk);
	pout("device:%p:synchronization fence created %p\n", surf_p.dev.vk, surf_p.dev.fence);

	memset(&info, 0, sizeof(info));
	info.type = vk_struct_type_fence_create_info;
	vk_create_fence(&info, &surf_p.dev.fence_color_space_layout);
	IF_FATALVK("%d:device:%p:unable to create the synchronization fence for the color space layout transition\n", r, surf_p.dev.vk);
	pout("device:%p:color space layout transition synchronization fence created %p\n", surf_p.dev.vk, surf_p.dev.fence_color_space_layout);
}
STATIC void misc_cbs_init_once(void)
{
	s32 r;
	struct vk_cb_alloc_info_t alloc_info;

	memset(&alloc_info, 0, sizeof(alloc_info));
	alloc_info.type = vk_struct_type_cb_alloc_info;
	alloc_info.cp = surf_p.dev.cp;
	alloc_info.lvl = vk_cb_lvl_primary;
	alloc_info.cbs_n = 1;
	vk_alloc_cbs(&alloc_info,npv_vk_surf_p.dev.misc_cbs);
	IF_FATALVK("%d:device:%p:unable to allocate a miscellaneous command buffer from %p command pool\n", r, surf_p.dev.vk, surf_p.dev.cp);
	pout("device:%p:allocated a miscellaneous command buffers for our swapchain images from %p command pool\n", surf_p.dev.vk, surf_p.dev.cp);
}
STATIC void swpchn_imgs_cbs_init_once(void)
{
	s32 r;
	struct vk_cb_alloc_info_t alloc_info;

	memset(&alloc_info, 0, sizeof(alloc_info));
	alloc_info.type = vk_struct_type_cb_alloc_info;
	alloc_info.cp = surf_p.dev.cp;
	alloc_info.lvl = vk_cb_lvl_primary;
	alloc_info.cbs_n = swpchn_imgs_n_max;
	vk_alloc_cbs(&alloc_info,npv_vk_surf_p.dev.cbs);
	IF_FATALVK("%d:device:%p:unable to allocate command buffers for our swapchain images from %p command pool\n", r, surf_p.dev.vk, surf_p.dev.cp);
	pout("device:%p:allocated %u command buffers for our swapchain images from %p command pool\n", surf_p.dev.vk, surf_p.dev.swpchn.imgs_n, surf_p.dev.cp);
}
