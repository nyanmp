constant_u32 {
	tmp_phydevs_n_max = 16,
	tmp_phydev_q_fams_n_max = 16,
	tmp_present_modes_n_max = 16
};
struct tmp_phydev_t {
	struct vk_phydev_t *vk;
	u8 q_fams_n;
	struct vk_q_fam_props_t q_fams[tmp_phydev_q_fams_n_max];
	bool is_discret_gpu;
	struct vk_phydev_mem_props_t mem_props;
	bool q_fams_surf_support[tmp_phydev_q_fams_n_max];
};
/*============================================================================*/
STATIC void *loader_l;
STATIC struct vk_instance_t *instance_l;
/*----------------------------------------------------------------------------*/
STATIC struct tmp_phydev_t tmp_phydevs_l[tmp_phydevs_n_max];
STATIC u32 tmp_phydevs_n_l;
STATIC u32 tmp_present_modes_l[tmp_present_modes_n_max];
STATIC u32 tmp_present_modes_n_l;
