#ifndef NPV_PUBLIC_H
#define NPV_PUBLIC_H
/*
 * code protected with a GNU affero GPLv3 license 
 * copyright (C) 2020 Sylvain BERTRAND
 */
#include <stdbool.h>
#include <stdarg.h>
#include <stdint.h>
#include "npv/c_fixing.h"
/*----------------------------------------------------------------------------*/
#include "npv/namespace/public.h"
/*----------------------------------------------------------------------------*/
#include "npv/public/state.frag.h"
/*----------------------------------------------------------------------------*/
STATIC void pout(u8 *fmt, ...);
STATIC void vpout(u8 *fmt, va_list ap);
STATIC void perr(u8 *fmt, ...);
STATIC void vperr(u8 *fmt, va_list ap);
STATIC void warning(u8 *fmt, ...);
STATIC void vwarning(u8 *fmt, va_list ap);
STATIC void fatal(u8 *fmt, ...);
STATIC void vfatal(u8 *fmt, va_list ap);
STATIC void exit_ok(u8 *fmt, ...);
/*----------------------------------------------------------------------------*/
#define CLEANUP
#include "npv/namespace/public.h"
#undef CLEANUP
#endif
