STATIC void npv_xcb_evt(void) { loop 
{
	/* draining evts */
	xcb_generic_event_t *e;	

	e = npv_dl_xcb_poll_for_event(npv_xcb_p.c);
	if (e == 0)
		break;
	npv_xcb_evt_handle(e);
	free(e);
}}
STATIC void npv_xcb_init_once(u16 win_width, u16 win_height,
							bool start_fullscreen)
{
	int r;

	memset(&npv_xcb_p, 0, sizeof(npv_xcb_p));
	npv_xcb_p.width = win_width;
	npv_xcb_p.height = win_height;

	npv_xcb_libs_load();
	npv_xcb_syms();

	npv_xcb_connect();
	npv_xcb_scr_get();
	npv_xcb_screensaver_heartbeat_timer_init_once();
	npv_xcb_mouse_visibilty_init_once();
	npv_xcb_win_create();
	npv_xcb_wm_class();
	npv_xcb_wm_name();
	npv_xcb_wm_hints(); /* before the win is mapped */
	npv_xcb_win_map();

	r = npv_dl_xcb_flush(npv_xcb_p.c);
	if (r <= 0)
		npv_xcb_fatal("%d:xcb:'%s':connection:%p:screen:%d:root window id:%#x:window id:%#x:unable to flush the connection\n", r, npv_xcb_p.disp_env, npv_xcb_p.c, npv_xcb_p.scr_idx, npv_xcb_p.scr->root, npv_xcb_p.win_id);
	npv_xcb_pout("'%s':connection:%p:connection flushed\n", npv_xcb_p.disp_env, npv_xcb_p.c);
	npv_xcb_fullscreen_init_once(start_fullscreen);
}
STATIC void npv_xcb_screensaver_heartbeat_timer_start(void)
{
	struct itimerspec t;
	int r;

	memset(&t, 0, sizeof(t));
	t.it_value.tv_sec = npv_xcb_screensaver_heartbeat_timeout_s;
	t.it_interval.tv_sec = npv_xcb_screensaver_heartbeat_timeout_s;
	r = timerfd_settime(npv_xcb_p.screensaver_heartbeat_timer_fd, 0, &t, 0);
	if (r == -1)
		npv_xcb_fatal("unable to arm the screensaver heartbeat timer to %u seconds\n", npv_xcb_screensaver_heartbeat_timeout_s);
}
STATIC void npv_xcb_screensaver_heartbeat_timer_evt(void)
{
	int r;
	uint64_t exps_n;

	exps_n = 0;
	r = read(npv_xcb_p.screensaver_heartbeat_timer_fd, &exps_n,
								sizeof(exps_n));
	if (r == -1)
		npv_xcb_fatal("unable to read the number of timer expirations related to the xcb screensaver heartbeat\n");
	if (!npv_paused_p) {
		npv_dl_xcb_force_screen_saver(npv_xcb_p.c,
							XCB_SCREEN_SAVER_RESET);
		npv_dl_xcb_flush(npv_xcb_p.c);
	}
}
STATIC void npv_xcb_mouse_visibility_timer_evt(void)
{
	int r;
	uint64_t exps_n;
	xcb_query_pointer_cookie_t cookie;
	xcb_query_pointer_reply_t *reply;
	xcb_generic_error_t *err;

	exps_n = 0;
	r = read(npv_xcb_p.mouse_visibility_timer_fd, &exps_n, sizeof(exps_n));
	if (r == -1)
		npv_xcb_fatal("unable to read the number of timer expirations related to the xcb mouse visibility\n");
	if (npv_xcb_p.mouse_hidden)
		return;
	cookie = npv_dl_xcb_query_pointer(npv_xcb_p.c, npv_xcb_p.win_id);
	err = 0;
	reply = npv_dl_xcb_query_pointer_reply(npv_xcb_p.c, cookie, &err);
	if (reply == 0) {
		npv_xcb_warning("an error occured while handling the mouse pointer visibility:%d\n", err->error_code);
		free(err);
		return;
	}
	if (reply->root_x == npv_xcb_p.last_root_x
				&& reply->root_y == npv_xcb_p.last_root_y) {
		npv_dl_xcb_xfixes_hide_cursor(npv_xcb_p.c, npv_xcb_p.win_id);
		npv_dl_xcb_flush(npv_xcb_p.c);
		npv_xcb_p.mouse_hidden = true;
	}
	free(reply);
}
#define NET_WM_STATE_TOGGLE 2
STATIC void npv_cmd_fullscreen_toggle(void)
{
	npv_xcb_fullscreen_action(NET_WM_STATE_TOGGLE);
}
#undef NET_WM_STATE_TOGGLE
