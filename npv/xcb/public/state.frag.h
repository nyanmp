constant_u32 {
	npv_xcb_screensaver_heartbeat_timeout_s =  30,
	npv_xcb_mouse_visibility_interval_s = 3
};
STATIC struct {
	u8 *disp_env;
	xcb_connection_t *c;
	int fd;
	xcb_setup_t *setup;
	int scr_idx;
	xcb_screen_t *scr;
	u32 win_id;
	u16 width;
	u16 height;

	int screensaver_heartbeat_timer_fd;

	int mouse_visibility_timer_fd;
	bool mouse_hidden;
	int16_t last_root_x;
	int16_t last_root_y;
} npv_xcb_p;
