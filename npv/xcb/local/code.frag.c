STATIC void npv_xcb_fatal(u8 *fmt, ...)
{
	va_list ap;

	npv_perr("xcb:");
	va_start(ap, fmt);
	npv_vfatal(fmt, ap);
	va_end(ap); /* unreachable */
}
STATIC void npv_xcb_warning(u8 *fmt, ...)
{
	va_list ap;

	npv_perr("xcb:");
	va_start(ap, fmt);
	npv_vwarning(fmt, ap);
	va_end(ap);
}
STATIC void npv_xcb_pout(u8 *fmt, ...)
{
	va_list ap;

	npv_pout("xcb:");
	va_start(ap, fmt);
	npv_vpout(fmt, ap);
	va_end(ap);
}
STATIC void npv_xcb_libs_load(void)
{
	npv_xcb_lib_l = dlopen("libxcb.so.1", RTLD_LAZY);
	if (npv_xcb_lib_l == 0)
		npv_xcb_fatal("%s:unable to load the xcb dynamic shared library\n", dlerror());
	npv_xcb_xfixes_lib_l = dlopen("libxcb-xfixes.so.0", RTLD_LAZY);
	if (npv_xcb_xfixes_lib_l == 0)
		npv_xcb_fatal("%s:unable to load the xcb xfixes dynamic shared library\n", dlerror());
}
#define XCB_DLSYM(x) \
	npv_dl_##x = dlsym(npv_xcb_lib_l, #x); \
	if (npv_dl_##x == 0) \
		npv_xcb_fatal("%s:unable to find " #x "\n", dlerror());
#define XCB_XFIXES_DLSYM(x) \
	npv_dl_##x = dlsym(npv_xcb_xfixes_lib_l, #x); \
	if (npv_dl_##x == 0) \
		npv_xcb_fatal("%s:unable to find " #x "\n", dlerror());
STATIC void npv_xcb_syms(void)
{
	XCB_DLSYM(xcb_connect);
	XCB_DLSYM(xcb_get_file_descriptor);
	XCB_DLSYM(xcb_generate_id);
	XCB_DLSYM(xcb_connection_has_error);
	XCB_DLSYM(xcb_get_setup);
	XCB_DLSYM(xcb_setup_roots_length);
	XCB_DLSYM(xcb_setup_roots_iterator);
	XCB_DLSYM(xcb_screen_next);
	XCB_DLSYM(xcb_create_window);
	XCB_DLSYM(xcb_map_window);
	XCB_DLSYM(xcb_map_window_checked);
	XCB_DLSYM(xcb_request_check);
	XCB_DLSYM(xcb_flush);
	XCB_DLSYM(xcb_wait_for_event);
	XCB_DLSYM(xcb_poll_for_event);
	XCB_DLSYM(xcb_change_property);
	XCB_DLSYM(xcb_disconnect);
	XCB_DLSYM(xcb_force_screen_saver);
	XCB_DLSYM(xcb_get_extension_data);
	XCB_DLSYM(xcb_query_pointer);
	XCB_DLSYM(xcb_query_pointer_reply);
	XCB_DLSYM(xcb_send_event);
	XCB_DLSYM(xcb_intern_atom);
	XCB_DLSYM(xcb_intern_atom_reply);
	XCB_XFIXES_DLSYM(xcb_xfixes_id);
	XCB_XFIXES_DLSYM(xcb_xfixes_query_version);
	XCB_XFIXES_DLSYM(xcb_xfixes_show_cursor);
	XCB_XFIXES_DLSYM(xcb_xfixes_hide_cursor);
}
#undef XCB_DLSYM
STATIC void npv_xcb_win_create(void)
{
	u32 value_mask;
	u32 value_list[2];

	npv_xcb_p.win_id = npv_dl_xcb_generate_id(npv_xcb_p.c);
	npv_xcb_pout("'%s':connection:%p:screen:%d:root window id:%#x:window id=%#x\n", npv_xcb_p.disp_env, npv_xcb_p.c, npv_xcb_p.scr_idx, npv_xcb_p.scr->root, npv_xcb_p.win_id);

	value_mask = XCB_CW_BACK_PIXEL | XCB_CW_EVENT_MASK;
	value_list[0] = npv_xcb_p.scr->black_pixel;
	value_list[1] = XCB_EVENT_MASK_KEY_RELEASE
			| XCB_EVENT_MASK_STRUCTURE_NOTIFY
			| XCB_EVENT_MASK_POINTER_MOTION
			| XCB_EVENT_MASK_BUTTON_MOTION
			| XCB_EVENT_MASK_LEAVE_WINDOW
			| XCB_EVENT_MASK_ENTER_WINDOW;
	npv_dl_xcb_create_window(npv_xcb_p.c, XCB_COPY_FROM_PARENT,
					npv_xcb_p.win_id,
					npv_xcb_p.scr->root, 0, 0,
					npv_xcb_p.width, npv_xcb_p.height,
					0,
					XCB_WINDOW_CLASS_INPUT_OUTPUT,
					npv_xcb_p.scr->root_visual,
					value_mask, value_list);
}
STATIC void npv_xcb_win_map(void)
{
	xcb_void_cookie_t cookie;
	xcb_generic_error_t *e;

    	cookie = npv_dl_xcb_map_window_checked(npv_xcb_p.c, npv_xcb_p.win_id);
	npv_xcb_pout("'%s':connection:%p:screen:%d:root window id:%#x:window id:%#x:map window request cookie=%#x\n", npv_xcb_p.disp_env, npv_xcb_p.c, npv_xcb_p.scr_idx, npv_xcb_p.scr->root, npv_xcb_p.win_id, cookie);

	e = npv_dl_xcb_request_check(npv_xcb_p.c, cookie);
	if (e != 0)
		npv_xcb_fatal("'%s':connection:%p:screen:%d:root window id:%#x:window id:%#x:unable to map window\n", npv_xcb_p.disp_env, npv_xcb_p.c, npv_xcb_p.scr_idx, npv_xcb_p.scr->root, npv_xcb_p.win_id);
	npv_xcb_pout("'%s':connection:%p:screen:%d:root window id:%#x:window id:%#x:window mapped\n", npv_xcb_p.disp_env, npv_xcb_p.c, npv_xcb_p.scr_idx, npv_xcb_p.scr->root, npv_xcb_p.win_id);
}
/*
 * a disp is n scrs and 1 [set of] keyboard[s] and 1 [set of] mouse[s]
 * 1 scr could be n monitors
 * nowdays: usually 1 scr per display
 * 1 scr has 1 root win
 */
STATIC void npv_xcb_connect(void)
{
	int r;

	npv_xcb_p.disp_env = getenv("DISPLAY");
	if (npv_xcb_p.disp_env == 0 || npv_xcb_p.disp_env[0] == 0)
		npv_xcb_fatal("no x11 DISPLAY environment variable, exiting\n");

	npv_xcb_p.scr_idx = 0;
	/* should be 0 though */
	npv_xcb_p.c = npv_dl_xcb_connect(0, &npv_xcb_p.scr_idx);
	r = npv_dl_xcb_connection_has_error(npv_xcb_p.c);
	if (r > 0)
		npv_xcb_fatal("%d:%s:error while connecting to the x11 server\n", r, npv_xcb_p.disp_env);
	npv_xcb_pout("'%s':connection=%p, default screen index is %d (should be 0)\n", npv_xcb_p.disp_env, npv_xcb_p.c, npv_xcb_p.scr_idx);

	npv_xcb_p.fd = npv_dl_xcb_get_file_descriptor(npv_xcb_p.c);
	if (npv_xcb_p.fd == -1)
		npv_xcb_fatal("'%s':unable to get the connection file descriptor for epoll\n", npv_xcb_p.disp_env);
	npv_xcb_pout("'%s':connection:%p:file descriptor %d\n", npv_xcb_p.disp_env, npv_xcb_p.c, npv_xcb_p.fd);
}
STATIC void npv_xcb_scr_get(void)
{
	xcb_screen_iterator_t iter;
	int scrs_n;
	int i;

	npv_xcb_p.setup = npv_dl_xcb_get_setup(npv_xcb_p.c);

	scrs_n = npv_dl_xcb_setup_roots_length(npv_xcb_p.setup);
	npv_xcb_pout("'%s':connection:%p:has %d screens (should be 1)\n", npv_xcb_p.disp_env, npv_xcb_p.c, scrs_n);

	iter = npv_dl_xcb_setup_roots_iterator(npv_xcb_p.setup);
	i = 0;
	npv_xcb_p.scr = 0;
	loop {
		if (iter.rem == 0)
			break; /* no more scr to iterate on */

		if (i == npv_xcb_p.scr_idx) {
			npv_xcb_p.scr = iter.data;
			break;
		}
		npv_dl_xcb_screen_next(&iter);
	}
	npv_xcb_pout("'%s':connection:%p:screen:%d:root window id:%#x:width=%d pixels\n", npv_xcb_p.disp_env, npv_xcb_p.c, npv_xcb_p.scr_idx, npv_xcb_p.scr->root, npv_xcb_p.scr->width_in_pixels);
	npv_xcb_pout("'%s':connection:%p:screen:%d:root window id:%#x:height=%d pixels\n", npv_xcb_p.disp_env, npv_xcb_p.c, npv_xcb_p.scr_idx, npv_xcb_p.scr->root, npv_xcb_p.scr->height_in_pixels);
	npv_xcb_pout("'%s':connection:%p:screen:%d:root window id:%#x:white pixel=0x%08x\n", npv_xcb_p.disp_env, npv_xcb_p.c, npv_xcb_p.scr_idx, npv_xcb_p.scr->root, npv_xcb_p.scr->white_pixel);
	npv_xcb_pout("'%s':connection:%p:screen:%d:root window id:%#x:black pixel=0x%08x\n", npv_xcb_p.disp_env, npv_xcb_p.c, npv_xcb_p.scr_idx, npv_xcb_p.scr->root, npv_xcb_p.scr->black_pixel);
}
#define MIN_SZ_BIT	(1 << 4)
#define MAX_SZ_BIT	(1 << 5)
#define BASE_SZ_BIT	(1 << 8)
#define FLAGS		0
/* 4 padding dwords */
#define MIN_WIDTH	5
#define MIN_HEIGHT	6
#define MAX_WIDTH	7
#define MAX_HEIGHT	8
#define BASE_WIDTH	15
#define BASE_HEIGHT	16
#define DWORDS_N	18
STATIC void npv_xcb_wm_hints(void)
{
#if 0
	/* the following will tell the wm the win is not resizable */
	u32 data[DWORDS_N];

	memset(data, 0, sizeof(data));
	data[FLAGS] = MIN_SZ_BIT | MAX_SZ_BIT;
	/*
	 * XXX: min == max --> the wm must understand this information as the
	 * win being non-resizeable.
	 * next: unlock the size and recreate the vulkan swpchn at each x11
	 * resize evt.
	 */
	data[MIN_WIDTH] = npv_xcb_p.width;
	data[MIN_HEIGHT] = npv_xcb_p.height;
	data[MAX_WIDTH] = npv_xcb_p.width;
	data[MAX_HEIGHT] = npv_xcb_p.height;
	npv_dl_xcb_change_property(npv_xcb_p.c, XCB_PROP_MODE_REPLACE,
				npv_xcb_p.win_id, XCB_ATOM_WM_NORMAL_HINTS,
				XCB_ATOM_WM_SIZE_HINTS, 32, DWORDS_N, data);
#endif
#if 0
	/* the following will set the base size */
	u32 data[DWORDS_N];

	memset(data, 0, sizeof(data));
	data[FLAGS] = BASE_SZ_BIT;
	data[BASE_WIDTH] = npv_xcb_p.width;
	data[BASE_HEIGHT] = npv_xcb_p.height;

	npv_dl_xcb_change_property(npv_xcb_p.c, XCB_PROP_MODE_REPLACE,
				npv_xcb_p.win_id, XCB_ATOM_WM_NORMAL_HINTS,
				XCB_ATOM_WM_SIZE_HINTS, 32, DWORDS_N, data);
#endif
}
#undef MIN_SZ_BIT
#undef MAX_SZ_BIT
#undef BASE_SZ_BIT
#undef FLAGS
#undef MIN_WIDTH
#undef MIN_HEIGHT
#undef MAX_WIDTH
#undef MAX_HEIGHT
#undef BASE_WIDTH
#undef BASE_HEIGHT
#undef DWORDS_N
#define NPV_WM_CLASS "npv\0npv"
STATIC void npv_xcb_wm_class(void)
{
	npv_dl_xcb_change_property(npv_xcb_p.c, XCB_PROP_MODE_REPLACE,
				npv_xcb_p.win_id, XCB_ATOM_WM_CLASS,
				XCB_ATOM_STRING, 8, sizeof(NPV_WM_CLASS),
				NPV_WM_CLASS);
}
#undef NPV_WM_CLASS
#define NPV_WM_NAME "npv"
STATIC void npv_xcb_wm_name(void)
{
	npv_dl_xcb_change_property(npv_xcb_p.c, XCB_PROP_MODE_REPLACE,
				npv_xcb_p.win_id, XCB_ATOM_WM_NAME,
				XCB_ATOM_STRING, 8, strlen(NPV_WM_NAME),
				NPV_WM_NAME);
}
#undef NPV_WM_NAME
STATIC void npv_xcb_evt_key_release(xcb_generic_event_t *evt)
{
	u8 b;
	xcb_key_release_event_t *key;

	key = (xcb_key_release_event_t*)evt;
	b = 0;
	loop {
		if (b == ARRAY_N(npv_x11_binds))
			break;
		if (key->detail == npv_x11_binds[b].keycode) {
			npv_xcb_pout("'%s':connection:%p:event:key release:keycode:%#02x:running command for bind \"%s\"\n", npv_xcb_p.disp_env, npv_xcb_p.c, key->detail, npv_x11_binds[b].name);
			npv_x11_binds[b].cmd();
			return;
		}
		++b;
	}
	npv_xcb_pout("'%s':connection:%p:event:key release:keycode:%#02x\n", npv_xcb_p.disp_env, npv_xcb_p.c, key->detail);
}
STATIC void npv_xcb_evt_leave_win(xcb_generic_event_t *evt)
{
	if (npv_xcb_p.mouse_hidden) {
		npv_dl_xcb_xfixes_show_cursor(npv_xcb_p.c, npv_xcb_p.win_id);
		npv_dl_xcb_flush(npv_xcb_p.c);
	}
	npv_xcb_p.mouse_hidden = false;
	npv_xcb_p.last_root_x = -1;
	npv_xcb_p.last_root_y = -1;
}
STATIC void npv_xcb_mouse_visibility_timer_start(void)
{
	struct itimerspec t;
	int r;

	memset(&t, 0, sizeof(t));
	t.it_value.tv_sec = npv_xcb_mouse_visibility_interval_s;
	r = timerfd_settime(npv_xcb_p.mouse_visibility_timer_fd, 0, &t, 0);
	if (r == -1)
		npv_xcb_fatal("unable to arm the mouse visibility timer to %u seconds\n", npv_xcb_mouse_visibility_interval_s);
}
STATIC void npv_xcb_evt_enter_win(xcb_generic_event_t *evt)
{
	xcb_enter_notify_event_t *ene;

	ene = (xcb_enter_notify_event_t*)evt;
	npv_dl_xcb_xfixes_show_cursor(npv_xcb_p.c, npv_xcb_p.win_id);
	npv_dl_xcb_flush(npv_xcb_p.c);
	npv_xcb_p.mouse_hidden = false;
	npv_xcb_p.last_root_x = ene->root_x;
	npv_xcb_p.last_root_y = ene->root_y;
	npv_xcb_mouse_visibility_timer_start();
}
STATIC void npv_xcb_evt_motion(xcb_generic_event_t *evt)
{
	xcb_motion_notify_event_t *mne;

	mne = (xcb_motion_notify_event_t*)evt;
	if (npv_xcb_p.mouse_hidden) {
		npv_dl_xcb_xfixes_show_cursor(npv_xcb_p.c, npv_xcb_p.win_id);
		npv_dl_xcb_flush(npv_xcb_p.c);
	}
	npv_xcb_p.mouse_hidden = false;
	npv_xcb_p.last_root_x = mne->root_x;
	npv_xcb_p.last_root_y = mne->root_y;
	npv_xcb_mouse_visibility_timer_start();
}
#if 0
typedef struct xcb_configure_notify_event_t {
    uint8_t      response_type;
    uint8_t      pad0;
    uint16_t     sequence;
    xcb_window_t event;
    xcb_window_t window;
    xcb_window_t above_sibling;
    int16_t      x;
    int16_t      y;
    uint16_t     width;
    uint16_t     height;
    uint16_t     border_width;
    uint8_t      override_redirect;
    uint8_t      pad1;
} xcb_configure_notify_event_t;
#endif
STATIC void npv_xcb_evt_cfg_notify(xcb_generic_event_t *evt)
{
	xcb_configure_notify_event_t *cne;

	cne = (xcb_configure_notify_event_t*)evt;
	npv_xcb_p.width = cne->width;
	npv_xcb_p.height = cne->height;
#if 0
	PERR("CFG_NOTIFY_EVT:x=%d y=%d w=%u h=%u bw=%u\n", cne->x, cne->y, cne->width, cne->height, cne->border_width);
#endif
}
STATIC void npv_xcb_evt_handle(xcb_generic_event_t *evt)
{
	u8 evt_code;
	/*
	 * do not discriminate evts generated by clients using sendevent
	 * requests (note: "client message" evts have always their most
	 * significant bit set)
	 */
	evt_code = evt->response_type & 0x7f;

	switch (evt_code) {
	case XCB_KEY_RELEASE:
		npv_xcb_evt_key_release(evt);
		break;
	case XCB_CONFIGURE_NOTIFY:
		npv_xcb_evt_cfg_notify(evt);
		break;	
	case XCB_MOTION_NOTIFY:
		npv_xcb_evt_motion(evt);
		break;
	case XCB_LEAVE_NOTIFY:
		npv_xcb_evt_leave_win(evt);
		break;
	case XCB_ENTER_NOTIFY:
		npv_xcb_evt_enter_win(evt);
		break;
	default:
		break;
	}
}
STATIC void npv_xcb_screensaver_heartbeat_timer_init_once(void)
{
	errno = 0;
	npv_xcb_p.screensaver_heartbeat_timer_fd = timerfd_create(
						CLOCK_MONOTONIC, TFD_NONBLOCK);
	if (npv_xcb_p.screensaver_heartbeat_timer_fd == -1)
		npv_xcb_fatal("unable to get a timer file descriptor for the screensaver heartbeat:%s\n", strerror(errno));
}
STATIC void npv_xcb_mouse_visibilty_init_once(void)
{
	struct xcb_query_extension_reply_t const *xfixes;
	xcb_xfixes_query_version_cookie_t qec;

	xfixes = npv_dl_xcb_get_extension_data(npv_xcb_p.c,
							npv_dl_xcb_xfixes_id);
	if (xfixes->response_type != 1 || xfixes->present == 0)
		npv_xcb_warning("the server is missing xfixes extension\n");
	/* the hice/show cursor is supported from version 4 */
	npv_dl_xcb_xfixes_query_version(npv_xcb_p.c, 4, 0);
	/*--------------------------------------------------------------------*/
	npv_xcb_p.mouse_hidden = false;
	npv_xcb_p.last_root_x = -1;
	npv_xcb_p.last_root_y = -1;

	errno = 0;
	npv_xcb_p.mouse_visibility_timer_fd = timerfd_create(CLOCK_MONOTONIC,
								TFD_NONBLOCK);
	if (npv_xcb_p.mouse_visibility_timer_fd == -1)
		npv_xcb_fatal("unable to get a timer file descriptor for the mouse visibility management:%s\n", strerror(errno));
}
#define NET_WM_STATE			"_NET_WM_STATE"
#define NET_WM_STATE_FULLSCREEN		"_NET_WM_STATE_FULLSCREEN"
#define SOURCE_NORMAL_APP		1
#define STRLEN(x)			(sizeof(x)-1)
#define DONT_PROPAGATE_TO_ANCESTORS	0
STATIC void npv_xcb_fullscreen_action(u32 action)
{
	xcb_intern_atom_cookie_t cookie_net_wm_state;
	xcb_intern_atom_cookie_t cookie_net_wm_state_fullscreen;
	xcb_intern_atom_reply_t *reply;
	xcb_generic_error_t *err;
	xcb_atom_t net_wm_state;
	xcb_atom_t net_wm_state_fullscreen;
	xcb_client_message_event_t cm_evt;

	cookie_net_wm_state = npv_dl_xcb_intern_atom(npv_xcb_p.c, 1,
					STRLEN(NET_WM_STATE), NET_WM_STATE);
	cookie_net_wm_state_fullscreen = npv_dl_xcb_intern_atom(npv_xcb_p.c, 1,
		STRLEN(NET_WM_STATE_FULLSCREEN), NET_WM_STATE_FULLSCREEN);
	err = 0;
	reply = npv_dl_xcb_intern_atom_reply(npv_xcb_p.c, cookie_net_wm_state,
									&err);
	if (reply == 0) {
		npv_xcb_warning("unable to set fullscreen:unable to get the _NET_WM_STATE atom:%d\n", err->error_code);
		free(err);
		return;
	}
	net_wm_state = reply->atom;
	free(reply);
	err = 0;
	reply = npv_dl_xcb_intern_atom_reply(npv_xcb_p.c,
					cookie_net_wm_state_fullscreen, &err);
	if (reply == 0) {
		npv_xcb_warning("unable to set fullscreen:unable to get the _NET_WM_STATE_FULLSCREEN atom:%d\n", err->error_code);
		free(err);
		return;
	}
	net_wm_state_fullscreen = reply->atom;
	free(reply);
	/*--------------------------------------------------------------------*/
	memset(&cm_evt, 0, sizeof(cm_evt));
	cm_evt.response_type = XCB_CLIENT_MESSAGE;
	cm_evt.format = 32;
	cm_evt.window = npv_xcb_p.win_id;
	cm_evt.type = net_wm_state;
	cm_evt.data.data32[0] = action;
	cm_evt.data.data32[1] = net_wm_state_fullscreen;
	cm_evt.data.data32[3] = SOURCE_NORMAL_APP;
	/*
	 * client message send event req must use the evt mask below and must
	 * not propagate the evt, see ewmh specs
	 */
	npv_dl_xcb_send_event(npv_xcb_p.c, DONT_PROPAGATE_TO_ANCESTORS,
			npv_xcb_p.scr->root, XCB_EVENT_MASK_STRUCTURE_NOTIFY
			| XCB_EVENT_MASK_SUBSTRUCTURE_REDIRECT , (u8*)&cm_evt);
	npv_dl_xcb_flush(npv_xcb_p.c);
}
#undef NET_WM_STATE
#undef NET_WM_STATE_FULLSCREEN
#undef NET_WM_STATE_ADD
#undef SOURCE_NORMAL_APP
#undef STRLEN
#undef DONT_PROPAGATE_TO_ANCESTORS
#define NET_WM_STATE_ADD 1
STATIC void npv_xcb_fullscreen_init_once(bool start_fullscreen)
{
	xcb_intern_atom_cookie_t cookie_net_wm_state;
	xcb_intern_atom_cookie_t cookie_net_wm_state_fullscreen;
	xcb_intern_atom_reply_t *reply;
	xcb_generic_error_t *err;
	xcb_atom_t net_wm_state;
	xcb_atom_t net_wm_state_fullscreen;
	xcb_client_message_event_t cm_evt;

	if (!start_fullscreen)
		return;
	npv_xcb_fullscreen_action(NET_WM_STATE_ADD);
}
#undef NET_WM_STATE_ADD
#ifdef NPV_DEBUG
STATIC void npv_cmd_debug_toggle(void)
{
	npv_debug_p = !npv_debug_p;
}
#endif
