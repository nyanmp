STATIC void *npv_xcb_lib_l;
STATIC void *npv_xcb_xfixes_lib_l;
/*----------------------------------------------------------------------------*/
STATIC xcb_connection_t *(*npv_dl_xcb_connect)(u8 *displayname, int *scr);
STATIC int (*npv_dl_xcb_get_file_descriptor)(xcb_connection_t *c);
STATIC u32 (*npv_dl_xcb_generate_id)(xcb_connection_t *c);
STATIC int (*npv_dl_xcb_connection_has_error)(xcb_connection_t *c);
STATIC xcb_setup_t *(*npv_dl_xcb_get_setup)(xcb_connection_t *c);
STATIC int (*npv_dl_xcb_setup_roots_length)(xcb_setup_t *s);
STATIC xcb_screen_iterator_t (*npv_dl_xcb_setup_roots_iterator)(xcb_setup_t *s);
STATIC void (*npv_dl_xcb_screen_next)(xcb_screen_iterator_t *iter);
STATIC void (*npv_dl_xcb_create_window)(xcb_connection_t *c, u8 depth,
	xcb_window_t wid, xcb_window_t parent, s16 x, s16 y, u16 width,
	u16 heigth, u16 border_width, u16 class, xcb_visualid_t visual,
	u32 value_mask, u32 *value_list);
STATIC xcb_void_cookie_t (*npv_dl_xcb_map_window)(xcb_connection_t *c,
							xcb_window_t win);
STATIC xcb_void_cookie_t (*npv_dl_xcb_map_window_checked)(xcb_connection_t *c,
							xcb_window_t win);
STATIC xcb_generic_error_t *(*npv_dl_xcb_request_check)(xcb_connection_t *c,
						xcb_void_cookie_t cookie);
STATIC int (*npv_dl_xcb_flush)(xcb_connection_t *c);
STATIC xcb_generic_event_t *(*npv_dl_xcb_wait_for_event)(xcb_connection_t *c);
STATIC xcb_generic_event_t *(*npv_dl_xcb_poll_for_event)(xcb_connection_t *c);
STATIC void (*npv_dl_xcb_disconnect)(xcb_connection_t *c);
STATIC xcb_void_cookie_t (*npv_dl_xcb_change_property)(xcb_connection_t *c,
	u8 mode, xcb_window_t window, xcb_atom_t property, xcb_atom_t type,
					u8 format, u32 data_len, void *data);
STATIC xcb_void_cookie_t (*npv_dl_xcb_force_screen_saver)(xcb_connection_t *c,
								u8 mode);
STATIC struct xcb_query_extension_reply_t const
			*(*npv_dl_xcb_get_extension_data)(xcb_connection_t *c,
							xcb_extension_t *ext);
STATIC xcb_extension_t *npv_dl_xcb_xfixes_id;
STATIC xcb_void_cookie_t (*npv_dl_xcb_xfixes_show_cursor)(xcb_connection_t *c,
							xcb_window_t window);
STATIC xcb_void_cookie_t (*npv_dl_xcb_xfixes_hide_cursor)(xcb_connection_t *c,
							xcb_window_t window);
STATIC xcb_query_pointer_reply_t *(*npv_dl_xcb_query_pointer_reply)(
			xcb_connection_t *c, xcb_query_pointer_cookie_t cookie,
						xcb_generic_error_t **e);
STATIC xcb_query_pointer_cookie_t (*npv_dl_xcb_query_pointer)(
				xcb_connection_t *c, xcb_window_t window);
STATIC xcb_xfixes_query_version_cookie_t (*npv_dl_xcb_xfixes_query_version)(
			xcb_connection_t *c, u32 client_major_version,
						u32 client_minor_version);
xcb_void_cookie_t (*npv_dl_xcb_send_event)(xcb_connection_t *c, u8 propagate,
			xcb_window_t destination, u32 event_mask, u8 *event);
xcb_intern_atom_cookie_t (*npv_dl_xcb_intern_atom)(xcb_connection_t *c,
				u8 only_if_exists, u16 name_len, u8 *name);
xcb_intern_atom_reply_t *(*npv_dl_xcb_intern_atom_reply)(xcb_connection_t *c,
		xcb_intern_atom_cookie_t cookie, xcb_generic_error_t **e);
