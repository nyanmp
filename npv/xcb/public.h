#ifndef NPV_XCB_PUBLIC_H
#define NPV_XCB_PUBLIC_H
/*
 * code protected with a GNU affero GPLv3 license 
 * copyright (C) 2020 Sylvain BERTRAND
 */
#include <stdbool.h>
#include <xcb/xcb.h>
#include "npv/c_fixing.h"
/*----------------------------------------------------------------------------*/
#include "npv/xcb/public/state.frag.h"
/*----------------------------------------------------------------------------*/
STATIC void npv_xcb_init_once(u16 width, u16 height, bool start_fullscreen);
STATIC void npv_xcb_evt(void);
STATIC void npv_xcb_screensaver_heartbeat_timer_start(void);
STATIC void npv_xcb_screensaver_heartbeat_timer_evt(void);
STATIC void npv_xcb_mouse_visibility_timer_evt(void);
#endif
