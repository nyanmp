#ifndef NPV_XCB_MAIN_C
#define NPV_XCB_MAIN_C
/*
 * code protected with a GNU affero GPLv3 license 
 * copyright (C) 2020 Sylvain BERTRAND
 */
#include <stdbool.h>
#include <stdarg.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <dlfcn.h>
#include <sys/timerfd.h>
#include <xcb/xcb.h>
#include <xcb/xfixes.h>
#include "npv/c_fixing.h"
/*----------------------------------------------------------------------------*/
#include "npv/config.h"
/*----------------------------------------------------------------------------*/
#include "npv/global.h"
#include "npv/public.h"
#include "npv/xcb/public.h"
#include "npv/video/public.h"
#include "npv/xcb/local/state.frag.c"
/*----------------------------------------------------------------------------*/
#include "npv/xcb/local/code.frag.c"
#include "npv/xcb/public/code.frag.c"
#endif
