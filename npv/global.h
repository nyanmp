#ifndef NPV_GLOBAL_H
#define NPV_GLOBAL_H
/*
 * code protected with a GNU affero GPLv3 license 
 * copyright (C) 2020 Sylvain BERTRAND
 */
#include <stdio.h>
#include <stdlib.h>
#include "c_fixing.h"
#define STR_SZ U8_MAX
#endif
