#ifndef CLEANUP
#define avcodec_receive_audio_set		avcodec_receive_frame
#define avfilter_bufsink_get_audio_set		av_buffersink_get_frame
#define avfilter_bufsink_get_audio_fmt		av_buffersink_get_format
#define avfilter_bufsrc_add_audio_set_flags	av_buffersrc_add_frame_flags
#define avutil_audio_fr_fmt_is_planar		av_sample_fmt_is_planar
#define avutil_audio_set_ref_alloc		av_frame_alloc
#define avutil_audio_fr_fmt_t			AVSampleFormat
#define avutil_audio_set_ref_t			AVFrame
#define avutil_audio_set_unref			av_frame_unref
#define avutil_get_audio_fr_fmt_str		av_get_sample_fmt_string
#define avutil_get_bytes_per_sample		av_get_bytes_per_sample
/*----------------------------------------------------------------------------*/
/* some struct members, may conflict with the video ones */
#define fr_rate					sample_rate
#define fr_fmt					sample_fmt
#define frs_n					nb_samples
/*============================================================================*/
#else
#undef avcodec_receive_audio_set
#undef avfilter_bufsink_get_audio_set
#undef avfilter_bufsink_get_audio_fmt
#undef avfilter_bufsrc_add_audio_fr_flags
#undef avutil_audio_fr_fmt_is_planar
#undef avutil_audio_set_ref_alloc
#undef avutil_audio_fr_fmt_t
#undef avutil_audio_set_ref_t
#undef avutil_audio_set_unref
#undef avutil_get_audio_fr_fmt_str
#undef avutil_get_bytes_per_sample
/*----------------------------------------------------------------------------*/
#undef fr_rate
#undef fr_fmt
#undef frs_n
#endif
