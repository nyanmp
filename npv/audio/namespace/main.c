#ifndef CLEANUP
#define alsa_recover				npv_audio_alsa_recover
#define chans_buf_init				npv_audio_chans_buf_init
#define chans_buf_inc				npv_audio_chans_buf_inc
#define dec_a_grow				npv_audio_dec_a_grow
#define dec_a_unref_all				npv_audio_dec_a_unref_all
#define dec_ctx_mutex_l				npv_audio_dec_ctx_mutex_l
#define dec_flush				npv_audio_dec_flush
#define dec_l					npv_audio_dec_l
#define dec_set_filter				npv_audio_dec_set_filter
#define draining_state_handle			npv_audio_draining_state_handle
#define draining_state_switch			npv_audio_draining_state_switch
#define fatal					npv_audio_fatal
#define ff_fmt2pcm_layout_best_effort		npv_audio_ff_fmt2pcm_layout_best_effort
#define pcm2ff_strict				npv_audio_pcm2ff_strict
/*----------------------------------------------------------------------------*/
/* from audio/filt */
#define filt_cfg				npv_audio_filt_cfg
#define filt_p					npv_audio_filt_p
#define filt_push_dec_set			npv_audio_filt_push_dec_set
/*----------------------------------------------------------------------------*/
#define init_once_local				npv_audio_init_once_local
#define init_once_public			npv_audio_init_once_public
#define init_pcm_once_public			npv_audio_init_pcm_once_public
#define kernel_ts_types_str_l			npv_audio_kernel_ts_types_str_l
#define pcm_chmaps2ff_chans_layout		npv_audio_pcm_chmaps2ff_chans_layout
#define pcm_cfg_hw_core_best_effort		npv_audio_pcm_cfg_hw_core_best_effort
#define pcm_filt_frs_write			npv_audio_pcm_filt_frs_write
#define pcm_hw_access_decide			npv_audio_pcm_hw_access_decide
#define pcm_hw_access_decide_x			npv_audio_pcm_hw_access_decide_x
#define pcm_hw_buf_sz_cfg			npv_audio_pcm_hw_buf_sz_cfg
#define pcm_hw_chans_n_decide			npv_audio_pcm_hw_chans_n_decide
#define pcm_hw_fmt_decide			npv_audio_pcm_hw_fmt_decide
#define pcm_hw_fmt_decide_x			npv_audio_pcm_hw_fmt_decide_x
#define pcm_hw_rate_decide			npv_audio_pcm_hw_rate_decide
#define pcm_layout2ff_fmt_strict		npv_audio_pcm_layout2ff_fmt_strict
#define pcm_name_l				npv_audio_pcm_name_l
#define pcm_pout_l				npv_audio_pcm_pout_l
#define pcm_perr_l				npv_audio_pcm_perr_l
#define pcm_silence_bufs_l			npv_audio_pcm_silence_bufs_l
#define pcm_silence_frs_write			npv_audio_pcm_silence_frs_write
#define pcm2ff					npv_audio_pcm2ff
#define pout					npv_audio_pout
#define warning					npv_audio_warning
/*============================================================================*/
#else
#undef alsa_recover
#undef chans_buf_init
#undef chans_buf_inc
#undef dec_a_grow
#undef dec_a_unref_all
#undef dec_ctx_mutex_l
#undef dec_l
#undef dec_set_filter
#undef dec_flush
#undef draining_state_handle
#undef draining_state_switch
#undef fatal
#undef ff_fmt2pcm_layout_best_effort
#undef filt_cfg
#undef filt_p
#undef filt_push_dec_set
#undef init_once_local
#undef init_once_public
#undef init_pcm_once_public
#undef kernel_ts_types_str_l
#undef pcm_chmaps2ff_chans_layout
#undef pcm_cfg_hw_core_best_effort
#undef pcm_filt_frs_write
#undef pcm_hw_access_decide
#undef pcm_hw_access_decide_x
#undef pcm_hw_buf_sz_cfg
#undef pcm_hw_chans_n_decide
#undef pcm_hw_fmt_decide
#undef pcm_hw_fmt_decide_x
#undef pcm_hw_rate_decide
#undef pcm_layout2ff_fmt_strict
#undef pcm_name_l
#undef pcm_pout_l
#undef pcm_perr_l
#undef pcm_silence_bufs_l
#undef pcm_silence_frs_write
#undef pcm2ff_strict
#undef pout
#undef warning
#endif
