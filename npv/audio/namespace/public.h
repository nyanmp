#ifndef CLEANUP
#define dec_ctx_cfg			npv_audio_dec_ctx_cfg
#define dec_ctx_lock			npv_audio_dec_ctx_lock
#define dec_ctx_p			npv_audio_dec_ctx_p
#define dec_ctx_unlock			npv_audio_dec_ctx_unlock
#define dec_flush			npv_audio_dec_flush
#define dec_sets_lock			npv_audio_dec_sets_lock
#define dec_sets_p			npv_audio_dec_sets_p
#define dec_sets_unlock			npv_audio_dec_sets_unlock
#define dec_sets_receive_avail		npv_audio_dec_sets_receive_avail
#define dec_set_try_receive		npv_audio_dec_set_try_receive
#define draining_p			npv_audio_draining_p
#define draining_state_evt		npv_audio_draining_state_evt
#define draining_timer_fd_p		npv_audio_draining_timer_fd_p
#define evt_pcm_write			npv_audio_evt_pcm_write
#define init_once			npv_audio_init_once
#define pcm_cfg_epoll			npv_audio_pcm_cfg_epoll
#define pcm_cfg_hw_best_effort		npv_audio_pcm_cfg_hw_best_effort
#define pcm_cfg_sw			npv_audio_pcm_cfg_sw
#define pcm_p				npv_audio_pcm_p
#define pcm_pollfds_p			npv_audio_pcm_pollfds_p
#define pcm_pollfds_n_p			npv_audio_pcm_pollfds_n_p
#define pcm_pollfds_n_max		npv_audio_pcm_pollfds_n_max
#define pcm_silence_bufs_cfg		npv_audio_pcm_silence_bufs_cfg
#define pkt_q_p				npv_audio_pkt_q_p
#define pkts_send			npv_audio_pkts_send
#define prepare				npv_audio_prepare
#define selected_ts_type_p		npv_audio_selected_ts_type_p
#define st_p				npv_audio_st_p
/*============================================================================*/
#else
#undef dec_ctx_cfg
#undef dec_ctx_lock
#undef dec_ctx_p
#undef dec_ctx_unlock
#undef dec_flush
#undef dec_sets_lock
#undef dec_sets_p
#undef dec_sets_unlock
#undef dec_sets_receive_avail
#undef dec_set_try_receive
#undef draining_p
#undef draining_state_evt
#undef draining_timer_fd_p
#undef evt_pcm_write
#undef init_once
#undef pcm_cfg_epoll
#undef pcm_cfg_hw_best_effort
#undef pcm_cfg_sw
#undef pcm_p
#undef pcm_pollfds_p
#undef pcm_pollfds_n_p
#undef pcm_pollfds_n_max
#undef pcm_silence_bufs_cfg
#undef pkt_q_p
#undef pkts_send
#undef prepare
#undef selected_ts_type
#undef st_p
#endif
