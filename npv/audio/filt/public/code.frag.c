#define AGAIN				0
#define PUSHED_ONE_SET			1
#define NO_DEC_SET			2
#define FILT_SWITCHED_TO_DRAINING	3
#define FILT_RECFG_REQUIRED		4
STATIC u8 filt_push_dec_set(AVChannelLayout *new_chans_layout,
			int *new_rate, enum avutil_audio_fr_fmt_t *new_fmt)
{
	u8 r8;
	int ri;
	avutil_audio_set_ref_t **a;

	npv_audio_dec_sets_lock();
	if (npv_audio_dec_sets_p.n == 0) {
		if (npv_audio_dec_sets_p.eof_receive) {
			ri = avfilter_bufsrc_add_audio_set_flags(abufsrc_l.ctx,
					0, AVFILTER_BUFSRC_FLAG_KEEP_REF);
			if (ri < 0) 
				fatal("ffmpeg:unable to notify the end of data to the filter source audio buffer context\n");
			pout("ffmpeg:interactive filter switched to draining\n");
			r8 = FILT_SWITCHED_TO_DRAINING;
			goto unlock;
		}
		r8 = NO_DEC_SET;
		goto unlock;
	}
	a = npv_audio_dec_sets_p.a;
	/*
	 * XXX: this is were dynamic audio recfg is triggered and we get out of
	 * the critical section asap
	 */
	if (is_recfg_required(a[0])) {
		/* because we are in a critical section */
		ri = av_channel_layout_copy(new_chans_layout, &a[0]->ch_layout);
		if (ri < 0)
			fatal("ffmpeg:unable to copy the channel layout of the set of frames as a new channel layout\n");
		*new_rate = a[0]->rate;
		*new_fmt = a[0]->fmt;
		r8 = FILT_RECFG_REQUIRED;
		goto unlock;
	}
	/* the dec_sets_p bufs will be unref in avcodec_audio_receive_set */
	ri = avfilter_bufsrc_add_audio_set_flags(abufsrc_l.ctx, a[0],
						AVFILTER_BUFSRC_FLAG_KEEP_REF);
	if (ri >= 0) {
		/* rotate the ptrs if needed */
		if (npv_audio_dec_sets_p.n > 1) {
			avutil_audio_set_ref_t *save;

			save = a[0];
			avutil_audio_set_unref(save);
			memmove(&a[0], &a[1], sizeof(*a)
						* (npv_audio_dec_sets_p.n_max - 1));
			a[npv_audio_dec_sets_p.n_max - 1] = save;
		}
		npv_audio_dec_sets_p.n--;
		r8 = PUSHED_ONE_SET;
		goto unlock;
	} else if (ri == AVERROR(EAGAIN)) {
		r8 = AGAIN;
		goto unlock;
	}
	fatal("ffmpeg:unable to submit a decoder set of frames to the filter source audio buffer context\n");
unlock:
	npv_audio_dec_sets_unlock();
	return r8;
}
#undef AGAIN
#undef PUSHED_ONE_SET
#undef NO_DEC_SET
#undef PUSHED_NULL_SET
#undef FILT_SWITCHED_TO_DRAINING
#undef FILT_RECFG_REQUIRED
#define HAVE_FILT_SET	1
#define EOF_FILT	2
STATIC u8 filt_set_get(void)
{
	int r;
	/*
	 * the last dec set should switch the filt in draining mode, and
	 * filt_p.set won't matter.
	 */
	r = avfilter_bufsink_get_audio_set(filt_p.abufsink_ctx, filt_p.set);
	if (r >= 0) {
		filt_p.pcm_written_ufrs_n = 0;
		return HAVE_FILT_SET;
	} else if (r == AVUTIL_AVERROR_EOF) {
		return EOF_FILT;
	}
	fatal("ffmpeg:error while getting frames from the filter\n");
}
#undef HAVE_FILT_SET
#undef EOF_FILT
#define DONT_PRINT_INFO false
STATIC void filt_flush(void)
{
	avutil_audio_set_unref(filt_p.set);
	filt_p.pcm_written_ufrs_n = 0;
}
#undef DONT_PRINT_INFO
STATIC void init_once(double initial_vol)
{
	init_once_local();
	init_once_public(initial_vol);
}
STATIC void cfg(avutil_rational_t tb,
		AVChannelLayout *src_chans_layout, int src_rate,
		enum avutil_audio_fr_fmt_t src_fmt,
		bool muted, double vol,
		AVChannelLayout *dst_chans_layout, int dst_rate,
		enum avutil_audio_fr_fmt_t dst_fmt, 
		bool print_info)
{
	int r;
	char *dump_str;

	avfilter_graph_free(&graph_l);

	graph_l = avfilter_graph_alloc();
	if (graph_l == 0)
		fatal("unable to create filter graph\n");
	abufsrc_cfg(tb, src_chans_layout, src_rate, src_fmt, print_info);
	/*--------------------------------------------------------------------*/
	r = av_channel_layout_copy(&abufsrc_l.key.chans_layout,
							src_chans_layout);
	if (r < 0)
		fatal("unable to copy the source channel layout to the key\n");
	abufsrc_l.key.rate = src_rate;
	abufsrc_l.key.fmt = src_fmt;
	/*--------------------------------------------------------------------*/
	vol_cfg(muted, vol);
	afmt_cfg(dst_chans_layout, dst_rate, dst_fmt, print_info);
	abufsink_cfg();
	r = avfilter_link(abufsrc_l.ctx, 0, vol_ctx_l, 0);
	if (r < 0)
		fatal("unable to connect the audio buffer source filter to the volume filter\n");
	r = avfilter_link(vol_ctx_l, 0, afmt_ctx_l, 0);
	if (r < 0)
		fatal("unable to connect the volume filter to the audio format filter\n");
        r = avfilter_link(afmt_ctx_l, 0, filt_p.abufsink_ctx, 0);
	if (r < 0)
		fatal("unable to connect the audio format filter to the audio buffer sink filter\n");
	r = avfilter_graph_config(graph_l, 0);
	if (r < 0)
		fatal("unable to configure the filter graph\n");
	if (!print_info)
		return;
	dump_str = avfilter_graph_dump(graph_l, 0);
	if (dump_str == 0) {
		warning("unable to get a filter graph description\n");
		return;
	}
	pout("GRAPH START-------------------------------------------------------\n");
	npv_pout("%s", dump_str);
	avutil_free(dump_str);
	pout("GRAPH END---------------------------------------------------------\n");
}
STATIC void npv_audio_filt_cmd_mute(void)
{
	int r;
	u8 vol_l10n_str[sizeof("xxx.xx")]; /* should be overkill */
	u8 resp[STR_SZ];

	if (filt_p.muted) {
		pout("COMMAND:unmuting\n");

		snprintf(vol_l10n_str, sizeof(vol_l10n_str), "%f", filt_p.vol);
		r = avfilter_graph_send_cmd(graph_l, "vol", "volume",
					vol_l10n_str, resp, sizeof(resp), 0);
		if (r < 0) {
			warning("ffmpeg:volume context:unable to mute the volume to 0:response from volume filter:%s\n", resp);
		}
		filt_p.muted = false;
	} else {
		pout("COMMAND:muting\n");

		r = avfilter_graph_send_cmd(graph_l, "vol", "volume",
				double_zero_l10n_str_l, resp, sizeof(resp), 0);
		if (r < 0) {
			warning("ffmpeg:volume context:unable to mute the volume to 0:response from volume filter:%s\n", resp);
		}
		filt_p.muted = true;
	}
}
STATIC void npv_audio_filt_cmd_vol_down(void)
{
	int r;
	u8 vol_l10n_str[sizeof("xxx.xx")]; /* should be overkill */
	u8 resp[STR_SZ];

	filt_p.vol -= VOL_DELTA;
	if (filt_p.vol < 0.)
		filt_p.vol = 0.;
	snprintf(vol_l10n_str, sizeof(vol_l10n_str), "%f", filt_p.vol);
	pout("COMMAND:volume down to value %s\n", vol_l10n_str);
	if (!filt_p.muted) {
		r = avfilter_graph_send_cmd(graph_l, "vol", "volume",
					vol_l10n_str, resp, sizeof(resp), 0);
		if (r < 0)
			warning("ffmpeg:volume context:unable to set the volume down to \"%s\":response from volume filter:\"%s\"\n", resp);
	}
}
STATIC void npv_audio_filt_cmd_vol_up(void)
{
	int r;
	u8 vol_l10n_str[sizeof("xxx.xx")]; /* should be overkill */
	u8 resp[STR_SZ];

	filt_p.vol += VOL_DELTA;
	if (filt_p.vol > 1.)
		filt_p.vol = 1.;
	snprintf(vol_l10n_str, sizeof(vol_l10n_str), "%f", filt_p.vol);
	pout("COMMAND:volume up to value %s\n", vol_l10n_str);
	if (!filt_p.muted) {
		r = avfilter_graph_send_cmd(graph_l, "vol", "volume",
					vol_l10n_str, resp, sizeof(resp), 0);
		if (r < 0)
			warning("ffmpeg:volume context:unable to set the volume up to \"%s\":response from volume filter:\"%s\"\n", resp);
	}
}
STATIC void npv_audio_filt_cmd_vol_down_small(void)
{
	int r;
	u8 vol_l10n_str[sizeof("xxx.xx")]; /* should be overkill */
	u8 resp[STR_SZ];

	filt_p.vol -= VOL_DELTA_SMALL;
	if (filt_p.vol < 0.)
		filt_p.vol = 0.;
	snprintf(vol_l10n_str, sizeof(vol_l10n_str), "%f", filt_p.vol);
	pout("COMMAND:volume down to value %s\n", vol_l10n_str);
	if (!filt_p.muted) {
		r = avfilter_graph_send_cmd(graph_l, "vol", "volume",
					vol_l10n_str, resp, sizeof(resp), 0);
		if (r < 0)
			warning("ffmpeg:volume context:unable to set the volume down to \"%s\":response from volume filter:\"%s\"\n", resp);
	}
}
STATIC void npv_audio_filt_cmd_vol_up_small(void)
{
	int r;
	u8 vol_l10n_str[sizeof("xxx.xx")]; /* should be overkill */
	u8 resp[STR_SZ];

	filt_p.vol += VOL_DELTA_SMALL;
	if (filt_p.vol > 1.)
		filt_p.vol = 1.;
	snprintf(vol_l10n_str, sizeof(vol_l10n_str), "%f", filt_p.vol);
	pout("COMMAND:volume up to value %s\n", vol_l10n_str);
	if (!filt_p.muted) {
		r = avfilter_graph_send_cmd(graph_l, "vol", "volume",
					vol_l10n_str, resp, sizeof(resp), 0);
		if (r < 0)
			warning("ffmpeg:volume context:unable to set the volume up to \"%s\":response from volume filter:\"%s\"\n", resp);
	}
}
