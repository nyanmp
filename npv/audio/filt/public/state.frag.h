/*
 * this filt is for processing which must stay interactive. we are lazy:
 * resampling is made interactive instead of being in a non-interactive filt.
 * this is the public part
 */
STATIC struct {
	avutil_audio_set_ref_t *set;
	snd_pcm_ufrs_t pcm_written_ufrs_n;

	double vol;
	bool muted;
	/* we will need to query some output props from the filt */
	avfilter_filt_ctx_t *abufsink_ctx;
} filt_p;
