#ifndef NPV_AUDIO_FILT_PUBLIC_H
#define NPV_AUDIO_FILT_PUBLIC_H
/*
 * code protected with a GNU affero GPLv3 license 
 * copyright (C) 2020 Sylvain BERTRAND
 */
#include <stdbool.h>
#include <libavutil/frame.h>
#include <libavutil/samplefmt.h>
#include <libavfilter/avfilter.h>
#include <alsa/asoundlib.h>
#include "npv/c_fixing.h"
#include "npv/global.h"
/*---------------------------------------------------------------------------*/
#include "npv/namespace/ffmpeg.h"
#include "npv/audio/namespace/ffmpeg.h"
#include "npv/namespace/alsa.h"
#include "npv/audio/filt/namespace/public.h"
/*---------------------------------------------------------------------------*/
#include "npv/audio/filt/public/state.frag.h"
/*---------------------------------------------------------------------------*/
STATIC void init_once(double initial_vol);
STATIC void cfg(avutil_rational_t tb,
		AVChannelLayout *src_chans_layout, int src_rate,
		enum avutil_audio_fr_fmt_t src_fmt, 
		bool muted, double vol,
		AVChannelLayout *dst_chans_layout, int dst_rate,
		enum avutil_audio_fr_fmt_t dst_fmt,
		bool print_info);
STATIC u8 filt_push_dec_set(AVChannelLayout *new_chans_layout,
			int *new_rate, enum avutil_audio_fr_fmt_t *new_fmt);
STATIC u8 filt_set_get(void);
STATIC void filt_flush(void);
/* out of audio filt namespace to avoid collision with npv_cmd_* prefix */
STATIC void npv_audio_filt_cmd_mute(void);
STATIC void npv_audio_filt_cmd_vol_up(void);
STATIC void npv_audio_filt_cmd_vol_down(void);
STATIC void npv_audio_filt_cmd_vol_up_small(void);
STATIC void npv_audio_filt_cmd_vol_down_small(void);
/*----------------------------------------------------------------------------*/
#define CLEANUP
#include "npv/namespace/ffmpeg.h"
#include "npv/audio/namespace/ffmpeg.h"
#include "npv/namespace/alsa.h"
#include "npv/audio/filt/namespace/public.h"
#undef CLEANUP
#endif
