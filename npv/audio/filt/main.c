#ifndef NPV_AUDIO_FILT_MAIN_C
#define NPV_AUDIO_FILT_MAIN_C
/*
 * code protected with a GNU affero GPLv3 license 
 * copyright (C) 2020 Sylvain BERTRAND
 */
#include <stdbool.h>
#include <stdio.h>
#include <pthread.h>
#include <stdarg.h>
#include <libavfilter/avfilter.h>
#include <libavfilter/buffersrc.h>
#include <libavfilter/buffersink.h>
#include <libavutil/opt.h>
#include <libavutil/channel_layout.h>
#include "npv/c_fixing.h"
/*----------------------------------------------------------------------------*/
#include "npv/config.h"
/*----------------------------------------------------------------------------*/
#include "npv/global.h"
#include "npv/public.h"
#include "npv/audio/public.h"
#include "npv/audio/filt/public.h"
/*----------------------------------------------------------------------------*/
#include "npv/namespace/ffmpeg.h"
#include "npv/audio/namespace/ffmpeg.h"
#include "npv/audio/filt/namespace/public.h"
#include "npv/audio/filt/namespace/main.c"
/*----------------------------------------------------------------------------*/
#include "npv/audio/filt/local/state.frag.c"
#include "npv/audio/filt/local/code.frag.c"
/*----------------------------------------------------------------------------*/
#include "npv/audio/filt/public/code.frag.c"
/*----------------------------------------------------------------------------*/
#define CLEANUP
#include "npv/namespace/ffmpeg.h"
#include "npv/audio/namespace/ffmpeg.h"
#include "npv/audio/filt/namespace/public.h"
#include "npv/audio/filt/namespace/main.c"
#undef CLEANUP
#endif
