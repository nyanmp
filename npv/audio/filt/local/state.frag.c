STATIC avfilter_filt_graph_t *graph_l;
STATIC struct {
	const avfilter_filt_t *this;
	avfilter_filt_ctx_t *ctx;
	/* key to detect required recfg */
	struct {
		AVChannelLayout chans_layout;
		int rate;
		enum avutil_audio_fr_fmt_t fmt;
	} key;
} abufsrc_l;
STATIC avfilter_filt_ctx_t *vol_ctx_l;
STATIC const avfilter_filt_t *vol_l;
STATIC u8 double_zero_l10n_str_l[sizeof("xxx.xx")];
STATIC avfilter_filt_ctx_t *afmt_ctx_l;
STATIC const avfilter_filt_t *afmt_l;
STATIC const avfilter_filt_t *abufsink_l;
