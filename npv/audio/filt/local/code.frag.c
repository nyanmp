STATIC void fatal(u8 *fmt, ...)
{
	va_list ap;

	npv_perr("audio:filter:");
	va_start(ap, fmt);
	npv_vfatal(fmt, ap);
	va_end(ap);
}
STATIC void warning(u8 *fmt, ...)
{
	va_list ap;

	npv_perr("audio:filter:");
	va_start(ap, fmt);
	npv_vwarning(fmt, ap);
	va_end(ap);
}
STATIC void pout(u8 *fmt, ...)
{
	va_list ap;

	npv_pout("audio:filter:");
	va_start(ap, fmt);
	npv_vpout(fmt, ap);
	va_end(ap);
}
STATIC bool is_recfg_required(avutil_audio_set_ref_t *src_set)
{
	
	if (av_channel_layout_compare(&src_set->ch_layout,
					&abufsrc_l.key.chans_layout) != 0
	     ||	src_set->rate		!= abufsrc_l.key.rate
	     ||	src_set->fmt		!= abufsrc_l.key.fmt)
		return true;
	return false;
}
STATIC void abufsrc_cfg(avutil_rational_t tb, AVChannelLayout *chans_layout,
		int rate, enum avutil_audio_fr_fmt_t fmt, bool print_info)
{
	int r;
	u8 chans_layout_str[STR_SZ]; /* should be overkill */

	abufsrc_l.this = avfilter_get_by_name("abuffer");
	if (abufsrc_l.this == 0)
		fatal("audio buffer source:could not find the filter\n");
	abufsrc_l.ctx = avfilter_graph_alloc_filt(graph_l, abufsrc_l.this,
								"src_abuf");
	if (abufsrc_l.ctx == 0)
		fatal("audio buffer source context:could not allocate the instance in the filter graph\n");
	r = avutil_opt_set(abufsrc_l.ctx, "sample_fmt",
					avutil_get_audio_fr_fmt_name(fmt),
						AVUTIL_OPT_SEARCH_CHILDREN);
	if (r < 0)
		fatal("audio buffer source context:unable to set the decoder frame format option\n");
	r = avutil_opt_set_int(abufsrc_l.ctx, "sample_rate",
					rate, AVUTIL_OPT_SEARCH_CHILDREN);
	if (r < 0)
		fatal("audio buffer source context:unable to set the decoder rate option\n");
	av_channel_layout_describe(chans_layout, chans_layout_str,
						sizeof(chans_layout_str));
	if (print_info)
		pout("audio buffer source context:using channels layout \"%s\"\n", chans_layout_str);
	r = avutil_opt_set(abufsrc_l.ctx, "channel_layout", chans_layout_str,
						AVUTIL_OPT_SEARCH_CHILDREN);
	if (r < 0)
		fatal("audio buffer source context:unable to set the decoder channel layout option\n");
	r = avutil_opt_set_q(abufsrc_l.ctx, "time_base", tb, 
						AVUTIL_OPT_SEARCH_CHILDREN);
	if (r < 0)
		fatal("audio buffer source context:unable to set the time base option\n");
	r = avfilter_init_str(abufsrc_l.ctx, 0);
	if (r < 0)
		fatal("audio buffer source context:unable to initialize\n");
}
STATIC void vol_cfg(bool muted, double vol_cfg)
{
	double vol;
	u8 vol_l10n_str[sizeof("xxx.xx")]; /* should be overkill */
	int r;

	vol_l = avfilter_get_by_name("volume");
	if (vol_l == 0)
        	fatal("volume:could not find the filter\n");
	vol_ctx_l = avfilter_graph_alloc_filt(graph_l, vol_l, "vol");
	if (vol_ctx_l == 0)
		fatal("volume context:could not allocate the instance in the filter graph\n");
	if (muted)
		vol = 0.0;
	else
		vol = vol_cfg;
	/* yeah the radix is localized, can be '.', ','... */
	snprintf(vol_l10n_str, sizeof(vol_l10n_str), "%f", vol);
	r = avutil_opt_set(vol_ctx_l, "volume", vol_l10n_str,
						AVUTIL_OPT_SEARCH_CHILDREN);
	if (r < 0)
		fatal("volume context:unable to set the volume option\n");
	r = avfilter_init_str(vol_ctx_l, 0);
	if (r < 0)
		fatal("volume buffer context:unable to initialize\n");
}
STATIC void afmt_cfg(AVChannelLayout *chans_layout, int rate, 
				enum avutil_audio_fr_fmt_t fmt, bool print_info)
{
	int r;
	u8 rate_str[sizeof("dddddd")];
	u8 chans_layout_str[STR_SZ]; /* should be overkill */

	afmt_l = avfilter_get_by_name("aformat");
	if (afmt_l == 0)
		fatal("audio format:could not find the filter");
	afmt_ctx_l = avfilter_graph_alloc_filt(graph_l, afmt_l, "afmt");
	if (afmt_ctx_l == 0)
		fatal("audio format:could not allocate the instance in the filter graph\n");
	r = avutil_opt_set(afmt_ctx_l, "sample_fmts",
		avutil_get_audio_fr_fmt_name(fmt), AVUTIL_OPT_SEARCH_CHILDREN);
	if (r < 0)
		fatal("audio format context:could to set the pcm sample format\n");
	snprintf(rate_str, sizeof(rate_str), "%d", rate);
	r = avutil_opt_set(afmt_ctx_l, "sample_rates", rate_str,
						AVUTIL_OPT_SEARCH_CHILDREN);
	if (r < 0)
		fatal("audio format context:could not set the pcm rate\n");
	av_channel_layout_describe(chans_layout, chans_layout_str,
						sizeof(chans_layout_str));
	r = avutil_opt_set(afmt_ctx_l, "channel_layouts", chans_layout_str,
						AVUTIL_OPT_SEARCH_CHILDREN);
	if (r < 0)
		fatal("audio format context:could not set the layout of channels\n");
	if (print_info)
		pout("audio format context:channel layout is \"%s\"\n", chans_layout_str);
	r = avfilter_init_str(afmt_ctx_l, 0);
	if (r < 0)
		fatal("audio format context:unable to initialize\n");
}
STATIC void abufsink_cfg(void)
{
	int r;

	abufsink_l = avfilter_get_by_name("abuffersink");
	if (abufsink_l == 0)
		fatal("audio buffer sink:could not find the filter\n");
	filt_p.abufsink_ctx = avfilter_graph_alloc_filt(graph_l, abufsink_l,
								"sink_abuf");
	if (filt_p.abufsink_ctx == 0)
		fatal("audio buffer sink context:could not allocate the instance in the filter graph\n");
	r = avfilter_init_str(filt_p.abufsink_ctx, 0);
	if (r < 0)
		fatal("audio buffer sink context:unable to initialize\n");
}
STATIC void init_once_local(void)
{
	graph_l = 0;
	memset(&abufsrc_l, 0, sizeof(abufsrc_l));
	vol_ctx_l = 0;
	vol_l = 0;
	afmt_ctx_l = 0;
	afmt_l = 0;
	abufsink_l = 0;
	/* floating point strs are localized... erk... */
	snprintf(double_zero_l10n_str_l, sizeof(double_zero_l10n_str_l),
								"%f", 0.);
}
STATIC void init_once_public(double initial_vol)
{
	filt_p.set = avutil_audio_set_ref_alloc();
	if (filt_p.set == 0)
		fatal("ffmpeg:unable to allocate a reference on set of frames for the public part of the interactive latency filter\n");
	filt_p.pcm_written_ufrs_n = 0;
	filt_p.vol = initial_vol;
	filt_p.muted = false;
	filt_p.abufsink_ctx = 0;
}
