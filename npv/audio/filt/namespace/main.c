#ifndef CLEANUP
#define abufsink_cfg		npv_audio_filt_abufsink_cfg
#define abufsink_l		npv_audio_filt_abufsink_l
#define abufsrc_cfg		npv_audio_filt_abufsrc_cfg
#define abufsrc_l		npv_audio_filt_abufsrc_l
#define afmt_cfg		npv_audio_filt_afm_cfg
#define afmt_ctx_l		npv_audio_filt_afmt_ctx_l
#define afmt_l			npv_audio_filt_afmt_l
#define double_zero_l10n_str_l	npv_audio_filt_double_zero_l10n_str_l
#define fatal			npv_audio_filt_fatal
#define graph_l			npv_audio_filt_graph_l
#define init_once_local		npv_audio_filt_init_once_local
#define init_once_public	npv_audio_filt_init_once_public
#define is_recfg_required	npv_audio_filt_is_recfg_required
#define pout			npv_audio_filt_pout
#define vol_cfg			npv_audio_filt_vol_cfg
#define vol_ctx_l		npv_audio_filt_vol_ctx_l
#define vol_l			npv_audio_filt_vol_l
#define warning			npv_audio_filt_warning
/*----------------------------------------------------------------------------*/
/* some member names */
#define fmt			format
#define rate			sample_rate
/*============================================================================*/
#else
#undef abufsink_cfg
#undef abufsink_l
#undef abufsrc_cfg
#undef abufsrc_l
#undef afmt_cfg
#undef afmt_ctx_l
#undef afmt_l
#undef double_zero_l10n_str_l
#undef fatal
#undef graph_l
#undef init_once_local
#undef init_once_public
#undef is_recfg_required
#undef pout
#undef vol_cfg
#undef vol_ctx_l
#undef vol_l
#undef warning
/*----------------------------------------------------------------------------*/
#undef rate
#undef fmt
#endif
