#ifndef CLEANUP
#define cfg			npv_audio_filt_cfg
#define filt_flush		npv_audio_filt_flush
#define filt_p			npv_audio_filt_p
#define filt_push_dec_set	npv_audio_filt_push_dec_set
#define filt_set_get		npv_audio_filt_set_get
#define init_once		npv_audio_filt_init_once
/*============================================================================*/
#else
#undef cfg
#undef filt_flush
#undef filt_p
#undef filt_push_dec_set
#undef filt_set_get
#undef init_once
#endif
