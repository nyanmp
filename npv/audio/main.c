#ifndef NPV_AUDIO_MAIN_C
#define NPV_AUDIO_MAIN_C
/*
 * code protected with a GNU affero GPLv3 license 
 * copyright (C) 2020 Sylvain BERTRAND
 */
#include <stdbool.h>
#include <stdint.h>
#include <stdarg.h>
#include <string.h>
#include <stdio.h>
#include <unistd.h>
#include <signal.h>
#include <pthread.h>
#include <sys/timerfd.h>
#include <sys/epoll.h>
#include <libavcodec/avcodec.h>
#include <libavutil/avutil.h>
#include <libavutil/channel_layout.h>
#include <libavutil/error.h>
#include <libavutil/frame.h>
#include <libavutil/rational.h>
#include <libavutil/samplefmt.h>
#include <libavutil/opt.h>
#include <alsa/asoundlib.h>
#include "npv/c_fixing.h"
#include "npv/global.h"
#include "npv/public.h"
#include "npv/pkt_q/public.h"
#include "npv/fmt/public.h"
#include "npv/audio/public.h"
#include "npv/audio/filt/public.h"
#include "npv/video/public.h"
#include "npv/clk/public.h"
/*----------------------------------------------------------------------------*/
#include "npv/config.h"
/*----------------------------------------------------------------------------*/
#include "npv/namespace/ffmpeg.h"
#include "npv/audio/namespace/ffmpeg.h"
#include "npv/namespace/alsa.h"
#include "npv/audio/namespace/public.h"
#include "npv/audio/namespace/main.c"
/*----------------------------------------------------------------------------*/
/*
 * XXX: we don't know how the alsa silence machinery works, then we use brutal
 * silence bufs
 */
/*----------------------------------------------------------------------------*/
#define DRAINING_TIMER_INTERVAL_NSECS_N 100000000 /* 0.1 sec */
/*----------------------------------------------------------------------------*/
#include "npv/audio/local/state.frag.c"
/*----------------------------------------------------------------------------*/
#include "npv/audio/local/code.frag.c"
#include "npv/audio/public/code.frag.c"
/*----------------------------------------------------------------------------*/
#undef DRAINING_TIMER_INTERVAL_NSECS_N
/*---------------------------------------------------------------------------*/
#define CLEANUP
#include "npv/namespace/ffmpeg.h"
#include "npv/audio/namespace/ffmpeg.h"
#include "npv/namespace/alsa.h"
#include "npv/audio/namespace/public.h"
#include "npv/audio/namespace/main.c"
#undef CLEANUP
#endif
