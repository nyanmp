STATIC avcodec_codec_ctx_t *dec_ctx_p;
STATIC struct {
	pthread_mutex_t mutex;

	bool eof_receive;	/* "receiving" from the dec returned eof */
	u32 n_max;
	u32 n;
	avutil_audio_set_ref_t **a;
} dec_sets_p;
STATIC struct npv_pkt_q_t *pkt_q_p;
/*
 * we copy some stream data in the case the stream does vanish or is replaced
 * (don't know how ffmpeg does handle this)
 */
STATIC struct {
	int idx;
	int id;
	avutil_rational_t tb;
	int64_t start_time;
} st_p;
/*----------------------------------------------------------------------------*/
/* alsa -- start */
STATIC snd_pcm_t *pcm_p;
constant_u32 {
	pcm_pollfds_n_max = 16 /* banzai */
};
STATIC bool draining_p;
STATIC int draining_timer_fd_p;
STATIC u8 pcm_pollfds_n_p;
STATIC struct pollfd pcm_pollfds_p[pcm_pollfds_n_max];
STATIC s8 selected_ts_type_p;
/* alsa -- end */
/*----------------------------------------------------------------------------*/
