STATIC void dec_ctx_cfg(avcodec_params_t *params)
{
	int r;

	dec_l = avcodec_find_dec(params->codec_id);
	if (dec_l == 0)
		fatal("ffmpeg:unable to find a proper decoder\n");
	avcodec_free_context(&dec_ctx_p);
	dec_ctx_p = avcodec_alloc_ctx(dec_l);
	if (dec_ctx_p == 0)
		fatal("ffmpeg:unable to allocate a decoder context\n");
	/* XXX: useless ? */
	r = avcodec_params_to_ctx(dec_ctx_p, params);
	if (r < 0)
		fatal("ffmpeg:unable to apply stream codec parameters in codec context\n");
	/* XXX: ffmpeg thread count default is 1, set to 0 = auto */
	dec_ctx_p->thread_count = 0;
	r = avcodec_open2(dec_ctx_p, dec_l, 0);
	if (r < 0)
		fatal("ffmpeg:unable to open the decoder context\n");
}
#define AGAIN		0
#define HAVE_DEC_SET	1
#define EOF_DEC		2
STATIC u8 dec_set_try_receive(void)
{
	int r;
	u32 last;

	if (dec_sets_p.eof_receive)
		return EOF_DEC;
	if (dec_sets_p.n == dec_sets_p.n_max)
		dec_a_grow();
	last = dec_sets_p.n;
	/* will unref any previous dec_sets_p.a[x] bufs for us */
	r = avcodec_receive_audio_set(dec_ctx_p, dec_sets_p.a[last]);
	if (r == AVUTIL_AVERROR(EAGAIN)) 
		return AGAIN;
	else if (r == 0) {
#ifdef NPV_DEBUG
		if (npv_debug_p)
			npv_perr("DEBUG:audio:received set pts=%ld\n", dec_sets_p.a[last]->pts);
#endif
		++dec_sets_p.n;
		return HAVE_DEC_SET;
	} else if (r == AVUTIL_AVERROR_EOF) {
		pout("ffmpeg:last decoder set of frames reached (receiving)\n");
		dec_sets_p.eof_receive = true;
		return EOF_DEC;
	}
	fatal("ffmpeg:error while receiving a set of frames from the decoder\n");
}
#undef AGAIN
#undef HAVE_DEC_SET
#undef EOF_DEC
#define AGAIN		0
#define HAVE_DEC_SET	1
#define EOF_DEC		2
/*
 * this can be long and we don't want to lock that long the q of audio frs
 * for the alsa writer, then do finer-grained locking here
 */
STATIC void dec_sets_receive_avail(void) { loop
{
	u8 r;

	dec_ctx_lock();
	dec_sets_lock();
	r = dec_set_try_receive();
	dec_sets_unlock();
	dec_ctx_unlock();
	if (r == HAVE_DEC_SET)
		continue;
	else if (r == AGAIN || r == EOF_DEC)
		break;
}}
#undef AGAIN
#undef HAVE_DEC_SET
#undef EOF_DEC
STATIC void draining_state_evt(void)
{
	int r;
	uint64_t exps_n;

	r = read(draining_timer_fd_p, &exps_n, sizeof(exps_n));
	if (r == -1)
		fatal("unable to read the number of draining state timer expirations\n");
	draining_state_handle();
}
#define AGAIN			0
#define RECOVERED		1
#define INCOMPLETE_WRITE	1
#define NONE 0
STATIC void evt_pcm_write(void)
{
	u8 pre_x;
	snd_pcm_sfrs_t alsa_r;
	snd_pcm_ufrs_t ufrs_n;
	u8 r;

	pre_x = atomic_load(&npv_pre_x_p);
	if (pre_x != NONE) /* TODO: play silence here */
		return;
	alsa_r = snd_pcm_avail(pcm_p);
	r = alsa_recover(alsa_r);
	if (r == AGAIN || r == RECOVERED)
		return;
	/* r == CONTINUE */
	ufrs_n = (snd_pcm_ufrs_t)alsa_r;
	if (npv_paused_p) {
		pcm_silence_frs_write(ufrs_n);
		return;
	}
	r = pcm_filt_frs_write(&ufrs_n);
	if (r == INCOMPLETE_WRITE)
		pcm_silence_frs_write(ufrs_n);
	/* DRAINING | ALL_FRS_WRITTEN | RECOVERED */
}
#undef AGAIN
#undef RECOVERED
#undef INCOMPLETE_WRITE
#undef NONE
STATIC void pcm_silence_bufs_cfg(snd_pcm_t *pcm, bool print_info)
{
	int r;
	snd_pcm_hw_params_t *hw_params;
	snd_pcm_ufrs_t buf_ufrs_n;
	snd_pcm_fmt_t fmt;
	snd_pcm_access_t access;
	unsigned int chans_n;
	u8 c;

	r = snd_pcm_hw_params_malloc(&hw_params);
	if (r < 0)
		fatal("silence:alsa:unable to allocate memory for a hardware parameters container\n");
	r = snd_pcm_hw_params_current(pcm, hw_params);
	if (r != 0)
		fatal("silence:alsa:unable to get the pcm hardware parameters\n");
	r = snd_pcm_hw_params_get_buf_sz(hw_params, &buf_ufrs_n);
	if (r < 0)
		fatal("silence:alsa:unable to get the number of frames in the pcm buffer\n");
	r = snd_pcm_hw_params_get_format(hw_params, &fmt);
	if (r < 0)
		fatal("silence:alsa:unable to get the pcm format\n");
	r = snd_pcm_hw_params_get_access(hw_params, &access);
	if (r < 0)
		fatal("silence:alsa:unable to get the pcm access mode\n");
	r = snd_pcm_hw_params_get_channels(hw_params, &chans_n);
	if (r < 0)
		fatal("silence:alsa:unable to get the pcm number of channels\n");

	/* wipe silence bufs first */
	c = 0;
	loop {
		if (c == AVUTIL_DATA_PTRS_N)
			break;
		if (pcm_silence_bufs_l[c] != 0) {
			free(pcm_silence_bufs_l[c]);
			pcm_silence_bufs_l[c] = 0;
		}
		++c;
	}
	if (access == SND_PCM_ACCESS_RW_INTERLEAVED
				|| access == SND_PCM_ACCESS_MMAP_INTERLEAVED) {
		ssize_t buf_bytes_n;

		buf_bytes_n = snd_pcm_frames_to_bytes(pcm,
						(snd_pcm_sframes_t)buf_ufrs_n);
		if (buf_bytes_n <= 0)
			fatal("silence:alsa:interleaved:unable to get the pcm number of bytes of all buffer frames\n");
		pcm_silence_bufs_l[0] = malloc((size_t)buf_bytes_n);
		if (pcm_silence_bufs_l[0] == 0)
			fatal("silence:interleaved:unable to allocate the silence buffer of %d bytes\n", (int)buf_bytes_n);
		if (print_info)
			pout("silence:interleaved:buffer of %d bytes is allocated\n", (int)buf_bytes_n);
		r = snd_pcm_format_set_silence(fmt, pcm_silence_bufs_l[0],
						(unsigned int)buf_ufrs_n);
		if (r < 0)
			fatal("silence:interleaved:unable to fill with silence the buffer\n");
		pout("silence:interleaved:silence buffer filled with %u silence frames\n", buf_ufrs_n);
	} else if (access == SND_PCM_ACCESS_RW_NONINTERLEAVED
			 || access == SND_PCM_ACCESS_MMAP_NONINTERLEAVED) {
		ssize_t buf_bytes_n;
		long buf_samples_n;

		buf_samples_n = (long)buf_ufrs_n;
		buf_bytes_n = snd_pcm_samples_to_bytes(pcm, buf_samples_n);
		if (buf_bytes_n <= 0)
			fatal("silence:alsa:non interleaved:unable to get the pcm number of total bytes of all buffer samples\n");
		c = 0;
		loop {
			if (c == chans_n)
				break;
			pcm_silence_bufs_l[c] = malloc((size_t)buf_bytes_n);
			if (pcm_silence_bufs_l[c] == 0)
				fatal("silence:non interleaved:unable to allocate silence buffer %u of %d bytes\n", c, (int)buf_bytes_n);
			r = snd_pcm_format_set_silence(fmt,
						pcm_silence_bufs_l[c],
						(unsigned int)buf_samples_n);
			if (r < 0)
				fatal("silence:non interleaved:unable to fill with silence the buffer\n");
			if (print_info)
				pout("silence:non interleaved:buffer[%u] of %d bytes is allocated\n", c, (int)buf_bytes_n);
			++c;
		}
		if (print_info)
			pout("silence:non interleaved:allocated %u silence buffers for %u frames\n", chans_n, (unsigned int)buf_ufrs_n);
	} else
		fatal("silence:the pcm access type is not supported\n");
	snd_pcm_hw_params_free(hw_params);
}
STATIC void init_once(u8 *pcm_str)
{
	init_once_local();
	init_once_public(pcm_str);
}
STATIC void dec_flush(void)
{
	npv_pkt_q_unref_all(pkt_q_p);
	dec_a_unref_all();
	dec_sets_p.eof_receive = false;
	avcodec_flush_bufs(dec_ctx_p);
}
STATIC void dec_ctx_lock(void)
{
	int r;

	r = pthread_mutex_lock(&dec_ctx_mutex_l);
	if (r != 0)
		fatal("%d:unable to lock the decoder context\n", r);
}
STATIC void dec_ctx_unlock(void)
{
	int r;

	r = pthread_mutex_unlock(&dec_ctx_mutex_l);
	if (r != 0)
		fatal("%d:unable to unlock the decoder context\n", r);
}
STATIC void dec_sets_lock(void)
{
	int r;

	r = pthread_mutex_lock(&dec_sets_p.mutex);
	if (r != 0)
		fatal("%d:unable to lock the array of decoder sets\n", r);
}
STATIC void dec_sets_unlock(void)
{
	int r;

	r = pthread_mutex_unlock(&dec_sets_p.mutex);
	if (r != 0)
		fatal("%d:unable to unlock the array of decoder sets\n", r);
}
STATIC void pcm_cfg_hw_best_effort(snd_pcm_t *pcm, int chans_n, int rate,
					enum avutil_audio_fr_fmt_t ff_fmt)
{
	int r;
	s8 i;
	snd_pcm_access_t access;
	snd_pcm_hw_params_t *hw_params;

	pout("ALSA:HW_PARAMS START------------------------------------------------------------\n");
	r = snd_pcm_hw_params_malloc(&hw_params);
	if (r < 0)
		fatal("alsa:unable to allocate hardware parameters context\n");
	pcm_cfg_hw_core_best_effort(pcm, hw_params, chans_n, rate, ff_fmt);
	r = snd_pcm_hw_params(pcm, hw_params);
	if (r != 0)
		fatal("alsa:unable to install the hardware parameters\n");
	r = snd_pcm_hw_params_current(pcm, hw_params);
	if (r != 0)
		fatal("alsa:unable to get current hardware parameters\n");
	snd_pcm_hw_params_dump(hw_params, pcm_pout_l);
	i = 0;
	selected_ts_type_p = -1;
	loop {
		if (i == ARRAY_N(kernel_ts_types_str_l))
			break;
		r = snd_pcm_hw_params_supports_audio_ts_type(hw_params, i);
		if (r == 1) {
			selected_ts_type_p = i;
			pout("kernel audio timestamp type \"%s\" is supported for the current configuration\n", kernel_ts_types_str_l[i]);
		}
		++i;
	}
	/*
	 * we selected the most accurate, namely with the highest idx, audio ts
	 * type
	 */
	pout("%s will be used for the audio based clock\n", kernel_ts_types_str_l[selected_ts_type_p]);
	snd_pcm_hw_params_free(hw_params);
	pout("ALSA:HW_PARAMS END--------------------------------------------------------------\n");
}
STATIC void pcm_cfg_sw(snd_pcm_t *pcm)
{
	int r;
	snd_pcm_sw_params_t *sw_params;

	pout("ALSA:SW_PARAMS START------------------------------------------------------------\n");
	r = snd_pcm_sw_params_malloc(&sw_params);
	if (r != 0)
		fatal("alsa:unable to allocate software parameters structure\n");
	r = snd_pcm_sw_params_current(pcm, sw_params);
	if (r != 0)
		fatal("alsa:unable to get current software parameters\n");
	r = snd_pcm_sw_params_set_period_evt(pcm, sw_params, 1);
	if (r != 0)
		fatal("alsa:unable to enable period event\n");
	/* enable ts to be sure */
	r = snd_pcm_sw_params_set_tstamp_mode(pcm, sw_params,
							SND_PCM_TSTAMP_ENABLE);
	if (r < 0)
		fatal("unable to set timestamp mode:%s\n", snd_strerror(r));
	r = snd_pcm_sw_params(pcm, sw_params);
	if (r != 0)
		fatal("alsa:unable to install sotfware parameters\n");
	snd_pcm_sw_params_dump(sw_params, pcm_pout_l);
	snd_pcm_sw_params_free(sw_params);
	pout("ALSA:SW_PARAMS END--------------------------------------------------------------\n");
}
STATIC void npv_audio_pcm_cfg_epoll(snd_pcm_t *pcm)
{
	int r;

	r = snd_pcm_poll_descriptors_n(pcm);
	pout("alsa:have %d poll file descriptors\n", r);
	if ((r <= 0) || (r > pcm_pollfds_n_max))
		fatal("alsa:invalid count of alsa poll file descriptors\n");
	pcm_pollfds_n_p =(u8)r;
	memset(pcm_pollfds_p, 0, sizeof(pcm_pollfds_p));
	snd_pcm_poll_descriptors(pcm, pcm_pollfds_p, (unsigned int)pcm_pollfds_n_p);
}
/* we do per-loop fine-grained locking */
#define sz size
STATIC void pkts_send(void) { loop
{
	int r;
	avcodec_pkt_ref_t *pr;

	npv_pkt_q_lock(pkt_q_p);
	if (pkt_q_p->n == 0) 
		goto unlock_and_return;
	pr = pkt_q_p->q[0];
	dec_ctx_lock();
	r = avcodec_send_pkt(dec_ctx_p, pr);
	dec_ctx_unlock();
	if (r == AVERROR(EAGAIN)) /* dec is full and the pkt is rejected */
		goto unlock_and_return;
	else if (r == AVUTIL_AVERROR_EOF) /* the dec is in draining mode */
		goto unlock_and_return;
	else if (r != 0)
		fatal("error while sending a packet to the decoder\n");
	/* r == 0 */
	npv_pipeline_limits_lock();
	npv_pipeline_limits_p.pkts.bytes_n -= pr->sz;
	npv_pipeline_limits_unlock();

	npv_pkt_q_deq(pkt_q_p);
	avcodec_pkt_unref(pr);
	npv_pkt_q_unlock(pkt_q_p);
	continue;

unlock_and_return:
	npv_pkt_q_unlock(pkt_q_p);
	return;
}}
#undef sz
#define CHANS_N_NOT_OVERRIDDEN 0
#define RATE_NOT_OVERRIDDEN 0
#define AUDIO_FR_FMT_NOT_OVERRIDDEN AVUTIL_AUDIO_FR_FMT_NONE
#define PRINT_INFO true
STATIC void npv_audio_prepare(int override_initial_ff_chans_n, 
			int override_initial_ff_rate,
			enum avutil_audio_fr_fmt_t override_initial_ff_fmt)
{
	int dst_rate;
	enum avutil_audio_fr_fmt_t dst_fmt;
	int initial_ff_chans_n;
	int initial_ff_rate;
	enum avutil_audio_fr_fmt_t initial_ff_fmt;
	int r;

	/*
	 * do our best to match the pcm cfg to audio ff dec output OR the user
	 * overrides, BUT we don't expect to match it "exactly": a ff filt
	 * will bridge the difference
	 */
	if (override_initial_ff_chans_n == CHANS_N_NOT_OVERRIDDEN)
		/* we presume the dec_ctx has at least a valid chans_n */
		initial_ff_chans_n = dec_ctx_p->ch_layout.nb_channels;
	else
		initial_ff_chans_n = override_initial_ff_chans_n;
	if (override_initial_ff_rate == RATE_NOT_OVERRIDDEN)
		initial_ff_rate = dec_ctx_p->fr_rate;
	else
		initial_ff_rate = override_initial_ff_rate;
	if (override_initial_ff_fmt == AUDIO_FR_FMT_NOT_OVERRIDDEN)
		initial_ff_fmt = dec_ctx_p->fr_fmt;
	else
		initial_ff_fmt = override_initial_ff_fmt;
	npv_audio_pcm_cfg_hw_best_effort(pcm_p, initial_ff_chans_n,
					initial_ff_rate, initial_ff_fmt);
	npv_audio_pcm_silence_bufs_cfg(pcm_p, PRINT_INFO);
	npv_audio_pcm_cfg_sw(pcm_p);
	npv_audio_pcm_cfg_epoll(pcm_p); /* must be done AFTER pcm sw params are set */
	pout("ALSA PCM DUMP START-------------------------------------------------------------\n");
	snd_pcm_dump(pcm_p, pcm_pout_l);
	pout("ALSA PCM DUMP END---------------------------------------------------------------\n");
}
#undef CHANS_N_NOT_OVERRIDDEN
#undef RATE_NOT_OVERRIDDEN
#undef AUDIO_FR_FMT_NOT_OVERRIDDEN
#undef PRINT_INFO
