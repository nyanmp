#ifndef NPV_AUDIO_PUBLIC_H
#define NPV_AUDIO_PUBLIC_H
/*
 * code protected with a GNU affero GPLv3 license 
 * copyright (C) 2020 Sylvain BERTRAND
 */
#include <stdbool.h>
#include <poll.h>
#include <pthread.h>
#include <libavformat/avformat.h>
#include <libavcodec/avcodec.h>
#include <libavutil/samplefmt.h>
#include <alsa/asoundlib.h>
#include "npv/c_fixing.h"
#include "npv/global.h"
#include "npv/pkt_q/public.h"
/*---------------------------------------------------------------------------*/
#include "npv/namespace/ffmpeg.h"
#include "npv/audio/namespace/ffmpeg.h"
#include "npv/namespace/alsa.h"
#include "npv/audio/namespace/public.h"
/*---------------------------------------------------------------------------*/
#include "npv/audio/public/state.frag.h"
/*---------------------------------------------------------------------------*/
STATIC void dec_ctx_cfg(avcodec_params_t *params);
STATIC void dec_ctx_lock(void);
STATIC void dec_ctx_unlock(void);
STATIC void dec_sets_lock(void);
STATIC void dec_sets_unlock(void);
STATIC void dec_flush(void);
STATIC u8 dec_set_try_receive(void);
STATIC void dec_sets_receive_avail(void);
STATIC void draining_state_evt(void);
STATIC void evt_pcm_write(void);
STATIC void pcm_cfg_hw_best_effort(snd_pcm_t *pcm, int chans_n, int rate,
					enum avutil_audio_fr_fmt_t ff_fmt);
STATIC void pcm_silence_bufs_cfg(snd_pcm_t *pcm, bool print_info);
STATIC void pcm_cfg_sw(snd_pcm_t *pcm);
STATIC void pcm_cfg_epoll(snd_pcm_t *pcm);
STATIC void init_once(u8 *pcm_str);
STATIC void pkts_send(void);
STATIC void prepare(int override_initial_ff_chans_n,
			int override_initial_ff_rate,
			enum avutil_audio_fr_fmt_t override_initial_ff_fmt);
/*----------------------------------------------------------------------------*/
#define CLEANUP
#include "npv/namespace/ffmpeg.h"
#include "npv/audio/namespace/ffmpeg.h"
#include "npv/namespace/alsa.h"
#include "npv/audio/namespace/public.h"
#undef CLEANUP
#endif
