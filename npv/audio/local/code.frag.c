STATIC void fatal(u8 *fmt, ...)
{
	va_list ap;

	npv_perr("audio:");
	va_start(ap, fmt);
	npv_vfatal(fmt, ap);
	va_end(ap); /* unreachable */
}
STATIC void warning(u8 *fmt, ...)
{
	va_list ap;

	npv_perr("audio:");
	va_start(ap, fmt);
	npv_vwarning(fmt, ap);
	va_end(ap);
}
STATIC void pout(u8 *fmt, ...)
{
	va_list ap;

	npv_pout("audio:");
	va_start(ap, fmt);
	npv_vpout(fmt, ap);
	va_end(ap);
}
/* base on kernel api at the time we wrote this code */
STATIC u8 *kernel_ts_types_str_l[] = {
	"compat",
	"default",
	"link",
	"link absolute",
	"link estimated",
	"link synchonized"
};
/*
 * XXX: if it is ever used significantly, a fine granularity wiring strategy
 * will be implemented instead of using the default wiring
 */
STATIC void pcm_chmaps2ff_chans_layout(AVChannelLayout *ff_chans_layout,
		snd_pcm_t *pcm, unsigned int pcm_chans_n, bool print_info)
{
	int r;	
	snd_pcm_chmap_t *pcm_chmap;
	u8 chans_layout_str[STR_SZ]; /* should be overkill */

	pcm_chmap = snd_pcm_get_chmap(pcm);
	if (pcm_chmap == 0) {
		if (print_info)
			pout("alsa:no pcm channel map available, wiring to default ffmpeg channel layout\n");
	} else {
		if (print_info)
			pout("alsa:your pcm device support channel maps, but fine granularity wiring strategy is not implemented, using default ffmpeg layout\n");
		free(pcm_chmap);
	}
	/* XXX: will need to get back at it if a wiring strategy is required */
	av_channel_layout_default(ff_chans_layout, (int)pcm_chans_n);
	av_channel_layout_describe(ff_chans_layout, chans_layout_str,
						sizeof(chans_layout_str));
	if (print_info)
		pout("alsa channel map wired to ffmpeg channel layout:\"%s\" (%u pcm channels)\n", chans_layout_str, pcm_chans_n);
}
STATIC bool ff_fmt2pcm_layout_best_effort(enum avutil_audio_fr_fmt_t ff_fmt,
			snd_pcm_fmt_t *alsa_fmt, snd_pcm_access_t *alsa_access)
{
	static u8 ff_fmt_str[STR_SZ];

	avutil_get_audio_fr_fmt_str(ff_fmt_str, STR_SZ, ff_fmt);
	/* XXX: only classic non-mmap ones */
	switch (ff_fmt) {
	case AVUTIL_AUDIO_FR_FMT_U8:
		*alsa_fmt = SND_PCM_FMT_U8;
		*alsa_access = SND_PCM_ACCESS_RW_INTERLEAVED;
		break;
	case AVUTIL_AUDIO_FR_FMT_S16:
		*alsa_fmt = SND_PCM_FMT_S16;
		*alsa_access = SND_PCM_ACCESS_RW_INTERLEAVED;
		break;
	case AVUTIL_AUDIO_FR_FMT_S32:
		*alsa_fmt = SND_PCM_FMT_S32;
		*alsa_access = SND_PCM_ACCESS_RW_INTERLEAVED;
		break;
	case AVUTIL_AUDIO_FR_FMT_FLT:
		*alsa_fmt = SND_PCM_FMT_FLOAT;
		*alsa_access = SND_PCM_ACCESS_RW_INTERLEAVED;
		break;
	/* ff "planar" fmts are actually non interleaved fmts */
	case AVUTIL_AUDIO_FR_FMT_U8P:
		*alsa_fmt = SND_PCM_FMT_U8;
		*alsa_access = SND_PCM_ACCESS_RW_NONINTERLEAVED;
		break;
	case AVUTIL_AUDIO_FR_FMT_S16P:
		*alsa_fmt = SND_PCM_FMT_S16;
		*alsa_access = SND_PCM_ACCESS_RW_NONINTERLEAVED;
		break;
	case AVUTIL_AUDIO_FR_FMT_S32P:
		*alsa_fmt = SND_PCM_FMT_S32;
		*alsa_access = SND_PCM_ACCESS_RW_NONINTERLEAVED;
		break;
	case AVUTIL_AUDIO_FR_FMT_FLTP:
		*alsa_fmt = SND_PCM_FMT_FLOAT;
		*alsa_access = SND_PCM_ACCESS_RW_NONINTERLEAVED;
		break;
	default:
		pout("best effort:unable to wire ffmpeg sample format \"%sbits\" to alsa sample format, \n,", ff_fmt_str);
		return false;
	}
	pout("best effort:ffmpeg format \"%sbits\" (%u bytes) to alsa layout \"%s\" and access \"%s\"\n", ff_fmt_str, av_get_bytes_per_sample(ff_fmt), snd_pcm_fmt_desc(*alsa_fmt), snd_pcm_access_name(*alsa_access));
	return true;
}
STATIC void pcm_hw_chans_n_decide(snd_pcm_t *pcm,
		snd_pcm_hw_params_t *pcm_hw_params, int chans_n)
{
	int r;
	unsigned int chans_n_max;
	unsigned int chans_n_min;

	r = snd_pcm_hw_params_test_chans_n(pcm, pcm_hw_params,
							(unsigned int)chans_n);
	if (r == 0) {
		r = snd_pcm_hw_params_set_chans_n(pcm, pcm_hw_params,
							(unsigned int)chans_n);
		if (r != 0)
			fatal("alsa:unable to restrict pcm device to %d channels, count which was successfully tested\n", chans_n);
		pout("alsa:using %d channels\n", chans_n);
		return;
	}	
	pout("alsa:unable to use %d channels\n", chans_n);
	/* try to use the max chans n the pcm can */
	r = snd_pcm_hw_params_get_chans_n_max(pcm_hw_params, &chans_n_max);
	if (r != 0) 
		fatal("alsa:unable to get the maximum count of pcm device channels\n");
	r = snd_pcm_hw_params_test_chans_n(pcm, pcm_hw_params, chans_n_max);
	if (r == 0) {
		r = snd_pcm_hw_params_set_chans_n(pcm, pcm_hw_params,
								chans_n_max);
		if (r != 0)
			fatal("alsa:unable to restrict pcm device to %u channels, count which was successfully tested\n", chans_n_max);
		pout("alsa:using pcm maximum %u channels\n", chans_n_max);
		return;
	}
	/* ok... last try, the pcm dev min chans n */
	r = snd_pcm_hw_params_get_chans_n_min(pcm_hw_params, &chans_n_min);
	if (r != 0) 
		fatal("alsa:unable to get the minimum count of pcm device channels\n");
	r = snd_pcm_hw_params_test_chans_n(pcm, pcm_hw_params, chans_n_min);
	if (r == 0) {
		r = snd_pcm_hw_params_set_chans_n(pcm, pcm_hw_params,
								chans_n_min);
		if (r != 0)
			fatal("alsa:unable to restrict pcm device to %u channels, count which was successfully tested\n", chans_n_min);
		pout("alsa:using pcm device minimum %u channels\n", chans_n_min);
		return;
	}
	fatal("alsa:unable to find a suitable count of channels\n");
}
STATIC void pcm_hw_rate_decide(snd_pcm_t *pcm,
				snd_pcm_hw_params_t *pcm_hw_params, int rate)
{
	int r;
	unsigned int rate_max;
	unsigned int rate_near;
	unsigned int rate_min;

	r = snd_pcm_hw_params_test_rate(pcm, pcm_hw_params, (unsigned int)rate,
							SND_PCM_ST_PLAYBACK);
	if (r == 0) {
		r = snd_pcm_hw_params_set_rate(pcm, pcm_hw_params,
				(unsigned int)rate, SND_PCM_ST_PLAYBACK);
		if (r != 0)
			fatal("alsa:unable to restrict pcm device to %dHz, which was successfully tested\n", rate);
		pout("alsa:using %dHz\n", rate);
		return;
	}	
	pout("alsa:unable to use %dHz\n", rate);
	/* try to use the max rate the pcm can */
	r = snd_pcm_hw_params_get_rate_max(pcm_hw_params, &rate_max,
							SND_PCM_ST_PLAYBACK);
	if (r != 0) 
		fatal("alsa:unable to get the maximum rate of pcm device\n");
	r = snd_pcm_hw_params_test_rate(pcm, pcm_hw_params, rate_max,
							SND_PCM_ST_PLAYBACK);
	if (r == 0) {
		r = snd_pcm_hw_params_set_rate(pcm, pcm_hw_params, rate_max,
							SND_PCM_ST_PLAYBACK);
		if (r != 0)
			fatal("alsa:unable to restrict pcm device to %uHz, which was successfully tested\n", rate_max);
		pout("alsa:using pcm device %uHz\n", rate_max);
		return;
	}
	/* try to use a rate "near" of what the pcm dev can */
	rate_near = rate;
	r = snd_pcm_hw_params_set_rate_near(pcm, pcm_hw_params, &rate_near,
							SND_PCM_ST_PLAYBACK);
	if (r == 0) {
		pout("alsa:using pcm device %uHz\n", rate_near);
		return;
	}
	/* even a "near" rate did failed... try the min */
	r = snd_pcm_hw_params_get_rate_min(pcm_hw_params, &rate_min,
							SND_PCM_ST_PLAYBACK);
	if (r != 0) 
		fatal("alsa:unable to get the minimum rate of pcm device\n");
	r = snd_pcm_hw_params_test_rate(pcm, pcm_hw_params, rate_min,
							SND_PCM_ST_PLAYBACK);
	if (r == 0) {
		r = snd_pcm_hw_params_set_rate(pcm, pcm_hw_params, rate_min,
							SND_PCM_ST_PLAYBACK);
		if (r != 0)
			fatal("alsa:unable to restrict pcm device to %uHz, which was successfully tested\n", rate_min);
		pout("alsa:using pcm device %uHz\n", rate_min);
		return;
	}
	fatal("alsa:unable to find a suitable rate\n");
}
STATIC bool pcm_hw_fmt_decide_x(snd_pcm_t *pcm,
			snd_pcm_hw_params_t *pcm_hw_params, snd_pcm_fmt_t fmt)
{
	int r;

	r = snd_pcm_hw_params_test_fmt(pcm, pcm_hw_params, fmt);
	if (r != 0) 
		return false;
	r = snd_pcm_hw_params_set_fmt(pcm, pcm_hw_params, fmt);
	if (r != 0)
		fatal("alsa:unable to restrict pcm device to \"%s\", which was successfully tested\n", snd_pcm_fmt_desc(fmt));
	pout("alsa:using last resort \"%s\" format\n", snd_pcm_fmt_desc(fmt));
	return true;
}
#define PCM_HW_FMT_DECIDE_X(fmt) pcm_hw_fmt_decide_x(pcm, pcm_hw_params, fmt)
STATIC void pcm_hw_fmt_decide(snd_pcm_t *pcm,
			snd_pcm_hw_params_t *pcm_hw_params, bool best_effort,
						 snd_pcm_fmt_t best_effort_fmt)
{
	int r;
	snd_pcm_fmt_t *fmt;

	if (best_effort) {	
		r = snd_pcm_hw_params_test_fmt(pcm, pcm_hw_params,
							best_effort_fmt);
		if (r == 0) {
			r = snd_pcm_hw_params_set_fmt(pcm, pcm_hw_params,
							best_effort_fmt);
			if (r != 0)
				fatal("alsa:unable to restrict pcm device to \"%s\", which was successfully tested\n", snd_pcm_fmt_desc(best_effort_fmt));
			pout("alsa:using best effort \"%s\" format\n", snd_pcm_fmt_desc(best_effort_fmt));
			return;
		}
	}
	/* then we try to select from the reasonable "best" to the lowest */
	/* prefer fmts we know supported by ff */
	if (PCM_HW_FMT_DECIDE_X(SND_PCM_FMT_FLOAT))
		return;
	if (PCM_HW_FMT_DECIDE_X(SND_PCM_FMT_S32))
		return;
	if (PCM_HW_FMT_DECIDE_X(SND_PCM_FMT_S16))
		return;
	if (PCM_HW_FMT_DECIDE_X(SND_PCM_FMT_U8))
		return;
	/*
	 * from here, at the time of writting, those fmts have no ff
 	 * wiring, but we are alsa centric here, validate that later
	 */
	if (PCM_HW_FMT_DECIDE_X(SND_PCM_FMT_U32))
		return;
	if (PCM_HW_FMT_DECIDE_X(SND_PCM_FMT_S24))
		return;
	if (PCM_HW_FMT_DECIDE_X(SND_PCM_FMT_U24))
		return;
	if (PCM_HW_FMT_DECIDE_X(SND_PCM_FMT_U16))
		return;
	if (PCM_HW_FMT_DECIDE_X(SND_PCM_FMT_S8))
		return;
	fatal("alsa:unable to find a suitable format\n");
}
#undef PCM_HW_FMT_DECIDE_X
STATIC bool pcm_hw_access_decide_x(snd_pcm_t *pcm,
		snd_pcm_hw_params_t *pcm_hw_params, snd_pcm_access_t access)
{
	int r;

	r = snd_pcm_hw_params_test_access(pcm, pcm_hw_params, access);
	if (r != 0)
		return false;
	r = snd_pcm_hw_params_set_access(pcm, pcm_hw_params, access);
	if (r != 0)
		fatal("alsa:unable to restrict pcm device to \"%s\", which was successfully tested\n", snd_pcm_access_name(access));
	pout("alsa:using last resort \"%s\" access\n", snd_pcm_access_name(access));
	return true;
}
#define PCM_HW_ACCESS_DECIDE_X(access) \
			pcm_hw_access_decide_x(pcm, pcm_hw_params, access)
/* XXX: only classic non-mmap ones */
STATIC void pcm_hw_access_decide(snd_pcm_t *pcm,
			snd_pcm_hw_params_t *pcm_hw_params, bool best_effort,
					snd_pcm_access_t best_effort_access)
{
	int r;
	snd_pcm_access_t access;

	if (best_effort) {
		r = snd_pcm_hw_params_test_access(pcm, pcm_hw_params,
							best_effort_access);
		if (r == 0) {
			r = snd_pcm_hw_params_set_access(pcm, pcm_hw_params,
							best_effort_access);
			if (r != 0)
				fatal("alsa:unable to restrict pcm device to \"%s\", which was successfully tested\n", snd_pcm_access_name(best_effort_access));
			pout("alsa:using best effort \"%s\" access\n", snd_pcm_access_name(best_effort_access));
			return;
		}
	}
	/* brute force */
	if (PCM_HW_ACCESS_DECIDE_X(SND_PCM_ACCESS_RW_INTERLEAVED))
		return;
	if (PCM_HW_ACCESS_DECIDE_X(SND_PCM_ACCESS_RW_NONINTERLEAVED))
		return;
	fatal("alsa:unable to find a suitable access\n");
}
#undef PCM_HW_ACCESS_DECIDE_X
/*
 * latency control: some audio bufs can be huge (tested on a pulseaudio with 10
 * secs audio buf). if we are careless, we will quickly fill this buf which is
 * worth a significant amount of time, hence will add huge latency to our
 * interactive audio filtering (vol...). in the case of the 10 secs pulseaudio
 * buf, it means if you want to mute the audio, it will happen 10 secs later.
 * we add lantency control by limiting the sz of the dev audio buf, in periods
 * n.
 * we choose roughly 0.25 secs, or roughly (rate / 4) frs.
 */
STATIC void pcm_hw_buf_sz_cfg(snd_pcm_t *pcm,
					snd_pcm_hw_params_t *pcm_hw_params)
{
	int r;
	snd_pcm_ufrs_t latency_control_target_buf_ufrs_n;
	snd_pcm_ufrs_t latency_control_buf_ufrs_n;
	unsigned int rate;

	r = snd_pcm_hw_params_get_rate(pcm_hw_params, &rate, 0);
	if (r < 0) {
		warning("alsa:latency control:DISABLING LATENCY CONTROL:unable to get the decided rate from the current device parameters\n");
		return;
	}
	latency_control_target_buf_ufrs_n = (snd_pcm_ufrs_t)rate;
	latency_control_target_buf_ufrs_n /= 4;
	latency_control_buf_ufrs_n = latency_control_target_buf_ufrs_n;
	r = snd_pcm_hw_params_set_buf_sz_near(pcm, pcm_hw_params,
						&latency_control_buf_ufrs_n);
	if (r < 0) {
		warning("alsa:latency control:DISABLING_LATENCY_CONTROL:unable to set the audio buffer size (count of frames) to %u periods for the current device parameters\n", latency_control_buf_ufrs_n);
		return;
	}
	pout("alsa:latency control:target buffer frame count is %u (~0.25 sec), got an audio buffer size set to %u frames\n", latency_control_target_buf_ufrs_n, latency_control_buf_ufrs_n);
}
/*
 * this function will "decide" the pcm dev cfg:
 * the goal is to be the "closest" to the provided params,
 * the "gap" will have to "filled" with ff filts
 *
 * the "strategy" is a "fall-thru" (chans n then ... then ...) which
 * will "restrict" the pcm dev cfg further at each step
 *
 * we try to use a sensible restrict order regarding audio props
 */
STATIC void pcm_cfg_hw_core_best_effort(snd_pcm_t *pcm,
				snd_pcm_hw_params_t *pcm_hw_params, int chans_n,
				int rate, enum avutil_audio_fr_fmt_t ff_fmt)
{
	int r;
	bool best_effort_wiring_success;
	snd_pcm_fmt_t fmt_from_best_effort;
	snd_pcm_access_t access_from_best_effort;

	/* the return value is from a first refine of the raw hw params */
	r = snd_pcm_hw_params_any(pcm, pcm_hw_params);
	if (r < 0)
		fatal("alsa:unable to populate the hardware parameters context\n");
	pcm_hw_chans_n_decide(pcm, pcm_hw_params, chans_n);
	pcm_hw_rate_decide(pcm, pcm_hw_params, rate);
	/* try our best with the fmt */
	best_effort_wiring_success = ff_fmt2pcm_layout_best_effort(
		ff_fmt, &fmt_from_best_effort, &access_from_best_effort);
	pcm_hw_fmt_decide(pcm, pcm_hw_params, best_effort_wiring_success,
							fmt_from_best_effort);
	pcm_hw_access_decide(pcm, pcm_hw_params, best_effort_wiring_success,
						access_from_best_effort);
	pcm_hw_buf_sz_cfg(pcm, pcm_hw_params);
}
STATIC void dec_a_grow(void)
{
	u32 new_idx;

	new_idx = dec_sets_p.n_max;
	dec_sets_p.a = realloc(dec_sets_p.a, sizeof(*dec_sets_p.a)
						* (dec_sets_p.n_max + 1));
	if (dec_sets_p.a == 0)
		fatal("unable to allocate memory for an additional pointer on a reference of a decoder set of frames\n");
	dec_sets_p.a[new_idx] = avutil_audio_set_ref_alloc();
	if (dec_sets_p.a[new_idx] == 0)
		fatal("ffmpeg:unable to allocate a reference of a decoder set of frames\n");
	++dec_sets_p.n_max;
}
#define AGAIN		0
#define RECOVERED	1
#define CONTINUE	2
STATIC u8 alsa_recover(snd_pcm_sfrs_t r)
{
	if (r >= 0)
		return CONTINUE;
	/* r < 0 */
	if (r == -EAGAIN)
		return AGAIN;
	else if (r == -EPIPE || r == -ESTRPIPE) {
		/* underrun or suspended */
		int r_recovered;

		r_recovered = snd_pcm_recover(pcm_p, (int)r, 0);
		if (r_recovered == 0) {
			warning("alsa:pcm recovered\n");
			return RECOVERED;
		}
		fatal("alsa:unable to recover from suspend/underrun\n");
	} 
	fatal("alsa:fatal/unhandled error\n");
}
#undef AGAIN
#undef RECOVERED
#undef CONTINUE
#define NO		0
#define AGAIN		0
#define RECOVERED	1
STATIC void pcm_silence_frs_write(snd_pcm_ufrs_t ufrs_n) { loop
{
	int alsa_r;
	u8 r_recover;
	int is_planar_fmt;

	if (ufrs_n == 0)
		break;
	is_planar_fmt = avutil_audio_fr_fmt_is_planar(
						npv_audio_filt_p.set->fmt);
	if (is_planar_fmt == NO)
		alsa_r = snd_pcm_writei(pcm_p, pcm_silence_bufs_l[0], ufrs_n);
	else
		alsa_r = snd_pcm_writen(pcm_p, pcm_silence_bufs_l, ufrs_n);
	r_recover = alsa_recover(alsa_r);
	if (r_recover == AGAIN)
		continue;
	else if (r_recover == RECOVERED)
		break;
	/* r_recover == CONTINUE */
	ufrs_n -= (snd_pcm_ufrs_t)alsa_r;
}}
#undef NO
#undef AGAIN
#undef RECOVERED
#define NO 0
STATIC void chans_buf_init(u8 **chans_buf, int start_fr_idx)
{
	int is_planar_fmt;
	int sample_bytes_n;

	sample_bytes_n = avutil_get_bytes_per_sample(npv_audio_filt_p.set->fmt);
	is_planar_fmt = avutil_audio_fr_fmt_is_planar(
						npv_audio_filt_p.set->fmt);
	if (is_planar_fmt == NO) { /* or is pcm interleaved */
		int fr_bytes_n;

		fr_bytes_n = sample_bytes_n
				* npv_audio_filt_p.set->ch_layout.nb_channels;
		chans_buf[0] = (u8*)npv_audio_filt_p.set->data[0] + start_fr_idx
								* fr_bytes_n;
	} else { /* ff planar or pcm noninterleaved */
		int p;

		p = 0;
		loop {
			if (p == npv_audio_filt_p.set->ch_layout.nb_channels)
				break;
			chans_buf[p] = (u8*)npv_audio_filt_p.set->data[p]
						+ start_fr_idx * sample_bytes_n;
			++p;
		}	
	}
}
#undef NO
#define NO 0
STATIC void chans_buf_inc(u8 **chans_buf, int inc)
{
	int is_planar_fmt;
	int sample_bytes_n;

	sample_bytes_n = avutil_get_bytes_per_sample(npv_audio_filt_p.set->fmt);
	is_planar_fmt = avutil_audio_fr_fmt_is_planar(
						npv_audio_filt_p.set->fmt);
	if (is_planar_fmt == NO) { /* or is pcm interleaved */
		int fr_bytes_n;

		fr_bytes_n = sample_bytes_n
				* npv_audio_filt_p.set->ch_layout.nb_channels;
		chans_buf[0] = (u8*)chans_buf[0] + inc * fr_bytes_n;
	} else { /* ff planar or pcm noninterleaved */
		int p;

		p = 0;
		loop {
			if (p == npv_audio_filt_p.set->ch_layout.nb_channels)
				break;
			chans_buf[p] = (u8*)chans_buf[p] + inc * sample_bytes_n;
			++p;
		}	
	}
}
#undef NO
STATIC void draining_state_handle(void)
{
	int r;

	r = snd_pcm_drain(pcm_p);
	if (r != 0) {
		snd_pcm_state_t state;

		if (r == -EAGAIN)
			return;
		 /*
		  * the pcm state can change asynchronously.
		  * _old_ behavior:
		  * if the draining was successful, the pcm should be in SETUP
		  * state, and in this state, snd_pcm_drain will fail
		  * _new and fixed__ behavior:
		  * this won't happen because calling snd_pcm_drain while in
		  * the SETUP state won't return an error anymore
		  */
		state = snd_pcm_state(pcm_p);
		if (state != SND_PCM_STATE_SETUP)
			fatal("alsa:an error occured switching to/checking the pcm draining state\n");
		/* here pcm state is SND_PCM_STATE_SETUP */
	}
	npv_exit("alsa pcm drained or similar, exiting\n");
}
STATIC void draining_state_switch(void)
{
	int r;
	u8 i;
	struct itimerspec t;

	draining_p = true;
	draining_state_handle();
	/* remove the alsa epoll fds */
	i = 0;
	loop {
		if (i == pcm_pollfds_n_p)
			break;
		/* in theory, it is thread safe */
		r = epoll_ctl(npv_ep_fd_p, EPOLL_CTL_DEL, pcm_pollfds_p[i].fd,
									0);
		if (r == -1)
			fatal("unable to remove the alsa file descriptors from epoll\n");
		++i;
	}
	/* start the draining timer */
	memset(&t, 0, sizeof(t));
	/* initial and interval */
	t.it_value.tv_nsec = DRAINING_TIMER_INTERVAL_NSECS_N;
	t.it_interval.tv_nsec = DRAINING_TIMER_INTERVAL_NSECS_N;
	r = timerfd_settime(draining_timer_fd_p, 0, &t, 0);
	if (r == -1)
		fatal("unable to arm the draining timer\n");
}
/* fatal if the wiring cannot be done */
STATIC void pcm_layout2ff_fmt_strict(snd_pcm_fmt_t alsa_fmt,
	snd_pcm_access_t alsa_access, enum avutil_audio_fr_fmt_t *ff_fmt,
								bool print_info)
{
	/*
	 * ff fmt byte order is always native.
	 * here we handle little endian only
	 */
	switch (alsa_fmt) {
	case SND_PCM_FMT_FLOAT:
		if (alsa_access == SND_PCM_ACCESS_RW_INTERLEAVED)
			*ff_fmt = AVUTIL_AUDIO_FR_FMT_FLT;
		else
			*ff_fmt = AVUTIL_AUDIO_FR_FMT_FLTP;
		break;
	case SND_PCM_FMT_S32:
		if (alsa_access == SND_PCM_ACCESS_RW_INTERLEAVED)
			*ff_fmt = AVUTIL_AUDIO_FR_FMT_S32;
		else
			*ff_fmt = AVUTIL_AUDIO_FR_FMT_S32P;
		break;
	case SND_PCM_FMT_S16:
		if (alsa_access == SND_PCM_ACCESS_RW_INTERLEAVED)
			*ff_fmt = AVUTIL_AUDIO_FR_FMT_S16;
		else
			*ff_fmt = AVUTIL_AUDIO_FR_FMT_S16P;
		break;
	case SND_PCM_FMT_U8:
		if (alsa_access == SND_PCM_ACCESS_RW_INTERLEAVED)
			*ff_fmt = AVUTIL_AUDIO_FR_FMT_U8;
		else
			*ff_fmt = AVUTIL_AUDIO_FR_FMT_U8P;
		break;
	default:
		fatal("unable to wire strictly alsa layout \"%s\"/\"%s\" to a ffmpeg format\n", snd_pcm_fmt_desc(alsa_fmt), snd_pcm_access_name(alsa_access));
	}
	if (print_info) {
		u8 ff_fmt_str[STR_SZ];

		avutil_get_audio_fr_fmt_str(ff_fmt_str, sizeof(ff_fmt_str),
								*ff_fmt);
		pout("alsa pcm layout \"%s\"/\"%s\" wired strictly to ffmpeg format \"%sbits\"\n", snd_pcm_fmt_desc(alsa_fmt), snd_pcm_access_name(alsa_access), ff_fmt_str);
	}
}
STATIC void pcm2ff_strict(snd_pcm_t *pcm, AVChannelLayout *ff_chans_layout,
	int *ff_rate, enum avutil_audio_fr_fmt_t *ff_fmt, bool print_info)
{
	int r;
	snd_pcm_hw_params_t *hw_params;
	unsigned int pcm_chans_n;
	unsigned int pcm_rate;
	snd_pcm_fmt_t pcm_fmt;
	snd_pcm_access_t pcm_access;
	
	r = snd_pcm_hw_params_malloc(&hw_params);
	if (r < 0)
		fatal("alsa:unable to allocate hardware parameters context for ffmpeg filter wiring\n");
	r = snd_pcm_hw_params_current(pcm, hw_params);
	if (r != 0)
		fatal("alsa:unable to get current hardware parameters for ffmpeg filter wiring\n");
	r = snd_pcm_hw_params_get_access(hw_params, &pcm_access);
	if (r < 0)
		fatal("alsa:unable to get the pcm access for ffmpeg filter wiring\n");
	r = snd_pcm_hw_params_get_fmt(hw_params, &pcm_fmt);
	if (r < 0)
		fatal("alsa:unable to get the pcm format for ffmpeg filter wiring\n");
	/*--------------------------------------------------------------------*/
	pcm_layout2ff_fmt_strict(pcm_fmt, pcm_access, ff_fmt, print_info);
	/*--------------------------------------------------------------------*/
	r = snd_pcm_hw_params_get_rate(hw_params, &pcm_rate,
							SND_PCM_ST_PLAYBACK);
	if (r < 0)
		fatal("alsa:unable to get the pcm rate for ffmpeg filter wiring\n");
	*ff_rate = (int)pcm_rate;
	r = snd_pcm_hw_params_get_chans_n(hw_params, &pcm_chans_n);
	if (r < 0)
		fatal("alsa:unable to get the pcm count of channels for ffmpeg filter wiring\n");
	/*--------------------------------------------------------------------*/
	pcm_chmaps2ff_chans_layout(ff_chans_layout, pcm, pcm_chans_n,
								print_info);
	/*--------------------------------------------------------------------*/
	snd_pcm_hw_params_free(hw_params);
}
#define NO_DEC_SET		2
#define FILT_RECFG_REQUIRED	4
#define EOF_FILT		2
#define DRAINING		3
#define HAVE_FILT_SET		1
#define PRINT_INFO		true
/* synchronous filt or will end up asynchronous in the pipeline one day */
STATIC u8 dec_set_filter(void)
{
	u8 r;

	loop {
		AVChannelLayout new_chans_layout;
		int new_rate;
		enum avutil_audio_fr_fmt_t new_fmt;
		AVChannelLayout dst_chans_layout;
		int dst_rate;
		enum avutil_audio_fr_fmt_t dst_fmt;

		memset(&new_chans_layout, 0, sizeof(new_chans_layout));
		memset(&dst_chans_layout, 0, sizeof(dst_chans_layout));

		r = filt_push_dec_set(&new_chans_layout, &new_rate, &new_fmt);
		if (r == NO_DEC_SET) {/* pipeline not fast enough */ 
			av_channel_layout_uninit(&new_chans_layout);
			return NO_DEC_SET;
		}
		else if (r != FILT_RECFG_REQUIRED) {
			/* FILT_SWITCHED_TO_DRAINING | PUSHED_ONE_SET | AGAIN */
			av_channel_layout_uninit(&new_chans_layout);
			break;
		}
		/* FILT_RECFG_REQUIRED */
		pcm2ff_strict(npv_audio_pcm_p, &dst_chans_layout, &dst_rate,
							&dst_fmt, PRINT_INFO);
		filt_cfg(st_p.tb,
			&new_chans_layout, new_rate, new_fmt,
			filt_p.muted, filt_p.vol,
			&dst_chans_layout, dst_rate, dst_fmt, PRINT_INFO);
		av_channel_layout_uninit(&new_chans_layout);
		av_channel_layout_uninit(&dst_chans_layout);
	}
	r = npv_audio_filt_set_get();
	if (r == EOF_FILT) {
		draining_state_switch();
		return DRAINING;
	}
	return HAVE_FILT_SET;
}
#undef NO_DEC_SET
#undef FILT_RECFG_REQUIRED
#undef EOF_FILT
#undef DRAINING
#undef HAVE_FILT_SET
#undef PRINT_INFO
#define NO			0
#define AGAIN			0
#define RECOVERED		1
#define CONTINUE 		2
#define NO_DEC_SET		2
#define ALL_FRS_WRITTEN		0
/* #define RECOVERED		1 */
#define INCOMPLETE_WRITE	2
#define DRAINING		3
#define PRINT_INFO		true
STATIC u8 pcm_filt_frs_write(snd_pcm_ufrs_t *ufrs_n) { loop
{
	u8 chan_buf;
	u8 *chans_buf[AVUTIL_DATA_PTRS_N];
	snd_pcm_ufrs_t ufrs_to_write_n;
	snd_pcm_ufrs_t filt_set_remaining_ufrs_n; /* for clarity */
	int is_planar_fmt;
	snd_pcm_ufrs_t written_ufrs_n; /* for clarity */

	if (*ufrs_n == 0)
		return ALL_FRS_WRITTEN;
	/* synchronous filtering */
	if (npv_audio_filt_p.set->frs_n == 0) {
		u8 r;

		r = dec_set_filter();
		if (r == NO_DEC_SET || r == DRAINING)
			return r;
		/* HAVE_FILT_SET */
		npv_audio_filt_p.pcm_written_ufrs_n = 0;
	}
	chans_buf_init(chans_buf, (int)npv_audio_filt_p.pcm_written_ufrs_n);
	filt_set_remaining_ufrs_n = (snd_pcm_ufrs_t)npv_audio_filt_p.set->frs_n
					- npv_audio_filt_p.pcm_written_ufrs_n;
	if (filt_set_remaining_ufrs_n > *ufrs_n)
		ufrs_to_write_n = *ufrs_n;
	else	
		ufrs_to_write_n = filt_set_remaining_ufrs_n;
	is_planar_fmt = avutil_audio_fr_fmt_is_planar(
						npv_audio_filt_p.set->fmt);
	written_ufrs_n = 0;
	loop { /* short write loop */
		snd_pcm_sfrs_t alsa_r;
		u8 r_recover;

		if (is_planar_fmt == NO)
			alsa_r = snd_pcm_writei(pcm_p, chans_buf[0],
					ufrs_to_write_n - written_ufrs_n);
		else
			alsa_r = snd_pcm_writen(pcm_p, (void**)chans_buf,
					ufrs_to_write_n - written_ufrs_n);
		r_recover = alsa_recover(alsa_r);
		if (r_recover == AGAIN)
			continue;
		else if (r_recover == RECOVERED) {
			/* account for the written frs anyway */
			if (npv_audio_filt_p.pcm_written_ufrs_n == 0)
				npv_clk_ref_time_point_update(
						npv_audio_filt_p.set->pts,
								written_ufrs_n);
			npv_audio_filt_p.pcm_written_ufrs_n += written_ufrs_n;
			if ((int)npv_audio_filt_p.pcm_written_ufrs_n ==
						npv_audio_filt_p.set->frs_n)
				/* set audio_filt_p.set->frs_n = 0 */
				avutil_audio_set_unref(npv_audio_filt_p.set);
			return RECOVERED;
		}
		/* r_recover == CONTINUE */
		written_ufrs_n += (snd_pcm_ufrs_t)alsa_r;
		if (written_ufrs_n == ufrs_to_write_n)
			break;
		chans_buf_inc(chans_buf, (int)alsa_r); 
	}
	/*
	 * this is here we update our ref time point for the audio clk 
	 * because with a new filt set of frs, we get a new ts
	 *
	 * XXX: getting the "right" ts from ff is shady/convoluted?
	 */
	if (npv_audio_filt_p.pcm_written_ufrs_n == 0)
		npv_clk_ref_time_point_update(npv_audio_filt_p.set->pts,
								written_ufrs_n);
	npv_audio_filt_p.pcm_written_ufrs_n += written_ufrs_n;
	*ufrs_n -= written_ufrs_n;

	if ((int)npv_audio_filt_p.pcm_written_ufrs_n
						== npv_audio_filt_p.set->frs_n)
		/* set audio_filt_p.av->frs_n = 0 */
		avutil_audio_set_unref(npv_audio_filt_p.set);
}}
#undef NO
#undef AGAIN
#undef RECOVERED
#undef CONTINUE
#undef NO_DEC_SET
#undef ALL_FRS_WRITTEN
#undef INCOMPLETE_WRITE
#undef DRAINING
#undef PRINT_INFO
STATIC void init_pcm_once_public(u8 *pcm_str)
{
	int r;

	r = snd_pcm_open(&pcm_p, pcm_str, SND_PCM_ST_PLAYBACK,
							SND_PCM_NONBLOCK);
	if (r < 0) {
		if (r == -EAGAIN)
			fatal("alsa:\"%s\" pcm is already in use\n", pcm_str);
		else
			fatal("alsa:unable to open \"%s\" pcm for playback\n", pcm_str);
	}
}
STATIC void init_once_public(u8 *pcm_str)
{
	int r;

	memset(&st_p, 0, sizeof(st_p));
	pkt_q_p = npv_pkt_q_new("audio");
	dec_ctx_p = 0;
	dec_sets_p.eof_receive = false;
	dec_sets_p.n_max = 0;
	dec_sets_p.n = 0;
	dec_sets_p.a = 0;
	init_pcm_once_public(pcm_str);
	selected_ts_type_p = -1;
	/* linux bug: still no CLOCK_MONOTONIC_RAW for timerfd */
	errno = 0;
	draining_timer_fd_p = timerfd_create(CLOCK_MONOTONIC, TFD_NONBLOCK);
	if (draining_timer_fd_p == -1)
		fatal("unable to get a draining timer file descriptor:%s\n", strerror(errno));
	draining_p = false;
	r = pthread_mutex_init(&dec_ctx_mutex_l, 0);
	if (r != 0)
		fatal("%d:unable to init the decoder mutex\n", r);
	r = pthread_mutex_init(&dec_sets_p.mutex, 0);
	if (r != 0)
		fatal("%d:unable to init the mutex for the array of decoder sets\n", r);
}
STATIC void init_once_local(void)
{
	int r;

	dec_l = 0;
	r = snd_output_stdio_attach(&pcm_pout_l, stdout, 0);
	if (r < 0)
		fatal("alsa:unable to attach stdout\n");
	r = snd_output_stdio_attach(&pcm_perr_l, stderr, 0);
	if (r < 0)
		fatal("alsa:unable to attach stderr\n");
	memset(pcm_silence_bufs_l, 0, sizeof(pcm_silence_bufs_l));
}
STATIC void dec_a_unref_all(void)
{
	u16 set;

	set = 0;
	loop {
		if (set == dec_sets_p.n)
			break;
		avutil_audio_set_unref(dec_sets_p.a[set]);
		++set;
	}
	dec_sets_p.n = 0;
}
