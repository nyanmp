/* ff dec */
STATIC avcodec_codec_t *dec_l; /* same mutex than dec_ctx_p */
STATIC pthread_mutex_t dec_ctx_mutex_l;
/*----------------------------------------------------------------------------*/
/* alsa */
STATIC snd_output_t *pcm_pout_l;
STATIC snd_output_t *pcm_perr_l;
/*----------------------------------------------------------------------------*/
/* we will inject silence frs while paused */
STATIC void *pcm_silence_bufs_l[AVUTIL_DATA_PTRS_N];
