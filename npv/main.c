#ifndef NPV_MAIN_C
#define NPV_MAIN_C
/*
 * code protected with a GNU affero GPLv3 license 
 * copyright (C) 2020 Sylvain BERTRAND
 */
/*
 * this is not a library, then we could not care less about memory management
 * and/or similar cleanup: we have a virtual machine with a garbage collector,
 * it is linux.
 */
#include <stdbool.h>
#include <locale.h>
#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <signal.h>
#include <pthread.h>
#include <errno.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <termios.h> /* used while waiting for the osd */
#include <sys/epoll.h>
#include <sys/signalfd.h>
#include <sys/timerfd.h>
#include <dlfcn.h>
#include <libavformat/avformat.h>
#include <libavutil/samplefmt.h>
#include <libavutil/mathematics.h>
#include <libavfilter/buffersink.h>
#include <alsa/asoundlib.h>
/*---------------------------------------------------------------------------*/
#include "npv/c_fixing.h"
/*---------------------------------------------------------------------------*/
#include "config.h"
/*---------------------------------------------------------------------------*/
#include "npv/global.h"
#include "npv/public.h"
#include "npv/fmt/public.h"
#include "npv/pipeline/public.h"
#include "npv/audio/filt/public.h"
#include "npv/audio/public.h"
#include "npv/video/public.h"
#include "npv/video/osd/public.h"
#include "npv/xcb/public.h"
#include "npv/vk/public.h"
#include "npv/thdsws/public.h"
#include "npv/clk/public.h"
/*---------------------------------------------------------------------------*/
#include "npv/namespace/public.h"
#include "npv/namespace/ffmpeg.h"
#include "npv/namespace/alsa.h"
#include "npv/audio/namespace/ffmpeg.h"
#include "npv/namespace/public.h"
#include "npv/namespace/main.c"
/*---------------------------------------------------------------------------*/
#include "npv/local/state.frag.c"
#include "npv/local/code.frag.c"
#include "npv/public/code.frag.c"
/*--------------------------------------------------------------------------*/
#define CLEANUP
#include "npv/namespace/public.h"
#include "npv/namespace/ffmpeg.h"
#include "npv/namespace/alsa.h"
#include "npv/audio/namespace/ffmpeg.h"
#include "npv/namespace/public.h"
#include "npv/namespace/main.c"
#undef CLEANUP
/*============================================================================*/
#include "npv/pkt_q/main.c"
#include "npv/fmt/main.c"
#include "npv/audio/main.c"
#include "npv/audio/filt/main.c"
#include "npv/video/osd/main.c"
#include "npv/video/main.c"
#include "npv/xcb/main.c"
#include "npv/vk/main.c"
#include "npv/clk/main.c"
#include "npv/pipeline/main.c"
#include "npv/thdsws/main.c"
/*----------------------------------------------------------------------------*/
#endif
