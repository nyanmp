STATIC FT_Library library_l;
STATIC struct {
	/*
	 * use caches, coze we will probably share fonts with libass,
	 * if libass is still properly coded and have a clean set of
	 * dependencies
	 */
	FTC_Manager manager;
    	FTC_ImageCache img_cache;
	struct {
		u8 **files;
		FT_Face *ft;
		u8 *ids;
		u8 n;
	} faces;

	struct {
		FT_ULong unicode;

		u8 face_id;
		FT_UInt idx;
		FT_BitmapGlyph bg;
		FTC_Node node;
	} gs[GS_N];
} cache_l;
STATIC u16 timer_baseline_l;
STATIC struct {
	u16 baseline;
	u16 start_column;
} volume_l;
/* only used to restore pixels from a paused img */
STATIC struct {
	void *scaler_pixs;
	void *pixs;
	s16 width;
	s16 height;
	u32 line_bytes_n;
	u8 timer_gs[sizeof(TIMER_TMPL_MAX)];
	bool dirty;
} restore_l;
STATIC bool timer_on;
