STATIC void fatal(void *fmt, ...)
{
	va_list ap;

	npv_perr("osd:");
	va_start(ap, fmt);
	npv_vfatal(fmt, ap);
	va_end(ap);
	exit(EXIT_FAILURE);
}
STATIC void warning(void *fmt, ...)
{
	va_list ap;

	npv_perr("osd:");
	va_start(ap, fmt);
	npv_vwarning(fmt,ap);
	va_end(ap);
}
STATIC void pout(void *fmt, ...)
{
	va_list ap;

	npv_pout("osd:");
	va_start(ap, fmt);
	npv_vpout(fmt, ap);
	va_end(ap);
}
STATIC void cache_nodes_release(void)
{
	u8 i;

	i = 0;
	loop {
		if (i == GS_N)
			break;
		if (cache_l.gs[i].node != 0) {
			FTC_Node_Unref(cache_l.gs[i].node, cache_l.manager);
			cache_l.gs[i].node = 0;
		}
		++i;
	}
}
STATIC void our_finalizer(void *object)
{
	FT_Face f;
	FT_Face *f_ptr;

	f = object;
	f_ptr = f->generic.data; /* we need to zero our pointer */
	*f_ptr = 0;
}
STATIC void face_lookup(FT_Face *f, u8 i)
{
	FT_Error ft_r;

	if (cache_l.faces.ft[i] != 0) {
		*f = cache_l.faces.ft[i];
		return;
	}
	ft_r = FTC_Manager_LookupFace(cache_l.manager, &cache_l.faces.ids[i],
									f);
	if (ft_r != FT_Err_Ok)
		fatal("freetype:cache_l:unable to lookup for face %s\n", cache_l.faces.files[i]);
}
STATIC void gs_init_once(void)
{
	u8 i;

	cache_l.gs[G_PERCENT_SIGN].unicode		= 0x25;
	cache_l.gs[G_SOLIDUS].unicode			= 0x2f;
	cache_l.gs[G_0].unicode				= 0x30;
	cache_l.gs[G_1].unicode				= 0x31;
	cache_l.gs[G_2].unicode				= 0x32;
	cache_l.gs[G_3].unicode				= 0x33;
	cache_l.gs[G_4].unicode				= 0x34;
	cache_l.gs[G_5].unicode				= 0x35;
	cache_l.gs[G_6].unicode				= 0x36;
	cache_l.gs[G_7].unicode				= 0x37;
	cache_l.gs[G_8].unicode				= 0x38;
	cache_l.gs[G_9].unicode				= 0x39;
	cache_l.gs[G_COLON].unicode			= 0x3a;
	cache_l.gs[G_QUESTION_MARK].unicode		= 0x3f;
	cache_l.gs[G_MINUS].unicode			= 0x2212;
	cache_l.gs[G_PLAY].unicode			= 0x23f5;
	cache_l.gs[G_PAUSE].unicode			= 0x23f8;
	cache_l.gs[G_GREATER_THAN].unicode		= 0x3e;
	cache_l.gs[G_VERTICAL_BAR].unicode		= 0x7c;
	cache_l.gs[G_BLACK_LOWER_RIGHT_TRIANGLE].unicode= 0x25e2;

	i = 0;
	loop {
		u8 j;

		if (i == GS_N)
			break;
		j = 0;
		loop {
			FT_Face f;

			if (j == cache_l.faces.n) {
				warning("freetype:unable to find a valid glyph index for our glyph %u\n", i);
				cache_l.gs[i].face_id = 0;
				break;
			}
			face_lookup(&f, j);
			cache_l.gs[i].idx = FT_Get_Char_Index(f,
							cache_l.gs[i].unicode);
			if (cache_l.gs[i].idx != 0) {
				cache_l.gs[i].face_id = j;
				break;
			}
			++j;
		}
		++i;
	}
}
STATIC void out_tt_interpreters(FT_UInt iv)
{
	if ((TT_INTERPRETER_VERSION_35 & iv) != 0)
		pout("freetype:truetype:hinting interpreter version 35 available\n");
	if ((TT_INTERPRETER_VERSION_38 & iv) != 0)
		pout("freetype:truetype:hinting interpreter version 38 available\n");
	if ((TT_INTERPRETER_VERSION_40 & iv) != 0)
		pout("freetype:truetype:hinting interpreter version 40 available\n");
}
STATIC u8 *hinting_engine_str(FT_Int32 he)
{
	if (he == FT_HINTING_FREETYPE)
		return "freetype";
	else if (he == FT_HINTING_ADOBE)
		return "adobe";
	else
		return "unknown";
}
/* the id is the idx in the faces/fonts arrays */
STATIC FT_Error face_requester(FTC_FaceID face_id, FT_Library lib,
					FT_Pointer unused, FT_Face *aface)
{
	FT_Error ft_r;
	u8 *id;

	id = face_id;
	if (id == 0) {
		warning("freetype:cache_l:requester:no face id\n");
		return FT_Err_Invalid_Argument;
	}
	/* presume ptr equality means the same */
	if (lib != library_l) {
		warning("freetype:cache_l:requester:wrong lib\n");
		return FT_Err_Invalid_Library_Handle;
	}
	if (cache_l.faces.ft[*id] != NULL)
		goto exit;
	ft_r = FT_New_Face(lib, cache_l.faces.files[*id], -1,
							&cache_l.faces.ft[*id]);
	if (ft_r != FT_Err_Ok) {
		warning("freetype:unable to load the face(s) %s:%d\n", cache_l.faces.files[*id], ft_r);
		return ft_r;
	}
	pout("freetype:face %s has %ld faces\n", cache_l.faces.files[*id], cache_l.faces.ft[*id]->num_faces);
	ft_r = FT_Done_Face(cache_l.faces.ft[*id]);
	if (ft_r != FT_Err_Ok) {
		warning("freetype:unable to unload the face(s) %s:%d\n", cache_l.faces.files[*id], ft_r);
		return ft_r;
	}
	pout("freetype:opening face 0 of %s\n", cache_l.faces.files[*id]);
	ft_r = FT_New_Face(lib, cache_l.faces.files[*id], 0,
							&cache_l.faces.ft[*id]);
	if (ft_r != FT_Err_Ok) {
		warning("freetype:unable to open the face 0 from %s:%d\n", cache_l.faces.files[*id], ft_r);
		return ft_r;
	}
	/* install finalizer, to know when the cache let it go */
	cache_l.faces.ft[*id]->generic.finalizer = our_finalizer;
	cache_l.faces.ft[*id]->generic.data = &cache_l.faces.ft[*id];

	pout("freetype:face 0 of %s:\n", cache_l.faces.files[*id]);
	pout("freetype:\tnumber of glyphs = %ld\n", cache_l.faces.ft[*id]->num_glyphs);
	pout("freetype:\tfamily name=%s\n", cache_l.faces.ft[*id]->family_name);
	if (cache_l.faces.ft[*id]->style_name != 0)
		pout("freetype:\tstyle name=%s\n", cache_l.faces.ft[*id]->style_name);
exit:
	*aface = cache_l.faces.ft[*id];
	return FT_Err_Ok;
}
STATIC void timer_baseline_compute(void)
{
	FT_Int max;
	u8 i;
	/* 
	 * get the max distance, in n of lines, from the baseline, _excluded_,
	 * of the _rendered_ gs: play/pause, 0..9 and colon
	 */
#ifndef NPV_FACES_ASCII_ONLY
	max = cache_l.gs[G_PLAY].bg->top;
	if (cache_l.gs[G_PAUSE].bg->top > max)
		max = cache_l.gs[G_PAUSE].bg->top;
#else
	max = cache_l.gs[G_VERTICAL_BAR].bg->top;
	if (cache_l.gs[G_GREATER_THAN].bg->top > max)
		max = cache_l.gs[G_GREATER_THAN].bg->top;
#endif
	i = G_0;
	loop {
		if (i > G_COLON)
			break;
		if (cache_l.gs[i].bg->top > max)
			max = cache_l.gs[i].bg->top;
		++i;
	}
	pout("timer:baseline:max top=%d\n", max);
	timer_baseline_l = (u16)max;
}
STATIC u32 digit_advance_max(void)
{
	u8 i;
	u32 max;
	u16 max_i;
	u16 max_f;

	max = cache_l.gs[G_0].bg->root.advance.x;
	max_i = (u16)(max >> 16);
	max_f = (u16)(max & 0xffff);
	i = G_1;
	loop {
		u32 digit_advance;
		u16 digit_advance_i;
		u16 digit_advance_f;

		if (i > G_9)
			break;
		digit_advance = (u32)(cache_l.gs[i].bg->root.advance.x);
		digit_advance_i = (u16)(digit_advance >> 16);
		digit_advance_f = (u16)(digit_advance & 0xffff);
		if (digit_advance_i > max_i) {
			max_i = digit_advance_i;
			max_f = digit_advance_f;
		} else if (digit_advance_i == max_i) {
			if (digit_advance_f > max_f) {
				max_i = digit_advance_i;
				max_f = digit_advance_f;
			}
		}
		++i;
	}
	return max;
}
/* template:S100% where S is the a volume symbol */
STATIC void volume_pen_start_location_compute(u16 width, u16 height)
{
	u16 steps;
	u16 indicator_start_column;
	s16 indicator_width;  /* we can start as a negative number */
	u32 advance;
	u16 advance_i;
	u16 advance_f;
	/*
	 * we must take into account the "full" width of the left glyph, namely
	 * the volume symbol, and the "full" width of the right glyph, namely
	 * the percent glyph
	 */
	/*
	 * we add or remove some colums, that depends on the "left" position of
	 * the glyph bitmap from the pen position: if the left border of the
	 * bitmap is "before" the pen position ("left" is <0), we add some
	 * colmuns. if the left border of the bitmap is "after" the pen
	 * position ("left is >=0), we remove some columns.  (yes, this is
	 * excessive brain f*ckage)
	 */
	indicator_width = (s16)(-cache_l.gs[G_PERCENT_SIGN].bg->left);
	advance = (u32)cache_l.gs[G_PERCENT_SIGN].bg->root.advance.x;
	advance_i = (u16)(advance >> 16);
	advance_f = (u16)(advance & 0xffff);
	indicator_width += (s16)(advance_i + (advance_f != 0 ? 1 : 0));
	/* for the digits, we account only the advance width */
	advance = (u32)cache_l.gs[G_1].bg->root.advance.x;
	advance_i = (u16)(advance >> 16);
	advance_f = (u16)(advance & 0xffff);
	indicator_width += (s16)(advance_i + (advance_f != 0 ? 1 : 0));
	advance = digit_advance_max();
	advance_i = (u16)(advance >> 16);
	advance_f = (u16)(advance & 0xffff);
	indicator_width += 2 * (advance_i + (advance_f != 0 ? 1 : 0));
	/* we must account of the "full width of the last symbol */
	indicator_width += (s16)cache_l.gs[G_PERCENT_SIGN].bg->left;
	/* divided by 3, because the bitmap is lcd decimated, see freetype doc*/
	indicator_width += (s16)(cache_l.gs[G_PERCENT_SIGN].bg->bitmap.width
									/ 3);
	volume_l.baseline = height / 2; /* ~ middle of the screen */
	/*
	 * should be "enough", and we re-adjust the start pen position based
	 * on the columns we did add or remove
	 */
	volume_l.start_column = (width - (u16)indicator_width) / 2;
	volume_l.start_column = (u16)((s16)volume_l.start_column
				- (s16)cache_l.gs[G_PERCENT_SIGN].bg->left);
}
/*
 * in order:
 *   - fmt
 *   - audio st (we are audio driven)
 *   - video st
 */
STATIC bool duration_resolve(int64_t *duration, avutil_rational_t *tb)
{
	if (npv_fmt_ctx_p->duration != AV_NOPTS_VALUE) {
		*duration = npv_fmt_ctx_p->duration;	
		memcpy(tb, &AV_TIME_BASE_Q, sizeof(*tb));
		return true;
	}
	if (npv_fmt_ctx_p->streams[npv_audio_st_p.idx]->duration != AV_NOPTS_VALUE) {
		*duration =
			npv_fmt_ctx_p->streams[npv_audio_st_p.idx]->duration;
		memcpy(tb, &(npv_fmt_ctx_p->streams[npv_audio_st_p.idx]->tb),
								sizeof(*tb));
		return true;
	}
	if (npv_fmt_ctx_p->streams[npv_video_st_p.idx]->duration != AV_NOPTS_VALUE) {
		*duration =
			npv_fmt_ctx_p->streams[npv_video_st_p.idx]->duration;
		memcpy(tb, &(npv_fmt_ctx_p->streams[npv_video_st_p.idx]->tb),
								sizeof(*tb));
		return true;
	}
	return false;
}
STATIC u8 *timer_to_gs(int64_t ts, avutil_rational_t tb)
{
	/* P = Play, S = minus Sign */
	static u8 gs[sizeof(TIMER_TMPL_MAX)];
	u8 i;
	bool is_neg;
	int64_t remaining;
	int64_t hours_n;
	int64_t hours_d;
	int64_t hours_dx;
	int64_t mins_n;
	int64_t secs_n;
	int64_t msecs_n;
	int64_t one_hour; /* in ffmpeg time_base units */
	int64_t one_min; /* in ffmpeg time_base units */
	int64_t one_sec; /* in ffmpeg time_base units */
	int64_t one_msec; /* in ffmpeg time_base units */
	int64_t duration;
	avutil_rational_t duration_tb;

	i = 0;
#ifndef NPV_FACES_ASCII_ONLY
	if (npv_paused_p)
		gs[i++] = G_PAUSE;
	else
		gs[i++] = G_PLAY;
#else
	if (npv_paused_p) {
		gs[i] = G_VERTICAL_BAR;
		gs[i + 1] = G_VERTICAL_BAR;
		i += 2;
	} else
		gs[i++] = G_GREATER_THAN;
#endif

	if (ts < 0) {
		ts = -ts;
		is_neg = true;
		gs[i++] = G_MINUS;
	} else
		is_neg = false;

	one_hour = INT64_C(3600) * (int64_t)tb.den / (int64_t)tb.num;
	one_min = INT64_C(60) * (int64_t)tb.den / (int64_t)tb.num;
	one_sec = (int64_t)tb.den / (int64_t)tb.num;
	one_msec = one_sec / INT64_C(1000);

	hours_n = ts / one_hour;
	if (hours_n >= 100) {
		gs[i++] = G_QUESTION_MARK;
		gs[i++] = G_QUESTION_MARK;
	} else {
		gs[i++] = hours_n / 10 + G_0;
		gs[i++] = hours_n % 10 + G_0;
	}
	gs[i++] = G_COLON;

	remaining = ts % one_hour;	
	mins_n = remaining / one_min;
	gs[i++] = mins_n / 10 + G_0;
	gs[i++] = mins_n % 10 + G_0;

	gs[i++] = G_COLON;

	remaining = remaining % one_min;
	secs_n = remaining / one_sec;
	gs[i++] = secs_n / 10 + G_0;
	gs[i++] = secs_n % 10 + G_0;

	gs[i++] = G_SOLIDUS;
	/*--------------------------------------------------------------------*/
	/* duration */
	if (!duration_resolve(&duration, &duration_tb)) {
		gs[i++] = G_QUESTION_MARK;
		gs[i++] = G_QUESTION_MARK;
		gs[i++] = G_COLON;
		gs[i++] = G_QUESTION_MARK;
		gs[i++] = G_QUESTION_MARK;
		gs[i++] = G_COLON;
		gs[i++] = G_QUESTION_MARK;
		gs[i++] = G_QUESTION_MARK;
		goto exit;
	}
	one_hour = INT64_C(3600) * (int64_t)duration_tb.den
						/ (int64_t)duration_tb.num;
	one_min = INT64_C(60) * (int64_t)duration_tb.den
						/ (int64_t)duration_tb.num;
	one_sec = (int64_t)duration_tb.den / (int64_t)duration_tb.num;
	one_msec = one_sec / INT64_C(1000);

	hours_n = duration / one_hour;
	if (hours_n >= 100) {
		gs[i++] = G_QUESTION_MARK;
		gs[i++] = G_QUESTION_MARK;
	} else {
		gs[i++] = hours_n / 10 + G_0;
		gs[i++] = hours_n % 10 + G_0;
	}
	gs[i++] = G_COLON;

	remaining = duration % one_hour;	
	mins_n = remaining / one_min;
	gs[i++] = mins_n / 10 + G_0;
	gs[i++] = mins_n % 10 + G_0;

	gs[i++] = G_COLON;

	remaining = remaining % one_min;
	secs_n = remaining / one_sec;
	gs[i++] = secs_n / 10 + G_0;
	gs[i++] = secs_n % 10 + G_0;
	/*--------------------------------------------------------------------*/
exit:
	gs[i] = GS_N; /* perfect hash ! (ahah) */
	return gs;
}
struct pen_t {
	s16 x;
	s16 y;
};
#define BLEND	0
STATIC void pix_do(u8 mode, u8 r, u8 g, u8 b, u8 *scaler_pix, u8 *restore_pix)
{
	if (mode == BLEND) {
		restore_pix[0] = scaler_pix[0];
		restore_pix[1] = scaler_pix[1];
		restore_pix[2] = scaler_pix[2];
		/* we presume a BGRA pixel */
		scaler_pix[0] = b;
		scaler_pix[1] = g;
		scaler_pix[2] = r;
	} else { /* RESTORE */
		scaler_pix[0] = restore_pix[0];
		scaler_pix[1] = restore_pix[1];
		scaler_pix[2] = restore_pix[2];
	}
}
#undef BLEND
/* FT_BitmapGlyph is a ptr */
#define BLEND	0
STATIC void g_x(bool mode, FT_BitmapGlyph gb, struct pen_t *p)
{
	s16 x;
	s16 y;
	s16 gb_x;
	s16 gb_y;

	/* if top is 0, the bitmap top line is the baseline */
	y = p->y - gb->top; 
	gb_y = 0;
	loop {
		if (gb_y == gb->bitmap.rows)
			break;
		x = p->x + gb->left;
		gb_x = 0;
		loop {

			if (gb_x == gb->bitmap.width)
				break;
			if (x >= 0 && y >= 0 && x < restore_l.width
						&& y < restore_l.height) {
				u8 *scaler_pix;
				u8 *restore_pix;
				u32 offset;
				u8 *gb_sdf; /* ptr on sdf val */
				/* 3 + 1 = BGR + A */
				offset = restore_l.line_bytes_n * y + (3 + 1)
									* x;
				scaler_pix = (u8*)restore_l.scaler_pixs
								+ offset;
				restore_pix = (u8*)restore_l.pixs + offset;

				gb_sdf = gb->bitmap.buffer + gb->bitmap.pitch
								* gb_y + gb_x;
				/* the spread is 2 pixels */
				if (128 <= *gb_sdf) {
					if (*gb_sdf < 192) /* may end up in user configuration */
						pix_do(mode, 0, 0, 0, scaler_pix,
								restore_pix);
					else {
						if (*gb_sdf != 255) {
							/* [128,254] -> [0,127] * 2 */
							u8 gb_sdf_rescale = (0x7f & *gb_sdf) << 1;
							pix_do(mode, gb_sdf_rescale, gb_sdf_rescale,
									gb_sdf_rescale, scaler_pix,
										restore_pix);
						} else {
							pix_do(mode, 0xff, 0xff, 0xff,
								scaler_pix, restore_pix);
						}
					}
				}
			}
			++gb_x;
			++x;
		}
		++gb_y;
		++y;
	}
}
#undef BLEND
#define BLEND 0
STATIC void rop_x(u8 mode, u8 *gs)
{
	struct pen_t pen;	
	u8 i;

	if (mode == BLEND)
		memcpy(restore_l.timer_gs, gs, sizeof(TIMER_TMPL_MAX));
	pen.x = 0;
	pen.y = timer_baseline_l;
	i = 0;
	loop {
		FT_BitmapGlyph bg;
		u32 advance_x;
		s16 advance_x_i;
		u16 advance_x_f;

		if (gs[i] == GS_N)
			break;
		bg = cache_l.gs[gs[i]].bg;
		g_x(mode, bg, &pen);
		advance_x = bg->root.advance.x;
		advance_x_i = advance_x >> 16;
		advance_x_f = advance_x & 0xffff;
		pen.x += advance_x_i + (s16)(advance_x_f != 0 ? 1 : 0);
		++i;
	}
}
#undef BLEND
