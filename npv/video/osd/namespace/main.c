#ifndef CLEANUP
#define cache_l					npv_video_osd_cache_l
#define cache_nodes_release			npv_video_osd_cache_nodes_release
#define digit_advance_max			npv_video_osd_digit_advance_max
#define duration_resolve			npv_video_osd_duration_resolve
#define face_lookup				npv_video_osd_face_lookup
#define face_requester				npv_video_osd_face_requester
#define fatal					npv_video_osd_fatal
#define g_x					npv_video_osd_g_x
#define gs_init_once				npv_video_osd_gs_init_once
#define hinting_engine_str			npv_video_osd_hinting_engine_str
#define library_l				npv_video_osd_library_l
#define our_finalizer				npv_video_osd_our_finalizer
#define out_tt_interpreters			npv_video_osd_out_tt_interpreters
#define pen_t					npv_video_osd_pen_t
#define pix_do					npv_video_osd_pix_do
#define pout					npv_video_osd_pout
#define restore_l				npv_video_osd_restore_l
#define rop_x					npv_video_osd_rop_x
#define timer_baseline_compute			npv_video_osd_baseline_compute
#define timer_baseline_l			npv_video_osd_timer_baseline_l
#define timer_on				npv_video_osd_timer_on
#define timer_to_gs				npv_video_osd_timer_to_gs
#define volume_l				npv_video_osd_volume_l
#define volume_pen_start_location_compute	npv_video_osd_volume_pen_start_location_compute
#define warning					npv_video_osd_warning
/*----------------------------------------------------------------------------*/
#else
#undef cache_l
#undef cache_nodes_release
#undef digit_advance_max
#undef duration_resolve
#undef face_lookup
#undef face_requester
#undef fatal
#undef g_x
#undef gs_init_once
#undef hinting_engine_str
#undef library_l
#undef our_finalizer
#undef out_tt_interpreters
#undef pen_t
#undef pix_do
#undef pout
#undef restore_l
#undef rop_x
#undef timer_baseline_compute
#undef timer_baseline_l
#undef timer_on
#undef timer_to_gs
#undef volume_l
#undef volume_pen_start_location_compute
#undef warning
#endif
