#ifndef CLEANUP
#define clear_dirty		npv_video_osd_clear_dirty
#define init_once		npv_video_osd_init_once
#define rop_blend		npv_video_osd_rop_blend
#define rop_restore		npv_video_osd_rop_restore
#define update_dimensions	npv_video_osd_update_dimensions
/*----------------------------------------------------------------------------*/
#else
#undef clear_dirty
#undef init_once
#undef update_dimensions
#undef rop_blend
#undef rop_restore
#endif
