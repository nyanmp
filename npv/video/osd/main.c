#ifndef NPV_VIDEO_OSD_MAIN_C
#define NPV_VIDEO_OSD_MAIN_C
#include <stdbool.h>
#include <stdint.h>
#include <stdarg.h>

#include <libavutil/avutil.h>

#include <ft2build.h>
#include FT_FREETYPE_H
#include FT_CACHE_H
#include FT_MODULE_H
#include FT_DRIVER_H
#include FT_LCD_FILTER_H

#include "npv/c_fixing.h"
#include "npv/public.h"
#include "npv/fmt/public.h"
#include "npv/audio/public.h"
#include "npv/video/public.h"
/*----------------------------------------------------------------------------*/
#include "npv/namespace/ffmpeg.h"
#include "npv/video/osd/namespace/public.h"
#include "npv/video/osd/namespace/main.c"
/*----------------------------------------------------------------------------*/
#define G_PERCENT_SIGN			0
#define G_SOLIDUS			1
#define G_0				2
#define G_1				3
#define G_2				4
#define G_3				5
#define G_4				6
#define G_5				7
#define G_6				8
#define G_7				9
#define G_8				10
#define G_9				11
#define G_COLON				12
#define G_QUESTION_MARK			13
#define G_MINUS				14
#define G_PLAY				15
#define G_PAUSE				16
#define G_BLACK_LOWER_RIGHT_TRIANGLE	17
#define G_GREATER_THAN			18
#define G_VERTICAL_BAR			19
#define GS_N				20
#define TIMER_TMPL_MAX			"PPS00:00:00/00:00:00"
#define TIMER_GS_N_MAX			20
/*----------------------------------------------------------------------------*/
#include "npv/video/osd/local/state.frag.c"
/*----------------------------------------------------------------------------*/
#include "npv/video/osd/local/code.frag.c"
#include "npv/video/osd/public/code.frag.c"
/*----------------------------------------------------------------------------*/
#undef G_PERCENT_SIGN
#undef G_SOLIDUS
#undef G_0
#undef G_1
#undef G_2
#undef G_3
#undef G_4
#undef G_5
#undef G_6
#undef G_7
#undef G_8
#undef G_9
#undef G_COLON
#undef G_QUESTION_MARK
#undef G_MINUS
#undef G_PLAY
#undef G_PAUSE
#undef G_BLACK_LOWER_RIGHT_TRIANGLE
#undef GS_N
#undef TIMER_TMPL
#undef TIMER_TMPL_GS_N
/*----------------------------------------------------------------------------*/
#define CLEANUP
#include "npv/namespace/ffmpeg.h"
#include "npv/video/osd/namespace/public.h"
#include "npv/video/osd/namespace/main.c"
#undef CLEANUP
#endif
