#ifndef NPV_VIDEO_OSD_PUBLIC_H
#define NPV_VIDEO_OSD_PUBLIC_H
#include <stdint.h>

#include <ft2build.h>
#include FT_FREETYPE_H
#include FT_GLYPH_H /* TODO: temporary for gb_get */

#include "npv/c_fixing.h"
/*----------------------------------------------------------------------------*/
#include "npv/video/osd/namespace/public.h"
/*----------------------------------------------------------------------------*/
STATIC void clear_dirty(void);
STATIC void init_once(u8 **faces_files);
STATIC void init_once(u8 **faces_files);
STATIC void update_dimensions(void *scaler_pixs, u16 width, u16 height,
							u32 line_bytes_n);
STATIC void rop_blend(s64 now);
STATIC void rop_restore(void);
/*----------------------------------------------------------------------------*/
#define CLEANUP
#include "npv/video/osd/namespace/public.h"
#undef CLEANUP
#endif
