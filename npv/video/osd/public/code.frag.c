STATIC void init_once(u8 **faces_files)
{
	FT_Error ft_r;
	/* adobe or legacy freetype */
	FT_UInt cff;
	FT_UInt type1;
	FT_UInt t1cid;
	/* the different versions of freetype truetype hinters */
	FT_UInt tt_interpreters;
	u8 id; /* face cache_l id, idx in the arrays */

	memset(&cache_l, 0, sizeof(cache_l));
	ft_r = FT_Init_FreeType(&library_l);
	if (ft_r != FT_Err_Ok)
		fatal("freetype:unable to get a freetype2 library_l handle:%d\n", ft_r);
	ft_r = FTC_Manager_New(library_l, 0, 0, 0, face_requester, 0,
							&cache_l.manager);
	if (ft_r != 0)
		fatal("freetype:unable to new a cache_l manager\n");
    	ft_r = FTC_ImageCache_New(cache_l.manager, &cache_l.img_cache);
	if (ft_r != 0)
		fatal("freetype:unable to new a image cache_l\n");
	/* ccf, type1 and t1cid hinting engines */
	ft_r = FT_Property_Get(library_l, "cff", "hinting-engine", &cff);
	if (ft_r != FT_Err_Ok)
		warning("freetype:no cff module or no hinting engine property\n");
	else
		pout("freetype:ccf:using the %s hinting engine\n", hinting_engine_str(cff));
	ft_r = FT_Property_Get(library_l, "type1", "hinting-engine", &type1);
	if (ft_r != FT_Err_Ok)
		warning("freetype:no type1 module or no hinting engine property\n");
	else
		pout("freetype:type1:using the %s hinting engine\n", hinting_engine_str(type1));
	ft_r = FT_Property_Get(library_l, "t1cid", "hinting-engine", &t1cid);
	if (ft_r != FT_Err_Ok)
		warning("freetype:no t1cid module or no hinting engine property\n");
	else
		pout("freetype:t1cid:using the %s hinting engine\n", hinting_engine_str(t1cid));
	/* truetype various hinting engines */
	ft_r = FT_Property_Get(library_l, "truetype", "interpreter-version",
							&tt_interpreters);
	if (ft_r != FT_Err_Ok)
		warning("freetype:no truetype module or no truetype hinting interpreter version\n");
	else {
		pout("freetype:truetype:hinting interpreters 0x%x\n", tt_interpreters);
		out_tt_interpreters(tt_interpreters);
	}
	/* we leave all defaults for the various hinting engines */
	#if 0 // TODO: REMOVE SINCE WE ARE SWITCHING TO MONOCHROME
	/*
	 * the lcd filtering stuff (_rendering_ and _not hinting_) must be
	 * explicitely enabled, if available
	 */
	ft_r = FT_Library_SetLcdFilter(library_l, FT_LCD_FILTER_DEFAULT);
	if (ft_r != FT_Err_Ok)
		warning("freetype:no lcd filtering in this freetype library_l:%d\n", ft_r);
	else
		pout("freetype:default lcd filtering enabled\n");
	#endif
	/*====================================================================*/
	/* prepare our side of the font cache */
	cache_l.faces.files = faces_files;
	id = 0;
	loop { /* count the font files */
		if (cache_l.faces.files[id] == 0)
			break;
		pout("font file:%s\n", cache_l.faces.files[id]);
		++id;
	}
	pout("%u font files provided\n", id);
	cache_l.faces.n = id;
	cache_l.faces.ft = calloc(cache_l.faces.n, sizeof(*cache_l.faces.ft));
	if (cache_l.faces.ft == 0)
		fatal("unable to allocate an array of %u freetype sets of faces\n", cache_l.faces.n);
	/*--------------------------------------------------------------------*/
	/* freetype cache wants face id pointers to be uniq in memory... */
	cache_l.faces.ids = calloc(cache_l.faces.n, sizeof(*cache_l.faces.ids));
	if (cache_l.faces.ids == 0)
		fatal("unable to allocate an array of %u ids\n", cache_l.faces.n);
	id = 0;
	loop {
		if (id == cache_l.faces.n)
			break;
		cache_l.faces.ids[id] = id; /* we did miss something */
		++id;
	}
	/*====================================================================*/
	gs_init_once();
	memset(&restore_l, 0, sizeof(restore_l));
	timer_on = false;
}
STATIC void update_dimensions(void *scaler_pixs, u16 width, u16 height,
							u32 line_bytes_n)
{
	static const FT_Int sdf_cfg_spread = 2;
	static const FT_Int sdf_cfg_flip_sign = 0;
	static const FT_Bool sdf_cfg_overlaps = (FT_Bool)false;
	FT_Error ft_r;
	FT_Face f;
	FTC_ScalerRec scaler_rec;
	u8 i;

	cache_nodes_release();
	/*
	 * choices which may end up in the config file:
	 * XXX: ascender in most font files ~ em height
	 */
	scaler_rec.width = (FT_UInt)width / TIMER_GS_N_MAX \
			* (FT_UInt)npv_em_adjust_numerator / (FT_UInt)npv_em_adjust_denominator;
	scaler_rec.height = scaler_rec.width;
	pout("scaler:heuristic:em to %ux%u pixels from width=%u pixels and user configuration\n", scaler_rec.width, scaler_rec.height, width);
	i = 0;
	loop {
		FT_Glyph g;

		if (i == GS_N)
			break;
		memset(&scaler_rec, 0, sizeof(scaler_rec));
		/* XXX: freetype cache wants ids with uniq ptrs */
		scaler_rec.face_id =
				&(cache_l.faces.ids[cache_l.gs[i].face_id]);
		scaler_rec.width = (FT_UInt)width / TIMER_GS_N_MAX \
			* (FT_UInt)npv_em_adjust_numerator / (FT_UInt)npv_em_adjust_denominator;
		scaler_rec.height = scaler_rec.width;
		scaler_rec.pixel = 1;
		/*
		 * using the SDF render mode in order to draw properly the
 		 * contour of glyphs
		 */
		ft_r = FT_Property_Set(npv_video_osd_library_l, "sdf", "spread", &sdf_cfg_spread);
		if (ft_r != FT_Err_Ok)
			fatal("freetype:unable to set the spread property of the sdf renderer:0x%02x\n",ft_r);
		ft_r = FT_Property_Set(npv_video_osd_library_l, "sdf", "flip_sign", &sdf_cfg_flip_sign);
		if (ft_r != FT_Err_Ok)
			fatal("freetype:unable to set the flip_sign property of the sdf renderer:0x%02x\n",ft_r);
		ft_r = FT_Property_Set(npv_video_osd_library_l, "sdf", "overlaps", &sdf_cfg_overlaps);
		if (ft_r != FT_Err_Ok)
			fatal("freetype:unable to set the overlaps property of the sdf renderer:0x%02x\n",ft_r);
		ft_r = FTC_ImageCache_LookupScaler(cache_l.img_cache,
			&scaler_rec, FT_LOAD_RENDER
					| FT_LOAD_TARGET_(FT_RENDER_MODE_SDF),
			cache_l.gs[i].idx, &g, &cache_l.gs[i].node);
		if (ft_r != FT_Err_Ok)
			fatal("freetype:cache_l:failed to look for glyph %u with our target size:0x%02x\n", i, ft_r);
		if (g->format != FT_GLYPH_FORMAT_BITMAP)
			fatal("freetype:the glyph %u was not rendered in bitmap\n", i);
		cache_l.gs[i].bg = (FT_BitmapGlyph)g;
		++i;
	}
	timer_baseline_compute();
	volume_pen_start_location_compute(width, height);
	restore_l.scaler_pixs = scaler_pixs;
	free(restore_l.pixs);
	restore_l.pixs = calloc(4, (u32)height * line_bytes_n);
	restore_l.width = width;
	restore_l.height = height;
	restore_l.line_bytes_n = line_bytes_n;
	restore_l.dirty = false;
}
#define BLEND 0
STATIC void rop_blend(s64 now)
{
	u8 *timer_gs;

	if (!timer_on || restore_l.dirty)
		return;
	timer_gs = timer_to_gs(now,
				npv_fmt_ctx_p->streams[npv_video_st_p.idx]->tb);
	rop_x(BLEND, timer_gs);
	restore_l.dirty = true;
}
#undef BLEND
#define RESTORE 1
STATIC void rop_restore(void)
{
	if (!restore_l.dirty)
		return;
	rop_x(RESTORE, restore_l.timer_gs); 
	restore_l.dirty = false;
}
#undef RESTORE
STATIC void clear_dirty(void)
{
	restore_l.dirty = false;
}
STATIC void npv_cmd_osd_timer_toggle(void)
{
	if (timer_on) {
		timer_on = false;
		pout("timer off\n");
	} else { 
		timer_on = true;
		pout("timer on\n");
	}
}
