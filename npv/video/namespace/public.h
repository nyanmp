#ifndef CLEANUP
#define aspect_ratio		npv_video_aspect_ratio
#define dec_ctx_cfg		npv_video_dec_ctx_cfg
#define dec_ctx_lock		npv_video_dec_ctx_lock
#define dec_ctx_p		npv_video_dec_ctx_p
#define dec_ctx_unlock		npv_video_dec_ctx_unlock
#define dec_flush		npv_video_dec_flush
#define dec_fr_priv_t		npv_video_dec_fr_priv
#define dec_fr_try_receive	npv_video_dec_fr_try_receive
#define dec_frs_receive_avail	npv_video_dec_frs_receive_avail
#define dec_frs_lock		npv_video_dec_frs_lock
#define dec_frs_p		npv_video_dec_frs_p
#define dec_frs_unlock		npv_video_dec_frs_unlock
#define init_once		npv_video_init_once
#define pkt_q_p			npv_video_pkt_q_p
#define pkts_send		npv_video_pkts_send
#define scaler_p		npv_video_scaler_p
#define st_p			npv_video_st_p
#define timer_fd_p		npv_video_timer_fd_p
#define timer_slow_start	npv_video_timer_slow_start
#define timer_start		npv_video_timer_start
#define timer_evt		npv_video_timer_evt
/*============================================================================*/
#else
#undef aspect_ratio
#undef dec_ctx_cfg
#undef dec_ctx_lock
#undef dec_ctx_p
#undef dec_ctx_unlock
#undef dec_flush
#undef dec_fr_priv_t
#undef dec_fr_try_receive
#undef dec_frs_receive_avail
#undef dec_frs_lock
#undef dec_frs_p
#undef dec_frs_unlock
#undef init_once
#undef pkt_q_p
#undef pkts_send
#undef scaler_p
#undef st_p
#undef timer_fd_p
#undef timer_140fps_start
#undef timer_10fps_start
#undef timer_evt
#endif
