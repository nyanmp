#ifndef CLEANUP
#define avutil_video_fr_ref_t		AVFrame
#define avutil_video_fr_ref_move	av_frame_move_ref
/*============================================================================*/
#else
#undef avutil_video_fr_ref_t
#undef avutil_video_fr_ref_move
#endif
