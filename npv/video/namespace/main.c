#ifndef CLEANUP
/*----------------------------------------------------------------------------*/
#define avcodec_receive_video_fr	avcodec_receive_frame
#define avutil_video_fr_ref_alloc	av_frame_alloc
#define avutil_video_fr_unref		av_frame_unref
/*----------------------------------------------------------------------------*/
#define are_sems_available		npv_video_are_sems_available
#define blit_compute_offsets		npv_video_blit_compute_offsets
#define blit_l				npv_video_blit_l
#define blit_vp_t			npv_video_blit_vp_t
#define blit_setup			npv_video_blit_setup
#define blits_request_update		npv_video_blits_request_update
#define dec_a_grow			npv_video_dec_a_grow
#define dec_ctx_mutex_l			npv_video_dec_ctx_mutex_l
#define dec_l				npv_video_dec_l
#define discont_backward_handle		npv_video_discont_backward_handle
#define fatal				npv_video_fatal
#define fatalff				npv_video_fatalff
#define fatalvk				npv_video_fatalvk
#define fr_drop				npv_video_fr_drop
#define frs_drop			npv_video_frs_drop
#define frs_clear_last_qed_to_pe	npv_video_frs_clear_last_qed_to_pe
#define frs_reset			npv_video_frs_reset
#define img_mem_barrier_run_once	npv_video_img_mem_barrier_run_once
#define init_once_local			npv_video_init_once_local
#define init_once_public		npv_video_init_once_public
#define interrupted_rendering		npv_video_iinterrupted_rendering
#define last_fr_sent_to_pe_l		npv_video_last_fr_sent_to_pe_l
#define match_mem_type			npv_video_match_mem_type
#define normal_rendering		npv_video_normal_rendering
#define poutff				npv_video_poutff
#define receive_fr_l			npv_video_receive_fr_l
#define scaler_img_create		npv_video_scaler_img_create
#define scaler_img_destroy		npv_video_scaler_img_destroy
#define scaler_img_dev_mem_alloc	npv_video_scaler_img_dev_mem_alloc
#define scaler_img_dev_mem_bind		npv_video_scaler_img_dev_mem_bind
#define scaler_img_dev_mem_map		npv_video_scaler_img_dev_mem_map
#define scaler_img_layout_to_general	npv_video_scaler_img_layout_to_general
#define scaler_img_subrsrc_layout_get	npv_video_scaler_img_subrsrc_layout_get
#define select_fr			npv_video_select_fr
#define send_to_pe			npv_video_send_to_pe
#define start_scaling			npv_video_start_scaling
#define start_swpchn_next_img		npv_video_start_swpchn_next_img
#define timer_ack			npv_video_timer_ack
#define tmp_mem_rqmts_l			npv_video_tmp_mem_rqmts_l
#define tmp_scaler_img_mem_rqmts_get	npv_video_tmp_scaler_img_mem_rqmts_get
#define try_alloc_scaler_img_dev_mem	npv_video_try_alloc_scaler_img_dev_mem
#define warning				npv_video_warning
#define warningvk			npv_video_warningvk
/*============================================================================*/
#else
/*----------------------------------------------------------------------------*/
#undef avcodec_receive_video_fr
#undef avutil_video_fr_ref_alloc
#undef avutil_video_fr_unref
/*----------------------------------------------------------------------------*/
#undef are_sems_available
#undef blit_compute_offsets
#undef blit_l
#undef blit_vp_t
#undef blit_setup
#undef blits_request_update
#undef dec_a_grow
#undef dec_ctx_mutex_l
#undef dec_l
#undef discont_backward_handle
#undef fatal
#undef fatalff
#undef fatalvk
#undef fr_drop
#undef frs_clear_last_qed_to_pe
#undef frs_drop
#undef frs_reset
#undef img_mem_barrier_run_once
#undef init_once_local
#undef init_once_public
#undef interrupted_rendering
#undef last_fr_sent_to_pe_l
#undef match_mem_type
#undef normal_rendering
#undef poutff
#undef receive_fr_l
#undef scaler_img_create
#undef scaler_img_destroy
#undef scaler_img_dev_mem_alloc
#undef scaler_img_dev_mem_bind
#undef scaler_img_dev_mem_map
#undef scaler_img_layout_to_general
#undef scaler_img_subrsrc_layout_get
#undef select_fr
#undef send_to_pe
#undef start_scaling
#undef start_swpchn_next_img
#undef timer_ack
#undef tmp_mem_rqmts_l
#undef tmp_scaler_img_mem_rqmts_get
#undef try_alloc_scaler_img_dev_mem
#undef warning
#undef warningvk
#endif
