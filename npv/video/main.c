#ifndef NPV_VIDEO_MAIN_C
#define NPV_VIDEO_MAIN_C
/*
 * code protected with a GNU affero GPLv3 license 
 * copyright (C) 2020 Sylvain BERTRAND
 */
#include <stdbool.h>
#include <stdarg.h>
#include <stdint.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/timerfd.h>
#include <xcb/xcb.h>
#include <libavcodec/avcodec.h>
#include <libavutil/frame.h>
#include <libavutil/pixdesc.h>
#include "npv/c_fixing.h"
#include "npv/nyanvk/consts.h"
#include "npv/nyanvk/types.h"
#include "npv/global.h"
#include "npv/public.h"
#include "npv/fmt/public.h"
#include "npv/clk/public.h"
#include "npv/xcb/public.h"
#include "npv/thdsws/public.h"
#include "npv/pkt_q/public.h"
#include "npv/video/public.h"
#include "npv/video/public.h"
#include "npv/vk/public.h"
#include "npv/vk/api_usage.h"
/*----------------------------------------------------------------------------*/
#include "npv/namespace/ffmpeg.h"
#include "npv/video/namespace/ffmpeg.h"
#include "npv/video/namespace/public.h"
#include "npv/video/namespace/main.c"
/*----------------------------------------------------------------------------*/
#define IF_FATALVVK(...) IF_FATALVK("video:" __VA_ARGS__)
/*----------------------------------------------------------------------------*/
#include "npv/video/local/state.frag.c"
/*----------------------------------------------------------------------------*/
#include "npv/video/local/code.frag.c"
#include "npv/video/public/code.frag.c"
/*----------------------------------------------------------------------------*/
#undef IF_FATALVVK
/*----------------------------------------------------------------------------*/
#define CLEANUP
#include "npv/namespace/ffmpeg.h"
#include "npv/video/namespace/ffmpeg.h"
#include "npv/video/namespace/public.h"
#include "npv/video/namespace/main.c"
#undef CLEANUP
#endif
