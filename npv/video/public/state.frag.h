STATIC avcodec_codec_ctx_t *dec_ctx_p;
STATIC struct npv_pkt_q_t *pkt_q_p;
/*
 * we copy some stream data in the case the stream does vanish or is replaced
 * (don't know how ffmpeg does handle this)
 */
STATIC struct {
	int idx;
	int id;
	avutil_rational_t tb;
	int64_t start_time;
} st_p;
STATIC int timer_fd_p;
/*----------------------------------------------------------------------------*/
struct dec_fr_priv_t;
STATIC struct {
	pthread_mutex_t mutex;

	bool eof_receive; /* "receiving" from the dec returned eof */

	struct {
		s64 prev_now;
		bool resyncing;
	} discont_backward;

	u16 n_max;
	u16 n;
	/* we did not factor the 2 following fields on purpose */
	avutil_video_fr_ref_t **a;
	struct dec_fr_priv_t **priv_a;
} dec_frs_p;
/*----------------------------------------------------------------------------*/
STATIC struct {
	pthread_mutex_t mutex;
	struct {
		struct vk_img_t *vk;
		struct vk_subrsrc_layout_t layout;
		struct vk_dev_mem_t *dev_mem;
		void *data;
		avutil_video_fr_ref_t *fr;
	} img;
	struct npv_thdsws_ctx_t *ctx;
} scaler_p;
/*----------------------------------------------------------------------------*/
STATIC struct {
	u32 width;
	u32 height;
} aspect_ratio;
