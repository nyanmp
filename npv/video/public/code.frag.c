#define NO_FR 0
STATIC void init_once_public(void)
{
	int r;

	/* linux bug: still no CLOCK_MONOTONIC_RAW for timerfd */
	errno = 0;
	timer_fd_p = timerfd_create(CLOCK_MONOTONIC, TFD_NONBLOCK);
	if (timer_fd_p == -1)
		fatal("unable to get a timer file descriptor:%s\n", strerror(errno));
	memset(&st_p, 0, sizeof(st_p));
	pkt_q_p = npv_pkt_q_new("video");
	dec_ctx_p = 0;
	r = pthread_mutex_init(&dec_ctx_mutex_l, 0);
	if (r != 0)
		fatal("unable to create the mutex for the decoder context\n");

	dec_frs_p.eof_receive = false;
	dec_frs_p.discont_backward.prev_now = AV_NOPTS_VALUE;
	dec_frs_p.discont_backward.resyncing = false;
	dec_frs_p.n_max = 0;
	dec_frs_p.n = 0;
	dec_frs_p.a = 0;
	dec_frs_p.priv_a = 0;
	r = pthread_mutex_init(&dec_frs_p.mutex, 0);
	if (r != 0)
		fatal("unable to create the mutex for the array of frames\n");

	scaler_p.img.vk = 0;
	memset(&scaler_p.img.layout, 0, sizeof(scaler_p.img.layout));
	scaler_p.img.dev_mem = 0;
	scaler_p.img.data = 0;
	scaler_p.img.fr = NO_FR;

	last_fr_sent_to_pe_l = NO_FR;

	aspect_ratio.width = 0;
	aspect_ratio.height = 0;

	/* we are targetting AVUTIL_PIX_FMT_RGB32/sRGB */
	scaler_p.ctx = npv_thdsws_init_once();
	if (scaler_p.ctx == 0)
		fatalff("scaler:unable to initialize\n");
}
#undef NO_FR
STATIC void init_once(void)
{
	init_once_public();
	init_once_local();
}
STATIC void dec_ctx_cfg(avcodec_params_t *params)
{
	int r;

	dec_l = avcodec_find_dec(params->codec_id);
	if (dec_l == 0)
		fatalff("unable to find a proper decoder\n");
	avcodec_free_context(&dec_ctx_p);
	dec_ctx_p = avcodec_alloc_ctx(dec_l);
	if (dec_ctx_p == 0)
		fatalff("unable to allocate an decoder context\n");
	/* XXX: useless ? */
	r = avcodec_params_to_ctx(dec_ctx_p, params);
	if (r < 0)
		fatalff("unable to apply stream codec parameters in codec context\n");
	/* XXX: ffmpeg thread count default is 1, set to 0 = auto */
	dec_ctx_p->thread_count = 0;
	r = avcodec_open2(dec_ctx_p, dec_l, 0);
	if (r < 0)
		fatalff("unable to open the decoder context\n");

	/* we will define the video aspect ratio with those values */
	aspect_ratio.width = params->width;
	aspect_ratio.height = params->height;
}
STATIC void timer_start(void)
{
	struct itimerspec t;
	int r;

	memset(&t, 0, sizeof(t));
	/* initial and interval */
	t.it_value.tv_nsec = 1; /* asap */
	t.it_interval.tv_nsec = 2000000; /* 2ms */
	r = timerfd_settime(timer_fd_p, 0, &t, 0);
	if (r == -1)
		fatal("unable to arm the timer\n");
}
STATIC void timer_slow_start(void)
{
	struct itimerspec t;
	int r;

	memset(&t, 0, sizeof(t));
	/* initial and interval */
	t.it_value.tv_nsec = 1; /* asap */
	t.it_interval.tv_nsec = 100000000; /* 100ms */
	r = timerfd_settime(timer_fd_p, 0, &t, 0);
	if (r == -1)
		fatal("unable to arm the timer\n");
}
#define AGAIN	0
#define HAVE_FR	1
#define EOF_DEC	2
STATIC u8 dec_fr_try_receive(void)
{
	int r;

	dec_ctx_lock();
	r = avcodec_receive_video_fr(dec_ctx_p, receive_fr_l);
	dec_ctx_unlock();
	if (r == AVUTIL_AVERROR(EAGAIN))
		return AGAIN;
	else if (r == 0) {
		u16 last;

		dec_frs_lock();
		if (dec_frs_p.n == dec_frs_p.n_max)
			dec_a_grow();
		last = dec_frs_p.n;
		avutil_video_fr_ref_move(dec_frs_p.a[last], receive_fr_l);
		memset(dec_frs_p.priv_a[last], 0, sizeof(**dec_frs_p.priv_a));
		++dec_frs_p.n;
		dec_frs_unlock();
		return HAVE_FR;
	} else if (r == AVUTIL_AVERROR_EOF) {
		poutff("last decoder frame reached (receiving)\n");
		dec_frs_lock();
		dec_frs_p.eof_receive = true;
		dec_frs_unlock();
		return EOF_DEC;
	}
	fatalff("error while receiving frame from the decoder\n");
}
#undef AGAIN
#undef HAVE_FR
#undef EOF_DEC
#define AGAIN	0
#define HAVE_FR	1
#define EOF_DEC	2
STATIC void dec_frs_receive_avail(void) { loop
{
	u8 r;

	r = dec_fr_try_receive();
	if (r == HAVE_FR)
		continue;
	else if (r == AGAIN || r == EOF_DEC)
		break;
}}
#undef AGAIN
#undef HAVE_FR
#undef EOF_DEC
#define NO_FR 0
STATIC void dec_flush(void)
{
	npv_pkt_q_unref_all(pkt_q_p);
	frs_reset();
	dec_frs_p.eof_receive = false;
	last_fr_sent_to_pe_l = NO_FR;
	avcodec_flush_bufs(dec_ctx_p);
}
#undef NO_FR
STATIC void dec_ctx_lock(void)
{
	int r;

	r = pthread_mutex_lock(&dec_ctx_mutex_l);
	if (r != 0)
		fatal("%d:unable to lock the video decoder context\n", r);
}
STATIC void dec_ctx_unlock(void)
{
	int r;

	r = pthread_mutex_unlock(&dec_ctx_mutex_l);
	if (r != 0)
		fatal("%d:unable to unlock the video decoder context\n", r);
}
STATIC void dec_frs_lock(void)
{
	int r;

	r = pthread_mutex_lock(&dec_frs_p.mutex);
	if (r != 0)
		fatal("%d:unable to lock the array of decoder frames\n", r);
}
STATIC void dec_frs_unlock(void)
{
	int r;

	r = pthread_mutex_unlock(&dec_frs_p.mutex);
	if (r != 0)
		fatal("%d:unable to unlock the array of decoder frames\n", r);
}
/* go non-blocking or a worker thread is needed */
#define NO_FR 0
#define TS_FROM_CLK_OK 0
#define READY		0
#define NOT_READY	1
#define SENT		0
/* XXX: we do want to lock the frs q the least amount of time as possible */
STATIC void normal_rendering(void)
{
	u8 r;
	s64 now;
	avutil_video_fr_ref_t  *fr;
	struct dec_fr_priv_t *fr_priv;
	u32 swpchn_img;

	r = npv_clk_get_video_st_ts(&now);
	if (r != TS_FROM_CLK_OK)
		return;
	/* lock --------------------------------------------------------------*/
	dec_frs_lock();
	if (dec_frs_p.n == 0) {
		dec_frs_unlock();
		return;
	}
	select_fr(now, &fr, &fr_priv);
	frs_drop(now, fr);
	discont_backward_handle(now);
	dec_frs_unlock();
	/* unlock ------------------------------------------------------------*/
	if (fr == NO_FR)
		return;
	/* in pause, we "redraw" the same img all the time */
	if (!npv_paused_p && fr == last_fr_sent_to_pe_l)
		return;
	if (npv_thdsws_is_busy(scaler_p.ctx))
		return;
	if (scaler_p.img.fr != fr) {
		start_scaling(fr, fr_priv);
		return;
	}
	if (npv_paused_p)
		npv_video_osd_rop_restore(); /* subtitles will happen here */
	npv_video_osd_rop_blend(now); /* subtitles will happen here */
	/*--------------------------------------------------------------------*/
	/*
	 * are we still sending to pe ?
	 * (we could go fine-grained, namely per swapchain image)
	 */
	vk_get_fence_status(npv_vk_surf_p.dev.fence);
	if (r == vk_not_ready)
		return; /* nope still sending to pe */
	else if (r != vk_success)
		npv_vk_fatal("%d:device:%p:unable to get fence %p status\n", r, npv_vk_surf_p.dev.vk, npv_vk_surf_p.dev.fence);
	/* here, we are finished sending to pe */
	loop { /* because the swpchn can change for many reasons */
		r = start_swpchn_next_img(&swpchn_img);
		if (r == NOT_READY)
			return;
		/* here, we started to acquire the next swapchain image */
		blit_setup(swpchn_img);
		r = send_to_pe(swpchn_img);
		if (r == SENT)
			break;
		/* r == SWPCHN_UPDATED */
	}
	last_fr_sent_to_pe_l = fr;
	fr_priv->was_qed_to_pe = true; /* drop detection */
}
#undef NO_FR
#undef TS_FROM_CLK_OK
#undef READY
#undef NOT_READY
#undef SENT
STATIC void interrupted_rendering(void)
{
	/*
	 * XXX: it is where that pre[fill|decode] indicators ("interrupted")
	 * should be implemented with with an alternative code path. Here,
	 * merging "normal" and "interrupted" code path would probably lead to
	 * excessive code factorization at a pathological scale.
	 */
	return;
}
#define NONE 0
STATIC void timer_evt(void)
{
	u8 pre_x;

	timer_ack();	
	pre_x = atomic_load(&npv_pre_x_p);
	if (pre_x == NONE)
		normal_rendering();
	else /* pre[fill|decode] */
		interrupted_rendering();
}
#undef NONE
/* we do per-loop fine-grained locking */
#define sz size
STATIC void pkts_send(void) { loop
{
	int r;
	avcodec_pkt_ref_t *pr;

	npv_pkt_q_lock(pkt_q_p);
	if (pkt_q_p->n == 0) 
		goto unlock_and_return;
	pr = pkt_q_p->q[0];
	dec_ctx_lock();
	r = avcodec_send_pkt(dec_ctx_p, pr);
	dec_ctx_unlock();
	if (r == AVERROR(EAGAIN)) /* dec is full and the pkt is rejected */
		goto unlock_and_return;
	else if (r == AVUTIL_AVERROR_EOF) /* the dec is in draining mode */
		goto unlock_and_return;
	else if (r != 0)
		fatal("error while sending a packet to the decoder\n");
	/* r == 0 */
	npv_pipeline_limits_lock();
	npv_pipeline_limits_p.pkts.bytes_n -= pr->sz;
	npv_pipeline_limits_unlock();

	npv_pkt_q_deq(pkt_q_p);
	avcodec_pkt_unref(pr);
	npv_pkt_q_unlock(pkt_q_p);
	continue;

unlock_and_return:
	npv_pkt_q_unlock(pkt_q_p);
	return;
}}
#undef sz
