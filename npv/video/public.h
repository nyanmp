#ifndef NPV_VIDEO_PUBLIC_H
#define NPV_VIDEO_PUBLIC_H
/*
 * code protected with a GNU affero GPLv3 license 
 * copyright (C) 2020 Sylvain BERTRAND
 */
#include <stdbool.h>
#include <xcb/xcb.h>
#include <libavformat/avformat.h>
#include <libavcodec/avcodec.h>
#include "npv/c_fixing.h"
#include "npv/nyanvk/consts.h"
#include "npv/nyanvk/types.h"
/*----------------------------------------------------------------------------*/
#include "npv/namespace/ffmpeg.h"
#include "npv/video/namespace/ffmpeg.h"
#include "npv/video/namespace/public.h"
/*----------------------------------------------------------------------------*/
#include "npv/video/public/state.frag.h"
/*----------------------------------------------------------------------------*/
STATIC void dec_ctx_cfg(avcodec_params_t *params);
STATIC void dec_flush(void);
STATIC u8 dec_fr_try_receive(void);
STATIC void dec_frs_receive_avail(void);
STATIC void init_once(void);
STATIC void timer_evt(void);
STATIC void timer_start(void);
STATIC void timer_slow_start(void);
STATIC void dec_ctx_lock(void);
STATIC void dec_ctx_unlock(void);
STATIC void dec_frs_lock(void);
STATIC void dec_frs_unlock(void);
STATIC void pkts_send(void);
/*----------------------------------------------------------------------------*/
#define CLEANUP
#include "npv/namespace/ffmpeg.h"
#include "npv/video/namespace/ffmpeg.h"
#include "npv/video/namespace/public.h"
#undef CLEANUP
#endif
