STATIC void fatal(u8 *fmt, ...)
{
	va_list ap;

	npv_perr("video:");
	va_start(ap, fmt);
	npv_vfatal(fmt, ap);
	va_end(ap); /* unreachable */
}
STATIC void warning(u8 *fmt, ...)
{
	va_list ap;

	npv_perr("video:");
	va_start(ap, fmt);
	npv_vwarning(fmt, ap);
	va_end(ap);
}
STATIC void fatalff(u8 *fmt, ...)
{
	va_list ap;

	npv_perr("ffmpeg:");
	va_start(ap, fmt);
	npv_vfatal(fmt, ap);
	va_end(ap); /* unreachable */
}
STATIC void poutff(u8 *fmt, ...)
{
	va_list ap;

	npv_pout("ffmpeg:");
	va_start(ap, fmt);
	npv_vpout(fmt, ap);
	va_end(ap);
}
STATIC void fatalvk(u8 *fmt, ...)
{
	va_list ap;

	npv_perr("video:");
	va_start(ap, fmt);
	npv_vk_vfatal(fmt, ap);
	va_end(ap); /* unreachable */
}
STATIC void warningvk(u8 *fmt, ...)
{
	va_list ap;

	npv_perr("video:");
	va_start(ap, fmt);
	npv_vk_vwarning(fmt, ap);
	va_end(ap);
}
#define NONE 0
STATIC void init_once_local(void)
{
	u8 i;

	dec_l = 0;
	memset(&tmp_mem_rqmts_l, 0, sizeof(tmp_mem_rqmts_l));
	i = 0;
	loop {
		if (i == npv_vk_swpchn_imgs_n_max)
			break;
		blit_l[i].vp.width = -1;
		blit_l[i].vp.height = -1;
		blit_l[i].vp.top_left.x = -1;
		blit_l[i].vp.top_left.y = -1;
		blit_l[i].vp.top_left.z = -1;
		blit_l[i].vp.bottom_right.x = -1;
		blit_l[i].vp.bottom_right.y = -1;
		blit_l[i].vp.bottom_right.z = -1;
		blit_l[i].update_requested = true;
		++i;
	}
	receive_fr_l = avutil_video_fr_ref_alloc();
}
#undef NONE
STATIC void scaler_img_create(avutil_video_fr_ref_t *fr)
{
	struct vk_img_create_info_t info;
	s32 r;

	memset(&info, 0, sizeof(info));
	info.type = vk_struct_type_img_create_info;
	info.img_type = vk_img_type_2d;
	info.texel_mem_blk_fmt = vk_texel_mem_blk_fmt_b8g8r8a8_srgb;
	info.extent.width = (u32)fr->width;
	info.extent.height = (u32)fr->height;
	info.extent.depth = 1;
	info.mip_lvls_n = 1;
	info.samples_n = vk_samples_n_1_bit;
	info.array_layers_n = 1;
	info.img_tiling = vk_img_tiling_linear;
	info.usage = vk_img_usage_transfer_src_bit;
	info.initial_layout = vk_img_layout_undefined;
	vk_create_img(&info, &scaler_p.img.vk);
	IF_FATALVVK("%d:device:%p:unable to create scaler frame image\n", r, npv_vk_surf_p.dev.vk);
}
STATIC void img_mem_barrier_run_once(struct vk_img_mem_barrier_t *b)
{
	s32 r;
	struct vk_cb_begin_info_t begin_info;
	struct vk_submit_info_t submit_info;

	memset(&begin_info, 0, sizeof(begin_info));
	begin_info.type = vk_struct_type_cb_begin_info;
	begin_info.flags = vk_cb_usage_one_time_submit_bit;
	vk_begin_cb(npv_vk_surf_p.dev.misc_cbs[
					npv_vk_color_space_layout_transition],
								&begin_info);
	IF_FATALVVK("%d:unable to begin recording the layout transition command buffer for the vulkan image which will be used by ffmpeg color space converter\n", r, npv_vk_surf_p.dev.misc_cbs[npv_vk_color_space_layout_transition]);
	/*--------------------------------------------------------------------*/
	/* don't wait on any pipe stage, neither make wait any pipe stage */
	vk_cmd_pl_barrier(npv_vk_surf_p.dev.misc_cbs[
					npv_vk_color_space_layout_transition],
		vk_pl_stage_top_of_pipe_bit, vk_pl_stage_bottom_of_pipe_bit,
		0, 0, 1, b);
	/*--------------------------------------------------------------------*/
	vk_end_cb(npv_vk_surf_p.dev.misc_cbs[
					npv_vk_color_space_layout_transition]);
	IF_FATALVVK("%d:unable to end recording of the layout transition command buffer for the vulkan image which will be written to by ffmpeg color space converter\n", r, npv_vk_surf_p.dev.misc_cbs[npv_vk_color_space_layout_transition]);
	/*--------------------------------------------------------------------*/
	memset(&submit_info, 0, sizeof(submit_info));
	submit_info.type = vk_struct_type_submit_info;
	submit_info.cbs_n = 1;
	submit_info.cbs = &npv_vk_surf_p.dev.misc_cbs[
					npv_vk_color_space_layout_transition];
	vk_q_submit(&submit_info, npv_vk_surf_p.dev.fence_color_space_layout);
	IF_FATALVVK("%d:queue:%p:unable to submit the layout transition command buffer for the vulkan image which will be written to by ffmpeg color space converter\n", r, npv_vk_surf_p.dev.q);
	/*--------------------------------------------------------------------*/
	/*
	 * we cannot leave without the transition being done since we will
	 * fire up the ffmpeg color space converter right away , then wait for
	 * this fence to signal
	 */
	vk_wait_for_fences(&npv_vk_surf_p.dev.fence_color_space_layout);
	if (r == vk_timeout)
		npv_vk_fatal("%d:device:%p:wait for fence %p timeout\n", r, npv_vk_surf_p.dev.vk, npv_vk_surf_p.dev.fence_color_space_layout);
	else if (r != vk_success)
		npv_vk_fatal("%d:device:%p:unable to wait for fence %p\n", r, npv_vk_surf_p.dev.vk, npv_vk_surf_p.dev.fence_color_space_layout);
	vk_reset_fences(&npv_vk_surf_p.dev.fence_color_space_layout);
	IF_FATALVVK("%d:device:%p:unable to reset the color space layout fence\n", r, npv_vk_surf_p.dev.vk, npv_vk_surf_p.dev.fence_color_space_layout);
	/*--------------------------------------------------------------------*/
	/*
	 * since it is tagged to run once its state_p is invalid, we need to
	 * reset it to the initial state_p
	 */
	vk_reset_cb(npv_vk_surf_p.dev.misc_cbs[
					npv_vk_color_space_layout_transition]);
	IF_FATALVVK("%d:command buffer:%p:unable to reset the layout transition command buffer for the vulkan image which will be written to by the ffmpeg color space converter\n", r, npv_vk_surf_p.dev.misc_cbs[npv_vk_color_space_layout_transition]);
}
/* once in general layout, the dev sees the img */
STATIC void scaler_img_layout_to_general(void)
{
	struct vk_img_mem_barrier_t b;
	struct vk_img_subrsrc_range_t *r;

	memset(&b, 0, sizeof(b));
	b.type = vk_struct_type_img_mem_barrier;
	b.old_layout = vk_img_layout_undefined;
	b.new_layout = vk_img_layout_general;
	b.src_q_fam = vk_q_fam_ignored;
	b.dst_q_fam = vk_q_fam_ignored;
	b.img = scaler_p.img.vk;
	r = &b.subrsrc_range;
	r->aspect = vk_img_aspect_color_bit;
	r->lvls_n = 1;
	r->array_layers_n = 1;
	img_mem_barrier_run_once(&b);
}
STATIC void scaler_img_subrsrc_layout_get(void)
{
	struct vk_img_subrsrc_t s;

	memset(&s, 0, sizeof(s));
	/* 1 subrsrc = uniq color plane of mip lvl 0 and array 0 */
	s.aspect = vk_img_aspect_color_bit;
	vk_get_img_subrsrc_layout(scaler_p.img.vk, &s, &scaler_p.img.layout);
}
STATIC void tmp_scaler_img_mem_rqmts_get(void)
{
	struct vk_img_mem_rqmts_info_t info;
	struct vk_mem_rqmts_t *rqmts;
	s32 r;

	memset(&info, 0, sizeof(info));
	info.type = vk_struct_type_img_mem_rqmts_info;
	info.img = scaler_p.img.vk;
	rqmts = &tmp_mem_rqmts_l;
	memset(rqmts, 0, sizeof(*rqmts));
	rqmts->type = vk_struct_type_mem_rqmts;
	vk_get_img_mem_rqmts(&info, rqmts);
	IF_FATALVVK("%d:device:%p:unable to get memory requirements for scaler image\n", r, npv_vk_surf_p.dev.vk);
}
/* we want coherent memory to avoid to have to flush the memory range */
#define WANTED_MEM_PROPS (vk_mem_prop_host_visible_bit \
| vk_mem_prop_host_cached_bit | vk_mem_prop_host_coherent_bit)
#define IS_DEV_LOCAL(x) (((x)->prop_flags & vk_mem_prop_dev_local_bit) != 0)
STATIC bool match_mem_type(u8 mem_type_idx,
		struct vk_mem_rqmts_t *img_rqmts, bool ignore_gpu_is_discret)
{
	struct vk_mem_type_t *mem_type;

	/* first check this mem type is in our img rqmts */
	if (((1 << mem_type_idx) & img_rqmts->core.mem_type_bits) == 0)
		return false;
	mem_type = &npv_vk_surf_p.dev.phydev.mem_types[mem_type_idx];
	if (!ignore_gpu_is_discret)
		if (npv_vk_surf_p.dev.phydev.is_discret_gpu
						&& IS_DEV_LOCAL(mem_type))
			return false;
	if ((mem_type->prop_flags & WANTED_MEM_PROPS) == WANTED_MEM_PROPS)
		return true;
	return false;
}
#undef WANTED_MEM_PROPS
#undef IS_DEV_LOCAL
STATIC bool try_alloc_scaler_img_dev_mem(struct vk_mem_rqmts_t *img_rqmts,
								u8 mem_type_idx)
{
	struct vk_mem_alloc_info_t info;
	s32 r;

	memset(&info, 0, sizeof(info));
	info.type = vk_struct_type_mem_alloc_info;
	info.sz = img_rqmts->core.sz;
	info.mem_type_idx = mem_type_idx;
	vk_alloc_mem(&info, &scaler_p.img.dev_mem);
	if (r < 0) {
		warningvk("%d:device:%p:unable to allocate %lu bytes from physical dev %p memory type %u\n", r, npv_vk_surf_p.dev.vk, img_rqmts->core.sz, npv_vk_surf_p.dev.phydev.vk, mem_type_idx);
		return false;
	}
	return true;
}
/*
 * we are looking for host visible and host cached mem. on discret gpu we would
 * like non dev local mem that in order to avoid wasting video ram. if we have
 * a discret gpu but could not find a mem type without dev local mem, let's
 * retry with only host visible and host cached mem.
 */
#define IGNORE_GPU_IS_DISCRET true
STATIC void scaler_img_dev_mem_alloc(void)
{
	struct vk_mem_rqmts_t *img_rqmts;
	u8 mem_type;

	img_rqmts = &tmp_mem_rqmts_l;
	mem_type = 0;
	loop {
		if (mem_type == npv_vk_surf_p.dev.phydev.mem_types_n)
			break;
		if (match_mem_type(mem_type, img_rqmts, !IGNORE_GPU_IS_DISCRET))
			if (try_alloc_scaler_img_dev_mem(img_rqmts, mem_type))
				return;
		++mem_type;
	}
	if (!npv_vk_surf_p.dev.phydev.is_discret_gpu)
		fatalvk("physical device:%p:scaler image:unable to find proper memory type or to allocate memory\n", npv_vk_surf_p.dev.phydev.vk);
	/*
	 * lookup again, but relax the match based on discret gpu constraint for 
	 * gpu
	 */
	mem_type = 0;
	loop {
		if (mem_type == npv_vk_surf_p.dev.phydev.mem_types_n)
			break;
		if (match_mem_type(mem_type, img_rqmts, IGNORE_GPU_IS_DISCRET)
			&& try_alloc_scaler_img_dev_mem(img_rqmts, mem_type))
				return;
		++mem_type;
	}
	fatalvk("physical device:%p:unable to find proper memory type or to allocate memory\n", npv_vk_surf_p.dev.phydev.vk);
}
#undef IGNORE_GPU_IS_DISCRET
STATIC void scaler_img_dev_mem_bind(void)
{
	struct vk_bind_img_mem_info_t info;
	s32 r;

	memset(&info, 0, sizeof(info) * 1);
	info.type = vk_struct_type_bind_img_mem_info;
	info.img = scaler_p.img.vk;
	info.mem = scaler_p.img.dev_mem;
	/*
	 * TODO: switch to vkBindImageMemory2 if extension in vk 1.1 for
	 * consistency
	 */
	vk_bind_img_mem(&info);
	IF_FATALVVK("%d:device:%p:scaler image:unable to bind device memory to image\n", r, npv_vk_surf_p.dev.vk);
}
STATIC void scaler_img_dev_mem_map(void)
{
	s32 r;

	vk_map_mem(scaler_p.img.dev_mem, &scaler_p.img.data);
	IF_FATALVVK("%d:device:%p:scaler image:unable to map image memory\n", r, npv_vk_surf_p.dev.vk);
}
STATIC void dec_a_grow(void)
{
	u16 new_idx;

	new_idx = dec_frs_p.n_max;
	dec_frs_p.a = realloc(dec_frs_p.a, sizeof(*dec_frs_p.a)
						* (dec_frs_p.n_max + 1));
	if (dec_frs_p.a == 0)
		fatal("unable to allocate memory for an additional pointer on a decoded frame reference\n");
	dec_frs_p.priv_a = realloc(dec_frs_p.priv_a,
			sizeof(*dec_frs_p.priv_a) * (dec_frs_p.n_max + 1));
	if (dec_frs_p.priv_a == 0)
		fatal("unable to allocate memory for an additional pointer on private data for decoded frames\n");

	dec_frs_p.a[new_idx] = avutil_video_fr_ref_alloc();
	if (dec_frs_p.a[new_idx] == 0)
		fatal("ffmpeg:unable to allocate a decoded frame reference\n");
	dec_frs_p.priv_a[new_idx] = calloc(1, sizeof(**dec_frs_p.priv_a));
	if (dec_frs_p.priv_a[new_idx] == 0)
		fatal("unable to allocate decoded frame private data\n");

	++dec_frs_p.n_max;
}
#define NO_FR 0
/* extract a fr ref, shift the a, push it back at the e, and unref its bufs */
STATIC void fr_drop(u16 fr)
{
	struct dec_fr_priv_t *priv_save;
	avutil_video_fr_ref_t *save;

	priv_save = dec_frs_p.priv_a[fr];
	if (!priv_save->was_qed_to_pe)
		warning("dropping undisplayed frame\n");
	save = dec_frs_p.a[fr];
	avutil_video_fr_unref(save);
	memset(priv_save, 0, sizeof(*priv_save));
	if (dec_frs_p.n_max > 1) {
		u16 e;

		e = dec_frs_p.n_max;
		memmove(&dec_frs_p.a[fr], &dec_frs_p.a[fr + 1],
					sizeof(*dec_frs_p.a) * (e - (fr + 1)));	
		dec_frs_p.a[e - 1] = save;

		memmove(&dec_frs_p.priv_a[fr], &dec_frs_p.priv_a[fr + 1],
				sizeof(*dec_frs_p.priv_a) * (e - (fr + 1)));	
		dec_frs_p.priv_a[e - 1] = priv_save;
	}
	dec_frs_p.n--;
}
#undef NO_FR
#define NO_FR 0
STATIC void frs_drop(s64 now, avutil_video_fr_ref_t *selected_fr)
{
	u16 fr;

	if (selected_fr == NO_FR)
		return;
#ifdef NPV_DEBUG
	if (npv_debug_p)
		npv_perr("DEBUG:%s:selected_fr.pts=%ld\n", __func__, selected_fr->pts);
#endif
	/*
	 * since the frs are supposed inorder, first drop as many of as possible
	 * frs received before the selected one
	 */
	fr = 0;
	loop {
		if (dec_frs_p.a[fr] == selected_fr)
			break;
		/* don't touch the scaler fr */
		if (dec_frs_p.a[fr] != scaler_p.img.fr) {
			if (dec_frs_p.a[fr] == last_fr_sent_to_pe_l)
				last_fr_sent_to_pe_l = NO_FR;
			fr_drop(fr); /* do not advance */
		} else
			++fr;
	}
}
#undef NO_FR
#define NO_FR 0
STATIC void select_fr(s64 now, avutil_video_fr_ref_t **selected_fr,
					struct dec_fr_priv_t **selected_fr_priv)
{
	u16 fr_idx;
	u16 selected_fr_idx;
	u64 selected_fr_delta;

	fr_idx = 0;
	*selected_fr = NO_FR;
	selected_fr_delta = S64_MAX;
#ifdef NPV_DEBUG
	if (npv_debug_p)
		npv_perr("DEBUG:%s:scanning %u(max=%u) frames, now=%ld\n", __func__, dec_frs_p.n, dec_frs_p.n_max, now);
#endif
	loop {
		u64 delta;

		if (fr_idx == dec_frs_p.n)
			break;
		delta = s64_abs(now - (s64)dec_frs_p.a[fr_idx]->pts);
#ifdef NPV_DEBUG
		if (npv_debug_p)
			npv_perr("DEBUG:%s:frame %u pts=%ld delta=%lu/%lu ms\n", __func__, fr_idx, (s64)dec_frs_p.a[fr_idx]->pts, delta, (delta * (u64)st_p.tb.num * 1000) / (u64)st_p.tb.den);
#endif
		if (delta < selected_fr_delta) {
			*selected_fr = dec_frs_p.a[fr_idx];
			*selected_fr_priv = dec_frs_p.priv_a[fr_idx];
			selected_fr_idx = fr_idx;
			selected_fr_delta = delta;
		}
		++fr_idx;
	}
}
#undef NO_FR
STATIC void frs_reset(void)
{
	u16 fr;

	fr = 0;
	loop {
		if (fr == dec_frs_p.n)
			break;
		avutil_video_fr_unref(dec_frs_p.a[fr]);
		memset(dec_frs_p.priv_a[fr], 0, sizeof(**dec_frs_p.priv_a));
		++fr;
	}
	dec_frs_p.n = 0;
}
STATIC void scaler_img_destroy(void)
{
	vk_destroy_img(scaler_p.img.vk);
	scaler_p.img.vk = 0;
	vk_unmap_mem(scaler_p.img.dev_mem);
	scaler_p.img.data = 0;
	vk_free_mem(scaler_p.img.dev_mem);
	scaler_p.img.dev_mem = 0;
}
STATIC void blit_compute_offsets(u8 swpchn_img, 
					struct vk_extent_2d_t *new_vp)
{
	struct blit_vp_t *vp;
	s32 want_width;
	s32 want_height;

	vp = &blit_l[swpchn_img].vp;
	/*
	 * XXX: THE BOUNDS OF THE BLIT ARE NOT PIXEL OFFSETS! THOSE ARE
	 * INTEGER BOUNDS FOR TEXELS COORDS WHICH ARE TAKEN AT THE CENTER OF
	 * EACH PIXEL: NAMELY LAST TEXEL INTEGER BOUND = LAST PIXEL OFFSET + 1.
	 */
	want_width = new_vp->height * aspect_ratio.width / aspect_ratio.height;
	want_height = new_vp->width * aspect_ratio.height / aspect_ratio.width;
	if (want_width < new_vp->width) {
		s32 gap;

		vp->top_left.y = 0;
		vp->bottom_right.y = new_vp->height;
	
		gap = new_vp->width - want_width;
		vp->top_left.x = gap / 2;	
		vp->bottom_right.x = new_vp->width - gap / 2;
	} else if (want_height < new_vp->height) {
		s32 gap;

		vp->top_left.x = 0;
		vp->bottom_right.x = new_vp->width;

		gap = new_vp->height - want_height;
		vp->top_left.y = gap / 2;
		vp->bottom_right.y = new_vp->height - gap / 2;
	} else {
		vp->top_left.x = 0;
		vp->top_left.y = 0;
		vp->bottom_right.x = new_vp->width;
		vp->bottom_right.y = new_vp->height;
	}
	/* keep track in order to detect change */
	vp->width = new_vp->width;
	vp->height = new_vp->height;
}
STATIC void blit_setup(u8 swpchn_img)
{
	s32 r;
	struct vk_cb_begin_info_t begin_info;
	struct vk_img_mem_barrier_t img_b;
	struct vk_mem_barrier_t mb;
	struct vk_img_blit_t region;
	struct vk_extent_2d_t *current;
	union vk_clr_color_val_t clr_color_val;
	struct vk_img_subrsrc_range_t range;
	struct npv_vk_texel_mem_blk_fmt_t *b8g8r8a8_srgb;
	u32 filt;

	current = &npv_vk_surf_p.dev.phydev.surf_caps.core.current_extent;

	if (!blit_l[swpchn_img].update_requested
			&& blit_l[swpchn_img].vp.width == current->width
			&& blit_l[swpchn_img].vp.height == current->height)
		return;

	blit_compute_offsets(swpchn_img, current);

	/* XXX: the fence is synchronizing the usage of this cb */
	vk_reset_cb(npv_vk_surf_p.dev.cbs[swpchn_img]);
	IF_FATALVVK("%d:swapchain img:%u:command buffer:%p:unable to reset\n", r, swpchn_img, npv_vk_surf_p.dev.cbs[swpchn_img]);
	/*--------------------------------------------------------------------*/
	memset(&begin_info, 0, sizeof(begin_info));
	begin_info.type = vk_struct_type_cb_begin_info;
	vk_begin_cb(npv_vk_surf_p.dev.cbs[swpchn_img], &begin_info);
	IF_FATALVVK("%d:swapchain img:%u:command buffer:%p:unable to begin recording\n", r, swpchn_img, npv_vk_surf_p.dev.cbs[swpchn_img]);
	/*--------------------------------------------------------------------*/
	/*
	 * Acquired img, from undefined layout (whatever layout discarded) to
	 * to general layout.
	 * Initially, acquired images are in undefined layout, it is only after
	 * their first presentation they are returned with a presentation layout
	 * and their content.
	 */
	memset(&img_b, 0, sizeof(img_b));
	img_b.type = vk_struct_type_img_mem_barrier;
	img_b.dst_access = vk_access_memory_write; /* the dst stage will want to clear then write */
	img_b.old_layout = vk_img_layout_undefined;
	img_b.new_layout = vk_img_layout_general;
	img_b.src_q_fam = vk_q_fam_ignored;
	img_b.dst_q_fam = vk_q_fam_ignored;
	img_b.img = npv_vk_surf_p.dev.swpchn.imgs[swpchn_img];
	img_b.subrsrc_range.aspect = vk_img_aspect_color_bit;
	img_b.subrsrc_range.lvls_n = 1;
	img_b.subrsrc_range.array_layers_n = 1;
	/*
	 * don't wait on any stage, block on the transfer stage for the
	 * following clear command
	 */
	vk_cmd_pl_barrier(npv_vk_surf_p.dev.cbs[swpchn_img],
		vk_pl_stage_top_of_pipe_bit, vk_pl_stage_transfer_bit,
		0, 0, 1, &img_b);
	/*--------------------------------------------------------------------*/
	/* clear the viewport with integer black pixels since we work in sRGB */
	memset(&clr_color_val, 0, sizeof(clr_color_val));
	memset(&range, 0, sizeof(range));
	range.aspect = vk_img_aspect_color_bit;
	range.lvls_n = 1;
	range.array_layers_n = 1;
	vk_cmd_clr_color_img(npv_vk_surf_p.dev.cbs[swpchn_img],
				npv_vk_surf_p.dev.swpchn.imgs[swpchn_img],
				vk_img_layout_general, &clr_color_val, 1,
									&range);
	/*--------------------------------------------------------------------*/
	/*
	 * insert a memory barrier between the clear color command and the 
	 * following transfer-blit-scale-filter
	 */
	memset(&mb, 0, sizeof(mb));
	mb.type = vk_struct_type_mem_barrier;
	mb.src_access = vk_access_memory_write; /* finish to clear everything */
	mb.dst_access = vk_access_memory_read | vk_access_memory_write; /* the dst stage will want to filter, let's expect reads and writes */
	vk_cmd_pl_barrier(npv_vk_surf_p.dev.cbs[swpchn_img],
		vk_pl_stage_transfer_bit, vk_pl_stage_transfer_bit,
		1, &mb, 0, 0);
	/*--------------------------------------------------------------------*/
	/* blit from cpu img to pe img */
	memset(&region, 0, sizeof(region));
	region.src_subrsrc.aspect = vk_img_aspect_color_bit;
	region.src_subrsrc.array_layers_n = 1;
	/* scaler */
	region.src_offsets[1].x = scaler_p.ctx->cfg.width;
	region.src_offsets[1].y = scaler_p.ctx->cfg.height;
	region.src_offsets[1].z = 1; /* see vk specs */
	region.dst_subrsrc.aspect = vk_img_aspect_color_bit;
	region.dst_subrsrc.array_layers_n = 1;
	/* xcb viewport */
	memcpy(&region.dst_offsets[0], &blit_l[swpchn_img].vp.top_left,
						sizeof(region.dst_offsets[0]));
	region.dst_offsets[0].z = 0; /* see vk specs */
	memcpy(&region.dst_offsets[1], &blit_l[swpchn_img].vp.bottom_right,
						sizeof(region.dst_offsets[1]));
	region.dst_offsets[1].z = 1; /* see vk specs */
	/* linear filtering */
	b8g8r8a8_srgb = &npv_vk_surf_p.dev.phydev.b8g8r8a8_srgb;
	if (b8g8r8a8_srgb->linear_tiling_has_linear_filt)
		filt = vk_filt_linear;
	else
		filt = vk_filt_nearest;
	vk_cmd_blit_img(npv_vk_surf_p.dev.cbs[swpchn_img], scaler_p.img.vk,
		npv_vk_surf_p.dev.swpchn.imgs[swpchn_img], &region, filt);
	/*--------------------------------------------------------------------*/
	/* acquired img general layout to presentation layout */
	memset(&img_b, 0, sizeof(img_b));
	img_b.type = vk_struct_type_img_mem_barrier;
	img_b.src_access = vk_access_memory_write; /* flush the final writes of the transfer-blit-scale-filter */
	img_b.old_layout = vk_img_layout_general;
	img_b.new_layout = vk_img_layout_present;
	img_b.src_q_fam = vk_q_fam_ignored;
	img_b.dst_q_fam = vk_q_fam_ignored;
	img_b.img = npv_vk_surf_p.dev.swpchn.imgs[swpchn_img];
	img_b.subrsrc_range.aspect = vk_img_aspect_color_bit;
	img_b.subrsrc_range.lvls_n = 1;
	img_b.subrsrc_range.array_layers_n = 1;
	/*
	 * wait on the previous transfer stage, but don't make wait any pipe
	 * stages since we are going to pe
	 */
	vk_cmd_pl_barrier(npv_vk_surf_p.dev.cbs[swpchn_img],
		vk_pl_stage_transfer_bit, vk_pl_stage_bottom_of_pipe_bit,
		0, 0, 1, &img_b);
	/*--------------------------------------------------------------------*/
	vk_end_cb(npv_vk_surf_p.dev.cbs[swpchn_img]);
	IF_FATALVVK("%d:swapchain img:%u:command buffer:%p:unable to end recording\n", r, swpchn_img, npv_vk_surf_p.dev.cbs[swpchn_img]);
	blit_l[swpchn_img].update_requested = false;
}
STATIC void blits_request_update(void)
{
	u8 i;

	i = 0;
	loop {
		if (i == npv_vk_surf_p.dev.swpchn.imgs_n)
			break;
		blit_l[i].update_requested = true;
		++i;
	}
}
#define READY		0
#define NOT_READY	1
STATIC u8 start_swpchn_next_img(u32 *swpchn_img) { loop
{
	struct vk_acquire_next_img_info_t info;
	s32 r;

	memset(&info, 0, sizeof(info));
	info.type = vk_struct_type_acquire_next_img_info;
	info.swpchn = npv_vk_surf_p.dev.swpchn.vk;
	info.timeout = 0; /* then we won't receive vk_timeout, but vk_not_ready */
	info.devs = 0x00000001; /* no device group then 1 */
	info.sem = npv_vk_surf_p.dev.sems[npv_vk_sem_acquire_img_done];
	/*
	 * XXX: for this vk func, the wait sem _MUST_ be unsignaled _AND_ have
	 * all its "wait" operations completed. state we secure with our usage
	 * of a fence.
	 */
	vk_acquire_next_img(&info, swpchn_img);
	if (r == vk_not_ready)
		return NOT_READY;
	else if (r == vk_out_of_date || r == vk_suboptimal) {
		vk_q_wait_idle(); /* very violent, hopefully not too long */
		IF_FATALVVK("%d:queue:%p:unable to wait for idle or completion to prepare for a swpchn update\n", r, npv_vk_surf_p.dev.q);
		npv_vk_swpchn_update();
		blits_request_update();
		continue;
	} else if (r >= 0)
		return READY; /* Careful, the pe may not have finished reading from the acquired image, you need to use the semaphore or the fence to know where the image is actually available. */
	npv_vk_fatal("%d:device:%p:unable to acquire next image from swapchain %p\n", r, npv_vk_surf_p.dev.vk, npv_vk_surf_p.dev.swpchn.vk);
}}
#undef READY
#undef NOT_READY
#define SENT		0
#define SWPCHN_UPDATED	1
STATIC u8 send_to_pe(u32 swpchn_img)
{
	struct vk_submit_info_t submit_info;
	struct vk_present_info_t present_info;
	u32 wait_dst_stage;
	s32 r;
	u32 idxs[1];
	/* run the command buffer and do present queue */
	/*--------------------------------------------------------------------*/
	memset(&submit_info, 0, sizeof(submit_info));
	submit_info.type = vk_struct_type_submit_info;
	submit_info.wait_sems_n = 1;
	/* 
	 * the "semaphore wait operation" will unsignal this semaphore once the
	 * "wait" is done.
	 */
	submit_info.wait_sems =
			&npv_vk_surf_p.dev.sems[npv_vk_sem_acquire_img_done];
	wait_dst_stage = vk_pl_stage_transfer_bit; /* we know our pre-recorded cb will only use the transfer stage */
	submit_info.wait_dst_stages = &wait_dst_stage;
	submit_info.cbs_n = 1;
	submit_info.cbs = &npv_vk_surf_p.dev.cbs[swpchn_img];
	submit_info.signal_sems_n = 1;
	submit_info.signal_sems = &npv_vk_surf_p.dev.sems[npv_vk_sem_blit_done];
	
	vk_reset_fences(&npv_vk_surf_p.dev.fence);/* XXX: coarse synchronization happens here */
	IF_FATALVVK("%d:device:%p:unable to reset the fence\n", r, npv_vk_surf_p.dev.vk, npv_vk_surf_p.dev.fence);

	vk_q_submit(&submit_info, npv_vk_surf_p.dev.fence);
	IF_FATALVVK("%d:queue:%p:unable to submit the image pre-recorded command buffer\n", r, npv_vk_surf_p.dev.q);
	/*--------------------------------------------------------------------*/
	idxs[0] = swpchn_img;
	memset(&present_info, 0, sizeof(present_info));
	present_info.type = vk_struct_type_present_info;
	/* 
	 * the "semaphore wait operation" will unsignal this semaphore once the
	 * "wait" is done.
	 */
	present_info.wait_sems_n = 1;
	present_info.wait_sems = &npv_vk_surf_p.dev.sems[npv_vk_sem_blit_done];
	present_info.swpchns_n = 1;
	present_info.swpchns = &npv_vk_surf_p.dev.swpchn.vk;
	present_info.idxs = idxs;
	present_info.results = 0;
	vk_q_present(&present_info);
	if (r == vk_out_of_date || r == vk_suboptimal) {
		vk_q_wait_idle(); /* very violent, hopefully not too long */
		IF_FATALVVK("%d:queue:%p:unable to wait for idle or completion to prepare for a swpchn update\n", r, npv_vk_surf_p.dev.q);
		npv_vk_swpchn_update();
		blits_request_update();
		return SWPCHN_UPDATED;
	}
	IF_FATALVVK("%d:queue:%p:unable to submit the image %u to the presentation engine\n", r, npv_vk_surf_p.dev.q, swpchn_img);
	return SENT;
}
#undef SENT
#undef SWPCHN_UPDATED
STATIC void start_scaling(avutil_video_fr_ref_t *fr,
						struct dec_fr_priv_t *fr_priv)
{

	if (scaler_p.ctx->cfg.width != fr->width
				|| scaler_p.ctx->cfg.height != fr->height) {
		if (scaler_p.img.vk != 0)
			scaler_img_destroy();
		scaler_img_create(fr);
		scaler_img_layout_to_general();
		scaler_img_subrsrc_layout_get();
		tmp_scaler_img_mem_rqmts_get();
		scaler_img_dev_mem_alloc();
		scaler_img_dev_mem_bind();
		scaler_img_dev_mem_map();

		blits_request_update();
		scaler_p.ctx->cfg.width = fr->width;
		scaler_p.ctx->cfg.height = fr->height;
		npv_video_osd_update_dimensions(scaler_p.img.data,
					(u16)fr->width, (u16)fr->height,
					(u32)scaler_p.img.layout.row_pitch);
	}
	scaler_p.ctx->cfg.src_fmt = fr->fmt;
	scaler_p.ctx->cfg.dst_fmt = AVUTIL_PIX_FMT_RGB32;
	scaler_p.ctx->cfg.flags = SWS_POINT; /* | SWS_PRINT_INFO */

	scaler_p.ctx->scale.src_slices = fr->data;
	scaler_p.ctx->scale.src_strides = fr->linesize;
	scaler_p.ctx->scale.dst_slice = scaler_p.img.data;
	scaler_p.ctx->scale.dst_stride = (u32)scaler_p.img.layout.row_pitch;
	npv_thdsws_run(scaler_p.ctx);
	scaler_p.img.fr = fr;
	/*
	 * saved pixs for restoration are now irrelevant since we did
	 * overwrite everything
	 */
	npv_video_osd_clear_dirty();
}
STATIC void timer_ack(void)
{
	int r;
	uint64_t exps_n;

	exps_n = 0;
	r = read(timer_fd_p, &exps_n, sizeof(exps_n));
	if (r == -1)
		warning("unable to read the number of timer expirations\n");
}
STATIC void discont_backward_handle(s64 now)
{
	s64 lo;
	s64 hi;
	s64 resync;
	u16 fr;
	/*
	 * Backward discont detected, we will look for a fr reasonably in sync:
	 * we will named this "resyncing".
	 * The issue is the dec frs a may not receive any new fr from the
	 * pipeline which would refuse to add more in order to avoid filling
	 * up memory.
	 * Then, we choose to "take the risk" once a backward discontinuity is
	 * detected, namely let the pipeline increase its hard limit and
	 * potentially fill up huge amount of memory. 
	 * This was done to handle some live streams which drops many seconds
	 * of frs and do some backward discontinuity at the same time.
	 * If in normal usage this actually fills up too often the memory,
	 * a tighter strategy would have to be implemented with the shrinking
	 * of the dec frs a.
	 */
	if (dec_frs_p.discont_backward.prev_now != AV_NOPTS_VALUE
				&& now < dec_frs_p.discont_backward.prev_now)
		dec_frs_p.discont_backward.resyncing = true;
	if (!dec_frs_p.discont_backward.resyncing)
		goto exit;
#ifdef NPV_DEBUG
	if (npv_debug_p)
		npv_perr("DEBUG:%s:running resyncing code\n", __func__);
#endif
	resync = (DISCONT_BACKWARD_RESYNC_MS * st_p.tb.den) / (st_p.tb.num
									* 1000);
	lo = now - resync;
	hi = now + resync;
	fr = 0;
	loop {
		if (fr == dec_frs_p.n)
			break;
		if (lo <= dec_frs_p.a[fr]->pts && dec_frs_p.a[fr]->pts <= hi) {
			/*
			 * We have a fr reasonably in sync in the dec frs a,
			 * let's end this.
			 */
			dec_frs_p.discont_backward.resyncing = false;
			break;
		}
		++fr;
	}
exit:
	dec_frs_p.discont_backward.prev_now = now;
}
