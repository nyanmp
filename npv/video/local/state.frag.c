struct dec_fr_priv_t {
	bool was_qed_to_pe;
};
/*===========================================================================*/
STATIC pthread_mutex_t dec_ctx_mutex_l;
STATIC avutil_video_fr_ref_t *receive_fr_l;
STATIC avcodec_codec_t *dec_l;
STATIC struct vk_mem_rqmts_t tmp_mem_rqmts_l;
STATIC struct {
	struct blit_vp_t {
		s32 width;
		s32 height;
		struct vk_offset_3d_t top_left;
		struct vk_offset_3d_t bottom_right;
	} vp;
	bool update_requested;
} blit_l[npv_vk_swpchn_imgs_n_max];
STATIC avutil_video_fr_ref_t *last_fr_sent_to_pe_l;
