#ifndef NPV_PIPELINE_MAIN_C
#define NPV_PIPELINE_MAIN_C
/*
 * code protected with a GNU affero GPLv3 license 
 * copyright (C) 2020 Sylvain BERTRAND
 */
#include <stdbool.h>
#include <stdarg.h>
#include <string.h>
#include <time.h>
#include <signal.h>
#include <stdint.h>
#include <pthread.h>
#include "npv/c_fixing.h"
#include "npv/global.h"
#include "npv/public.h"
#include "npv/pkt_q/public.h"
#include "npv/pipeline/public.h"
#include "npv/fmt/public.h"
#include "npv/audio/public.h"
#include "npv/video/public.h"
/*----------------------------------------------------------------------------*/
#include "npv/namespace/ffmpeg.h"
#include "npv/pipeline/namespace/public.h"
#include "npv/pipeline/namespace/main.c"
/*----------------------------------------------------------------------------*/
#include "npv/pipeline/local/state.frag.c"
/*----------------------------------------------------------------------------*/
#include "npv/pipeline/local/code.frag.c"
#include "npv/pipeline/public/code.frag.c"
/*----------------------------------------------------------------------------*/
#define CLEANUP
#include "npv/namespace/ffmpeg.h"
#include "npv/pipeline/namespace/public.h"
#include "npv/pipeline/namespace/main.c"
#undef CLEANUP
#endif
