#ifndef CLEANUP
#define audio_thd_start		npv_pipeline_audio_thd_start
#define init_once		npv_pipeline_init_once
#define limits_lock		npv_pipeline_limits_lock
#define limits_p		npv_pipeline_limits_p
#define limits_t		npv_pipeline_limits_t
#define limits_unlock		npv_pipeline_limits_unlock
#define limits_reset		npv_pipeline_limits_reset
#define prefill_reset		npv_pipeline_prefill_reset
#define read_thd_start		npv_pipeline_read_thd_start
#define video_thd_start		npv_pipeline_video_thd_start
/*============================================================================*/
#else
#undef audio_thd_start
#undef init_once
#undef limits_lock
#undef limits_p
#undef limits_t
#undef limits_unlock
#undef limits_reset
#undef prefill_reset
#undef read_thd_start
#undef video_thd_start
#endif
