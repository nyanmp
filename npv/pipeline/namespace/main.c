#ifndef CLEANUP
/*----------------------------------------------------------------------------*/
/* some struct fields */
#define frs_n		nb_samples
#define fr_rate		sample_rate
#define sz		size
/*----------------------------------------------------------------------------*/
#define audio					npv_pipeline_audio
#define audio_thd_entry				npv_pipeline_audio_thd_entry
#define eof_pkt_l				npv_pipeline_eof_pkt_l
#define fatal					npv_pipeline_fatal
#define have_enough_predecoded_audio_frs	npv_pipeline_have_enough_predecoded_audio_frs
#define have_enough_predecoded_video_frs	npv_pipeline_have_enough_predecoded_video_frs
#define pout					npv_pipeline_pout
#define rate_limiter_do_block			npv_pipeline_rate_limiter_do_block
#define rate_limiter_previous_tp		npv_pipeline_rate_limiter_previous_tp
#define read					npv_pipeline_read
#define read_thd_entry				npv_pipeline_read_thd_entry
#define timer_ack				npv_pipeline_timer_ack
#define timer_init_once				npv_pipeline_timer_init_once
#define video					npv_pipeline_video
#define video_thd_entry				npv_pipeline_video_thd_entry
#define wait					npv_pipeline_wait
/*============================================================================*/
#else
#undef audio
#undef audio_thd_entry
#undef eof_pkt_l
#undef fatal
#undef have_enough_predecoded_audio_frs
#undef have_enough_predecoded_video_frs
#undef pout
#undef rate_limiter_do_block
#undef rate_limiter_previous_tp
#undef read
#undef read_thd_entry
#undef timer_ack
#undef timer_init_once
#undef video
#undef video_thd_entry
#undef wait
/*----------------------------------------------------------------------------*/
#undef frs_n
#undef fr_rate
#undef sz
/*----------------------------------------------------------------------------*/
#endif

