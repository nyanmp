STATIC void fatal(u8 *fmt, ...)
{
	va_list ap;

	npv_perr("pipeline:");
	va_start(ap, fmt);
	npv_vfatal(fmt, ap);
	va_end(ap); /* unreachable */
}
STATIC void pout(u8 *fmt, ...)
{
	va_list ap;

	npv_pout("pipeline:");
	va_start(ap, fmt);
	npv_vpout(fmt, ap);
	va_end(ap);
}
STATIC void wait(long ns)
{
	struct timespec wanted;
	struct timespec rem;

	memset(&wanted, 0, sizeof(wanted));
	memset(&rem, 0, sizeof(rem));
	wanted.tv_nsec = ns;
	loop {
		int r;

		/* linux bug: cannot specify CLOCK_MONOTONIC_RAW */
		r = clock_nanosleep(CLOCK_MONOTONIC, 0, &wanted, &rem);
		if (r == 0)
			break;
		if (r != EINTR)
			fatal("data wait timer failed:%d\n", r);
		/* r == EINTR */
		memcpy(&wanted, &rem, sizeof(wanted));
		memset(&rem, 0, sizeof(rem));
	}
}
#define EOF_FMT 2
STATIC void read(void) { loop /* infinite loop */
{
	u8 r;
	/*
	 * we do finer-grained locking in there, since we do not want to lock
	 * the pkt qs during slow access
	 */
	r = npv_fmt_pkts_read_and_q();
	if (r == EOF_FMT) {
		npv_pkt_q_lock(npv_audio_pkt_q_p);
		if (!npv_pkt_q_has_eof(npv_audio_pkt_q_p))
			npv_pkt_q_enq(npv_audio_pkt_q_p, eof_pkt_l);
		npv_pkt_q_unlock(npv_audio_pkt_q_p);

		npv_pkt_q_lock(npv_video_pkt_q_p);
		if (!npv_pkt_q_has_eof(npv_video_pkt_q_p))
			npv_pkt_q_enq(npv_video_pkt_q_p, eof_pkt_l);
		npv_pkt_q_unlock(npv_video_pkt_q_p);
	}
	/* r == LIMITS_REACHED || EAGAIN */
	/* should be enough to avoid non kept-alive network connexion */
	wait(100000000); /* 100ms */
}}
#undef EOF_FMT
STATIC void *read_thd_entry(void *arg)
{
	int r;
	sigset_t sset;

	r = sigfillset(&sset);
	if (r == -1)
		fatal("read thread:unable to get a full signal mask\n");
	r = pthread_sigmask(SIG_SETMASK, &sset, 0);
	if (r != 0)
		fatal("read thread:unable to \"block\" \"all\" signals\n");
	read();
	/* unreachable */
}
/*
 * our alsa audio buf is (rate/4)~0.25s (interactivity like vol processing).
 * then we would like to have ~2 times this buf (~double buf) of decoded audio
 * frs. namely we try to have ~(2 *0.25s) of decoded audio frs.
 * due to discontinuity with ts and change of audio properties, we account for
 * the total amount of us based on the rate and frs n.
 */
STATIC bool have_enough_predecoded_audio_frs(void)
{
	u32 set;
	int64_t total_us;
	int64_t total_us_threshold;

	npv_audio_dec_sets_lock();
	if (npv_audio_dec_sets_p.eof_receive) {
		npv_audio_dec_sets_unlock();
		return true;
	} else if (npv_audio_dec_sets_p.n == 0) {
		npv_audio_dec_sets_unlock();
		return false;
	}
	/* from here we have at least 1 sets of audio frs */
	set = 0;
	total_us = 0;
	loop {
		if (set == (npv_audio_dec_sets_p.n - 1))
			break;	
		total_us += npv_audio_dec_sets_p.a[set]->frs_n * 1000000
					/ npv_audio_dec_sets_p.a[set]->fr_rate;
		++set;
	}
	npv_audio_dec_sets_unlock();
	/* 250 * 2 ms */
	total_us_threshold = 250 * 2 * 1000;
	if (total_us >= total_us_threshold)
		return true;	
	return false;
}
#define PREFILL 1
STATIC void audio(void) { loop /* infinite loop */
{
	u8 pre_x;

	pre_x = atomic_load(&npv_pre_x_p);
	if (pre_x != PREFILL && have_enough_predecoded_audio_frs()) {
		/* human ear is sensitive to ~20ms crack, make it 16ms */
		wait(16000000);
		continue;
	}
	/* can be long, finer-grained locking is done in there */
	npv_audio_pkts_send();
	if (pre_x != PREFILL)
		npv_audio_dec_sets_receive_avail();

}}
#undef PREFILL
STATIC void *audio_thd_entry(void *arg)
{
	int r;
	sigset_t sset;

	r = sigfillset(&sset);
	if (r == -1)
		fatal("send thread:unable to get a full signal mask\n");
	r = pthread_sigmask(SIG_SETMASK, &sset, 0);
	if (r != 0)
		fatal("send thread:unable to \"block\" \"all\" signals\n");
	audio();
	/* unreachable */
}
/* will return true to block, false to let it pass */
STATIC bool rate_limiter_do_block(void)
{
	int r;
	struct timespec current_tp;
	s64 current;
	s64 previous;

	memset(&current_tp, 0, sizeof(current_tp));
	r = clock_gettime(CLOCK_MONOTONIC, &current_tp);
	if (r == -1)
		return false; /* error, let it pass */
	previous = (s64)rate_limiter_previous_tp.tv_sec * 1000 +
				(s64)rate_limiter_previous_tp.tv_nsec / 1000000;
	current = (s64)current_tp.tv_sec * 1000
					+ (s64)current_tp.tv_nsec / 1000000;
#ifdef NPV_DEBUG
	if (npv_debug_p)
		npv_perr("DEBUG:%s:delta=%ld\n", __func__, current - previous);
#endif
	if ((current - previous) <= RESYNC_RATE_LIMITER_MS)
		return true;
	memcpy(&rate_limiter_previous_tp, &current_tp, sizeof(current_tp));
	return false;
}
/*
 * predecoded video frs are expensive from the perspectives of the cpu and the
 * mem hierarchy.
 * 1920 * 1080 * 4 bytes = 8MB. we like powers of 2.
 */
STATIC bool have_enough_predecoded_video_frs(void)
{
	bool r;

	npv_video_dec_frs_lock();
	if (npv_video_dec_frs_p.eof_receive) {
		r = true;
	} else if (npv_video_dec_frs_p.discont_backward.resyncing)
		/*
		 * We are looking for a "resyncing" fr, and to secure we 
		 * receive one, we must decode more frs or the dec frs a
		 * may endup full and dead locked.
		 * In pratice, this can burst mem with frs, let's rate
		 * limit it 16 ms (~60fps).
		 */ 
		r = rate_limiter_do_block();
	else if (npv_video_dec_frs_p.n < DEC_FRS_ARRAY_N_MAX) {
		r = false;
	} else /* >= DEC_FRS_ARRAY_N_MAX && ! RESYNCING */
		r = true;
	npv_video_dec_frs_unlock();
	return r;
}
#define PREFILL 1
STATIC void video(void) { loop /* infinite loop */
{	
	u8 pre_x;

	pre_x = atomic_load(&npv_pre_x_p);
	if (pre_x != PREFILL && have_enough_predecoded_video_frs()) {
		wait(8000000); /* 60fps or 16ms, let's check every 8ms */
		continue;
	}
	/*
	 * can be long, finer-grained locking is done in there. filling the dec
	 * bufs should be "faster" than decoding, and we may not have enough
	 * pkts to fill those bufs, then it should exit without filling up
	 * mem.
	 */
	npv_video_pkts_send(); /* fill the dec bufs */
	if (pre_x != PREFILL)
		npv_video_dec_frs_receive_avail();
}}
#undef PREFILL
STATIC void *video_thd_entry(void *arg)
{
	int r;
	sigset_t sset;

	r = sigfillset(&sset);
	if (r == -1)
		fatal("send thread:unable to get a full signal mask\n");
	r = pthread_sigmask(SIG_SETMASK, &sset, 0);
	if (r != 0)
		fatal("send thread:unable to \"block\" \"all\" signals\n");
	video();
	/* unreachable */
}
