STATIC void limits_reset(void)
{
	limits_p.pkts.bytes_n = 0;
	limits_p.pkts.limit.bytes_n = PIPELINE_BYTES_N_MAX;
}
STATIC void prefill_reset(u8 percent)
{
	if (percent > 100)
		fatal("invalid prefill of %u%% for the buffer for packet queues\n", percent);
	if (percent != 0) {
		limits_p.pkts.prefill.bytes_rem =
			limits_p.pkts.limit.bytes_n * percent / 100;
		pout("prefill size for the buffer for packet queues is %u%%/%"PRId64" bytes (100%%=%"PRId64" bytes)\n", percent, limits_p.pkts.prefill.bytes_rem, PIPELINE_BYTES_N_MAX);
	} else {
		limits_p.pkts.prefill.bytes_rem = 0;
		pout("prefill for the buffer for packet queues is disabled\n");
	}
}
STATIC void init_once(void)
{
	int r;

	eof_pkt_l = avcodec_pkt_ref_alloc();
	if (eof_pkt_l == 0)
		fatal("ffmpeg:unable to allocate a null/eof reference on a packet\n");
	eof_pkt_l->data = 0;
	eof_pkt_l->sz = 0;

	r = pthread_mutex_init(&limits_p.mutex, 0);
	if (r != 0)
		fatal("unable to initialize the mutex to guard the accounting of limits\n");
	memset(&rate_limiter_previous_tp, 0, sizeof(rate_limiter_previous_tp));
}
STATIC void limits_lock(void)
{
	int r;

	r = pthread_mutex_lock(&limits_p.mutex);
	if (r != 0)
		fatal("unable to lock the limits\n");
}
STATIC void limits_unlock(void)
{
	int r;

	r = pthread_mutex_unlock(&limits_p.mutex);
	if (r != 0)
		fatal("unable to unlock the limits\n");
}
STATIC void read_thd_start(void)
{
	int r;
	pthread_t id;
	pthread_attr_t attr;

	r = pthread_attr_init(&attr);
	if (r != 0)
		fatal("unable to initialize read thread attribute\n");
	r = pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_DETACHED);
	if (r != 0)
		fatal("unable to set the read thread attribute to detach mode\n");
	r = pthread_create(&id, &attr, &read_thd_entry, 0);
	if (r != 0)
		fatal("unable to create the read thread\n");
	pout("read thread %lu\n", (unsigned long)id);
	pthread_attr_destroy(&attr);
}
STATIC void audio_thd_start(void)
{
	int r;
	pthread_t id;
	pthread_attr_t attr;

	r = pthread_attr_init(&attr);
	if (r != 0)
		fatal("unable to initialize audio thread attribute\n");
	r = pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_DETACHED);
	if (r != 0)
		fatal("unable to set the audio thread attribute to detach mode\n");
	r = pthread_create(&id, &attr, &audio_thd_entry, 0);
	if (r != 0)
		fatal("unable to create the audio thread\n");
	pout("audio thread %lu\n", (unsigned long)id);
	pthread_attr_destroy(&attr);
}
STATIC void video_thd_start(void)
{
	int r;
	pthread_t id;
	pthread_attr_t attr;

	r = pthread_attr_init(&attr);
	if (r != 0)
		fatal("unable to initialize video thread attribute\n");
	r = pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_DETACHED);
	if (r != 0)
		fatal("unable to set the video thread attribute to detach mode\n");
	r = pthread_create(&id, &attr, &video_thd_entry, 0);
	if (r != 0)
		fatal("unable to create the video thread\n");
	pout("video thread %lu\n", (unsigned long)id);
	pthread_attr_destroy(&attr);
}
