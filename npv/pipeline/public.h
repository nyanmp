#ifndef NPV_PIPELINE_PUBLIC_H
#define NPV_PIPELINE_PUBLIC_H
/*
 * code protected with a GNU affero GPLv3 license 
 * copyright (C) 2020 Sylvain BERTRAND
 */
#include <pthread.h>
#include "npv/c_fixing.h"
/*----------------------------------------------------------------------------*/
#include "npv/pipeline/namespace/public.h"
/*----------------------------------------------------------------------------*/
struct limits_t {
	pthread_mutex_t mutex;
	struct {
		u64 bytes_n;
		struct {
			u64 bytes_n; /* arbitrary, init-ed once */
		} limit;
		struct {
			u8 percent;
			s64 bytes_rem;
		} prefill;
	} pkts;
};
/*----------------------------------------------------------------------------*/
#include "npv/pipeline/public/state.frag.h"
/*----------------------------------------------------------------------------*/
STATIC void limits_reset(void);
STATIC void prefill_reset(u8 percent);
STATIC void init_once(void);
STATIC void limits_lock(void);
STATIC void limits_unlock(void);
STATIC void read_thd_start(void);
STATIC void audio_thd_start(void);
STATIC void video_thd_start(void);
/*----------------------------------------------------------------------------*/
#define CLEANUP
#include "npv/pipeline/namespace/public.h"
#undef CLEANUP
#endif
