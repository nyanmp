	lightweight, vulkan, x11, linux, alsa, audio driven and somewhat
	self-limiting media pipeline video file player using ffmpeg. 
	"somewhat self-limiting": _without_ lockless and concurrently accessed
	circular buffers.
BUILDING:
	configure the key bindings and more in "config.h". see
	make-template.sh: this is a "one compilation unit" project. just
	compile main.c and link the compiler output file to your ffmpeg libs
	(we favor the static libs), freetype2 lib (we favor the static lib),
	your alsa lib (we should make this dynamically loaded). the vulkan
	loader lib and x11 xcb libs will be dynamically loaded. you have to
	define the preprocessor macro static as empty or "static". you will
	probably have to define the _gnu_source macro on most gnu/linux systems.
HACKING:
	*.c and *.h files, except the fragments *.frag.c *.frag.h,  must
	"successfully" individually compile (cc -c modulo a few warnings).
	header files are reduced into being forward function declarations, type
	declarations, and "shared" variables.

	In your text editor, we would recommend to have displayed the full file
	paths from the top source dir of the files you are editing, that in
	order to know where those files are in the component hierarchy, if you
	are public or local, etc.

	we introduce naming noise to avoid too much naming collision between
	identifiers:
	  - for struct/union types with a "_t" suffix
	  - for "local" variable names, a "_l" suffix
	  - for "public" variable names, a "_p" suffix
	this allow the project to grow to very large and keep namespace in
	check without the need of beyond sanity ultra complex compiler naming
	system. it is a way more reasonable tradeof and is quite less toxic
	(and in the end it kindof helps to keep track of where things are).
	we could avoid to use the suffixes ("_l" and "_p") for "long/uniquified
	identifier" on variables in global scope, but we chose to keep them.

	we did not use the ffmpeg video scale _filter_, because we did not
	manage to find without a deep code dive "the ffmpeg right way" to
	perform ourself the output buffer allocation, which is supposed to be
	mapped once into gpu accessible mem, then gpu blit-able. we did
	introduce some names inconsistency with scaler and blit: currently
	the ffmpeg scaler is only doing pixel format conversion, but the vulkan
	blit is doing the scaling and the transfer into gpu visible memory.

	additional important points:
	- always keep an eye on the content of the ABBREVIATIONS files. don't
	  worry, you'll get used to them quickly.
	- we did "rewrite" with the preprocessor ffmpeg and alsa identifiers to
	  mostly fit those abbreviations (and as an attempt to tidy a bit their
	  namespace and know where the things are actually going)
	- to lookup for the real symbols, use the output from the preprocessor,
	  or have a quick look at the header files
 	- to check your namespaces: define an empty STATIC macro and look at
	  the symbols in the output of the compiler, for instance using the
	  "objdump" or "readelf" tools.
	- we are using our custom vulkan headers.
	- av*_ (avfilter_, avformat, etc) prefixes are reserved for ffmpeg libs
	  (and the av_* prefix:we don't see it after tidying, but it is still
	  around).
	- sws_* prefix is reserved for the ffmpeg video scaling _library_
	- snd_* prefix is reserved for the alsa lib
	- vk_* prefix is reserved for the vulkan api
	- xcb_* prefix is reserved for x11 xcb client lib
	- all the prefixes from the operating system facilities: epoll_*,
	  pthread_*, etc.
	- _t suffix is reserved for non primitive types (namespace noise and
	  clarity)
	- _l suffix is reserved for "local" variable names (namespace noise
	  and helps knowing where things are)
	- _p suffix is reserved for "public" variable names (namespace noise
	  and helps knowing where things are)
PEDANTIC:
	careful of "standards", here iso c. many times "standards"
	are, to a certain extend, corporation scams. they want you to depend on
	their complexity and then will force you to use their implementations
	because it would be nowhere reasonable to reach the same level of
	compatibilty with modest development efforts. the best example is being
	the hard dependence of linux on gcc (clang, over than 1 decade,is a the
	proof that it would be hard). to approach those, a sane way would be to
	strip them down to a mimimal usage set. for instance, in iso c, function
	pointer and object pointer are not the same at all, due to some broken
	or very old hardware architectures (itanium, 8086 real-mode), but "posix
	standard" makes those pointers the same.
