#ifndef CLEANUP
#define deq		npv_pkt_q_deq
#define eofs_cleanup	npv_pkt_q_eofs_cleanup
#define enq		npv_pkt_q_enq
#define has_eof		npv_pkt_q_has_eof
#define lock		npv_pkt_q_lock
#define new		npv_pkt_q_new
#define pkt_q_t		npv_pkt_q_t
#define unlock		npv_pkt_q_unlock
#define unref_all	npv_pkt_q_unref_all
/*============================================================================*/
#else
#undef lock
#undef deq
#undef eofs_cleanup
#undef enq
#undef has_eof
#undef new
#undef pkt_q_t
#undef unlock
#undef unref_all
#endif
