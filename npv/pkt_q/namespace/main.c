#ifndef CLEANUP
#define fatal		npv_pkt_q_fatal
#define fatalx	 	npv_pkt_q_fatalx
#define warningx	npv_pkt_q_warningx
#define grow		npv_pkt_q_grow
/*---------------------------------------------------------------------------*/
/* some struct field names */
#define sz 	size
#define st_idx	stream_index
/*---------------------------------------------------------------------------*/
/*============================================================================*/
#else
#undef fatal
#undef fatalx
#undef warningx
#undef grow
/*---------------------------------------------------------------------------*/
#undef sz
#undef st_idx
/*---------------------------------------------------------------------------*/
#endif
