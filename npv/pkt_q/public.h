#ifndef NPV_PKT_Q_PUBLIC_H
#define NPV_PKT_Q_PUBLIC_H
#include <stdbool.h>
#include <libavcodec/avcodec.h>
#include "npv/c_fixing.h"
#include "npv/pipeline/public.h"
/*----------------------------------------------------------------------------*/
#include "npv/namespace/ffmpeg.h"
#include "npv/pkt_q/namespace/public.h"
/*----------------------------------------------------------------------------*/
struct pkt_q_t {
	pthread_mutex_t mutex;

	u32 n_max;
	u32 n; 
	avcodec_pkt_ref_t **q;
	u8 *msg_hdr;
};
STATIC void lock(struct pkt_q_t *this);
STATIC void unlock(struct pkt_q_t *this);
STATIC void enq(struct pkt_q_t *this, avcodec_pkt_ref_t *pr);
STATIC void deq(struct pkt_q_t *this);
STATIC void unref_all(struct pkt_q_t *this);
STATIC struct pkt_q_t *new(u8 *msg_hdr);
STATIC bool has_eof(struct pkt_q_t *this);
STATIC void eofs_cleanup(struct pkt_q_t *this);
/*----------------------------------------------------------------------------*/
#define CLEANUP
#include "npv/namespace/ffmpeg.h"
#include "npv/pkt_q/namespace/public.h"
#undef CLEANUP
/*----------------------------------------------------------------------------*/
#endif
