STATIC void lock(struct pkt_q_t *this)
{
	int r;

	r = pthread_mutex_lock(&this->mutex);
	if (r != 0)
		fatalx("%d:unable to lock packet queue\n", this, r);
}
STATIC void unlock(struct pkt_q_t *this)
{
	int r;

	r = pthread_mutex_unlock(&this->mutex);
	if (r != 0)
		fatalx("%d:unable to unlock packet queue\n", this, r);
}
/* actually rotate the pkt ref ptrs  */
STATIC void deq(struct pkt_q_t *this)
{
	if (this->n > 1) {
		avcodec_pkt_ref_t *save;

		save = this->q[0];
		memmove(&this->q[0], &this->q[1], (this->n_max - 1)
							* sizeof(this->q[0]));
		this->q[this->n_max - 1] = save;
	}
	this->n--;
}
/* steal the pkt reference */
STATIC void enq(struct pkt_q_t *this, avcodec_pkt_ref_t *pr)
{
	if (this->n == this->n_max)
		grow(this);
	avcodec_pkt_move_ref(this->q[this->n], pr);
	++this->n;
}
STATIC void unref_all(struct pkt_q_t *this)
{
	u32 pr;

	pr = 0;
	loop {
		if (pr == this->n)
			break;
		avcodec_pkt_unref(this->q[pr]);
		++pr;
	}
	this->n = 0;
}
STATIC struct pkt_q_t *new(u8 *msg_hdr)
{
	int r;
	struct pkt_q_t *this;

	this = malloc(sizeof(*this));
	if (this == 0)
		fatal("unable to allocate memory for the %s packet queue\n", msg_hdr);
	this->q = 0;
	this->n = 0;
	this->n_max = 0;
	this->msg_hdr = strdup(msg_hdr);
	r = pthread_mutex_init(&this->mutex, 0);
	if (r != 0)
		fatal("unable to init the mutex for the %s packet queue\n", msg_hdr);
	return this;
}
STATIC bool has_eof(struct pkt_q_t *this)
{
	if (this->n != 0) {
		avcodec_pkt_ref_t *last;

		last = this->q[this->n - 1];
		if (last->data == 0 && last->sz == 0)
			return true;
	}
	return false;
}
STATIC void eofs_cleanup(struct pkt_q_t *this)
{
	u32 pr_idx = 0;
				
	loop {
		avcodec_pkt_ref_t *pr;

		if (pr_idx == this->n)
			break;
		pr = this->q[pr_idx];
		if (pr->data == 0 && pr->sz == 0) {
			warningx("cleanup:EOF at packet %u/%u\n", this, pr_idx, this->n - 1);
		
			avcodec_pkt_unref(pr);
		
			/* shift if required */
			if (pr_idx < (this->n - 1)) {
				memmove(&this->q[pr_idx], &this->q[pr_idx + 1],
						(this->n_max - 1 - pr_idx) * sizeof(this->q[0]));
				this->q[this->n_max - 1] = pr;
			}
			(this->n)--;
		} else
			++pr_idx;
	}
}
