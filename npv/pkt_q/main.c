#ifndef NPV_PKT_Q_MAIN_C
#define NPV_PKT_Q_MAIN_C
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <pthread.h>
#include <libavcodec/avcodec.h>
#include "npv/c_fixing.h"
#include "npv/global.h"
#include "npv/public.h"
#include "npv/pkt_q/public.h"
#include "npv/pipeline/public.h"
/*----------------------------------------------------------------------------*/
#include "npv/namespace/ffmpeg.h"
#include "npv/pkt_q/namespace/public.h"
#include "npv/pkt_q/namespace/main.c"
/*----------------------------------------------------------------------------*/
#include "npv/pkt_q/local/code.frag.c"
#include "npv/pkt_q/public/code.frag.c"
/*----------------------------------------------------------------------------*/
#define CLEANUP
#include "npv/namespace/ffmpeg.h"
#include "npv/pkt_q/namespace/public.h"
#include "npv/pkt_q/namespace/main.c"
#undef CLEANUP
#endif
