STATIC void fatal(u8 *fmt, ...)
{
	va_list ap;

	npv_perr("packet queue:");
	va_start(ap, fmt);
	npv_vfatal(fmt, ap);
	va_end(ap); /* unreachable */
}
STATIC void fatalx(u8 *fmt, struct pkt_q_t *this, ...)
{
	va_list ap;

	npv_perr("%s:packet queue:", this->msg_hdr);
	va_start(ap, this);
	npv_vfatal(fmt, ap);
	va_end(ap); /* unreachable */
}
STATIC void warningx(u8 *fmt, struct pkt_q_t *this, ...)
{
	va_list ap;

	npv_perr("%s:packet queue:WARNING:", this->msg_hdr);
	va_start(ap, this);
	npv_vperr(fmt, ap);
	va_end(ap);
}
STATIC void grow(struct pkt_q_t *this)
{
	u32 p;
	u32 new_idx;

	new_idx = this->n_max;
	this->q = realloc(this->q, sizeof(*this->q) * (this->n_max + 1));
	if (this->q == 0)
		fatalx("unable to allocate more memory for packet reference pointers\n", this);
	this->q[new_idx] = avcodec_pkt_ref_alloc();
	if (this->q[new_idx] == 0)
		fatalx("ffmpeg:unable to allocate a new packet reference\n", this);
	++this->n_max;
}
