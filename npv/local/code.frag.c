/*
 * block as much as possible.
 * handle only async "usual" sigs, with sync signalfd.
 * allow some signals to go thru though. many sync signal can only
 * be handles with an async handler.
 * always presume the process "controlling terminal" is different than the
 * terminal connected on standard input and standard output 
 */
STATIC void sigs_init_once(void)
{
	int r;
	sigset_t sset;
	
	r = sigfillset(&sset);
	if (r == -1)
		fatal("unable to get a full signal mask\n");

	/* the "controlling terminal" line asks for a core dump, leave it be */
	r = sigdelset(&sset, SIGQUIT);
	if (r == -1)
		fatal("unable to remove SIGQUIT from our signal mask\n");

	r = pthread_sigmask(SIG_SETMASK, &sset, 0);
	if (r != 0)
		fatal("unable to \"block\" \"all\" signals\n");

	/* from here, we "steal" signals with signalfd */

	r = sigemptyset(&sset);
	if (r == -1)
		fatal("unable to get an empty signal mask\n");
	/* we are asked nicely to terminate */
	r = sigaddset(&sset, SIGTERM);
	if (r == -1)
		fatal("unable to add SIGTERM to our signal mask\n");
	/* the "controlling terminal" line (^c) asks nicely to terminate */
	r = sigaddset(&sset, SIGINT);
	if (r == -1)
		fatal("unable to add SIGINT to our signal mask\n");

	r = signalfd(-1, &sset, SFD_NONBLOCK);
	if (r == -1)
		fatal("unable to get a signalfd file descriptor\n");
	sig_fd_l = r;
}
/* we need a timer for the pre[fill|decode] state */
STATIC void pre_x_init_once(void)
{
	errno = 0;
	pre_x_timer_fd_p = timerfd_create(CLOCK_MONOTONIC, TFD_NONBLOCK);
	if (pre_x_timer_fd_p == -1)
		fatal("unable to get a timer file descriptor for the pre[fill|decode] states:%s\n", strerror(errno));
}
STATIC void evt_init_once(void)
{
	ep_fd_p = epoll_create1(0);
	if (ep_fd_p == -1)
		fatal("unable to create the epoll file descriptor\n");
}
STATIC void evt_add_all_fds(void)
{
	int r;
	u8 i;
	struct epoll_event evt;
	/*--------------------------------------------------------------------*/
	/* signals */
	evt.events = EPOLLIN;
	evt.data.fd = sig_fd_l;
	r = epoll_ctl(ep_fd_p, EPOLL_CTL_ADD, sig_fd_l, &evt);
	if (r == -1)
		fatal("unable to add the signalfd file descriptior to the epoll file descriptor\n");
	/*--------------------------------------------------------------------*/
	/* pre[fill|decode] timer */
	evt.events = EPOLLIN;
	evt.data.fd = pre_x_timer_fd_p;
	r = epoll_ctl(ep_fd_p, EPOLL_CTL_ADD, pre_x_timer_fd_p, &evt);
	if (r == -1)
		fatal("unable to add the pre[fill|decode] timer file descriptor\n");
	/*--------------------------------------------------------------------*/
	/* the video timer  */
	evt.events = EPOLLIN;
	evt.data.fd = npv_video_timer_fd_p;
	r = epoll_ctl(ep_fd_p, EPOLL_CTL_ADD, npv_video_timer_fd_p, &evt);
	if (r == -1)
		fatal("unable to add the video timer file descriptor\n");
	/*--------------------------------------------------------------------*/
	/* the x11 xcb file descriptor */
	evt.events = EPOLLIN;
	evt.data.fd = npv_xcb_p.fd;
	r = epoll_ctl(ep_fd_p, EPOLL_CTL_ADD, npv_xcb_p.fd, &evt);
	if (r == -1)
		fatal("unable to add the x11 xcb file descriptor\n");
	/*--------------------------------------------------------------------*/
	/* alsa pcm poll file descriptors */
	i = 0;
	loop {
		if (i == npv_audio_pcm_pollfds_n_p)
			break;
		evt.events = npv_audio_pcm_pollfds_p[i].events;
		evt.data.fd = npv_audio_pcm_pollfds_p[i].fd;
		r = epoll_ctl(ep_fd_p, EPOLL_CTL_ADD,
					npv_audio_pcm_pollfds_p[i].fd, &evt);
		if (r == -1)
			fatal("unable to add alsa poll file descriptor index %d to epoll file descriptor\n", i);
		++i;
	}
	/*--------------------------------------------------------------------*/
	/* the draining timer  */
	evt.events = EPOLLIN;
	evt.data.fd = npv_audio_draining_timer_fd_p;
	r = epoll_ctl(ep_fd_p, EPOLL_CTL_ADD, npv_audio_draining_timer_fd_p,
									&evt);
	if (r == -1)
		fatal("unable to add the draining timer file descriptor\n");
	/*--------------------------------------------------------------------*/
	/* the xcb screensaver heartbeat timer */
	evt.events = EPOLLIN;
	evt.data.fd = npv_xcb_p.screensaver_heartbeat_timer_fd;
	r = epoll_ctl(ep_fd_p, EPOLL_CTL_ADD,
				npv_xcb_p.screensaver_heartbeat_timer_fd, &evt);
	if (r == -1)
		fatal("unable to add the xcb screensaver heartbeat timer file descriptor\n");
	/*--------------------------------------------------------------------*/
	/* the xcb mouse visibility timer */
	evt.events = EPOLLIN;
	evt.data.fd = npv_xcb_p.mouse_visibility_timer_fd;
	r = epoll_ctl(ep_fd_p, EPOLL_CTL_ADD,
				npv_xcb_p.mouse_visibility_timer_fd, &evt);
	if (r == -1)
		fatal("unable to add the xcb screensaver heartbeat timer file descriptor\n");
}
STATIC void evt_sigs(void)
{
	int r;
	struct signalfd_siginfo siginfo;

	/* no short reads */
	r = read(sig_fd_l, &siginfo, sizeof(siginfo));
	if (r != sizeof(siginfo))
		fatal("unable to read signal information\n");

	switch (siginfo.ssi_signo) {
	case SIGTERM:
		exit_ok("received SIGTERM\n");
	case SIGINT:
		exit_ok("received SIGINT\n");
	default:
		warning("signal handle:unwanted signal %d received, discarding\n", siginfo.ssi_signo);
	}
}
STATIC void evt_accumulate(struct epoll_event *evt, bool *have_evt_sigs,
	bool *have_evt_pcm, bool *have_evt_video, bool *have_evt_pcm_draining,
	bool *have_evt_x11, bool *have_evt_xcb_screensaver_heartbeat,
	bool *have_evt_xcb_mouse_visibility, bool *have_evt_pre_x)
{
	u8 i;

	if (evt->data.fd == sig_fd_l) {
		if ((evt->events & EPOLLIN) != 0) {
			*have_evt_sigs = true;
			return;
		}
		fatal("event loop wait:signal:unexpected event\n");
	}
	/*-------------------------------------------------------------------*/
	/* only update alsa fds */
	i = 0;
	loop {
		if (i == npv_audio_pcm_pollfds_n_p)
			break;
		if (evt->data.fd == npv_audio_pcm_pollfds_p[i].fd) {
			npv_audio_pcm_pollfds_p[i].revents = evt->events;
			*have_evt_pcm = true;
			return;
		}
		++i;
	}
	/*-------------------------------------------------------------------*/
	if (evt->data.fd == npv_xcb_p.fd) {
		if ((evt->events & EPOLLIN) != 0) {
			*have_evt_x11 = true;
			return;
		}
	}
	/*-------------------------------------------------------------------*/
	if (evt->data.fd == npv_video_timer_fd_p) {
		if ((evt->events & EPOLLIN) != 0) {
			*have_evt_video = true;
			return;
		}
		fatal("event loop wait:video:unexpected event\n");
	}
	/*-------------------------------------------------------------------*/
	if (evt->data.fd == npv_audio_draining_timer_fd_p) {
		if ((evt->events & EPOLLIN) != 0) {
			*have_evt_pcm_draining = true;
			return;
		}
		fatal("event loop wait:audio draining timer:unexpected event\n");
	}
	/*-------------------------------------------------------------------*/
	if (evt->data.fd == npv_xcb_p.screensaver_heartbeat_timer_fd) {
		if ((evt->events & EPOLLIN) != 0) {
			*have_evt_xcb_screensaver_heartbeat = true;
			return;
		}
		fatal("event loop wait:xcb screensaver heartbeat timer:unexpected event\n");
	}
	/*-------------------------------------------------------------------*/
	if (evt->data.fd == npv_xcb_p.mouse_visibility_timer_fd) {
		if ((evt->events & EPOLLIN) != 0) {
			*have_evt_xcb_mouse_visibility = true;
			return;
		}
		fatal("event loop wait:xcb mouse visibility timer:unexpected event\n");
	}
	/*-------------------------------------------------------------------*/
	if (evt->data.fd == pre_x_timer_fd_p) {
		if ((evt->events & EPOLLIN) != 0) {
			*have_evt_pre_x = true;
			return;
		}
		fatal("event loop wait:pre[fill|decode] timer:unexpected event\n");
	}
}
/*
 * do _not_ block decoding based on a hard limit of video/audio data
 * availability, use a soft timer
 */
STATIC void predecode_wait_start(void)
{
	struct itimerspec t;
	int r;

	/* we target ~double audio buf, namely (0.25s * 2 ~ 500ms) */
	memset(&t, 0, sizeof(t));
	t.it_value.tv_nsec = 500000000; /* in ns */
	r = timerfd_settime(pre_x_timer_fd_p, 0, &t, 0);
	if (r == -1)
		fatal("unable to arm the predecode timer to 500 ms\n");
}
#define PREDECODE 2
STATIC void prefill_chk(void)
{
	s64 prefill;
	struct itimerspec t;
	int r;

	npv_pipeline_limits_lock();
	prefill = npv_pipeline_limits_p.pkts.prefill.bytes_rem;
	npv_pipeline_limits_unlock();
	if (prefill <= 0) {
		/* prefill is done, switch to predecode */
		atomic_store(&pre_x_p, PREDECODE);
		predecode_wait_start();
		return;
	}
	/* arm the timer for 100 ms */
	memset(&t, 0, sizeof(t));
	t.it_value.tv_nsec = 100000000; /* in ns */
	r = timerfd_settime(pre_x_timer_fd_p, 0, &t, 0);
	if (r == -1)
		fatal("unable to arm the prefill timer to 100 ms\n");
}
#undef PREDECODE
#define PREFILL		1
#define PREDECODE	2
#define NONE		0
STATIC void pre_x_evt(void)
{
	int r;
	uint64_t exps_n;
	u8 pre_x;

	/* timer ack */
	exps_n = 0;
	r = read(pre_x_timer_fd_p, &exps_n, sizeof(exps_n));
	if (r == -1)
		warning("unable to read the number of the pre[fill|decode] timer expirations\n");

	pre_x = atomic_load(&pre_x_p);
	if (pre_x == PREFILL) {
		prefill_chk_do_p = true;
		return;
	} else if (pre_x == PREDECODE) {
		/* predecode is done, switch to running/paused, namel no pre_x */
		atomic_store(&pre_x_p, NONE);
		return;
	}
	warning("got an spurious pre[fill|decode] timer event\n");
}
#undef PREFILL
#undef PREDECODE
#undef NONE
/*
 * XXX: remember that all heavy lifting should be done in other threads.
 * this thread should not "block" or perform "expensive" work.
 * "blocking", "expensive" work should be offloaded to other threads.
 */
#define EPOLL_EVTS_N 32 /* why not */
#define NONE		0
#define PREFILL		1
#define PREDECODE	2
STATIC void evts_loop(void) { loop
{
	u8 pre_x;
	int fds_n;
	int fd_idx;
	struct epoll_event evts[EPOLL_EVTS_N];
	bool have_evt_sigs;
	bool have_evt_pcm;
	bool have_evt_video;
	bool have_evt_pcm_draining;
	bool have_evt_x11;
	bool have_evt_xcb_screensaver_heartbeat;
	bool have_evt_xcb_mouse_visibility;
	bool have_evt_pre_x;
	int r;
	short pcm_evts;

	pre_x = atomic_load(&pre_x_p);
	if (pre_x == PREFILL && prefill_chk_do_p) {
		prefill_chk();
		prefill_chk_do_p = false;
	}
	errno = 0;
	memset(evts, 0, sizeof(evts));
	fds_n = epoll_wait(ep_fd_p, evts, EPOLL_EVTS_N, -1);
	if (fds_n == -1) {
		if (errno == EINTR) {
			warning("event loop wait:was interrupted by a signal\n");
			return;
		}
		fatal("event loop wait:an error occured\n");
	}

	have_evt_sigs = false;
	have_evt_pcm = false;
	have_evt_video = false;
	have_evt_pcm_draining = false;
	have_evt_x11 = false;
	have_evt_xcb_screensaver_heartbeat = false;
	have_evt_xcb_mouse_visibility = false;
	have_evt_pre_x = false;

	fd_idx = 0;
	loop {
		if (fd_idx == fds_n)
			break;
		evt_accumulate(&evts[fd_idx], &have_evt_sigs, &have_evt_pcm,
			&have_evt_video, &have_evt_pcm_draining,
			&have_evt_x11, &have_evt_xcb_screensaver_heartbeat,
			&have_evt_xcb_mouse_visibility, &have_evt_pre_x);
		++fd_idx;
	}

	/* once we have our evts, we use a sort of priority order */

	/* process any q-ed and we-handle sigs before anything */
	if (have_evt_sigs)
		evt_sigs(); 
	/*
	 * XXX: it may be more appropriate to break this in 2 steps: key inputs
 	 * (light processing), wins resizing (heavy processing)
	 */
	/* key input and win resizing */
	if (have_evt_x11)
		npv_xcb_evt();
	/* pre[fill|decode] timer evt */
	if (have_evt_pre_x)
		pre_x_evt();
	/* XXX: once in audio draining mode, this should not really happen */
	/* we are more sensitive to audio issues than video issues */
	if (have_evt_pcm) {
		/*
 		 * since alsa could use several file descriptors, only once the
		 * pollfds were properly updated we can actually know we got
		 * something from alsa
		 */
		r = snd_pcm_poll_descriptors_revents(npv_audio_pcm_p,
			npv_audio_pcm_pollfds_p, npv_audio_pcm_pollfds_n_p,
								&pcm_evts);
		if (r != 0)
			fatal("alsa:error processing the poll file descriptors\n");
		/* XXX: you can get POLLERR if in XRUN, will be handled later */
		if ((pcm_evts & POLLOUT) != 0)
			npv_audio_evt_pcm_write();
	}
	if (have_evt_video)
		npv_video_timer_evt();
	/* while audio is draining, video fr may need to be displayed */
	if (have_evt_pcm_draining)
		npv_audio_draining_state_evt();
	if (have_evt_xcb_screensaver_heartbeat)
		npv_xcb_screensaver_heartbeat_timer_evt();
	if (have_evt_xcb_mouse_visibility)
		npv_xcb_mouse_visibility_timer_evt();
}}
#undef EPOLL_EVTS_N
#undef NONE
#undef PREFILL
#undef PREDECODE
STATIC void ff_log_stdout(void *a, int b, const char *fmt, va_list ap)
{
	vprintf(fmt, ap);
}
struct ff_supported_audio_fr_fmt_t {
	u8 *str;
	enum avutil_audio_fr_fmt_t audio_fr_fmt;
};
/* this is the intersection of ff audio fr fmt and alsa pcm fmt */
STATIC struct ff_supported_audio_fr_fmt_t ff_supported_audio_fr_fmts[] = {
	{"u8", AVUTIL_AUDIO_FR_FMT_U8},
	{"u8planar", AVUTIL_AUDIO_FR_FMT_U8P},
	{"s16", AVUTIL_AUDIO_FR_FMT_S16},
	{"s16planar", AVUTIL_AUDIO_FR_FMT_S16P},
	{"s32",  AVUTIL_AUDIO_FR_FMT_S32},
	{"s32planar", AVUTIL_AUDIO_FR_FMT_S32P},
	{"float32", AVUTIL_AUDIO_FR_FMT_FLT},
	{"float32planar", AVUTIL_AUDIO_FR_FMT_FLTP},
	{"float64", AVUTIL_AUDIO_FR_FMT_DBL},
	{"float64planar", AVUTIL_AUDIO_FR_FMT_DBLP},
	{0,0}
};
STATIC void usage(void)
{
	struct ff_supported_audio_fr_fmt_t *fmt;

	pout("\
npv [-f send a fullscreen message to the window manager at start]\n\
    [-l live mode (seek/pause are disabled)]\n\
    [-p alsa pcm]\n\
    [-fc override initial ffmpeg count of channels used to approximate the alsa\n\
          pcm configuration]\n\
    [-fr override initial ffmpeg rate(hz) used to approximate the alsa pcm\n\
          configuration]\n\
    [-ff override initial ffmpeg audio frame format used to approximate the alsa\n\
          pcm configuration, see below for a list]\n\
    [-v volume(0..100)]\n\
    [-h window height in pixels]\n\
    [-w window width in pixels]\n\
    [-b packet buffer prefill wait(0..100)]\n\
    [-help]\n\
    url\n\
\n\
the ffmpeg audio frame formats which intersect alsa pcm audio formats are:\n"
   );
	fmt = ff_supported_audio_fr_fmts;
	loop {
		if (fmt->str == 0)
			break;
		pout("\t%s\n", fmt->str);
		++fmt;
	}
}
STATIC void opts_parse(int argc, u8 **args, u8 **url, bool *live,
		bool* start_fullscreen, u16 *w, u16 *h, u8 **pcm_str,
		int *override_initial_ff_chans_n, int *override_initial_ff_rate, 
		enum avutil_audio_fr_fmt_t *override_initial_ff_audio_fr_fmt,
					double *vol, u8 *pkts_prefill_percent)
{
	int i;
	int url_idx;

	i = 1;
	url_idx = -1;
	loop {
		if (i == argc)
			break;
		if (strcmp("-f", args[i]) == 0) {
			*start_fullscreen = true;	
			++i;
		} else if (strcmp("-l", args[i]) == 0) {
			*live = true;	
			++i;
		} else if (strcmp("-v", args[i]) == 0) {
			unsigned long vol_ul;

			if ((i + 1) == argc) {
				perr("-v:initial volume is missing\n");
				usage();
				exit(1);
			}
			vol_ul = strtoul(args[i + 1], 0, 10);
			if (vol_ul < 0 || 100 < vol_ul)
				fatal("-v:invalid volume value %lu (0..100)\n", vol_ul);
			*vol = (double)vol_ul / 100.;
			pout("-v:using initial volume %f\n", *vol);
			i += 2;
		} else if (strcmp("-h", args[i]) == 0) {
			unsigned long h_ul;

			if ((i + 1) == argc) {
				perr("-h:initial window pixel height is missing\n");
				usage();
				exit(1);
			}
			h_ul = strtoul(args[i + 1], 0, 10);
			if (h_ul == 0 || h_ul > U16_MAX)
				fatal("-h:invalid window pixel height %lu (1..%lu)\n", h_ul, U16_MAX);
			*h = (u16)h_ul;
			pout("-h:using initial window height %lu pixels\n", h_ul);
			i += 2;
		} else if (strcmp("-w", args[i]) == 0) {
			unsigned long w_ul;

			if ((i + 1) == argc) {
				perr("-h:initial window pixel width is missing\n");
				usage();
				exit(1);
			}
			w_ul = strtoul(args[i + 1], 0, 10);
			if (w_ul == 0 || w_ul > U16_MAX)
				fatal("-w:invalid window pixel width %lu (1..%lu)\n", w_ul, U16_MAX);
			*w = (u16)w_ul;
			pout("-w:using initial window width %lu pixels\n", w_ul);
			i += 2;
		} else if (strcmp("-b", args[i]) == 0) {
			if ((i + 1) == argc) {
				perr("-b:percent value for prefill of buffer of packet queues is missing\n");
				usage();
				exit(1);
			}
			*pkts_prefill_percent = (u8)strtoul(args[i + 1], 0, 10);
			pout("-v:using a %u prefilled buffer of packet queues\n", *pkts_prefill_percent);
			i += 2;
		} else if (strcmp("-p", args[i]) == 0) {
			*pcm_str = args[i + 1];
			pout("-p:alsa pcm \"%s\"\n", *pcm_str);
			i += 2;
		/*------------------------------------------------------------*/
		/* ff initial override for alsa pcm cfg  -- start */
		} else if (strcmp("-fr", args[i]) == 0) {
			if ((i + 1) == argc) {
				perr("-fr:override initial ffmpeg rate(hz) is missing\n");
				usage();
				exit(1);
			}
			*override_initial_ff_rate = (int)strtol(args[i + 1], 0,
									10);
			pout("-fr:override initial ffmpeg audio rate to %dHz used for alsa pcm configuration\n", *override_initial_ff_rate);
			i += 2;
		} else if (strcmp("-fc", args[i]) == 0) {
			if ((i + 1) == argc) {
				perr("-fc:override initial ffmpeg channel count is missing\n");
				usage();
				exit(1);
			}
			*override_initial_ff_chans_n = (int)strtol(args[i + 1],
									0, 10);
			pout("-fc:override initial ffmpeg count of channels to %d used for alsa pcm configuration\n", *override_initial_ff_chans_n);
			i += 2;
		} else if (strcmp("-ff", args[i]) == 0) {
			struct ff_supported_audio_fr_fmt_t *fmt;

			if ((i + 1) == argc) {
				perr("-fc:override initial ffmpeg audio frame format is missing\n");
				usage();
				exit(1);
			}
			fmt = ff_supported_audio_fr_fmts;
			loop {
				if  (fmt->str == 0) {
					perr("-ff:unknown ffmpeg audio frame format\n");
					usage();
					exit(1);
				}
				if (strcmp(fmt->str, args[i + 1]) == 0) {
					*override_initial_ff_audio_fr_fmt =
							fmt->audio_fr_fmt;
					break;
				}
				++fmt;
			}
			pout("-ff:override initial ffmpeg audio frame format is %s\n", avutil_get_audio_fr_fmt_name(fmt->audio_fr_fmt));
			i += 2;
		/* ff initial override for alsa pcm cfg  -- end */
		/*------------------------------------------------------------*/
		} else if (strcmp("-help", args[i]) == 0) {
			usage();
			exit(0);
		} else {
			url_idx = i;
			++i;
		}
	}
	if (url_idx == -1) {
		perr("missing url\n");
		usage();
		exit(1);
	}
	*url = args[url_idx];
	pout("url-->####%s####\n", *url);
}
#define PREFILL 1
STATIC void states_init_once(void)
{
	/*
	 * this state is driving several threads, we start in PREFILL asking for
	 * a prefill chk (it is possible to be in PREFILL state without asking
	 * for a prefill chk).
	 */
	atomic_store(&pre_x_p, PREFILL);
	prefill_chk_do_p = true;
	/* the following is accessed only from the main thread */
	paused_p = false;
}
#undef PREFILL
#define WIDTH_NOT_DEFINED 0
#define HEIGHT_NOT_DEFINED 0
STATIC void init_once(u8 *url, bool live, bool start_fullscreen,
		double initial_vol, u16 win_width, u16 win_height, u8 *pcm_str,
					avcodec_params_t **audio_codec_params,
					avcodec_params_t **video_codec_params,
					u8 **faces)
{
	avutil_rational_t *audio_st_tb;
	avutil_rational_t *video_st_tb;

	live_p = live;
	states_init_once();
	evt_init_once();
	sigs_init_once();
	pre_x_init_once();
	npv_vk_init_once(); /* generic plumbing */
	npv_audio_filt_init_once(initial_vol); 
	/* before audio_st_idx_p is actually used */
	npv_audio_init_once(pcm_str);
	npv_video_init_once(); /* before video_st_idx_p is actually used */
	npv_video_osd_init_once(faces);
	npv_clk_init_once();
	npv_pipeline_init_once();

	npv_fmt_init_once(url);
	/* we need something to start with */
	npv_fmt_probe_best_sts(
			&npv_video_st_p.idx, &npv_video_st_p.id, &video_st_tb,
				&npv_video_st_p.start_time, video_codec_params,
			&npv_audio_st_p.idx, &npv_audio_st_p.id, &audio_st_tb,
				&npv_audio_st_p.start_time, audio_codec_params);
	memcpy(&npv_audio_st_p.tb, audio_st_tb, sizeof(*audio_st_tb));
	memcpy(&npv_video_st_p.tb, video_st_tb, sizeof(*video_st_tb));
	if (win_width == WIDTH_NOT_DEFINED)
		win_width = (*video_codec_params)->width;
	if (win_height == HEIGHT_NOT_DEFINED)
		win_height = (*video_codec_params)->height;
	npv_xcb_init_once(win_width, win_height, start_fullscreen);
	npv_vk_surf_init_once(npv_xcb_p.c, npv_xcb_p.win_id);
}
#undef WIDTH_NOT_DEFINED
#undef HEIGHT_NOT_DEFINED
STATIC void prepare(double initial_vol, int override_initial_ff_chans_n, 
		int override_initial_ff_rate,
		enum avutil_audio_fr_fmt_t override_initial_ff_fmt,
		u8 pkts_prefill_percent, avcodec_params_t *audio_codec_params,
					avcodec_params_t *video_codec_params)
{
	npv_pipeline_limits_reset();
	npv_pipeline_prefill_reset(pkts_prefill_percent);
	npv_audio_dec_ctx_cfg(audio_codec_params);
	npv_video_dec_ctx_cfg(video_codec_params);
	npv_audio_prepare(override_initial_ff_chans_n, override_initial_ff_rate,
						override_initial_ff_fmt);
	evt_add_all_fds();
}
STATIC void npv_cmd_quit(void)
{
	exit_ok("quit command received\n");
}
STATIC void seek_lock(void)
{
	/* see lock_hierarchy file */
	npv_pkt_q_lock(npv_video_pkt_q_p);
	npv_video_dec_ctx_lock();
	npv_video_dec_frs_lock();
	npv_pkt_q_lock(npv_audio_pkt_q_p);
	npv_audio_dec_ctx_lock();
	npv_audio_dec_sets_lock();
	npv_pipeline_limits_lock();
	npv_fmt_ctx_lock();
}
STATIC void seek_unlock(void)
{
	/* see lock_hierarchy file */
	npv_fmt_ctx_unlock();
	npv_pipeline_limits_unlock();
	npv_audio_dec_sets_unlock();
	npv_audio_dec_ctx_unlock();
	npv_pkt_q_unlock(npv_audio_pkt_q_p);
	npv_video_dec_frs_unlock();
	npv_video_dec_ctx_unlock();
	npv_pkt_q_unlock(npv_video_pkt_q_p);
}
#define TS_FROM_CLK_OK 0
#define PREFILL 1
STATIC void seek_x(s64 delta)
{
	int a;
	u8 r;
	s64 now_audio_filt_ts;
	avutil_rational_t audio_filt_tb;
	s64 new_audio_filt_ts;
	s64 new_audio_st_ts;
	s64 now_audio_st_ts;

	if (npv_audio_draining_p) {
		warning("seek:audio is draining, seeking disable\n");
		return;
	}
	if (paused_p) {/* we don't seek in pause */
		warning("seek:disabled while paused\n");
		return;
	}
	if (live_p) {/* we don't seek in live mode */
		warning("seek:disabled in live mode\n");
		return;
	}

	npv_thdsws_wait_for_idle(npv_video_scaler_p.ctx);

	seek_lock();

	r = npv_clk_get_audio_filt_ts(&now_audio_filt_ts);
	if (r != TS_FROM_CLK_OK) {
		warning("seek:audio:clock timestamp unavailable, ignoring command\n");
		seek_unlock();
		return;
	}
	(void)snd_pcm_drop(npv_audio_pcm_p);
	/* XXX: a set of sts can share the same id for seeking */
	/*--------------------------------------------------------------------*/
	audio_filt_tb = avfilter_bufsink_tb_get(npv_audio_filt_p.abufsink_ctx);
	new_audio_filt_ts = now_audio_filt_ts + delta * audio_filt_tb.den
							/ audio_filt_tb.num;
	new_audio_st_ts = avutil_rescale_q(new_audio_filt_ts, audio_filt_tb,
							npv_audio_st_p.tb);
	pout("trying to seek to %"PRId64" audio filter time base units/%"PRId64" audio stream time base units\n", new_audio_filt_ts, new_audio_st_ts);
	a = avformat_seek_pkt(npv_fmt_ctx_p, npv_audio_st_p.id, new_audio_st_ts,
									0);
	if (a < 0) {
		pout("unable to seek to %"PRId64" audio filter time base units/%"PRId64" audio stream time base units\n", new_audio_filt_ts, new_audio_st_ts);
		goto try_restore_audio;
	}
	pout("global seek using audio seek to %"PRId64" audio filter time base units/%"PRId64" audio stream time base units\n", new_audio_filt_ts, new_audio_st_ts);
flush:
	npv_video_dec_flush();
	npv_audio_dec_flush();
	npv_audio_filt_flush();
	npv_fmt_flush();
	npv_clk_invalidate();
	npv_pipeline_limits_reset();
	npv_pipeline_prefill_reset(npv_pipeline_limits_p.pkts.prefill.percent);
	seek_unlock();
	
	atomic_store(&pre_x_p, PREFILL);
	prefill_chk_do_p = true;
	(void)snd_pcm_prepare(npv_audio_pcm_p);
	return;

try_restore_audio:
	now_audio_st_ts = avutil_rescale_q(now_audio_filt_ts, audio_filt_tb,
							npv_audio_st_p.tb);
	a = avformat_seek_pkt(npv_fmt_ctx_p, npv_audio_st_p.id, now_audio_st_ts,
									0);
	if (a < 0) /* we don't send an application error */
		exit_ok("unable to restore audio to %"PRId64" audio filter time base units/%"PRId64" audio stream time base units\n", now_audio_filt_ts, now_audio_st_ts);
	goto flush;
}
#undef TS_FROM_CLK_OK
#undef PREFILL
STATIC void npv_cmd_rewind(void)
{
	pout("COMMAND:rewind\n");
	seek_x(-SEEK_DELTA);
}

STATIC void npv_cmd_rewind_big(void)
{
	pout("COMMAND:rewind big\n");
	seek_x(-SEEK_DELTA_BIG);
}

STATIC void npv_cmd_fastforward(void)
{
	pout("COMMAND:fastforward\n");
	seek_x(SEEK_DELTA);
}

STATIC void npv_cmd_fastforward_big(void)
{
	pout("COMMAND:fastforward big\n");
	seek_x(SEEK_DELTA_BIG);
}

STATIC void npv_cmd_pause(void)
{
	if (npv_audio_draining_p) {
		warning("pause:audio is draining, toggling pause is disable\n");
		return;
	}
	if (live_p) {
		warning("pause:disabled in live mode\n");
		return;
	}
	if (paused_p) {
		int r;

		pout("COMMAND:unpause\n");
		paused_p = false;
		npv_fmt_ctx_lock();
		avformat_read_play(npv_fmt_ctx_p);
		npv_fmt_ctx_unlock();
		npv_clk_unpause();
		npv_video_timer_start();
	} else {
		int r;

		pout("COMMAND:pause\n");
		paused_p = true;
		npv_fmt_ctx_lock();
		avformat_read_pause(npv_fmt_ctx_p);
		npv_fmt_ctx_unlock();
		npv_clk_pause();
		npv_video_timer_slow_start();
	}
}
STATIC void npv_cmd_vol_up(void)
{
	npv_audio_filt_cmd_vol_up();
}
STATIC void npv_cmd_vol_down(void)
{
	npv_audio_filt_cmd_vol_down();
}
STATIC void npv_cmd_vol_up_small(void)
{
	npv_audio_filt_cmd_vol_up_small();
}
STATIC void npv_cmd_vol_down_small(void)
{
	npv_audio_filt_cmd_vol_down_small();
}
STATIC void npv_cmd_mute(void)
{
	npv_audio_filt_cmd_mute();
}
#define WIDTH_NOT_DEFINED 0
#define HEIGHT_NOT_DEFINED 0
#define CHANS_N_NOT_OVERRIDDEN 0
#define RATE_NOT_OVERRIDDEN 0
#define AUDIO_FR_FMT_NOT_OVERRIDDEN AVUTIL_AUDIO_FR_FMT_NONE
int main(int argc, u8 **args)
{
	bool live;
	bool start_fullscreen;
	u16 win_width;
	u16 win_height;
	u8 *pcm_str;
	/* audio override -- start */
	/*
	 * we could have got direct parameters for alsa, but doing only an
	 * override triggers an autoconfiguration of any missing parameters
	 * based on initial codec params. In other words, the user is not
	 * required to provide all audio params, the code will try to fit the
	 * missing ones with the initial codec params, which is the default
	 * behavior.  
	 */
	int override_initial_ff_chans_n;
	int override_initial_ff_rate;
	enum avutil_audio_fr_fmt_t override_initial_ff_audio_fr_fmt;
	/* audio override -- end */
	u8 *url;
	double initial_vol;
	avcodec_params_t *audio_codec_params;
	avcodec_params_t *video_codec_params;
	/* "turn on utf8" processing in used libs if any *AND* locale system */
	setlocale(LC_ALL, "");
	/* av_log_set_level(AV_LOG_VERBOSE); */
	/* av_log_set_level(AV_LOG_DEBUG); */
#ifdef NPV_DEBUG
	npv_debug_p = false;
#endif
	live = false;
	start_fullscreen = false;
	win_width = WIDTH_NOT_DEFINED;
	win_height = HEIGHT_NOT_DEFINED;
	url = 0;
	pcm_str = "default";
	override_initial_ff_chans_n = CHANS_N_NOT_OVERRIDDEN;
	override_initial_ff_rate = RATE_NOT_OVERRIDDEN;
	override_initial_ff_audio_fr_fmt = AUDIO_FR_FMT_NOT_OVERRIDDEN;
	url = 0;
	initial_vol = 1.;
	npv_pipeline_limits_p.pkts.prefill.percent = 0;
	opts_parse(argc, args, &url, &live, &start_fullscreen,
				&win_width, &win_height,
				&pcm_str, &override_initial_ff_chans_n,
				&override_initial_ff_rate,
				&override_initial_ff_audio_fr_fmt,
				&initial_vol,
				&npv_pipeline_limits_p.pkts.prefill.percent);
	init_once(url, live, start_fullscreen, initial_vol, win_width,
		win_height, pcm_str, &audio_codec_params, &video_codec_params,
								npv_faces);
	prepare(initial_vol, override_initial_ff_chans_n,
		override_initial_ff_rate, override_initial_ff_audio_fr_fmt,
		npv_pipeline_limits_p.pkts.prefill.percent, audio_codec_params,
							video_codec_params);

	/* switch the ffmpeg log to stdout for metadata/etc dump */
	avutil_log_set_callback(ff_log_stdout);
	avformat_dump_fmt(npv_fmt_ctx_p, 0, url, 0);
	avutil_log_set_callback(avutil_log_default_callback);
	/* we are in prefill state */
	npv_pipeline_read_thd_start();
	npv_pipeline_audio_thd_start();
	npv_pipeline_video_thd_start();
	npv_video_timer_start();
	npv_xcb_screensaver_heartbeat_timer_start();

	evts_loop();
	/* unreachable */
}
#undef WIDTH_NOT_DEFINED
#undef HEIGHT_NOT_DEFINED
#undef CHANS_N_NOT_OVERRIDDEN
#undef RATE_NOT_OVERRIDDEN
#undef AUDIO_FR_FMT_NOT_OVERRIDDEN
