#ifndef NPV_CONFIG_H
#define NPV_CONFIG_H
/*----------------------------------------------------------------------------*/
#undef NPV_DEBUG
/*----------------------------------------------------------------------------*/
#include <stdlib.h>
#include "npv/c_fixing.h"
/* some trash at google did hide noto fonts behind a web javascript app */
STATIC u8 *npv_faces[] = {
	"/share/fonts/dejavu/DejaVuSansMono-Bold.ttf",
	"/usr/share/fonts/dejavu/DejaVuSansMono-Bold.ttf",
	0
};
/*
 * face em square pixel size can be very far from actual glyph size, then
 * give the user some adjustment super powers
 */
STATIC u8 npv_em_adjust_numerator = 3;
STATIC u8 npv_em_adjust_denominator = 2;
/* for missing play/pause glyphs */
#define NPV_FACES_ASCII_ONLY 1
/* we don't use a xkb state machine, only bear 8bits truncated raw keycodes */
/*----------------------------------------------------------------------------*/
STATIC void npv_cmd_quit(void);
STATIC void npv_cmd_rewind(void);
STATIC void npv_cmd_fastforward(void);
STATIC void npv_cmd_rewind_big(void);
STATIC void npv_cmd_fastforward_big(void);
STATIC void npv_cmd_vol_up(void);
STATIC void npv_cmd_vol_down(void);
STATIC void npv_cmd_vol_up_small(void);
STATIC void npv_cmd_vol_down_small(void);
STATIC void npv_cmd_mute(void);
STATIC void npv_cmd_osd_timer_toggle(void);
STATIC void npv_cmd_pause(void);
STATIC void npv_cmd_fullscreen_toggle(void);
#ifdef NPV_DEBUG
STATIC void npv_cmd_debug_toggle(void);
STATIC void npv_cmd_debug_stats(void);
#endif
/*----------------------------------------------------------------------------*/
/* for documentation and example */
#define LINUX_KEY_ESC	0x01
#define LINUX_KEY_SPACE	0x39
/* x11 keycodes do offset linux keycodes by 8 */
#define X11_KEYCODE_ESC		(LINUX_KEY_ESC + 8)
#define X11_KEYCODE_SPACE	(LINUX_KEY_SPACE + 8)
/*----------------------------------------------------------------------------*/
struct npv_x11_bind_t {
	u8 keycode;		/* the 8bits truncated raw keycode */
	u8 *name;		/* friendly name */
	void (*cmd)(void);	/* bound cmd */
};
#define X11_BIND(x, y, z) {.keycode = x, .name = y, .cmd = z}
struct npv_x11_bind_t npv_x11_binds[] = {
	X11_BIND(X11_KEYCODE_ESC, "escape", npv_cmd_quit),
	X11_BIND(X11_KEYCODE_SPACE, "space", npv_cmd_pause),
	X11_BIND(0x71, "arrow left", npv_cmd_rewind),
	X11_BIND(0x72, "arrow right", npv_cmd_fastforward),
	X11_BIND(0x6e, "home", npv_cmd_rewind_big),
	X11_BIND(0x73, "end", npv_cmd_fastforward_big),
	X11_BIND(0x6f, "arrow up", npv_cmd_vol_up),
	X11_BIND(0x74, "arrow down", npv_cmd_vol_down),
	X11_BIND(0x48, "f6", npv_cmd_mute),
	X11_BIND(0x49, "f7", npv_cmd_vol_down_small),
	X11_BIND(0x4a, "f8", npv_cmd_vol_up_small),
	X11_BIND(0x43, "f1", npv_cmd_osd_timer_toggle),
	X11_BIND(0x5f, "f11", npv_cmd_fullscreen_toggle),
#ifdef NPV_DEBUG
	X11_BIND(0x4c, "f10", npv_cmd_debug_stats),
	X11_BIND(0x60, "f12", npv_cmd_debug_toggle),
#endif
};
#undef X11_BIND
#undef LINUX_KEY_ESC
#undef LINUX_KEY_SPACE
#undef X11_KEYCODE_ESC
#undef X11_KEYCODE_SPACE
/*----------------------------------------------------------------------------*/
#define VOL_DELTA 0.1 /* from 0.0 to 1.0 */
#define VOL_DELTA_SMALL 0.01 /* from 0.0 to 1.0 */
#define SEEK_DELTA INT64_C(10) /* 10 seconds */
#define SEEK_DELTA_BIG (INT64_C(4) * INT64_C(60)) /* 4 minutes */
/*============================================================================*/
/* kinky internal settings, modify with care */
/*
 * The targeted count of decoded video frames in the array (while not
 * resyncing). Decoded frames are "memory expensive".
 */
#define DEC_FRS_ARRAY_N_MAX 4
/*
 * video frames are presumed arriving inorder, but once a backward
 * discontinuity is detected, in order to avoid a full/dead locked array of
 * predecoded video frames, we unlock the size of array of decoded frames until
 * we "resynchronize". To decide if we found a resynchronizing frame, we define
 * a time window based on the following value.
 */
#define DISCONT_BACKWARD_RESYNC_MS 500
/*
 * the rate limiter while resyncing, since it can flood memory. We target 16 ms
 * or 60 fps
 */
#define RESYNC_RATE_LIMITER_MS 16
/*
 * our iap hls peering with twitch.tv is accute trash, ffmpeg allows us to
 * flush and retry. Let's do that in the pipeline read thread, may need a timer
 * to avoid hammering the peering links though.
 */
#define FMT_RETRIES_N 100

/* 10MB is roughly 8s of a 10Mb stream */
#define PIPELINE_BYTES_N_MAX 10000000
#endif
