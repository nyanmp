#!/bin/sh
cc main.c -lc -ldl -D_GNU_SOURCE -DSTATIC=static -I../ \
	$(pkg-config --cflags xcb) \
	$(pkg-config --cflags --libs alsa) \
	$(pkg-config --cflags --libs --static freetype2 libavcodec libavdevice \
		libavfilter libavformat libavutil libpostproc libswresample \
		libswscale) \
	-o /tmp/npv
