#ifndef NPV_CLK_MAIN_C
#define NPV_CLK_MAIN_C
/*
 * code protected with a GNU affero GPLv3 license 
 * copyright (C) 2020 Sylvain BERTRAND
 */
/*---------------------------------------------------------------------------*/
#include <string.h>
#include <pthread.h>
#include <stdarg.h>
#include <alsa/asoundlib.h>
#include <libavformat/avformat.h>
#include "npv/c_fixing.h"
#include "npv/audio/public.h"
#include "npv/audio/filt/public.h"
#include "npv/video/public.h"
/*---------------------------------------------------------------------------*/
#include "npv/namespace/alsa.h"
#include "npv/namespace/ffmpeg.h"
/*---------------------------------------------------------------------------*/
#define TS_RETURNED 0
/*---------------------------------------------------------------------------*/
#include "npv/clk/local/state.frag.c"
#include "npv/clk/local/code.frag.c"
#include "npv/clk/public/code.frag.c"
/*---------------------------------------------------------------------------*/
#undef TS_RETURNED
/*---------------------------------------------------------------------------*/
#define CLEANUP
#include "npv/namespace/alsa.h"
#include "npv/namespace/ffmpeg.h"
#undef CLEANUP
#endif
