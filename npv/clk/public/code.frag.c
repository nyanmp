STATIC void npv_clk_init_once(void)
{
	int r;

	r = snd_pcm_status_malloc(&npv_clk_l.ref.status);
	if (r != 0)
		npv_clk_fatal("alsa:unable to allocate an alsa status structure\n");
	r = snd_pcm_status_malloc(&npv_clk_l.now.status);
	if (r != 0)
		npv_clk_fatal("alsa:unable to allocate an alsa status structure\n");
	/* we pre-allocate alsa pcm hw_params container */
	r = snd_pcm_hw_params_malloc(&npv_clk_pcm_hw_params_l);
	if (r < 0)
		npv_clk_fatal("alsa:unable to allocate hardware parameters context\n");
	npv_clk_l.paused = false;
}
#define NO_REF_TIME_POINT 1
STATIC u8 npv_clk_get_audio_filt_ts(s64 *ts)
{
	int r;
	snd_pcm_audio_tstamp_config_t ac;
	snd_htimestamp_t hts;
	f64 now_ns;
	f64 ref_ns;
	f64 ref_status_ns;
	f64 ref_delay_frs_n;
	f64 ref_audio_filt_ts;
	avutil_rational_t audio_filt_tb;
	f64 audio_filt_tb_num;
	f64 audio_filt_tb_den;
	f64 audio_rate_num;
	unsigned int audio_rate_num_ui;
	f64 audio_rate_den;
	unsigned int audio_rate_den_ui;
	f64 written_frs_n;
	f64 now_audio_ts;
	f64 now_video_ts;

	if (!npv_clk_l.ref_valid)
		return NO_REF_TIME_POINT;
	if (!npv_clk_l.paused) {
		memset(npv_clk_l.now.status, 0, snd_pcm_status_sizeof());
		ac.report_delay = 0;
		ac.type_requested = (unsigned int)npv_audio_selected_ts_type_p;
		snd_pcm_status_set_audio_htstamp_config(npv_clk_l.now.status,
									&ac);
		r = snd_pcm_status(npv_audio_pcm_p, npv_clk_l.now.status);
		if (r < 0)
			npv_clk_fatal("alsa:unable to sample timing information for 'now' time point\n");
	}
	snd_pcm_status_get_audio_htstamp(npv_clk_l.now.status, &hts);
	now_ns = (f64)hts.tv_sec * 1e9 + (f64)hts.tv_nsec;

	snd_pcm_status_get_audio_htstamp(npv_clk_l.ref.status, &hts);
	ref_status_ns = (f64)hts.tv_sec * 1e9 + (f64)hts.tv_nsec;

	ref_audio_filt_ts = (f64)npv_clk_l.ref.audio_filt_ts;
	ref_delay_frs_n = (f64)snd_pcm_status_get_delay(npv_clk_l.ref.status);
	/* we are writting the output of the filt to the audio pcm */	
	audio_filt_tb = avfilter_bufsink_tb_get(npv_audio_filt_p.abufsink_ctx);
	audio_filt_tb_num = (f64)audio_filt_tb.num;
	audio_filt_tb_den = (f64)audio_filt_tb.den;
	/*--------------------------------------------------------------------*/
	r = snd_pcm_hw_params_current(npv_audio_pcm_p, npv_clk_pcm_hw_params_l);
	if (r != 0)
		npv_clk_fatal("alsa:unable to get current hardware parameters\n");
	r = snd_pcm_hw_params_get_rate_numden(npv_clk_pcm_hw_params_l,
					&audio_rate_num_ui, &audio_rate_den_ui);
	if (r < 0)
		npv_clk_fatal("alsa:unable to get exact rate\n");
	audio_rate_num = (f64)audio_rate_num_ui;
	audio_rate_den = (f64)audio_rate_den_ui;
	/*--------------------------------------------------------------------*/
	written_frs_n = (f64)npv_clk_l.ref.written_ufrs_n;

	/* time translation */
	ref_ns = ref_status_ns + (ref_delay_frs_n - written_frs_n)
					* audio_rate_den * 1e9 / audio_rate_num;
	/* basic linear interpolation */
	now_audio_ts = audio_filt_tb_den * 1e-9 * (now_ns - ref_ns)
					/ audio_filt_tb_num + ref_audio_filt_ts;
	*ts = (s64)lrint(now_audio_ts);
	return TS_RETURNED;
}
#undef NO_REF_TIME_POINT
STATIC u8 npv_clk_get_video_st_ts(s64 *ts)
{
	avutil_rational_t audio_filt_tb;
	s64 now_audio_filt_ts;
	u8 r;

	audio_filt_tb = avfilter_bufsink_tb_get(npv_audio_filt_p.abufsink_ctx);
	r = npv_clk_get_audio_filt_ts(&now_audio_filt_ts);
	if (r != TS_RETURNED)
		return r;
	*ts = avutil_rescale_q(now_audio_filt_ts, audio_filt_tb,
							npv_video_st_p.tb);
	return TS_RETURNED;
}
STATIC void npv_clk_ref_time_point_update(s64 audio_filt_ts,
						snd_pcm_ufrs_t written_ufrs_n)
{
	int r;
	snd_pcm_audio_tstamp_config_t ac;

	memset(npv_clk_l.ref.status, 0, snd_pcm_status_sizeof());
	ac.report_delay = 0;
	ac.type_requested = (unsigned int)npv_audio_selected_ts_type_p;
	snd_pcm_status_set_audio_htstamp_config(npv_clk_l.ref.status, &ac);
	r = snd_pcm_status(npv_audio_pcm_p, npv_clk_l.ref.status);
	if (r < 0)
		npv_clk_fatal("unable to sample timing information for reference time point\n");
	npv_clk_l.ref.audio_filt_ts = audio_filt_ts;
	npv_clk_l.ref.written_ufrs_n = written_ufrs_n;
	npv_clk_l.ref_valid = true;
}
STATIC void npv_clk_invalidate(void)
{
	npv_clk_l.ref_valid = false;
}
STATIC void npv_clk_pause(void)
{
	int r;
	snd_pcm_audio_tstamp_config_t ac;

	npv_clk_l.paused = true;
	memset(npv_clk_l.now.status, 0, snd_pcm_status_sizeof());
	ac.report_delay = 0;
	ac.type_requested = (unsigned int)npv_audio_selected_ts_type_p;
	snd_pcm_status_set_audio_htstamp_config(npv_clk_l.now.status, &ac);
	r = snd_pcm_status(npv_audio_pcm_p, npv_clk_l.now.status);
	if (r < 0)
		npv_clk_fatal("alsa:unable to sample timing information for 'paused' time point\n");
}
STATIC void npv_clk_unpause(void)
{
	npv_clk_l.paused = false;
	npv_clk_invalidate();
}
