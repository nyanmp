#ifndef NPV_CLK_PUBLIC_H
#define NPV_CLK_PUBLIC_H
#include <stdint.h>
#include <alsa/asoundlib.h>
#include "npv/c_fixing.h"
/*---------------------------------------------------------------------------*/
#include "npv/namespace/alsa.h"
/*---------------------------------------------------------------------------*/
STATIC void npv_clk_init_once(void);
STATIC u8 npv_clk_get_audio_filt_ts(s64 *ts);
STATIC u8 npv_clk_get_video_st_ts(s64 *ts);
STATIC void npv_clk_ref_time_point_update(s64 audio_filt_ts,
						snd_pcm_ufrs_t written_ufrs_n);
STATIC void npv_clk_invalidate(void);
STATIC void npv_clk_pause(void);
STATIC void npv_clk_unpause(void);
/*---------------------------------------------------------------------------*/
#define CLEANUP
#include "npv/namespace/alsa.h"
#undef CLEANUP
#endif
