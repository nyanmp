STATIC void npv_clk_fatal(u8 *fmt, ...)
{
	va_list ap;

	npv_perr("clock:");
	va_start(ap, fmt);
	npv_vfatal(fmt, ap);
	va_end(ap);
}

