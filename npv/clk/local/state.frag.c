/*
 * considered to be shared by all the thds: we spin because it is supposed to
 * be short
 */
STATIC struct {
	/*
	 * 2 points in the time domain:
	 *     - ref
	 *     - now
	 * 
	 * the ref point is the top fr of a ff set of audio frs ("AVFrame"). 
	 * 
	 * the now point is interpolated using the ref point and the most
	 * accurate alsa audio timer can give us
	 *
	 */ 
	bool ref_valid; /* we may not have a ref point yet */
	bool paused;
	struct {
		snd_pcm_status_t *status;
		/*
		 * the n of audio frs (written to/mixed by) alsa from the ff set
 		 * of audio frs ("AVFrame") when the alsa status is sampled
		 */
		snd_pcm_ufrs_t written_ufrs_n;

		s64 audio_filt_ts; /* in ff audio filt tb units */
	} ref;
	struct {
		snd_pcm_status_t *status;
	} now;
} npv_clk_l;
STATIC snd_pcm_hw_params_t *npv_clk_pcm_hw_params_l;
