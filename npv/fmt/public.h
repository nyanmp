#ifndef NPV_FMT_PUBLIC_H
#define NPV_FMT_PUBLIC_H
/*
 * code protected with a GNU affero GPLv3 license 
 * copyright (C) 2020 Sylvain BERTRAND
 */
#include <libavformat/avformat.h>
#include "npv/c_fixing.h"
#include "npv/pipeline/public.h"
/*----------------------------------------------------------------------------*/
#include "npv/namespace/ffmpeg.h"
#include "npv/fmt/namespace/public.h"
/*----------------------------------------------------------------------------*/
#include "npv/fmt/public/state.frag.h"
/*----------------------------------------------------------------------------*/
STATIC u8 *duration_estimate_to_str(
				enum avformat_duration_estimation_method_t m);
STATIC void probe_best_sts(
	int *best_v_idx, int *best_v_id, avutil_rational_t **best_v_tb,
	int64_t *best_v_start_time, avcodec_params_t **best_v_codec_params,
	int *best_a_idx, int *best_a_id, avutil_rational_t **best_a_tb,
	int64_t *best_a_start_time, avcodec_params_t **best_a_codec_params);
STATIC void init_once(u8 *url);
STATIC u8 pkts_read_and_q(void);
STATIC void flush(void);
STATIC void ctx_lock(void);
STATIC void ctx_unlock(void);
/*----------------------------------------------------------------------------*/
#define CLEANUP
#include "npv/namespace/ffmpeg.h"
#include "npv/fmt/namespace/public.h"
#undef CLEANUP
#endif
