STATIC void fatal(u8 *fmt, ...)
{
	va_list ap;

	npv_perr("format:");
	va_start(ap, fmt);
	npv_vfatal(fmt, ap);
	va_end(ap); /* unreachable */
}
STATIC void warning(u8 *fmt, ...)
{
	va_list ap;

	npv_perr("format:WARNING:");
	va_start(ap, fmt);
	npv_vperr(fmt, ap);
	va_end(ap);
}
STATIC void pout(u8 *fmt, ...)
{
	va_list ap;

	npv_pout("format:");
	va_start(ap, fmt);
	npv_vpout(fmt, ap);
	va_end(ap);
}
STATIC bool did_reached_limits(void)
{
	bool r;

	npv_pipeline_limits_lock();
	if (npv_pipeline_limits_p.pkts.bytes_n >= npv_pipeline_limits_p.pkts.limit.bytes_n)
		r = true;
	else
		r = false;
	npv_pipeline_limits_unlock();
	return false;
}
STATIC void init_once_public(u8 *url)
{
	int r;
	AVDictionary *options = 0;

	av_dict_set(&options, "reconnect", "1", 0);
	/* av_dict_set(&options, "reconnect_at_eof", "1", 0);  breaks youtube live */
	av_dict_set(&options, "reconnect_on_network_error", "1", 0);
	av_dict_set(&options, "reconnect_streamed", "1", 0);

	ctx_p = 0;
	r = avformat_open_input(&ctx_p, url, NULL, &options);
	if (r < 0)
		fatal("ffmpeg:unable to open \"%s\"\n", url);
	av_dict_free(&options);
	r = pthread_mutex_init(&ctx_mutex_l, 0);
	if (r != 0)
		fatal("unable to init the format mutex\n");
}
STATIC void init_once_local(void)
{
	pkt_l = avcodec_pkt_ref_alloc();
	if (pkt_l == 0)
		fatal("ffmpeg:unable to allocate a reference on a packet for encoded/compressed audio/video\n");
}
