#ifndef CLEANUP
#define break_on_audio			npv_fmt_break_on_audio
#define break_on_video			npv_fmt_break_on_video
#define ctx_lock			npv_fmt_ctx_lock
#define ctx_p				npv_fmt_ctx_p
#define ctx_unlock			npv_fmt_ctx_unlock
#define duration_estimate_to_str	npv_fmt_duration_estimate_to_str
#define flush				npv_fmt_flush
#define init_once			npv_fmt_init_once
#define init_once_local			npv_fmt_init_once_local
#define init_once_public		npv_fmt_init_once_public
#define pkts_read_and_q			npv_fmt_pkts_read_and_q
#define probe_best_sts			npv_fmt_probe_best_sts
/*============================================================================*/
#else
#undef break_on_audio
#undef break_on_video
#undef ctx_lock
#undef ctx_p
#undef ctx_unlock
#undef duration_estimate_to_str
#undef flush
#undef init_once
#undef init_once_local
#undef init_once_public
#undef pkts_read_and_q
#undef probe_best_sts
#endif
