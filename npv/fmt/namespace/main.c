#ifndef CLEANUP
/*----------------------------------------------------------------------------*/
/* misc */
#define avformat_read_pkt	av_read_frame
#define programs_n		nb_programs
#define st			stream
#define st_idx			stream_index
#define sts_n			nb_streams
#define sz			size
/*----------------------------------------------------------------------------*/
#define audio			npv_fmt_break_on_audio
#define ctx_mutex_l		npv_fmt_ctx_mutex_l
#define did_reached_limits	npv_fmt_did_reached_limits
#define fatal			npv_fmt_fatal
#define pkt_l			npv_fmt_pkt_l
#define pout			npv_fmt_pout
#define video			npv_fmt_break_on_video
#define warning			npv_fmt_warning
/*============================================================================*/
#else
/*----------------------------------------------------------------------------*/
#undef avformat_read_pkt
#undef programs_n
#undef st
#undef st_idx
#undef sts_n
#undef sz
/*----------------------------------------------------------------------------*/
#undef audio
#undef ctx_mutex_l
#undef did_reached_limits
#undef fatal
#undef pout
#undef pkt_l
#undef video
#undef warning
#endif
