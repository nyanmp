#ifndef NPV_FMT_MAIN_C
#define NPV_FMT_MAIN_C
/*
 * code protected with a GNU affero GPLv3 license 
 * copyright (C) 2020 Sylvain BERTRAND
 */
#include <pthread.h>
#include <stdarg.h>
#include <stdint.h>
#include <libavformat/avformat.h>
#include <libavcodec/avcodec.h>
#include "npv/c_fixing.h"
#include "npv/global.h"
#include "npv/public.h"
#include "npv/pkt_q/public.h"
#include "npv/fmt/public.h"
#include "npv/audio/public.h"
#include "npv/video/public.h"
/*----------------------------------------------------------------------------*/
#include "npv/namespace/ffmpeg.h"
#include "npv/fmt/namespace/public.h"
#include "npv/fmt/namespace/main.c"
/*----------------------------------------------------------------------------*/
#include "npv/fmt/local/state.frag.c"
/*----------------------------------------------------------------------------*/
#include "npv/fmt/local/code.frag.c"
#include "npv/fmt/public/code.frag.c"
/*----------------------------------------------------------------------------*/
#define CLEANUP
#include "npv/namespace/ffmpeg.h"
#include "npv/fmt/namespace/public.h"
#include "npv/fmt/namespace/main.c"
#undef CLEANUP
#endif
