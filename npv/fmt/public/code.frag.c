STATIC void ctx_lock(void)
{
	int r;

	r = pthread_mutex_lock(&ctx_mutex_l);
	if (r != 0)
		fatal("%d:unable to lock the format context\n", r);
}
STATIC void ctx_unlock(void)
{
	int r;

	r = pthread_mutex_unlock(&ctx_mutex_l);
	if (r != 0)
		fatal("%d:unable to unlock the format context\n", r);
}
STATIC u8 *duration_estimate_to_str(enum avformat_duration_estimation_method_t
									m)
{
	switch (m) {
	case avformat_duration_from_pts:
		return "from PTS(Presentation TimeStamp)";
	case avformat_duration_from_st:
		return "from stream";
	case avformat_duration_from_bitrate:
		return "from bitrate";
	default:
		return "unkwown";
	}
}
#define AGAIN		0
#define LIMITS_REACHED	1
#define EOF_FMT		2
/*
 * we don't want to lock any pkt q for too long, then we do finer-grained
 * locking here
 */
STATIC u8 pkts_read_and_q(void) { loop
{
	int r;
	u8 retries_n;

	if (did_reached_limits())
		return LIMITS_REACHED;
	retries_n = 0;
	loop {
		/* XXX: there, new streams can appear (could some disappear?) */
		ctx_lock();
		r = avformat_read_pkt(ctx_p, pkt_l);
		ctx_unlock();
		if (r == AVERROR(EAGAIN))
			return AGAIN;
		else if (r == AVERROR_EOF)
			return EOF_FMT;
		else if (r != 0) {
			u32 pr_idx;

			if (retries_n == FMT_RETRIES_N)
				fatal("ffmpeg:error while reading coded/compressed data into packets\n");
			warning("ffmpeg:error while reading coded/compressed data into packets, flushing and more, retries = %u(%u)\n", retries_n, FMT_RETRIES_N);
			flush();
			/*
			 * XXX: with some trashy iap hls peering, network connectivity (often tls)
			 * errors will make ffmpeg inject eof pkts, clean them even if it may hang
			 * in the case of a legit eof.
			 */
			npv_pkt_q_lock(npv_audio_pkt_q_p);
			npv_pkt_q_eofs_cleanup(npv_audio_pkt_q_p);
			npv_pkt_q_unlock(npv_audio_pkt_q_p);

			npv_pkt_q_lock(npv_video_pkt_q_p);
			npv_pkt_q_eofs_cleanup(npv_video_pkt_q_p);
			npv_pkt_q_unlock(npv_video_pkt_q_p);

			++retries_n;
			continue;
		}
		break;
	}
	/* r == 0 */
	if (pkt_l->st_idx == npv_audio_st_p.idx) {
		npv_pipeline_limits_lock();
		npv_pipeline_limits_p.pkts.bytes_n += (u64)pkt_l->sz;
		if (npv_pipeline_limits_p.pkts.prefill.bytes_rem > 0)
			npv_pipeline_limits_p.pkts.prefill.bytes_rem -= (s64)pkt_l->sz;
		npv_pipeline_limits_unlock();

		npv_pkt_q_lock(npv_audio_pkt_q_p);
		/* will "steal" the pkt */
		npv_pkt_q_enq(npv_audio_pkt_q_p, pkt_l);
		npv_pkt_q_unlock(npv_audio_pkt_q_p);
	} else if (pkt_l->st_idx == npv_video_st_p.idx) {
		npv_pipeline_limits_lock();
		npv_pipeline_limits_p.pkts.bytes_n += (u64)pkt_l->sz;
		if (npv_pipeline_limits_p.pkts.prefill.bytes_rem > 0)
			npv_pipeline_limits_p.pkts.prefill.bytes_rem -= (s64)pkt_l->sz;
		npv_pipeline_limits_unlock();

		npv_pkt_q_lock(npv_video_pkt_q_p);
		/* will "steal" the pkt */
		npv_pkt_q_enq(npv_video_pkt_q_p, pkt_l);
		npv_pkt_q_unlock(npv_video_pkt_q_p);
	} else /* data, subtitles, non active sts, etc */
		avcodec_pkt_unref(pkt_l);
}}
#undef AGAIN
#undef LIMITS_REACHED
#undef EOF_FMT
STATIC void probe_best_sts(
	int *best_v_idx, int *best_v_id, avutil_rational_t **best_v_tb,
	int64_t *best_v_start_time, avcodec_params_t **best_v_codec_params,
	int *best_a_idx, int *best_a_id, avutil_rational_t **best_a_tb,
	int64_t *best_a_start_time, avcodec_params_t **best_a_codec_params)
{
	int r;

	/* probe beyond the header, if any */
	r = avformat_find_st_info(ctx_p, 0);
	if (r < 0)
		fatal("ffmpeg:unable to probe\n");
	r = avformat_find_best_st(ctx_p, AVUTIL_AVMEDIA_TYPE_AUDIO, -1, -1, 0,
									0);
	if (r < 0)
		fatal("ffmpeg:no audio stream found\n");
	/* we copy what we need */
	*best_a_idx = r;
	*best_a_id = ctx_p->sts[r]->id;
	*best_a_tb = &ctx_p->sts[r]->tb;
	*best_a_start_time = ctx_p->sts[r]->start_time;
	*best_a_codec_params = ctx_p->sts[r]->codecpar; /* used once for init */

	r = avformat_find_best_st(ctx_p, AVUTIL_AVMEDIA_TYPE_VIDEO, -1, -1, 0,
									0);
	if (r < 0)
		fatal("ffmpeg:no video stream found\n");
	/* we copy what we need */
	*best_v_idx = r;
	*best_v_id = ctx_p->sts[r]->id;
	*best_v_tb = &ctx_p->sts[r]->tb;
	*best_v_start_time = ctx_p->sts[r]->start_time;
	*best_v_codec_params = ctx_p->sts[r]->codecpar; /* used once for init */
	pout("####%s#### probing: %u streams and %u programs\n", ctx_p->url, ctx_p->sts_n, ctx_p->programs_n);
	pout("####%s#### probing: best video stream index=%d/id=%d, best audio stream index=%d/id=%d\n", ctx_p->url, *best_v_idx, *best_v_id, *best_a_idx, *best_a_id);
}
STATIC void flush(void)
{
	avformat_flush(ctx_p);
	avio_flush(ctx_p->pb);
}
STATIC void init_once(u8 *url)
{
	init_once_public(url);
	init_once_local();
}
