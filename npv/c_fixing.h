#ifndef NPV_C_FIXING_H
#define NPV_C_FIXING_H
/*
 * code protected with a GNU affero GPLv3 license 
 * copyright (C) 2020 Sylvain BERTRAND
 */
#include <stdint.h>
#include <limits.h>
#if __GNUC__ > 4
	#include <stdatomic.h>
#endif
#define u8 uint8_t
#define U8_MAX 0xff
#define s8 int8_t
#define u16 uint16_t
#define U16_MAX 0xffff
#define s16 int16_t
#define u32 uint32_t
#define u64 uint64_t
#define U64_MAX 0xffffffffffffffff
#define s64 int64_t
#define S64_MIN 0x8000000000000000
#define S64_MAX 0x7fffffffffffffff
#define s64_abs labs
#define s32 int32_t
#define f32 float
#define f64 double

#if UCHAR_WIDTH == 8
	#if __GNUC__ > 4
		#define atomic_u8 atomic_uchar
	#else
		#define atomic_u8 u8
	#endif
#else
	#error "unable to find the right atomic for a 8 bits byte, be sure to have __STDC_WANT_IEC_60559_BFP_EXT__ defined"
#endif
#if SHRT_WIDTH == 16
	#if __GNUC__ > 4
		#define atomic_u16 atomic_ushort
	#else
		#define atomic_u16 u16
	#endif
#else
	#error "unable to find the right atomic for an unsigned 16 bits word, be sure to have __STDC_WANT_IEC_60559_BFP_EXT__ defined"
#endif
#if UINT_WIDTH == 32
	#if __GNUC__ > 4
		#define atomic_u32 atomic_uint
	#else
		#define atomic_u32 u32
	#endif
#elif ULONG_WIDTH == 32
	#if __GNUC__ > 4
		#define atomic_u32 atomic_ulong
	#else
		#define atomic_u32 u32
	#endif
#else
	#error "unable to select the right atomic for an unsigned 32 bits word, be sure to have __STDC_WANT_IEC_60559_BFP_EXT__ defined"
#endif
#if LONG_WIDTH == 64
	#if __GNUC__ > 4
		#define atomic_s64 atomic_long
	#else
		#define atomic_s64 s64
	#endif
#else
	#error "unable to select the right atomic for a 64 bits signed word, be sure to have __STDC_WANT_IEC_60559_BFP_EXT__ defined"
#endif
#if __GNUC__ <= 4
	#define atomic_load(x) __atomic_load_n(x,__ATOMIC_SEQ_CST)
	#define atomic_store(x,y) __atomic_store_n(x,y,__ATOMIC_SEQ_CST)
#endif

#define loop for(;;)
/*
 * we don't know how to handle enum from the C "standard" perspective. this
 * is based on empirical conclusions based on gcc.
 * this is for compiler constant, not compiler constant _variables_
 */
#define constant_u32 enum

#define ARRAY_N(x) (sizeof(x) / sizeof((x)[0]))
#endif
