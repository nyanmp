STATIC void pout(u8 *fmt, ...)
{
	va_list ap;

	va_start(ap, fmt);
	flockfile(stdout);
	vfprintf(stdout, fmt, ap);
	funlockfile(stdout);
	va_end(ap);
}
STATIC void vpout(u8 *fmt, va_list ap)
{
	va_list aq;

	va_copy(aq, ap);
	flockfile(stdout);
	vfprintf(stdout, fmt, aq);
	funlockfile(stdout);
	va_end(aq);
}
STATIC void perr(u8 *fmt, ...)
{
	va_list ap;

	va_start(ap, fmt);
	flockfile(stderr);
	vfprintf(stderr, fmt, ap);
	funlockfile(stderr);
	va_end(ap);
}
STATIC void vperr(u8 *fmt, va_list ap)
{
	va_list aq;

	va_copy(aq, ap);
	flockfile(stderr);
	vfprintf(stderr, fmt, aq);
	funlockfile(stderr);
	va_end(aq);
}
STATIC void warning(u8 *fmt, ...)
{
	va_list ap;

	perr("WARNING:");
	va_start(ap, fmt);
	vperr(fmt, ap);
	va_end(ap);
}
STATIC void vwarning(u8 *fmt, va_list ap)
{
	va_list aq;

	perr("WARNING:");
	va_copy(aq, ap);
	vperr(fmt, aq);
	va_end(aq);
}
STATIC void fatal(u8 *fmt, ...)
{
	va_list ap;

	perr("FATAL:");
	va_start(ap, fmt);
	vperr(fmt, ap);
	va_end(ap);	
	exit(EXIT_FAILURE);
}
STATIC void vfatal(u8 *fmt, va_list ap)
{
	va_list aq;

	perr("FATAL:");
	va_copy(aq, ap);
	vperr(fmt, aq);
	va_end(aq);
	exit(EXIT_FAILURE);
}
STATIC void exit_ok(u8 *fmt, ...)
{
	va_list ap;

	pout("EXIT:");
	va_start(ap, fmt);
	vpout(fmt, ap);
	va_end(ap);
	exit(EXIT_SUCCESS);
}
#ifdef NPV_DEBUG
STATIC void npv_cmd_debug_stats(void)
{
	perr("DEBUG:STATS:pipeline:limits:pkts:audio_bytes_n=%lu/%lu:video_bytes_n=%lu/%lu\n", npv_pipeline_limits_p.pkts.audio_bytes_n, npv_pipeline_limits_p.pkts.limit.audio_bytes_n, npv_pipeline_limits_p.pkts.video_bytes_n, npv_pipeline_limits_p.pkts.limit.video_bytes_n);
	perr("DEBUG:STATS:video:pkts=%u/%u\n", npv_video_pkt_q_p->n, npv_video_pkt_q_p->n_max);
	perr("DEBUG:STATS:audio:pkts=%u/%u\n", npv_audio_pkt_q_p->n, npv_audio_pkt_q_p->n_max);
	perr("DEBUG:STATS:video:frames %u/%u\n", npv_video_dec_frs_p.n, npv_video_dec_frs_p.n_max);
	perr("DEBUG:STATS:audio:sets %u/%u\n", npv_audio_dec_sets_p.n, npv_audio_dec_sets_p.n_max);
	perr("DEBUG:STATS:video:aspect_ratio=%ux%u\n", npv_video_aspect_ratio.width, npv_video_aspect_ratio.height);
}
#endif
