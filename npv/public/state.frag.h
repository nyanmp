STATIC int ep_fd_p;
/* major states -- START -----------------------------------------------------*/
/* we first prefill, then predecode, and then run/pause */
/*
 * 0 none
 * 1 prefill
 * 2 predecode
 */
STATIC atomic_u8 pre_x_p;
STATIC int pre_x_timer_fd_p;
STATIC bool prefill_chk_do_p; /* currently, only accessed by the main loop */
STATIC bool paused_p;
/*NSPC*/
STATIC bool live_p; /* disable seek and pause */
/* states -- END   -----------------------------------------------------------*/
#ifdef NPV_DEBUG
STATIC bool npv_debug_p;
#endif
