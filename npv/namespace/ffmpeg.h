#ifndef CLEANUP
#define AVUTIL_AUDIO_FR_FMT_NONE	AV_SAMPLE_FMT_NONE
#define AVUTIL_AUDIO_FR_FMT_U8		AV_SAMPLE_FMT_U8
#define AVUTIL_AUDIO_FR_FMT_U8P		AV_SAMPLE_FMT_U8P
#define AVUTIL_AUDIO_FR_FMT_S16		AV_SAMPLE_FMT_S16
#define AVUTIL_AUDIO_FR_FMT_S32		AV_SAMPLE_FMT_S32
#define AVUTIL_AUDIO_FR_FMT_FLT		AV_SAMPLE_FMT_FLT
#define AVUTIL_AUDIO_FR_FMT_DBL		AV_SAMPLE_FMT_DBL
#define AVUTIL_AUDIO_FR_FMT_U8P		AV_SAMPLE_FMT_U8P
#define AVUTIL_AUDIO_FR_FMT_S16P	AV_SAMPLE_FMT_S16P
#define AVUTIL_AUDIO_FR_FMT_S32P	AV_SAMPLE_FMT_S32P
#define AVUTIL_AUDIO_FR_FMT_FLTP	AV_SAMPLE_FMT_FLTP
#define AVUTIL_AUDIO_FR_FMT_DBLP	AV_SAMPLE_FMT_DBLP
#define AVUTIL_OPT_SEARCH_CHILDREN	AV_OPT_SEARCH_CHILDREN
/*----------------------------------------------------------------------------*/
#define AVFILTER_BUFSRC_FLAG_PUSH	AV_BUFFERSRC_FLAG_PUSH
#define AVFILTER_BUFSRC_FLAG_KEEP_REF	AV_BUFFERSRC_FLAG_KEEP_REF
#define AVUTIL_AVERROR			AVERROR
#define AVUTIL_AVERROR_EOF		AVERROR_EOF
#define AVUTIL_AVMEDIA_TYPE_AUDIO	AVMEDIA_TYPE_AUDIO
#define AVUTIL_AVMEDIA_TYPE_VIDEO	AVMEDIA_TYPE_VIDEO
#define AVUTIL_DATA_PTRS_N		AV_NUM_DATA_POINTERS
#define AVUTIL_NOPTS_VALUE		AV_NOPTS_VALUE
#define AVUTIL_PIX_FMT_RGB32		AV_PIX_FMT_RGB32
/*----------------------------------------------------------------------------*/
#define avcodec_codec_t				const AVCodec
#define avcodec_codec_ctx_t			AVCodecContext
#define avcodec_find_dec			avcodec_find_decoder
#define avcodec_send_pkt			avcodec_send_packet
#define avcodec_alloc_ctx			avcodec_alloc_context3
#define avcodec_flush_bufs			avcodec_flush_buffers
#define avcodec_params_t			AVCodecParameters
#define avcodec_params_to_ctx			avcodec_parameters_to_context
#define avcodec_pkt_move_ref			av_packet_move_ref
#define avcodec_pkt_ref_alloc			av_packet_alloc
#define avcodec_pkt_ref_t			AVPacket
#define avcodec_pkt_unref			av_packet_unref 
#define avfilter_bufsink_tb_get			av_buffersink_get_time_base
#define avfilter_filt_t				AVFilter
#define avfilter_filt_ctx_t			AVFilterContext
#define avfilter_filt_graph_t			AVFilterGraph
#define avfilter_graph_alloc_filt		avfilter_graph_alloc_filter
#define avfilter_graph_send_cmd			avfilter_graph_send_command
#define avformat_ctx_t				AVFormatContext
#define avformat_dump_fmt			av_dump_format
#define avformat_duration_estimation_method_t	AVDurationEstimationMethod
#define avformat_duration_from_bitrate		AVFMT_DURATION_FROM_BITRATE
#define avformat_duration_from_pts		AVFMT_DURATION_FROM_PTS
#define avformat_duration_from_st		AVFMT_DURATION_FROM_STREAM
#define avformat_find_best_st			av_find_best_stream
#define avformat_find_st_info			avformat_find_stream_info
#define avformat_read_pause			av_read_pause
#define avformat_read_play			av_read_play
#define avformat_seek_pkt			av_seek_frame
#define avformat_st_t				AVStream
#define avutil_cpus_n				av_cpu_count
#define avutil_free				av_free
#define avutil_get_audio_fr_fmt_name		av_get_sample_fmt_name
#define avutil_get_pix_fmt_name			av_get_pix_fmt_name
#define avutil_get_pix_fmt_str			av_get_pix_fmt_string
#define avutil_log_default_callback		av_log_default_callback
#define avutil_log_set_callback			av_log_set_callback
#define avutil_opt_set				av_opt_set
#define avutil_opt_set_bin			av_opt_set_bin
#define avutil_opt_set_int			av_opt_set_int
#define avutil_opt_set_pixel_fmt		av_opt_set_pixel_fmt
#define avutil_opt_set_q			av_opt_set_q
#define avutil_pixel_fmt_t			AVPixelFormat
#define avutil_rational_t			AVRational
#define avutil_rescale_q			av_rescale_q
/* some struct members */
#define fmt					format
#define tb					time_base
#define sts					streams
/* scaler */
#define sws_ctx					SwsContext
#define sws_get_cached_ctx			sws_getCachedContext
/*============================================================================*/
#else
#undef AVUTIL_AUDIO_FR_FMT_NONE
#undef AVUTIL_AUDIO_FR_FMT_U8
#undef AVUTIL_AUDIO_FR_FMT_S16
#undef AVUTIL_AUDIO_FR_FMT_S32
#undef AVUTIL_AUDIO_FR_FMT_FLT
#undef AVUTIL_AUDIO_FR_FMT_U8P
#undef AVUTIL_AUDIO_FR_FMT_S16P
#undef AVUTIL_AUDIO_FR_FMT_S32P
#undef AVUTIL_AUDIO_FR_FMT_FLTP
#undef AVUTIL_OPT_SEARCH_CHILDREN
/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
#undef AVFILTER_BUFSRC_FLAG_PUSH
#undef AVFILTER_BUFSRC_FLAG_KEEP_REF
#undef AVUTIL_AVERROR
#undef AVUTIL_AVERROR_EOF
#undef AVUTIL_AVMEDIA_TYPE_AUDIO
#undef AVUTIL_AVMEDIA_TYPE_VIDEO
#undef AVUTIL_DATA_PTRS_N
#undef AVUTIL_NOPTS_VALUE
#undef AVUTIL_PIX_FMT_RGB32
/*----------------------------------------------------------------------------*/
#undef avcodec_codec_t
#undef avcodec_codec_ctx_t
#undef avcodec_find_dec
#undef avcodec_send_pkt
#undef avcodec_alloc_ctx
#undef avcodec_flush_bufs
#undef avcodec_params_t
#undef avcodec_params_to_ctx
#undef avcodec_pkt_make_refcounted
#undef avcodec_pkt_move_ref
#undef avcodec_pkt_ref_alloc
#undef avcodec_pkt_ref_t
#undef avcodec_pkt_unref
#undef avfilter_bufsink_tb_get
#undef avfilter_filt_t
#undef avfilter_filt_ctx_t
#undef avfilter_filt_graph_t
#undef avfilter_graph_alloc_filt
#undef avfilter_graph_send_cmd
#undef avformat_ctx_t
#undef avformat_dump_fmt
#undef avformat_duration_estimation_method_t
#undef avformat_duration_from_bitrate
#undef avformat_duration_from_pts
#undef avformat_duration_from_st
#undef avformat_find_best_st
#undef avformat_find_st_info
#undef avformat_read_pause
#undef avformat_read_play
#undef avformat_seek_pkt
#undef avformat_st_t
#undef avutil_cpus_n
#undef avutil_free
#undef avutil_get_audio_fr_fmt_name
#undef avutil_get_pix_fmt_name
#undef avutil_get_pix_fmt_str
#undef avutil_log_default_callback
#undef avutil_log_set_callback
#undef avutil_opt_set
#undef avutil_opt_set_bin
#undef avutil_opt_set_int
#undef avutil_opt_set_pixel_fmt
#undef avutil_opt_set_q
#undef avutil_pixel_fmt_t
#undef avutil_rational_t
#undef avutil_rescale_q
/*----------------------------------------------------------------------------*/
#undef fmt
#undef tb
#undef sts
#undef sws_ctx
#undef sws_get_cache_ctx
#endif
