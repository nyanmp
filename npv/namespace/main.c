#ifndef CLEANUP
#define duration_estimate_to_str	npv_duration_estimate_to_str
#define evt_accumulate			npv_evt_accumulate
#define evt_add_all_fds			npv_evt_add_all_fds
#define evt_init_once			npv_evt_init_once
#define evt_sigs			npv_evt_sigs
#define evts_loop			npv_evts_loop
#define ff_log_stdout			npv_ff_log_stdout
#define ff_supported_audio_fr_fmt_t	npv_ff_supported_audio_fr_fmt_t
#define ff_supported_audio_fr_fmts	npv_ff_supported_audio_fr_fmts
#define init_once			npv_init_once
#define opts_parse			npv_opts_parse
#define pre_x_evt			npv_pre_x_evt
#define pre_x_init_once			npv_pre_x_init_once
#define predecode_wait_start		npv_predecode_wait_start
#define prefill_chk			npv_prefill_chk
#define prepare				npv_prepare
#define seek_lock			npv_seek_lock
#define seek_unlock			npv_seek_unlock
#define seek_x				npv_seek_x
#define sig_fd_l			npv_sig_fd_l
#define sigs_init_once			npv_sigs_init_once
#define states_init_once		npv_states_init_once
#define ts_to_str			npv_ts_to_str
#define usage				npv_usage
/*============================================================================*/
#else
#undef duration_estimate_to_str
#undef evt_accumulate
#undef evt_add_all_fds
#undef evt_init_once
#undef evt_sigs
#undef evts_loop
#undef ff_log_stdout
#undef ff_supported_audio_fr_fmt_t
#undef ff_supported_audio_fr_fmts
#undef init_once
#undef opts_parse
#undef pre_x_evt
#undef pre_x_init_once
#undef predecode_wait_start
#undef prefill_chk
#undef prepare
#undef seek_lock
#undef seek_unlock
#undef seek_x
#undef sig_fd_l
#undef sigs_init_once
#undef states_init_once
#undef ts_to_str
#undef usage
#endif
