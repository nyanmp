#ifndef CLEANUP
#define chans_n channels
/*----------------------------------------------------------------------------*/
#define SND_PCM_FMT_UNKNOWN	SND_PCM_FORMAT_UNKNOWN
#define SND_PCM_FMT_FLOAT64	SND_PCM_FORMAT_FLOAT64
#define SND_PCM_FMT_FLOAT	SND_PCM_FORMAT_FLOAT
#define SND_PCM_FMT_S32		SND_PCM_FORMAT_S32
#define SND_PCM_FMT_U32		SND_PCM_FORMAT_U32
#define SND_PCM_FMT_S16		SND_PCM_FORMAT_S16
#define SND_PCM_FMT_S24		SND_PCM_FORMAT_S24
#define SND_PCM_FMT_U24		SND_PCM_FORMAT_U24
#define SND_PCM_FMT_U16		SND_PCM_FORMAT_U16
#define SND_PCM_FMT_U8		SND_PCM_FORMAT_U8
#define SND_PCM_FMT_S8		SND_PCM_FORMAT_S8
/*----------------------------------------------------------------------------*/
#define SND_PCM_ST_PLAYBACK	SND_PCM_STREAM_PLAYBACK
/*----------------------------------------------------------------------------*/
#define snd_pcm_fmt_t				snd_pcm_format_t
#define snd_pcm_fmt_desc			snd_pcm_format_description
#define snd_pcm_poll_descriptors_n		snd_pcm_poll_descriptors_count
#define snd_pcm_hw_params_get_buf_sz		snd_pcm_hw_params_get_buffer_size
#define snd_pcm_hw_params_get_buf_sz_near	snd_pcm_hw_params_get_buffer_size_near
#define snd_pcm_hw_params_get_chans_n		snd_pcm_hw_params_get_channels
#define snd_pcm_hw_params_get_chans_n_min	snd_pcm_hw_params_get_channels_min
#define snd_pcm_hw_params_get_chans_n_max	snd_pcm_hw_params_get_channels_max
#define snd_pcm_hw_params_get_fmt		snd_pcm_hw_params_get_format
#define snd_pcm_hw_params_set_buf_sz_near	snd_pcm_hw_params_set_buffer_size_near
#define snd_pcm_hw_params_set_chans_n		snd_pcm_hw_params_set_channels
#define snd_pcm_hw_params_set_fmt		snd_pcm_hw_params_set_format
#define snd_pcm_hw_params_test_chans_n		snd_pcm_hw_params_test_channels
#define snd_pcm_hw_params_test_fmt		snd_pcm_hw_params_test_format
#define snd_pcm_sfrs_t				snd_pcm_sframes_t
#define snd_pcm_sw_params_set_period_evt	snd_pcm_sw_params_set_period_event
#define snd_pcm_ufrs_t				snd_pcm_uframes_t
/*============================================================================*/
#else
#undef chans_n
/*----------------------------------------------------------------------------*/
#undef SND_PCM_FMT_FLOAT
#undef SND_PCM_FMT_S32
#undef SND_PCM_FMT_U32
#undef SND_PCM_FMT_S16
#undef SND_PCM_FMT_S24
#undef SND_PCM_FMT_U24
#undef SND_PCM_FMT_U16
#undef SND_PCM_FMT_U8
#undef SND_PCM_FMT_S8
/*----------------------------------------------------------------------------*/
#undef SND_PCM_ST_PLAYBACK
/*----------------------------------------------------------------------------*/
#undef snd_pcm_fmt_t
#undef snd_pcm_fmt_desc
#undef snd_pcm_poll_descriptors_n
#undef snd_pcm_hw_params_get_buf_sz
#undef snd_pcm_hw_params_get_buf_sz_near
#undef snd_pcm_hw_params_get_chans_n
#undef snd_pcm_hw_params_get_chans_n_min
#undef snd_pcm_hw_params_get_chans_n_max
#undef snd_pcm_hw_params_get_fmt
#undef snd_pcm_hw_params_set_buf_sz_near
#undef snd_pcm_hw_params_set_chans_n
#undef snd_pcm_hw_params_set_fmt
#undef snd_pcm_hw_params_test_chans_n
#undef snd_pcm_hw_params_test_fmt
#undef snd_pcm_sfrs_t
#undef snd_pcm_sw_params_set_period_evt
#undef snd_pcm_ufrs_t
#endif
