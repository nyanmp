#ifndef CLEANUP
#define ep_fd_p			npv_ep_fd_p
#define exit_ok			npv_exit
#define fatal			npv_fatal
#define live_p			npv_live_p
#define paused_p		npv_paused_p
#define perr			npv_perr
#define pout			npv_pout
#define pre_x_p			npv_pre_x_p
#define pre_x_timer_fd_p	npv_pre_x_timer_fd_p
#define prefill_chk_do_p	npv_prefill_chk_do_p
#define vfatal			npv_vfatal
#define vwarning		npv_vwarning
#define vpout			npv_vpout
#define vperr			npv_vperr
#define warning			npv_warning
/*============================================================================*/
#else
#undef ep_fd_p
#undef exit_ok
#undef fatal
#undef live_p
#undef paused_p
#undef perr
#undef pout
#undef pre_x_p
#undef pre_x_timer_fd_p
#undef prefill_chk_do_p
#undef vfatal
#undef vwarning
#undef vpout
#undef vperr
#undef warning
#endif
