#ifndef NYANVK_SYMS_GLOBAL_H
#define NYANVK_SYMS_GLOBAL_H
/*
 * this is public domain without any warranties of any kind
 * Sylvain BERTRAND
 */
/* XXX: define VK_GLOBAL_SYMS and cherry pick what you actually use */
#define VK_GLOBAL_SYMS_FULL \
	STATIC void *(*dl_vk_get_instance_proc_addr)(struct vk_instance_t *instance, u8 *name);\
	STATIC void *(*dl_vk_get_dev_proc_addr)(struct vk_dev_t *dev, u8 *name);\
\
	STATIC s32 (*dl_vk_enumerate_instance_version)(u32 *version);\
	STATIC s32 (*dl_vk_enumerate_instance_layer_props)(\
				u32 *props_n,\
				struct vk_layer_props_t *props);\
	STATIC s32 (*dl_vk_enumerate_instance_ext_props)(\
				u8 *layer_name,\
				u32 *props_n,\
				struct vk_ext_props_t *props);\
	STATIC s32 (*dl_vk_create_instance)(\
				struct vk_instance_create_info_t *info,\
				void *allocator,\
				struct vk_instance_t **instance);\
	STATIC s32 (*dl_vk_enumerate_phydevs)(\
				struct vk_instance_t *instance,\
				u32 *phydevs_n,\
				struct vk_phydev_t **phydevs);\
	STATIC s32 (*dl_vk_enumerate_dev_ext_props)(\
				struct vk_phydev_t *phydev,\
				u8 *layer_name,\
				u32 *props_n,\
				struct vk_ext_props_t *props);\
	STATIC void (*dl_vk_get_phydev_props)(\
				struct vk_phydev_t *phydev,\
				struct vk_phydev_props_t *props);\
	STATIC s32 (*dl_vk_create_dev)(\
				struct vk_phydev_t *phydev,\
				struct vk_dev_create_info_t *create_info,\
				void *allocator,\
				struct vk_dev_t **dev);\
	STATIC void (*dl_vk_get_phydev_q_fam_props)(\
				struct vk_phydev_t *phydev,\
				u32 *q_fam_props_n,\
				struct vk_q_fam_props_t *props);\
	STATIC s32 (*dl_vk_create_xcb_surf)(\
				struct vk_instance_t *instance,\
				struct vk_xcb_surf_create_info_t *info,\
				void *allocator,\
				struct vk_surf_t **surf);\
	STATIC void (*dl_vk_destroy_surf)(\
				struct vk_instance_t *instance,\
				struct vk_surf_t *surf,\
				void *allocator); \
	STATIC s32 (*dl_vk_get_phydev_surf_support)(\
				struct vk_phydev_t *phydev,\
				u32 q_fam,\
				struct vk_surf_t *surf,\
				u32 *supported);\
	STATIC s32  (*dl_vk_get_phydev_surf_texel_mem_blk_confs)(\
				struct vk_phydev_t *phydev,\
				struct vk_phydev_surf_info_t *info,\
				u32 *confs_n,\
				struct vk_surf_texel_mem_blk_conf_t *confs);\
	STATIC void (*dl_vk_get_phydev_mem_props)(\
				struct vk_phydev_t *phydev,\
				struct vk_phydev_mem_props_t *props);\
	STATIC void (*dl_vk_get_phydev_texel_mem_blk_fmt_props)(\
				struct vk_phydev_t *phydev,\
				u32 fmt,\
				struct vk_texel_mem_blk_fmt_props_t *props);\
	STATIC s32 (*dl_vk_get_phydev_surf_caps)(\
				struct vk_phydev_t *phydev,\
				struct vk_phydev_surf_info_t *info,\
				struct vk_surf_caps_t *caps);\
	STATIC s32 (*dl_vk_get_phydev_surf_present_modes)(\
				struct vk_phydev_t *phydev,\
				struct vk_surf_t *surf,\
				u32 *modes_n,\
				u32 *modes);
#endif
