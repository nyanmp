#ifndef NYANVK_SYMS_DEV_H
#define NYANVK_SYMS_DEV_H
/*
 * this is public domain without any warranties of any kind
 * Sylvain BERTRAND
 */
/* XXX: define VK_DEV_SYMS and cherry pick what you actually use */
#define VK_DEV_SYMS_FULL \
	void (*dl_vk_get_dev_q)(struct vk_dev_t *dev, u32 fam, u32 q_idx, struct vk_q_t **q); \
	s32 (*dl_vk_create_cp)( \
			struct vk_dev_t *dev, \
			struct vk_cp_create_info_t *create_info, \
			void *allocator, \
			struct vk_cp_t **vk_cp); \
	s32 (*dl_vk_create_swpchn)( \
			struct vk_dev_t *dev, \
			struct vk_swpchn_create_info_t *info, \
			void *allocator, \
			struct vk_swpchn_t **swpchn); \
	s32 (*dl_vk_destroy_swpchn)( \
			struct vk_dev_t *dev, \
			struct vk_swpchn_t *swpchn, \
			void *allocator); \
	s32 (*dl_vk_get_swpchn_imgs)( \
			struct vk_dev_t *dev, \
			struct vk_swpchn_t *swpchn, \
			u32 *imgs_n, \
			struct vk_img_t **imgs); \
	s32 (*dl_vk_create_img)( \
			struct vk_dev_t *dev, \
			struct vk_img_create_info_t *info, \
			void *allocator, \
			struct vk_img_t **img); \
	void (*dl_vk_destroy_img)( \
			struct vk_dev_t *dev, \
			struct vk_img_t *img, \
			void *allocator); \
	s32 (*dl_vk_get_img_mem_rqmts)( \
			struct vk_dev_t *dev, \
			struct vk_img_mem_rqmts_info_t *info, \
			struct vk_mem_rqmts_t *mem_rqmts); \
	s32 (*dl_vk_alloc_mem)( \
			struct vk_dev_t *dev, \
			struct vk_mem_alloc_info_t *info, \
			void *allocator, \
			struct vk_dev_mem_t **dev_mem); \
	void (*dl_vk_free_mem)( \
			struct vk_dev_t *dev, \
			struct vk_dev_mem_t *dev_mem, \
			void * allocator); \
	s32 (*dl_vk_bind_img_mem)( \
			struct vk_dev_t *dev, \
			u32 infos_n, \
			struct vk_bind_img_mem_info_t *infos); \
	s32 (*dl_vk_map_mem)( \
			struct vk_dev_t *dev, \
			struct vk_dev_mem_t *dev_mem, \
			u64 offset, \
			u64 sz, \
			u32 flags, \
			void **data); \
	void (*dl_vk_unmap_mem)( \
			struct vk_dev_t *dev, \
			struct vk_dev_mem_t *dev_mem); \
	s32 (*dl_vk_alloc_cbs)( \
			struct vk_dev_t *dev, \
			struct vk_cb_alloc_info_t *info, \
			struct vk_cb_t **cbs); \
	s32 (*dl_vk_begin_cb)( \
			struct vk_cb_t *cb, \
			struct vk_cb_begin_info_t *info); \
	s32 (*dl_vk_end_cb)(void *cb); \
	void (*dl_vk_cmd_pl_barrier)( \
			struct vk_cb_t *cb, \
			u32 src_stage, \
			u32 dst_stage, \
			u32 dep_flags, \
			u32 mem_barriers_n, \
			struct vk_mem_barrier_t *mem_barriers, \
			u32 buf_mem_barriers_n, \
			struct vk_buf_barrier_t *buf_mem_barriers, \
			u32 img_mem_barriers_n, \
			struct vk_img_mem_barrier_t *img_mem_barriers); \
	s32 (*dl_vk_q_submit)( \
			struct vk_q_t *q, \
			u32 submits_n, \
			struct vk_submit_info_t *submits, \
			struct vk_fence_t *fence); \
	s32 (*dl_vk_q_wait_idle)(struct vk_q_t *q); \
	s32 (*dl_vk_acquire_next_img)( \
			struct vk_dev_t *dev, \
			struct vk_acquire_next_img_info_t *info, \
			u32 *img_idx); \
	s32 (*dl_vk_q_present)( \
			struct vk_q_t *q, \
			struct vk_present_info_t *info); \
	s32 (*dl_vk_create_sem)( \
			struct vk_dev_t *dev, \
			struct vk_sem_create_info_t *info, \
			void *allocator, \
			struct vk_sem_t **sem); \
	s32 (*dl_vk_create_imgview)( \
			struct vk_dev_t *dev, \
			struct vk_imgview_create_info_t *info, \
			void *allocator, \
			struct vk_imgview_t **imgview); \
	s32 (*dl_vk_create_rp)( \
			struct vk_dev_t *dev, \
			struct vk_rp_create_info_t *info, \
			void *allocator, \
			struct vk_rp_t **rp); \
	s32 (*dl_vk_create_fb)( \
			struct vk_dev_t *dev, \
			struct vk_fb_create_info_t *info, \
			void *allocator, \
			struct vk_fb_t **fb); \
	s32 (*dl_vk_create_shmod)( \
			struct vk_dev_t *dev, \
			struct vk_shmod_create_info_t *info, \
			void *allocator, \
			struct vk_shmod_t **shmod); \
	s32 (*dl_vk_reset_cb)( \
			struct vk_cb_t *cb, \
			u32 flags); \
	void (*dl_vk_destroy_shmod)( \
			struct vk_dev_t *dev, \
			struct vk_shmod_t *shmod, \
			void *allocator); \
	s32 (*dl_vk_create_pl_layout)( \
			struct vk_dev_t *dev, \
			struct vk_pl_layout_create_info_t *info, \
			void *allocator, \
			struct vk_pl_layout_t **pl_layout); \
	s32 (*dl_vk_create_gfx_pls)( \
			struct vk_dev_t *dev, \
			struct vk_pl_cache_t *pl_cache, \
			u32 create_infos_n, \
			struct vk_gfx_pl_create_info_t *infos, \
			void *allocator, \
			struct vk_pl_t **pl); \
	void (*dl_vk_get_img_subrsrc_layout)( \
			struct vk_dev_t *dev, \
			struct vk_img_t *img, \
			struct vk_img_subrsrc_t *subrsrc, \
			struct vk_subrsrc_layout_t *layout); \
	void (*dl_vk_cmd_begin_rp)( \
			struct vk_cb_t *cb, \
			struct vk_rp_begin_info_t *rp_info, \
			struct vk_sp_begin_info_t *sp_info); \
	void (*dl_vk_cmd_end_rp)( \
			struct vk_cb_t *cb, \
			struct vk_sp_end_info_t *info); \
	void (*dl_vk_cmd_bind_pl)( \
			struct vk_cb_t *cb, \
			u32 pl_bind_point, \
			struct vk_pl_t *pl); \
	void (*dl_vk_cmd_blit_img)( \
			struct vk_cb_t *cb, \
			struct vk_img_t *src_img, \
			u32 src_img_layout, \
			struct vk_img_t *dst_img, \
			u32 dst_img_layout, \
			u32 regions_n, \
			struct vk_img_blit_t *regions, \
			u32 filter); \
	void (*dl_vk_cmd_draw)( \
			struct vk_cb_t *cb, \
			u32 vtxs_n, \
			u32 instances_n, \
			u32 first_vtx, \
			u32 first_instance); \
	void (*dl_vk_cmd_clr_color_img)(\
				struct vk_cb_t *cb,\
				struct vk_img_t *img,\
				u32 img_layout,\
				union vk_clr_color_val_t *color,\
				u32 ranges_n,\
				struct vk_img_subrsrc_range_t *ranges);\
	s32 (*dl_vk_create_fence)(\
				struct vk_dev_t *dev,\
				struct vk_fence_create_info_t *info,\
				void * allocator,\
				struct vk_fence_t **fence);\
	s32 (*dl_vk_get_fence_status)(\
				struct vk_dev_t *dev,\
				struct vk_fence_t *fence);\
	s32 (*dl_vk_reset_fences)(\
				struct vk_dev_t *dev,\
				u32 fences_n,\
				struct vk_fence_t **fences);\
	s32 (*dl_vk_wait_for_fences)(\
				struct vk_dev_t *dev,\
				u32 fences_n,\
				struct vk_fence_t **fences,\
				u32 wait_all,\
				u64 timeout);
#endif
