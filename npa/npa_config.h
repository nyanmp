/*
 * only basic esc seq soup for keyboard input, "ansi/ecma-48 + VTXXX + xterm"
 * utf8 terminal configured for 2 bytes c1 codes, hence the csi is the esc
 * ascii byte followed by the '[' ascii char. to account for any unknown esc
 * seq, we have a global timeout.
 * you have very good documentation on VTXXX (52/100/5xx) on the web, that
 * to understand well what is a terminal and from "where" all that comes
 * from.
 * 
 * or 1 utf8 char (up to 4 bytes and not 6 like in legacy iso)
 */
struct tty_bind_t {
	u8 *c;			/* utf8 char, up to 4 bytes, or esc seq */
	u8 sz;			/* size of whatever is c */
	u8 *name;		/* friendly name */
	void (*cmd)(void);	/* bound cmd */
};
#define INPUT_ESC_SEQ_TIMEOUT_SECS_N 1
#define TTY_BIND(x, y, z) {.c = x, .sz = (sizeof(x) - 1), .name = y, .cmd = z}
struct tty_bind_t tty_binds[] = {
	TTY_BIND("q", "letter q", cmd_quit),
	TTY_BIND("i", "letter i", cmd_info),
	TTY_BIND("\x1b[D", "arrow left", cmd_rewind),
	TTY_BIND("\x1b[C", "arrow right", cmd_fastforward),
	TTY_BIND("\x1b[A", "arrow up", cmd_vol_up),
	TTY_BIND("\x1b[B", "arrow down", cmd_vol_down),
	TTY_BIND("]", "closing square bracket", cmd_vol_up_small),
	TTY_BIND("[", "opening square bracket", cmd_vol_down_small),
	TTY_BIND("\x1b[H", "home", cmd_rewind_big),
	TTY_BIND("\x1b[4~", "end", cmd_fastforward_big),
	TTY_BIND("m", "letter m", cmd_mute),
	TTY_BIND(" ", "space", cmd_pause)
};
#undef TTY_BIND

#define VOL_DELTA 0.1 /* from 0.0 to 1.0 */
#define VOL_DELTA_SMALL 0.01 /* from 0.0 to 1.0 */
#define SEEK_DELTA INT64_C(10) /* 10 seconds */
#define SEEK_DELTA_BIG (INT64_C(4) * INT64_C(60)) /* 4 minutes */
