#ifndef NPA_C
#define NPA_C
/*
 * code protected with a GNU affero GPLv3 license 
 * copyright (C) 2020 Sylvain BERTRAND
 */
/*
 * ABBREVIATIONS
 *
 * abuf : Audio BUFfer
 * async : ASYNChronous
 * b(s) : Byte(S) (often)
 * buf(s) : BUFfer(S)
 * cb : Cicular Buffer
 * chan(s) : CHANnel(S)
 * cmd(s) : CoMmanD(S)
 * cp(s) : Code Point(s)
 * cfg : ConFiGuration
 * ctx : ConTeX
 * dec : DECoder/DECoded
 * desc : DESCription
 * dev : DEVice
 * e : End (usually a pointer on the byte past the last valid byte)
 * eof : End Of File
 * esc : ESCape
 * ep : EPoll
 * evt(s) : EVenT(S)
 * err : ERRor
 * fd : File Descriptor
 * ff : FFmpeg
 * filt : FILTer
 * fmt : ForMaT
 * fr(s) : FRame(S)
 * inc : INCrement
 * idx(s) : InDeX(S)
 * l10n : LocalizatioN (10 chars between L and N)
 * min(s) : MINute(S)
 * msec(s)  : MilliSECond(S)
 * n : couNt
 * nr : NumbeR
 * out : OUTput
 * pkt(s) : PacKeT(S)
 * pts : Presentation TimeStamp
 * rd : ReaD
 * ref(s) : REFerence(S)
 * s : Start
 * sd : SenD
 * sec(s) : SECond(S)
 * seq : SEQuence
 * sig(s) : SIGnal(S)
 * st : STream
 * str : STRing
 * sync : SYNChronous
 * sz : SiZe
 * thd(s) : THreaD(S)
 * tb : Time Base
 * ts : TimeStamp
 * vol  : VOLume
 */
/*
 * this is not a library, then we could not care less about memory management
 * and/or similar cleanup: we have a virtual machine with a garbage collector,
 * it is linux.
 *
 * we do presume we won't play more than 8 chans and ff chans layout will
 * fit alsa chans map
 *
 * XXX: we don't know how the alsa silence machinery works, then we use brutal
 * silence bufs
 */
/*----------------------------------------------------------------------------*/
/* C/posix */
#include <stdbool.h>
#include <locale.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <signal.h>
#include <pthread.h>
#include <unistd.h>
#include <termios.h>
#include <stdint.h>
#include <fcntl.h>
#if __GNUC__ > 4
	#include <stdatomic.h>
#endif
#include <limits.h>
#include <time.h>
#include <stdarg.h>
/* linux and compatible */
#include <sys/epoll.h>
#include <sys/signalfd.h>
#include <sys/timerfd.h>
/* ff */
#include <libavformat/avformat.h>
#include <libavcodec/avcodec.h>
#include <libavfilter/avfilter.h>
#include <libavfilter/buffersrc.h>
#include <libavfilter/buffersink.h>
#include <libavutil/opt.h>
/* alsa */
#include <alsa/asoundlib.h>
/*============================================================================*/
/* namespaces -- START */
/*----------------------------------------------------------------------------*/
/* fix C -- START */
#define u8 uint8_t
#define U8_MAX 255
#define u32 uint32_t
#define u16 uint16_t
#define f32 float
#define loop for(;;)
#if UCHAR_WIDTH == 8
	#if __GNUC__ > 4
		#define atomic_u8 atomic_uchar
	#else
		#define atomic_u8 u8
		#define atomic_uint unsigned int
		#define atomic_int int
		#define atomic_load(x) __atomic_load_n(x,__ATOMIC_SEQ_CST)
		#define atomic_store(x,y) __atomic_store_n(x,y,__ATOMIC_SEQ_CST)
	#endif
#else
	#error "unable to find the right atomic for a 8 bits byte, be sure to have __STDC_WANT_IEC_60559_BFP_EXT__ defined on recent gcc"
#endif
/* fix C -- END */
/*----------------------------------------------------------------------------*/
/* ff namespace -- START*/
#define AV_FR_FMT_NONE AV_SAMPLE_FMT_NONE
#define av_bufsink_get_frs av_buffersink_get_frame
#define av_bufsrc_add_frs_flags av_buffersrc_add_frame_flags
#define AV_BUFSRC_FLAG_KEEP_REF AV_BUFFERSRC_FLAG_KEEP_REF
#define AV_BUFSRC_FLAG_PUSH AV_BUFFERSRC_FLAG_PUSH
#define av_codec const AVCodec
#define av_codec_ctx AVCodecContext
#define av_dump_fmt av_dump_format
#define av_duration_estimation_method AVDurationEstimationMethod
#define av_filt AVFilter
#define av_filt_ctx AVFilterContext
#define av_filt_get_by_name avfilter_get_by_name
#define av_filt_graph AVFilterGraph
#define av_filt_graph_alloc avfilter_graph_alloc
#define av_filt_graph_alloc_filt avfilter_graph_alloc_filter
#define av_filt_graph_cfg avfilter_graph_config
#define av_filt_graph_dump avfilter_graph_dump
#define av_filt_graph_free avfilter_graph_free
#define av_filt_graph_send_cmd avfilter_graph_send_command
#define av_filt_init_str avfilter_init_str
#define av_filt_link avfilter_link
#define av_find_best_st av_find_best_stream
#define av_flush_bufs avcodec_flush_buffers
#define av_fmt_ctx AVFormatContext
#define AV_FMT_DURATION_FROM_BITRATE AVFMT_DURATION_FROM_BITRATE
#define AV_FMT_DURATION_FROM_PTS AVFMT_DURATION_FROM_PTS
#define AV_FMT_DURATION_FROM_ST AVFMT_DURATION_FROM_STREAM
#define av_fmt_find_st_info avformat_find_stream_info
#define av_fmt_flush avformat_flush
#define av_fmt_open_input avformat_open_input
#define av_fr_fmt_is_planar av_sample_fmt_is_planar
#define AV_FR_FMT_DBL AV_SAMPLE_FMT_DBL
#define AV_FR_FMT_DBLP AV_SAMPLE_FMT_DBLP
#define AV_FR_FMT_FLT AV_SAMPLE_FMT_FLT
#define AV_FR_FMT_FLTP AV_SAMPLE_FMT_FLTP
#define AV_FR_FMT_S16 AV_SAMPLE_FMT_S16
#define AV_FR_FMT_S16P AV_SAMPLE_FMT_S16P
#define AV_FR_FMT_S32 AV_SAMPLE_FMT_S32
#define AV_FR_FMT_S32P AV_SAMPLE_FMT_S32P
#define AV_FR_FMT_U8P AV_SAMPLE_FMT_U8P
#define AV_FR_FMT_U8 AV_SAMPLE_FMT_U8
#define av_frs_alloc av_frame_alloc
#define av_frs_set_silence av_samples_set_silence
#define av_frs_unref av_frame_unref
#define av_get_fr_fmt_name av_get_sample_fmt_name
#define av_get_fr_fmt_str av_get_sample_fmt_string
#define av_io_flush avio_flush
#define AV_MEDIA_TYPE_AUDIO AVMEDIA_TYPE_AUDIO
#define av_pkt AVPacket
#define av_pkt_unref av_packet_unref
#define av_read_pkt av_read_frame
#define av_rational AVRational
#define av_seek_pkt av_seek_frame
#define av_receive_frs avcodec_receive_frame
#define av_fr_fmt AVSampleFormat
#define av_frs AVFrame
#define av_sd_pkt avcodec_send_packet
#define av_st AVStream
#define fmt format
#define fr_fmt sample_fmt
#define fr_rate sample_rate
#define frs_n nb_samples
#define st_idx stream_index
#define sts streams
#define tb time_base
/* ff namespace -- END */
/*----------------------------------------------------------------------------*/
/* alsa namespace -- START */
#define snd_pcm_fmt_desc snd_pcm_format_description
#define snd_pcm_fmt_set_silence snd_pcm_format_set_silence
#define SND_PCM_FMT_FLT SND_PCM_FORMAT_FLOAT
#define SND_PCM_FMT_S16 SND_PCM_FORMAT_S16
#define SND_PCM_FMT_S24 SND_PCM_FORMAT_S24
#define SND_PCM_FMT_S32 SND_PCM_FORMAT_S32
#define SND_PCM_FMT_S8 SND_PCM_FORMAT_S8
#define snd_pcm_fmt_t snd_pcm_format_t
#define SND_PCM_FMT_U16 SND_PCM_FORMAT_U16
#define SND_PCM_FMT_U24 SND_PCM_FORMAT_U24
#define SND_PCM_FMT_U32 SND_PCM_FORMAT_U32
#define SND_PCM_FMT_U8 SND_PCM_FORMAT_U8
#define snd_pcm_frs_to_bytes snd_pcm_frames_to_bytes
#define snd_pcm_hw_params_get_buf_size snd_pcm_hw_params_get_buffer_size
#define snd_pcm_hw_params_get_chans  snd_pcm_hw_params_get_channels
#define snd_pcm_hw_params_get_chans_max snd_pcm_hw_params_get_channels_max
#define snd_pcm_hw_params_get_chans_min snd_pcm_hw_params_get_channels_min
#define snd_pcm_hw_params_get_fmt snd_pcm_hw_params_get_format
#define snd_pcm_hw_params_set_buf_size_near snd_pcm_hw_params_set_buffer_size_near
#define snd_pcm_hw_params_set_chans snd_pcm_hw_params_set_channels
#define snd_pcm_hw_params_set_fmt snd_pcm_hw_params_set_format
#define snd_pcm_hw_params_test_chans snd_pcm_hw_params_test_channels
#define snd_pcm_hw_params_test_fmt snd_pcm_hw_params_test_format
#define snd_pcm_sfrs_t snd_pcm_sframes_t
#define SND_PCM_ST_PLAYBACK SND_PCM_STREAM_PLAYBACK
#define snd_pcm_ufrs_t snd_pcm_uframes_t
/* alsa namespace -- END */
/* namespaces -- END */
/*============================================================================*/
#define ARRAY_N(x) (sizeof(x) / sizeof((x)[0]))
#define STR_SZ 255 /* sz and idx fit in 1 byte */
/*---------------------------------------------------------------------------*/
static u8 *current_url;
static int current_st_idx;
/* cmd_info must be fast, then a lockless copy of its data */
static struct {
	struct {
		int64_t duration;
		enum av_duration_estimation_method m;
	} fmt;
	struct {
		av_rational tb;
		int id;
		int64_t duration;
	} st;
} cmd_info_data;
/*---------------------------------------------------------------------------*/
/* linux and compatible */
static int ep_fd;
static int sig_fd;
/*---------------------------------------------------------------------------*/
/* alsa */
static snd_pcm_t *pcm_g;
static snd_output_t *pcm_pout;
static snd_output_t *pcm_perr;
#define PCM_POLLFDS_N_MAX 16 /* banzai */
static struct pollfd pcm_pollfds[PCM_POLLFDS_N_MAX];
static u8 pcm_pollfds_n;
/*---------------------------------------------------------------------------*/
/* ff dec */
static av_pkt *rd_thd_pkt;
static av_fmt_ctx *fmt_ctx;
static pthread_mutex_t fmt_ctx_mutex;
static av_codec *dec;
static av_codec_ctx *dec_ctx;
/*---------------------------------------------------------------------------*/
/* ff filt graph */
static av_filt_graph *filt_graph;
static av_filt_ctx *abufsrc_ctx;
static const av_filt *abufsrc_filt;
static struct { /* used to detected reconfiguration */
	AVChannelLayout chans_layout;
	int rate;
	enum av_fr_fmt fmt;
} abufsrc_key;
static av_filt_ctx *vol_ctx;
static const av_filt *vol_filt;
static u8 double_zero_l10n_str[sizeof("xxx.xx")];
static av_filt_ctx *afmt_ctx;
static const av_filt *afmt_filt;
static av_filt_ctx *abufsink_ctx;
static const av_filt *abufsink_filt;
/*---------------------------------------------------------------------------*/
/* a formally proven concurrently accessed lockless cb */
struct {
	atomic_u8 reset;
	atomic_u8 hold;

	atomic_int st_idx;	
	av_pkt **pkts;
	unsigned int pkts_n;
	atomic_uint rd;
	atomic_uint sd;
} cb;
/* running state */
struct {
	av_frs *av;
	bool pushed_in_filt_graph;
	int64_t most_recent_ts; /* a very "coarse-grained" clock */
} dec_frs;
struct {
	av_frs *av;
	float vol;
	bool muted;
	snd_pcm_ufrs_t pcm_written_ufrs_n;
} filt_frs;
/* we will inject silence frs while paused */
bool paused;
void *silence_bufs[AV_NUM_DATA_POINTERS];
/*----------------------------------------------------------------------------*/
/* tty */
static bool stdin_tty_cfg_modified;
static struct termios stdin_tio_save;
static int stdin_flags_save;
static bool stdout_is_tty;
/*----------------------------------------------------------------------------*/
static void cmd_quit(void);
static void cmd_rewind(void);
static void cmd_fastforward(void);
static void cmd_rewind_big(void);
static void cmd_fastforward_big(void);
static void cmd_vol_up(void);
static void cmd_vol_down(void);
static void cmd_vol_up_small(void);
static void cmd_vol_down_small(void);
static void cmd_mute(void);
static void cmd_info(void);
static void cmd_pause(void);
/*--------------------------------------------------------------------------*/
#include "npa_config.h"
/*----------------------------------------------------------------------------*/
/* input "state" machine (2 major states: "utf8" and "esc seq" */
static int input_timer_fd;
static bool input_esc_seq_mode;
static u8 input_b; /* input byte */

static u8 utf8_cp[4];
static u8 utf8_cp_next_byte; /* idx in utf8_cp */
static u8 utf8_cp_sz;

static u8 esc_seq[STR_SZ];
static u8 esc_seq_next_byte; /* idx in esc_seq */
#define esc_seq_sz esc_seq_next_byte /* the idx of the next byte is its sz */
/*----------------------------------------------------------------------------*/
static void pout(u8 *fmt, ...);
static void perr(u8 *fmt, ...);
static void warning(u8 *fmt, ...);
static void fatal(u8 *fmt, ...);
static void exit_ok(u8 *fmt, ...);
/*----------------------------------------------------------------------------*/
static void stdin_flags_restore(void)
{
	int r;

	r = fcntl(0, F_SETFL, stdin_flags_save);
	if (r == -1)
		warning("input:unable to restore the file flags of the standard input\n");
}
static void stdin_tty_cfg_restore(void)
{
	int r;
	struct termios tio_chk;

	if (!stdin_tty_cfg_modified)
		return;
	r = tcsetattr(0, TCSANOW, &stdin_tio_save);
	if (r == -1) {
		warning("input:unable to restore the terminal line attributes\t");
		return;
	}
	memset(&tio_chk, 0, sizeof(tio_chk));
	r = tcgetattr(0, &tio_chk);
	if (r == -1) {
		warning("input:unable to get the current terminal line attributes for restoration checking\n");
		return;
	}
	r = memcmp(&tio_chk, &stdin_tio_save, sizeof(tio_chk));
	if (r != 0)
		warning("input:only partial restoration of the terminal line attributes\n");
}
/*----------------------------------------------------------------------------*/
static void pout(u8 *fmt, ...)
{
	va_list ap;

	va_start(ap, fmt);
	vfprintf(stdout, fmt, ap);
	va_end(ap);
}
static void perr(u8 *fmt, ...)
{
	va_list ap;

	va_start(ap, fmt);
	vfprintf(stderr, fmt, ap);
	va_end(ap);
}
static void warning(u8 *fmt, ...)
{
	va_list ap;

	fprintf(stderr, "warning:");
	va_start(ap, fmt);
	vfprintf(stderr, fmt, ap);
	va_end(ap);
}
static void fatal(u8 *fmt, ...)
{
	va_list ap;

	fprintf(stderr, "fatal:");
	va_start(ap, fmt);
	vfprintf(stderr, fmt, ap);
	va_end(ap);
	stdin_flags_restore();
	stdin_tty_cfg_restore();
	exit(EXIT_FAILURE);
}
static void exit_ok(u8 *fmt, ...)
{
	va_list ap;

	fprintf(stderr, "exit_ok:");
	va_start(ap, fmt);
	vfprintf(stderr, fmt, ap);
	va_end(ap);
	stdin_flags_restore();
	stdin_tty_cfg_restore();
	exit(EXIT_SUCCESS);
}
/*----------------------------------------------------------------------------*/
static void fmt_ctx_lock(void)
{
	int r;

	r = pthread_mutex_lock(&fmt_ctx_mutex);
	if (r != 0)
		fatal("unable to lock the format context\n");
}
static void fmt_ctx_unlock(void)
{
	int r;

	r = pthread_mutex_unlock(&fmt_ctx_mutex);
	if (r != 0)
		fatal("unable to unlock the format context\n");
}
static u8 *duration_estimate_to_str(enum av_duration_estimation_method m)
{
	switch (m) {
	case AV_FMT_DURATION_FROM_PTS:
		return "from PTS(Presentation TimeStamp)";
	case AV_FMT_DURATION_FROM_ST:
		return "from stream";
	case AV_FMT_DURATION_FROM_BITRATE:
		return "from bitrate";
	default:
		return "unkwown";
	}
}
/* meh... */
static u8 *ts_to_str(int64_t ts, av_rational tb, int64_t *remaining)
{
	static u8 str[sizeof("~S00:00:00.000 remains S9223372036854775807 time base units")];
	bool is_neg;
	int64_t hours_n;
	int64_t mins_n;
	int64_t secs_n;
	int64_t msecs_n;
	int64_t one_hour; /* in ff tb units */
	int64_t one_min; /* in ff tb units */
	int64_t one_sec; /* in ff tb units */
	int64_t one_msec; /* in ff tb units */

	if (ts < 0) {
		ts = -ts;
		is_neg = true;
	} else
		is_neg = false;
	one_hour = INT64_C(3600) * (int64_t)tb.den / (int64_t)tb.num;
	one_min = INT64_C(60) * (int64_t)tb.den / (int64_t)tb.num;
	one_sec = (int64_t)tb.den / (int64_t)tb.num;
	one_msec = one_sec / INT64_C(1000);

	hours_n = ts / one_hour;

	*remaining = ts % one_hour;	
	mins_n = *remaining / one_min;

	*remaining = *remaining % one_min;
	secs_n = *remaining / one_sec;

	*remaining = *remaining % one_sec;
	msecs_n = *remaining / one_msec;

	/* account for all rounding errors */
	*remaining = ts - (hours_n * one_hour + mins_n * one_min
				+ secs_n * one_sec + msecs_n * one_msec);
	if (!is_neg) 
		snprintf(str, sizeof(str), "%02"PRId64":%02"PRId64":%02"PRId64".%03"PRId64, hours_n, mins_n, secs_n, msecs_n);
	else {
		str[0] = '-';
		snprintf(str + 1, sizeof(str) - 1, "%02"PRId64":%02"PRId64":%02"PRId64".%03"PRId64, hours_n, mins_n, secs_n, msecs_n);
	}
	return str;
}
#define RED if (stdout_is_tty) pout("\x1b[38;2;255;0;0m")
#define GREEN if (stdout_is_tty) pout("\x1b[38;2;0;255;0m")
#define BLUE if (stdout_is_tty) pout("\x1b[38;2;0;0;255m")
#define PURPLE if (stdout_is_tty) pout("\x1b[38;2;255;0;255m")
#define RESTORE if (stdout_is_tty) pout("\x1b[39;49m")
static void cmd_info(void)
{
	u8 *ts_str;
	int64_t remaining;
	u8 duration_str[sizeof("S9223372036854775807")];

	RESTORE;
	GREEN;pout("================================================================================\n");RESTORE;
	PURPLE;pout("%s\n", current_url);RESTORE;
	ts_str = ts_to_str(dec_frs.most_recent_ts, cmd_info_data.st.tb, &remaining);
	RED;pout("%s", ts_str);RESTORE;
	if (remaining != 0)
		pout(" remaining %"PRId64" time base units", remaining);
	else
		pout("\n");
	pout("\t%"PRId64" stream time base units (%d/%d seconds)\n", dec_frs.most_recent_ts, cmd_info_data.st.tb.num, cmd_info_data.st.tb.den);
	BLUE;pout("--------------------------------------------------------------------------------\n");RESTORE;
	pout("format:");
	if (cmd_info_data.fmt.duration == AV_NOPTS_VALUE) {
		pout("duration is not provided\n");
	} else {
		snprintf(duration_str, sizeof(duration_str), "%"PRId64, cmd_info_data.fmt.duration);
		ts_str = ts_to_str(cmd_info_data.fmt.duration, AV_TIME_BASE_Q,
								&remaining);
		pout("duration=");RED;pout("%s", ts_str);RESTORE;
		if (remaining != 0)
			pout(" remaining %"PRId64" av_time_base units\n", remaining);
		else
			pout("\n");
		pout("\t%s av_time_base units (1/%d seconds)\n\testimation method is %s\n", duration_str, AV_TIME_BASE, duration_estimate_to_str(cmd_info_data.fmt.m));
	}
	pout("stream:id=%d", cmd_info_data.st.id);
	if (cmd_info_data.st.duration == AV_NOPTS_VALUE) {
		pout(";duration is not provided\n");
	} else {
		snprintf(duration_str, sizeof(duration_str), "%"PRId64, cmd_info_data.st.duration);
		ts_str = ts_to_str(cmd_info_data.st.duration, cmd_info_data.st.tb, &remaining);
		pout(";duration=");RED;pout("%s\n", ts_str);RESTORE;
		if (remaining != 0)
			pout(" remaining %"PRId64" stream time base units\n", remaining);
		else
			pout("\n");
		pout("\t%s stream time base units (%d/%d seconds)\n", duration_str, cmd_info_data.st.tb.num, cmd_info_data.st.tb.den);
	}
	BLUE;pout("--------------------------------------------------------------------------------\n");RESTORE;
	pout("circular buffer: %u/%u/%u (read/send/max)\n", atomic_load(&cb.rd), atomic_load(&cb.sd), cb.pkts_n);
	if (paused) {
		RED;pout("paused\n");RESTORE;
	}
	if (filt_frs.muted) {
		RED;pout("muted\n");RESTORE;
	}
	GREEN;pout("^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n");RESTORE;
}
#undef RED
#undef GREEN
#undef BLUE
#undef PURPLE
#undef RESTORE
static void wait(long ns)
{
	struct timespec wanted;
	struct timespec rem;

	memset(&wanted, 0, sizeof(wanted));
	memset(&rem, 0, sizeof(rem));
	wanted.tv_nsec = ns;
	loop {
		int r;

		/* linux bug: cannot specify CLOCK_MONOTONIC_RAW */
		r = clock_nanosleep(CLOCK_MONOTONIC, 0, &wanted, &rem);
		if (r == 0)
			break;
		if (r != EINTR)
			fatal("wait timer failed:%d\n", r);
		/* r == EINTR */
		memcpy(&wanted, &rem, sizeof(wanted));
		memset(&rem, 0, sizeof(rem));
	}
}
static void wait_cb_filling(void)
{
	struct timespec wanted;
	struct timespec rem;

	memset(&wanted, 0, sizeof(wanted));
	memset(&rem, 0, sizeof(rem));
	wanted.tv_nsec = 4000000; /* 4 ms */
	loop {
		int r;

		/* linux bug: cannot specify CLOCK_MONOTONIC_RAW */
		r = clock_nanosleep(CLOCK_MONOTONIC, 0, &wanted, &rem);
		if (r == 0) {
			unsigned int rd;

			rd = atomic_load(&cb.rd);
			if (rd == (cb.pkts_n - 1))
				break;
			memset(&wanted, 0, sizeof(wanted));
			memset(&rem, 0, sizeof(rem));
			wanted.tv_nsec = 4000000; /* 4 ms */
			continue;
		}
		if (r != EINTR)
			fatal("wait cb filling timer failed:%d\n", r);
		/* r == EINTR */
		memcpy(&wanted, &rem, sizeof(wanted));
		memset(&rem, 0, sizeof(rem));
	}
}
#define RESET_DONE 0
static void do_reset(void)
{
	unsigned int i;

	fmt_ctx_lock();
	av_fmt_flush(fmt_ctx);
	av_io_flush(fmt_ctx->pb);
	fmt_ctx_unlock();
	i = 0;
	loop {
		av_pkt_unref(cb.pkts[i]);
		if (i == cb.pkts_n)
			break;
		++i;
	}
	atomic_store(&cb.rd, 0);
	atomic_store(&cb.sd, 0);
	atomic_store(&cb.reset, RESET_DONE);
}
#undef RESET_DONE
#define DO_RESET 1
#define HOLD 1
static void rd_loop(void) { loop /* infinite loop */
{
	int r;
	unsigned int rd;
	unsigned int next_rd;
	unsigned int sd;
	u8 reset;
	int st_idx;
	u8 hold;
	/*
	 * XXX: we actually perform reset on this thd, with a little lockfree
	 * protocol and reasonable wait loops
	 */
	reset = atomic_load(&cb.reset);
	if (reset == DO_RESET)
		do_reset();
	/*--------------------------------------------------------------------*/
	hold = atomic_load(&cb.hold);
	if (hold == HOLD) {
		wait(1000000); /* 1ms */
		continue;
	}
	/*--------------------------------------------------------------------*/
	rd = atomic_load(&cb.rd);
	sd = atomic_load(&cb.sd);
	next_rd = (rd + 1) % cb.pkts_n;
	if (next_rd == sd) { /* must sd first the sd slot */
		wait(1000000); /* 1ms */
		continue;
	}
	/*--------------------------------------------------------------------*/
	fmt_ctx_lock();
	r = av_read_pkt(fmt_ctx, rd_thd_pkt);
	fmt_ctx_unlock();
	if (r == AVERROR(EAGAIN)) {
		/* 1ms: it is sort of aggressive to check for reset  */
		wait(1000000);
		continue;
	} else if (r == AVERROR_EOF) {
		/* The "NULL" pkt ref to signal the EOF to the dec */
		cb.pkts[rd]->data = 0;
		cb.pkts[rd]->size = 0;
	} else if (r != 0)
		fatal("ffmpeg:error while demuxing coded/compressed data into packets\n");
	else { /* r == 0 */
		st_idx = atomic_load(&cb.st_idx);
		if (rd_thd_pkt->st_idx != st_idx) { /* sd_idx can be -1 */
			av_pkt_unref(rd_thd_pkt);
			continue;
		}
		av_packet_move_ref(cb.pkts[rd], rd_thd_pkt);
	}
	atomic_store(&cb.rd, next_rd);
}}
#undef DO_RESET
#undef HOLD
static void *rd_thd_entry(void *arg)
{
	int r;
	sigset_t sset;

	r = sigfillset(&sset);
	if (r == -1)
		fatal("read thread:unable to get a full signal mask\n");
	r = pthread_sigmask(SIG_SETMASK, &sset, 0);
	if (r != 0)
		fatal("read thread:unable to \"block\" \"all\" signals\n");
	rd_loop();
	/* unreachable */
}
static void rd_thd_start(int st_index)
{
	int r;
	pthread_t id;
	pthread_attr_t attr;

	atomic_store(&cb.st_idx, st_index);
	r = pthread_attr_init(&attr);
	if (r != 0)
		fatal("read thread:unable to initialize read thread attribute\n");
	r = pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_DETACHED);
	if (r != 0)
		fatal("read thread:unable to set the read thread attribute to detach mode\n");
	r = pthread_create(&id, &attr, &rd_thd_entry, 0);
	if (r != 0)
		fatal("read thread:unable to create the read thread\n");
	pout("read thread %lu\n", (unsigned long)id);
	pthread_attr_destroy(&attr);
}
static void cmd_quit(void)
{
	exit_ok("quit command received\n");
}
#define RESET_DONE	0
#define DO_RESET	1
static void rd_thd_reset(int st_idx)
{
	u32 loops_n;

	loops_n = 0;
	/* reset = RESET_DONE */
	atomic_store(&cb.st_idx, st_idx);
	atomic_store(&cb.reset, DO_RESET);
	loop {
		u8 reset;

		reset = atomic_load(&cb.reset);	
		if (reset == RESET_DONE)
			break;
		wait(1000000); /* 1ms */
		++loops_n;
		if (loops_n == 4000) /* 1ms * 4000 = 4s */
			fatal("read thread reset timeout\n");
	}
	/* reset = DO_RESET */
}
#undef RESET_DONE
#undef DO_RESET
/*
 * XXX: if it is ever used significantly, a fine granularity wiring strategy
 * will be implemented instead of using the default wiring
 */
static void pcm_chmaps2ff_chans_layout(AVChannelLayout *ff_chans_layout, 
		snd_pcm_t *pcm, unsigned int pcm_chans_n, bool print_info)
{
	int r;	

	snd_pcm_chmap_t *pcm_chmap;
	u8 chans_layout_str[STR_SZ]; /* should be overkill */

	pcm_chmap = snd_pcm_get_chmap(pcm);
	if (pcm_chmap == 0) {
		if (print_info)
			pout("alsa:no pcm channel map available, wiring to default ffmpeg channel layout\n");
	} else {
		if (print_info)
			pout("alsa:your pcm device support channel maps, but fine granularity wiring strategy is not implemented\n");
		free(pcm_chmap);
	}
	av_channel_layout_default(ff_chans_layout, (int)pcm_chans_n);
	av_channel_layout_describe(ff_chans_layout, chans_layout_str,
						sizeof(chans_layout_str));
	if (print_info)
		pout("alsa channel map wired to ffmpeg channel layout:\"%s\" (%u pcm channels)\n", chans_layout_str, pcm_chans_n);
}
/* fatal if the wiring cannot be done */
static void pcm_layout2ff_fmt_strict(snd_pcm_fmt_t alsa_fmt,
	snd_pcm_access_t alsa_access, enum av_fr_fmt *ff_fmt, bool print_info)
{
	/*
	 * ff fmt byte order is always native.
	 * here we handle little endian only
	 */
	switch (alsa_fmt) {
	case SND_PCM_FMT_FLT:
		if (alsa_access == SND_PCM_ACCESS_RW_INTERLEAVED)
			*ff_fmt = AV_FR_FMT_FLT;
		else
			*ff_fmt = AV_FR_FMT_FLTP;
		break;
	case SND_PCM_FMT_S32:
		if (alsa_access == SND_PCM_ACCESS_RW_INTERLEAVED)
			*ff_fmt = AV_FR_FMT_S32;
		else
			*ff_fmt = AV_FR_FMT_S32P;
		break;
	case SND_PCM_FMT_S16:
		if (alsa_access == SND_PCM_ACCESS_RW_INTERLEAVED)
			*ff_fmt = AV_FR_FMT_S16;
		else
			*ff_fmt = AV_FR_FMT_S16P;
		break;
	case SND_PCM_FMT_U8:
		if (alsa_access == SND_PCM_ACCESS_RW_INTERLEAVED)
			*ff_fmt = AV_FR_FMT_U8;
		else
			*ff_fmt = AV_FR_FMT_U8P;
		break;
	default:
		fatal("unable to wire strictly alsa layout \"%s\"/\"%s\" to a ffmpeg format\n", snd_pcm_fmt_desc(alsa_fmt), snd_pcm_access_name(alsa_access));
	}
	if (print_info) {
		u8 ff_fmt_str[STR_SZ];

		av_get_fr_fmt_str(ff_fmt_str, sizeof(ff_fmt_str), *ff_fmt);
		pout("alsa pcm layout \"%s\"/\"%s\" wired strictly to ffmpeg format \"%sbits\"\n", snd_pcm_fmt_desc(alsa_fmt), snd_pcm_access_name(alsa_access), ff_fmt_str);
	}
}
static void pcm2ff(snd_pcm_t *pcm, enum av_fr_fmt *ff_fmt, int *ff_rate,
			AVChannelLayout *ff_chans_layout, bool print_info)
{
	int r;
	snd_pcm_hw_params_t *hw_params;
	snd_pcm_access_t pcm_access;
	snd_pcm_fmt_t pcm_fmt;
	unsigned int pcm_rate;
	unsigned int pcm_chans_n;
	
	r = snd_pcm_hw_params_malloc(&hw_params);
	if (r < 0)
		fatal("alsa:unable to allocate hardware parameters context for ffmpeg filter wiring\n");
	r = snd_pcm_hw_params_current(pcm, hw_params);
	if (r != 0)
		fatal("alsa:unable to get current hardware parameters for ffmpeg filter wiring\n");
	r = snd_pcm_hw_params_get_access(hw_params, &pcm_access);
	if (r < 0)
		fatal("alsa:unable to get the pcm access for ffmpeg filter wiring\n");
	r = snd_pcm_hw_params_get_fmt(hw_params, &pcm_fmt);
	if (r < 0)
		fatal("alsa:unable to get the pcm format for ffmpeg filter wiring\n");
	/*--------------------------------------------------------------------*/
	pcm_layout2ff_fmt_strict(pcm_fmt, pcm_access, ff_fmt, print_info);
	/*--------------------------------------------------------------------*/
	r = snd_pcm_hw_params_get_rate(hw_params, &pcm_rate,
							SND_PCM_ST_PLAYBACK);
	if (r < 0)
		fatal("alsa:unable to get the pcm rate for ffmpeg filter wiring\n");
	*ff_rate = (int)pcm_rate;
	r = snd_pcm_hw_params_get_chans(hw_params, &pcm_chans_n);
	if (r < 0)
		fatal("alsa:unable to get the pcm count of channels for ffmpeg filter wiring\n");
	/*--------------------------------------------------------------------*/
	pcm_chmaps2ff_chans_layout(ff_chans_layout, pcm, pcm_chans_n,
								print_info);
	/*--------------------------------------------------------------------*/
	snd_pcm_hw_params_free(hw_params);
}
/* XXX: we don't program the tb of the filter since it should not be used */
static void abufsrc_cfg(enum av_fr_fmt fmt, int rate,
				AVChannelLayout *chans_layout, bool print_info)
{
	int r;
	u8 chans_layout_str[STR_SZ]; /* should be overkill */

	abufsrc_filt = av_filt_get_by_name("abuffer");
	if (abufsrc_filt == 0)
		fatal("audio buffer source:could not find the filter\n");
	abufsrc_ctx = av_filt_graph_alloc_filt(filt_graph, abufsrc_filt,
								"src_abuf");
	if (abufsrc_ctx == 0)
		fatal("audio buffer source context:could not allocate the instance in the filter graph\n");
	r = av_opt_set(abufsrc_ctx, "sample_fmt", av_get_fr_fmt_name(fmt),
							AV_OPT_SEARCH_CHILDREN);
	if (r < 0)
		fatal("audio buffer source context:unable to set the decoder frame format option\n");
	r = av_opt_set_int(abufsrc_ctx, "sample_rate", rate,
							AV_OPT_SEARCH_CHILDREN);
	if (r < 0)
		fatal("audio buffer source context:unable to set the decoder rate option\n");
	av_channel_layout_describe(chans_layout, chans_layout_str,
						sizeof(chans_layout_str));
	if (print_info)
		pout("audio buffer source context:using channels layout \"%s\"\n", chans_layout_str);
	r = av_opt_set(abufsrc_ctx, "channel_layout", chans_layout_str,
							AV_OPT_SEARCH_CHILDREN);
	if (r < 0)
		fatal("audio buffer source context:unable to set the decoder channel layout option\n");
	r = av_filt_init_str(abufsrc_ctx, 0);
	if (r < 0)
		fatal("audio buffer source context:unable to initialize\n");
}
static void vol_cfg(bool muted, double vol)
{
	double vol_double;
	u8 vol_l10n_str[sizeof("xxx.xx")]; /* should be overkill */
	int r;

	vol_filt = av_filt_get_by_name("volume");
	if (vol_filt == 0)
        	fatal("volume:could not find the filter\n");
	vol_ctx = av_filt_graph_alloc_filt(filt_graph, vol_filt, "vol");
	if (vol_ctx == 0)
		fatal("volume context:could not allocate the instance in the filter graph\n");
	if (muted)
		vol_double = 0.0;
	else
		vol_double = vol;
	/* yeah the radix is localized, can be '.', ','... */
	snprintf(vol_l10n_str, sizeof(vol_l10n_str), "%f", vol_double);
	r = av_opt_set(vol_ctx, "volume", vol_l10n_str, AV_OPT_SEARCH_CHILDREN);
	if (r < 0)
		fatal("volume context:unable to set the volume option\n");
	r = av_filt_init_str(vol_ctx, 0);
	if (r < 0)
		fatal("volume buffer context:unable to initialize\n");
}
static void afmt_cfg(enum av_fr_fmt fmt, int rate,
				AVChannelLayout *chans_layout, bool print_info)
{
	int r;
	u8 rate_str[sizeof("dddddd")];
	u8 chans_layout_str[STR_SZ]; /* should be overkill */

	afmt_filt = av_filt_get_by_name("aformat");
	if (afmt_filt == 0)
		fatal("audio format:could not find the filter");
	afmt_ctx = av_filt_graph_alloc_filt(filt_graph, afmt_filt, "afmt");
	if (afmt_ctx == 0)
		fatal("audio format:could not allocate the instance in the filter graph\n");
	r = av_opt_set(afmt_ctx, "sample_fmts", av_get_fr_fmt_name(fmt),
							AV_OPT_SEARCH_CHILDREN);
	if (r < 0)
		fatal("audio format context:could to set the pcm sample format\n");
	snprintf(rate_str, sizeof(rate_str), "%d", rate);
	r = av_opt_set(afmt_ctx, "sample_rates", rate_str,
							AV_OPT_SEARCH_CHILDREN);
	if (r < 0)
		fatal("audio format context:could not set the pcm rate\n");
	av_channel_layout_describe(chans_layout, chans_layout_str,
						sizeof(chans_layout_str));
	r = av_opt_set(afmt_ctx, "channel_layouts", chans_layout_str,
							AV_OPT_SEARCH_CHILDREN);
	if (r < 0)
		fatal("audio format context:could not set the layout of channels\n");
	if (print_info)
		pout("audio format context:channel layout is \"%s\"\n", chans_layout_str);
	r = av_filt_init_str(afmt_ctx, 0);
	if (r < 0)
		fatal("audio format context:unable to initialize\n");
}
static void abufsink_cfg(void)
{
	int r;

	abufsink_filt = av_filt_get_by_name("abuffersink");
	if (abufsink_filt == 0)
		fatal("audio buffer sink:could not find the filter\n");
	abufsink_ctx = av_filt_graph_alloc_filt(filt_graph, abufsink_filt,
								"sink_abuf");
	if (abufsink_ctx == 0)
		fatal("audio buffer sink context:could not allocate the instance in the filter graph\n");
	r = av_filt_init_str(abufsink_ctx, 0);
	if (r < 0)
		fatal("audio buffer sink context:unable to initialize\n");
}
static void dec_ctx_cfg(AVCodecParameters *params)
{
	int r;

	dec = avcodec_find_decoder(params->codec_id);
	if (dec == 0)
		fatal("ffmpeg:unable to find a proper decoder\n");
	avcodec_free_context(&dec_ctx);
	dec_ctx = avcodec_alloc_context3(dec);
	if (dec_ctx == 0)
		fatal("ffmpeg:unable to allocate a decoder context\n");
	/* XXX: useless ? */
	r = avcodec_parameters_to_context(dec_ctx, params);
	if (r < 0)
		fatal("ffmpeg:unable to apply codec parameters in codec context\n");
	/* XXX: ffmpeg thread count default is 1, set to 0 = auto */
	dec_ctx->thread_count = 0;
	r = avcodec_open2(dec_ctx, dec, 0);
	if (r < 0)
		fatal("ffmpeg:unable to open the decoder context\n");
}
static void filt_graph_cfg(
		enum av_fr_fmt src_fmt, int src_rate, 
		AVChannelLayout *src_chans_layout,
		bool muted, double vol,
		enum av_fr_fmt dst_fmt, int dst_rate,
		AVChannelLayout *dst_chans_layout, bool print_info)
{
	int r;
	char *dump_str;

	av_filt_graph_free(&filt_graph);

	filt_graph = av_filt_graph_alloc();
	if (filt_graph == 0)
		fatal("unable to create filter graph\n");
	abufsrc_cfg(src_fmt, src_rate, src_chans_layout, print_info);
	/*--------------------------------------------------------------------*/
	r = av_channel_layout_copy(&abufsrc_key.chans_layout, src_chans_layout);
	if (r < 0)
		fatal("unable to copy the source channel layout in the key\n");
	abufsrc_key.rate = src_rate;
	abufsrc_key.fmt = src_fmt;
	/*--------------------------------------------------------------------*/
	vol_cfg(muted, vol);
	afmt_cfg(dst_fmt, dst_rate, dst_chans_layout, print_info);
	abufsink_cfg();
	r = av_filt_link(abufsrc_ctx, 0, vol_ctx, 0);
	if (r < 0)
		fatal("unable to connect the audio buffer source filter to the volume filter\n");
	r = av_filt_link(vol_ctx, 0, afmt_ctx, 0);
	if (r < 0)
		fatal("unable to connect the volume filter to the audio format filter\n");
        r = av_filt_link(afmt_ctx, 0, abufsink_ctx, 0);
	if (r < 0)
		fatal("unable to connect the audio format filter to the audio buffer sink filter\n");
	r = av_filt_graph_cfg(filt_graph, 0);
	if (r < 0)
		fatal("unable to configure the filter graph\n");
	/*--------------------------------------------------------------------*/
	if (!print_info)
		return;
	dump_str = av_filt_graph_dump(filt_graph, 0);
	if (dump_str == 0) {
		warning("unable to get a filter graph description\n");
		return;
	}
	pout("GRAPH START-------------------------------------------------------\n");
	pout("%s", dump_str);
	av_free(dump_str);
	pout("GRAPH END---------------------------------------------------------\n");
}
#define DONT_PRINT_INFO false
static void filt_flush(void)
{
	enum av_fr_fmt dst_fmt;
	int dst_rate;
	int dst_chans_n;
	AVChannelLayout src_chans_layout;
	AVChannelLayout dst_chans_layout;
	int r;

	memset(&src_chans_layout, 0, sizeof(src_chans_layout));
	memset(&dst_chans_layout, 0, sizeof(dst_chans_layout));

	av_frs_unref(filt_frs.av);
	filt_frs.pcm_written_ufrs_n = 0;

	pcm2ff(pcm_g, &dst_fmt, &dst_rate, &dst_chans_layout,
							DONT_PRINT_INFO);
	/* the audio dec ctx may not have a valid chans layout */
	r = av_channel_layout_check(&dec_ctx->ch_layout);
	if (r == 0) /* XXX: we expect at least chans_n to be valid */
		av_channel_layout_default(&src_chans_layout,
						dec_ctx->ch_layout.nb_channels);
	else
		av_channel_layout_copy(&src_chans_layout, &dec_ctx->ch_layout);
	filt_graph_cfg(
		dec_ctx->fr_fmt, dec_ctx->fr_rate, &src_chans_layout,
		filt_frs.muted, filt_frs.vol,
		dst_fmt, dst_rate, &dst_chans_layout,
		DONT_PRINT_INFO);
	av_channel_layout_uninit(&src_chans_layout);
	av_channel_layout_uninit(&dst_chans_layout);
}
#undef DONT_PRINT_INFO
static void stdin_tty_init_once(void)
{
	int r;
	struct termios tio_new;
	struct termios tio_chk;

	r = isatty(0);	
	if (r == 0) {
		pout("input:standard input is not a terminal\n");
		return;
	}
	memset(&tio_new, 0, sizeof(tio_new));
	memset(&tio_chk, 0, sizeof(tio_chk));
	memset(&stdin_tio_save, 0, sizeof(stdin_tio_save));
	r = tcgetattr(0, &stdin_tio_save);
	if (r == -1)
		fatal("input:unable to get the current standard input terminal line attributes\n");
	tio_new = stdin_tio_save;
	tio_new.c_lflag &= ~(ICANON|ECHO); 
	tio_new.c_cc[VMIN] = 1; /* 1 "char", could be bytes from a utf8 code point */
	tio_new.c_cc[VTIME] = 0;
	r = tcsetattr(0, TCSANOW, &tio_new);
	stdin_tty_cfg_modified = true;
	if (r == -1)
		fatal("input:unable to set all standard input terminal line\n");
	r = tcgetattr(0, &tio_chk);
	if (r == -1)
		fatal("input:unable to get the current standard input terminal line attributes for checking\n");
	r = memcmp(&tio_chk, &tio_new, sizeof(tio_chk));
	if (r != 0)
		fatal("input:setting the wanted terminal line attributes failed\n");
}
static void stdin_flags_init_once(void)
{
	int r;
	/* switch the standard input to non-blocking */
	r = fcntl(0, F_GETFL);
	if (r == -1)
		fatal("input:unable to get the file flags of the standard input\n");
	stdin_flags_save = r;
	r |= O_NONBLOCK;
	r = fcntl(0, F_SETFL, r);
	if (r == -1)
		fatal("input:unable to set non-blocking operations on the standard input\n");
}
static void stdout_init_once(void)
{
	int r;

	r = isatty(1);	
	if (r == 0) {
		pout("output:standard output not is not a terminal\n");
		stdout_is_tty = false;
		return;
	}
	stdout_is_tty = true;
}
/*
 * block as much as possible.
 * handle only async "usual" sigs, with sync signalfd.
 * allow some signals to go thru though.
 * always presume the process "controlling terminal" is different than the
 * terminal connected on standard input and standard output 
 */
static void sigs_init_once(void)
{
	int r;
	sigset_t sset;
	
	r = sigfillset(&sset);
	if (r == -1)
		fatal("unable to get a full signal mask\n");
	/* the "controlling terminal" line asks for a core dump, leave it be */
	r = sigdelset(&sset, SIGQUIT);
	if (r == -1)
		fatal("unable to remove SIGQUIT from our signal mask\n");
	r = pthread_sigmask(SIG_SETMASK, &sset, 0);
	if (r != 0)
		fatal("unable to \"block\" \"all\" signals\n");
	/* from here, we "steal" signals with signalfd */
	r = sigemptyset(&sset);
	if (r == -1)
		fatal("unable to get an empty signal mask\n");
	/* we are asked nicely to terminate */
	r = sigaddset(&sset, SIGTERM);
	if (r == -1)
		fatal("unable to add SIGTERM to our signal mask\n");
	/* the "controlling terminal" line (^c) asks nicely to terminate */
	r = sigaddset(&sset, SIGINT);
	if (r == -1)
		fatal("unable to add SIGINT to our signal mask\n");
	r = signalfd(-1, &sset, SFD_NONBLOCK);
	if (r == -1)
		fatal("unable to get a signalfd file descriptor\n");
	sig_fd = r;
}
static void evt_init_once(void)
{
	int r;
	u8 i;
	struct epoll_event evt;

	ep_fd = epoll_create1(0);
	if (ep_fd == -1)
		fatal("unable to create the epoll file descriptor\n");
	/*--------------------------------------------------------------------*/
	/* signals */
	evt.events = EPOLLIN;
	evt.data.fd = sig_fd;
	r = epoll_ctl(ep_fd, EPOLL_CTL_ADD, sig_fd, &evt);
	if (r == -1)
		fatal("unable to add the signalfd file descriptior to the epoll file descriptor\n");
	/*--------------------------------------------------------------------*/
	/* standard input/terminal */
	evt.events = EPOLLIN;
	evt.data.fd = 0;
	r = epoll_ctl(ep_fd, EPOLL_CTL_ADD, 0, &evt);
	if (r == -1)
		fatal("unable to add the standard input to the epoll file descriptor\n");
	/*--------------------------------------------------------------------*/
	/* the timer in charge of accounting unknown esc seq */
	evt.events = EPOLLIN;
	evt.data.fd = input_timer_fd;
	r = epoll_ctl(ep_fd, EPOLL_CTL_ADD, input_timer_fd, &evt);
	if (r == -1)
		fatal("unable to add the timer file descriptor accounting for unknown escape sequences to epoll file descriptor\n");
}
static void input_state_init_once(void)
{
	input_timer_fd = timerfd_create(CLOCK_MONOTONIC, TFD_NONBLOCK);
	if (input_timer_fd == -1)
		fatal("unable to get a timer file descriptor\n");

	input_esc_seq_mode = false;

	esc_seq_next_byte = 0;
	utf8_cp_next_byte = 0;
}
static void input_utf8_cp_bind_cmd(void)
{
	u16 i;

	i = 0;
	loop {
		struct tty_bind_t *bind;

		if (i == ARRAY_N(tty_binds))
			break;
		bind = tty_binds + i;

		/* exclude esc seq binds */	
		if ((bind->c[0] != 0x1b) && (utf8_cp_sz == bind->sz)) {
			int r;

			r = memcmp(utf8_cp, bind->c, utf8_cp_sz);
			if (r == 0) {
				bind->cmd();
				return;
			}
		}
		++i;
	}
}
/*
 * to exit the esc seq input mode, we could implement a smart tree in order to
 * know if it is not possible to get any defined esc seq, or we could already
 * use the maximum bind esc seq sz
 */
static bool input_esc_seq_bind_cmd(void)
{
	u16 i;

	i = 0;
	loop {
		struct tty_bind_t *bind;

		if (i == ARRAY_N(tty_binds))
			break;
		bind = tty_binds + i;
		/* only esc seq binds */	
		if ((bind->c[0] == 0x1b) && (esc_seq_sz == bind->sz)) {
			int r;

			r = memcmp(esc_seq, bind->c, esc_seq_sz);
			if (r == 0) {
				bind->cmd();
				return true;
			}
		}
		++i;
	}
	return false;
}
static void input_byte_esc_seq(void)
{
	int r;
	struct itimerspec t;

	if (input_b == 0x1b) { 
		esc_seq[0] = 0x1b;
		esc_seq_next_byte = 1;
		memset(&t, 0, sizeof(t));
		t.it_value.tv_sec = INPUT_ESC_SEQ_TIMEOUT_SECS_N;
		r = timerfd_settime(input_timer_fd, 0, &t, 0);
		if (r == -1)
			fatal("unable to arm the timer to account for unknown input escape sequence from the terminal\n");
		return;
	}
	esc_seq[esc_seq_next_byte] = input_b;
	++esc_seq_next_byte;
	if (!input_esc_seq_bind_cmd() && (esc_seq_next_byte != STR_SZ))
		return;
	memset(&t, 0, sizeof(t));
	r = timerfd_settime(input_timer_fd, 0, &t, 0);
	if (r == -1)
		fatal("unable to disarm the timer used to account for unknown input escape sequence from the terminal\n");
	esc_seq_next_byte = 0;
	input_esc_seq_mode = false;
	utf8_cp_next_byte = 0;
}
static void input_byte_utf8_cp(void)
{
	if ((input_b & 0x80) != 0) { /* utf8 cp > 0x7f */
		if ((input_b & 0x40) != 0) { /* utf8 cp start byte */
			utf8_cp[0] = input_b;
			utf8_cp_next_byte = 1;
			if ((input_b & 0x20) == 0)
				utf8_cp_sz = 2;
			else if ((input_b & 0x10) == 0)
				utf8_cp_sz = 3;
			else /* if ((input_b & 0x08) == 0) */
				utf8_cp_sz = 4; /* must be 4 */
			return;
		}
		/* (b & 0x40) == 0, utf8 cp continuation byte */
		/*
		 * no start byte, discard (but should skip all following 
		 * continuation bytes and send the utf8 "invalid" cp)
		 */
		if (utf8_cp_next_byte == 0)
			return; 
		utf8_cp[utf8_cp_next_byte] = input_b;
		++utf8_cp_next_byte;
		if (utf8_cp_next_byte != utf8_cp_sz)
			return;
	} else {/* ascii 0x00 - 0x7f */
		if (input_b == 0x1b) {/* esc */
			/* change state and process the esc byte */
			input_esc_seq_mode = true;
			input_byte_esc_seq();
			return;
		}
		utf8_cp[0] = input_b;
		utf8_cp_sz = 1;
	}
	input_utf8_cp_bind_cmd();
}
/* we are either reading an utf8 cp or an esc seq */
static void evt_input_drain(void) { loop
{	
	ssize_t r;

	errno = 0;
	r = read(0, &input_b, 1);
	if (r == -1) {
		if (errno == EAGAIN) /* no more input data */
			break;	
		if (errno == EINTR) /* restart manually the call */
			continue;
		fatal("an error occured while reading the input\n");
	}
	if (r == 0)
		fatal("input end of file\n");
	if (!input_esc_seq_mode)
		input_byte_utf8_cp();
	else
		input_byte_esc_seq();
}}
static void evt_sigs(void)
{
	int r;
	struct signalfd_siginfo siginfo;

	/* no short reads */
	r = read(sig_fd, &siginfo, sizeof(siginfo));
	if (r != sizeof(siginfo))
		fatal("unable to read signal information\n");

	switch (siginfo.ssi_signo) {
	case SIGTERM:
		exit_ok("received SIGTERM\n");
	case SIGINT:
		exit_ok("received SIGINT\n");
	default:
		warning("signal handle:unwanted signal %d received, discarding\n", siginfo.ssi_signo);
	}
}
static void evt_timer(void)
{
	int r;
	struct itimerspec t;

	memset(&t, 0, sizeof(t));
	r = timerfd_settime(input_timer_fd, 0, &t, 0);
	if (r == -1)
		fatal("unable to disarm the timer used to account for unknown input escape sequences from the terminal\n");

	esc_seq_next_byte = 0;
	input_esc_seq_mode = false;

	utf8_cp_next_byte = 0;
}
static void evt_handle(struct epoll_event *evt, bool *pcm_evt)
{
	if (evt->data.fd == 0) {
		if ((evt->events & EPOLLIN) != 0)
			evt_input_drain();
		else
			fatal("event loop wait:input:unexpected event\n");
	} else if (evt->data.fd == sig_fd) {
		if ((evt->events & EPOLLIN) != 0)
			evt_sigs();
		else
			fatal("event loop wait:signal:unexpected event\n");
	} else if (evt->data.fd == input_timer_fd) {
		if ((evt->events & EPOLLIN) != 0)
			evt_timer();
		else
			fatal("event loop wait:timer:unexpected event\n");
	} else { /* only update alsa fds */
		u8 i;

		i = 0;
		loop {
			if (i == pcm_pollfds_n)
				break;

			if (evt->data.fd == pcm_pollfds[i].fd) {
				pcm_pollfds[i].revents = evt->events;
				*pcm_evt = true;
			}
			++i;
		}
	}
}
/* fill the dec with pkts */
static void dec_fill(void) { loop
{
	unsigned int sd;
	unsigned int rd;
	unsigned int next_sd;
	int r;

	rd = atomic_load(&cb.rd);
	sd = atomic_load(&cb.sd);
	/* at start, we must read pkt before we can send one */
	if (rd == sd)
		break;
	r = av_sd_pkt(dec_ctx, cb.pkts[sd]);
	 /* dec is full and the pkt was rejected, or the decoder is in EOF */
	if (r == AVERROR(EAGAIN) || r == AVERROR_EOF)
		break;
	else if (r != 0)
		fatal("ffmpeg:error while sending the packet to the decoder\n");
	/* r == 0 */
	av_pkt_unref(cb.pkts[sd]);
	next_sd = (sd + 1) % cb.pkts_n;
	atomic_store(&cb.sd, next_sd);
}}
static void dec_frs_get(void) { loop
{
	int r;

	dec_fill();

	/* will unref the dec_frs.av bufs for us */
	r = av_receive_frs(dec_ctx, dec_frs.av);
	if (r == AVERROR(EAGAIN))
		continue;
	else if (r == 0) {
		if (dec_frs.av->pts != AV_NOPTS_VALUE)
			dec_frs.most_recent_ts = dec_frs.av->pts;
		else if (dec_frs.av->pkt_dts != AV_NOPTS_VALUE)
			dec_frs.most_recent_ts = dec_frs.av->pkt_dts;
		else 
			dec_frs.most_recent_ts = dec_frs.av->pkt_dts;
		dec_frs.pushed_in_filt_graph = false;
		return; /* "return" the current dec_frs.av */
	} else if (r == AVERROR_EOF) {
		pout("ffmpeg:last decoder frames reached (receiving)\n");
		loop { /* spinning */
			snd_pcm_state_t state;

			r = snd_pcm_drain(pcm_g);
			if (r == 0)
				break;
			if (r == -EAGAIN)
				continue;
			 /*
			  * the pcm state can change asynchronously.
			  * _old_ behavior:
			  * if the draining was successful, the pcm
			  * should be in SETUP state, and in this
			  * state, snd_pcm_drain will fail
			  * _new and fixed__ behavior:
			  * this won't happen because calling
			  * snd_pcm_drain while in the SETUP state
			  * won't return an error anymore
			  */
			state = snd_pcm_state(pcm_g);
			if (state == SND_PCM_STATE_SETUP)
				break;
		}
		/**********************************************/
		exit_ok("finished playing\n");
		/**********************************************/
	}
	fatal("ffmpeg:error while receiving frames from the decoder\n");
}}
static bool is_recfg_required(av_frs *frs)
{
	if (av_channel_layout_compare(&frs->ch_layout,
						&abufsrc_key.chans_layout) != 0
	     ||	frs->fr_rate		!= abufsrc_key.rate
	     ||	frs->fmt		!= abufsrc_key.fmt)
		return true;
	return false;
}
#define AGAIN			0
#define PUSHED			1
#define FILT_RECFG_REQUIRED	2
static u8 filt_graph_push_dec_frs(AVChannelLayout *new_chans_layout,
				int *new_fr_rate, enum av_fr_fmt *new_fmt)
{
	int r;

	if (dec_frs.pushed_in_filt_graph)
		dec_frs_get();
	if (is_recfg_required(dec_frs.av)) {
		r = av_channel_layout_copy(new_chans_layout,
							&dec_frs.av->ch_layout);	
		if (r < 0)
			fatal("ffmpeg:unable to copy the decoder channel layout as the new channel layout\n");
		*new_fr_rate = dec_frs.av->fr_rate;
		*new_fmt = dec_frs.av->fmt;
		return FILT_RECFG_REQUIRED;
	}
	/* the dec_frs.av bufs will be unref in av_receive_frs */
	r = av_bufsrc_add_frs_flags(abufsrc_ctx, dec_frs.av,
						AV_BUFSRC_FLAG_KEEP_REF);
	if (r >= 0) {
		dec_frs.pushed_in_filt_graph = true;
		return PUSHED;
	} else if (r == AVERROR(EAGAIN))
		return AGAIN;
	fatal("ffmpeg:unable to submit a decoder set of frames to the filter source audio buffer context\n");
}
#undef AGAIN
#undef PUSHED
#undef FILT_RECFG_REQUIRED
static void filt_frs_get(void)
{
	int r;

	r = av_bufsink_get_frs(abufsink_ctx, filt_frs.av);
	if (r >= 0) {
		filt_frs.pcm_written_ufrs_n = 0;
		return;
	}
	fatal("ffmpeg:error while getting frames from the filter\n");
}
#define FILT_RECFG_REQUIRED	2
#define EOF_FILT		2
#define DRAINING		3
#define HAVE_FILT_SET		1
#define PRINT_INFO		true
/* synchronous filtering */
static void dec_frs_filter(void)
{
	loop {
		u8 r;
		AVChannelLayout new_chans_layout;
		int new_fr_rate;
		enum av_fr_fmt new_fmt;
		AVChannelLayout dst_chans_layout;
		int dst_rate;
		enum av_fr_fmt dst_fmt;

		memset(&new_chans_layout, 0, sizeof(new_chans_layout));
		memset(&dst_chans_layout, 0, sizeof(dst_chans_layout));

		r = filt_graph_push_dec_frs(&new_chans_layout, &new_fr_rate,
								&new_fmt);
		if (r != FILT_RECFG_REQUIRED) {
			/* PUSHED | AGAIN */
			av_channel_layout_uninit(&new_chans_layout);
			break;
		}
		/* FILT_RECFG_REQUIRED */
		pcm2ff(pcm_g, &dst_fmt, &dst_rate, &dst_chans_layout,
								PRINT_INFO);
		filt_graph_cfg(
			new_fmt, new_fr_rate, &new_chans_layout,
			filt_frs.muted, filt_frs.vol,
			dst_fmt, dst_rate, &dst_chans_layout, PRINT_INFO);
		av_channel_layout_uninit(&new_chans_layout);
		av_channel_layout_uninit(&dst_chans_layout);
	}
	filt_frs_get();
}
#undef FILT_RECFG_REQUIRED
#undef EOF_FILT
#undef DRAINING
#undef HAVE_FILT_SET
#undef PRINT_INFO
#define NO 0
static void chans_buf_init(u8 **chans_buf, int start_fr_idx)
{
	int is_planar_fmt;
	int sample_bytes_n;

	sample_bytes_n = av_get_bytes_per_sample(filt_frs.av->fmt);

	is_planar_fmt = av_fr_fmt_is_planar(filt_frs.av->fmt);
	if (is_planar_fmt == NO) { /* or is pcm interleaved */
		int fr_bytes_n;

		fr_bytes_n = sample_bytes_n
					* filt_frs.av->ch_layout.nb_channels;
		chans_buf[0] = (u8*)filt_frs.av->data[0] + start_fr_idx
								* fr_bytes_n;
	} else { /* ff planar or pcm noninterleaved */
		int p;

		p = 0;
		loop {
			if (p == filt_frs.av->ch_layout.nb_channels)
				break;
			chans_buf[p] = (u8*)filt_frs.av->data[p]
					+ start_fr_idx * sample_bytes_n;
			++p;
		}	
	}
}
#undef NO
#define NO 0
static void chans_buf_inc(u8 **chans_buf, int frs_inc)
{
	int is_planar_fmt;
	int sample_bytes_n;

	sample_bytes_n = av_get_bytes_per_sample(filt_frs.av->fmt);

	is_planar_fmt = av_fr_fmt_is_planar(filt_frs.av->fmt);
	if (is_planar_fmt == NO) { /* or is pcm interleaved */
		int fr_bytes_n;

		fr_bytes_n = sample_bytes_n
					* filt_frs.av->ch_layout.nb_channels;
		chans_buf[0] += frs_inc * fr_bytes_n;
	} else { /* ff planar or pcm noninterleaved */
		u8 p;

		p = 0;
		loop {
			if (p == filt_frs.av->ch_layout.nb_channels)
				break;
			chans_buf[p] += frs_inc * sample_bytes_n;
			++p;
		}
	}
}
#undef NO
#define NO 0
static void pcm_silence_frs_write(snd_pcm_ufrs_t ufrs_n)
{
	int is_planar_fmt;

	if (ufrs_n == 0)
		return;

	is_planar_fmt = av_fr_fmt_is_planar(filt_frs.av->fmt);
	if (is_planar_fmt == NO)
		(void)snd_pcm_writei(pcm_g, silence_bufs[0], ufrs_n);
	else
		(void)snd_pcm_writen(pcm_g, silence_bufs, ufrs_n);
}
#undef NO
#define NO 0
static void pcm_filt_frs_write(snd_pcm_ufrs_t ufrs_n) { loop
{
	u8 chan_buf;
	u8 *chans_buf[AV_NUM_DATA_POINTERS];
	snd_pcm_sfrs_t r0;
	snd_pcm_ufrs_t ufrs_to_write_n;
	snd_pcm_ufrs_t filt_frs_remaining_ufrs_n; /* for clarity */

	if (ufrs_n == 0)
		return;
	if (filt_frs.pcm_written_ufrs_n == (snd_pcm_ufrs_t)filt_frs.av->frs_n) {
		av_frs_unref(filt_frs.av);
		dec_frs_filter(); /* synchronous filtering */
	}
	/* create the chan buf pointers */
	chans_buf_init(chans_buf, (int)filt_frs.pcm_written_ufrs_n);
	filt_frs_remaining_ufrs_n = (snd_pcm_ufrs_t)filt_frs.av->frs_n
						- filt_frs.pcm_written_ufrs_n;
	if (filt_frs_remaining_ufrs_n > ufrs_n)
		ufrs_to_write_n = ufrs_n;
	else	
		ufrs_to_write_n = filt_frs_remaining_ufrs_n;

	loop {
		int is_planar_fmt;
		snd_pcm_ufrs_t written_ufrs_n;

		is_planar_fmt = av_fr_fmt_is_planar(filt_frs.av->fmt);
		if (is_planar_fmt == NO)
			r0 = snd_pcm_writei(pcm_g, chans_buf[0],
							ufrs_to_write_n);
		else
			r0 = snd_pcm_writen(pcm_g, (void**)chans_buf,
							ufrs_to_write_n);
		if (r0 < 0) {
			if (r0 == -EAGAIN) /* return to epoll */
				return;
			else if (r0 == -EPIPE || r0 == -ESTRPIPE) {
				/* underrun or suspended */
				int r1;

				r1 = snd_pcm_recover(pcm_g, (int)r0, 0);
				if (r1 == 0) {
					warning("alsa:pcm recovered going back to epoll\n");
					return; /* recovered, go back to epoll */
				}
				fatal("alsa:unable to recover from suspend/underrun\n");
			} 
			fatal("alsa:fatal/unhandled error while writing the frames\n");
		}
		/* r0 >= 0 */
		written_ufrs_n = (snd_pcm_ufrs_t)r0;
		filt_frs.pcm_written_ufrs_n += written_ufrs_n;
		ufrs_n -= written_ufrs_n;
		ufrs_to_write_n -= written_ufrs_n;
		chans_buf_inc(chans_buf, (int)written_ufrs_n);
		if (ufrs_to_write_n == 0)
			break;
	}
}}
#undef NO
static void evt_pcm_write(void)
{	
	snd_pcm_sfrs_t r0;
	/* try only 2 times */
	r0 = snd_pcm_avail(pcm_g);
	if (r0 < 0) {
		if (r0 == -EPIPE || r0 == -ESTRPIPE) {
			/* underrun or suspended */
			int r1;

			r1 = snd_pcm_recover(pcm_g, (int)r0, 0);
			if (r1 == 0) {
				warning("alsa:pcm recovered retrying to get some available frames\n");
				r0 = snd_pcm_avail(pcm_g);
				if (r0 < 0)
					fatal("alsa:unable to get some available frames after recovery\n");
			} else
				fatal("alsa:unable to recover from suspend/underrun\n");
		} else
			fatal("alsa:error getting some available frames\n");
	}

	if (paused)
		pcm_silence_frs_write((snd_pcm_ufrs_t)r0);
	else
		pcm_filt_frs_write((snd_pcm_ufrs_t)r0);
}
#define EPOLL_EVTS_N 8 /* why not */
static void evts_loop(void)
{
	int fds_n;
	int fd_idx;
	struct epoll_event evts[EPOLL_EVTS_N];
	bool pcm_evt;
	int r;
	short pcm_evts;

	errno = 0;
	memset(evts, 0, sizeof(evts));
	fds_n = epoll_wait(ep_fd, evts, EPOLL_EVTS_N, -1);
	if (fds_n == -1) {
		if (errno == EINTR) {
			warning("event loop wait:was interrupted by a signal\n");
			return;
		}
		fatal("event loop wait:an error occured\n");
	}
	pcm_evt = false;
	fd_idx = 0;
	loop {
		if (fd_idx == fds_n)
			break;
		evt_handle(&evts[fd_idx], &pcm_evt);
		++fd_idx;
	}
	if (!pcm_evt)
		return;
	/*
 	 * since alsa could use several file descriptors, only once the 
	 * pollfds were properly updated we can actually know we got
	 * something from alsa
	 */
	r = snd_pcm_poll_descriptors_revents(pcm_g, pcm_pollfds, pcm_pollfds_n,
								&pcm_evts);
	if (r != 0)
		fatal("alsa:error processing the poll file descriptors\n");
	if ((pcm_evts & ~POLLOUT) != 0)
		fatal("alsa:unexpected events\n");
	if ((pcm_evts & POLLOUT) != 0)
		evt_pcm_write();
}
#undef EPOLL_EVTS_N
static void silence_bufs_init_once(void)
{
	memset(silence_bufs, 0, sizeof(silence_bufs));
}
static void silence_bufs_cfg(snd_pcm_t *pcm, bool print_info)
{
	int r;
	snd_pcm_hw_params_t *hw_params;
	snd_pcm_ufrs_t buf_ufrs_n;
	snd_pcm_fmt_t fmt;
	snd_pcm_access_t access;
	unsigned int chans_n;
	u8 c;

	r = snd_pcm_hw_params_malloc(&hw_params);
	if (r < 0)
		fatal("silence:alsa:unable to allocate memory for a hardware parameters container\n");
	r = snd_pcm_hw_params_current(pcm, hw_params);
	if (r != 0)
		fatal("silence:alsa:unable to get the pcm hardware parameters\n");
	r = snd_pcm_hw_params_get_buf_size(hw_params, &buf_ufrs_n);
	if (r < 0)
		fatal("silence:alsa:unable to get the number of frs in the pcm buffer\n");
	r = snd_pcm_hw_params_get_fmt(hw_params, &fmt);
	if (r < 0)
		fatal("silence:alsa:unable to get the pcm format\n");
	r = snd_pcm_hw_params_get_access(hw_params, &access);
	if (r < 0)
		fatal("silence:alsa:unable to get the pcm access mode\n");
	r = snd_pcm_hw_params_get_chans(hw_params, &chans_n);
	if (r < 0)
		fatal("silence:alsa:unable to get the pcm number of channels\n");
	/* wipe silence bufs first */
	c = 0;
	loop {
		if (c == AV_NUM_DATA_POINTERS)
			break;
		if (silence_bufs[c] != 0) {
			free(silence_bufs[c]);
			silence_bufs[c] = 0;
		}
		++c;
	}
	if (access == SND_PCM_ACCESS_RW_INTERLEAVED
				|| access == SND_PCM_ACCESS_MMAP_INTERLEAVED) {
		ssize_t buf_bytes_n;

		buf_bytes_n = snd_pcm_frs_to_bytes(pcm,
						(snd_pcm_sfrs_t)buf_ufrs_n);
		if (buf_bytes_n <= 0)
			fatal("silence:alsa:interleaved:unable to get the pcm number of bytes of all buffer frames\n");
		silence_bufs[0] = malloc((size_t)buf_bytes_n);
		if (silence_bufs[0] == 0)
			fatal("silence:interleaved:unable to allocate the silence buffer of %d bytes\n", (int)buf_bytes_n);
		if (print_info)
			pout("silence:interleaved:buffer of %d bytes is allocated\n", (int)buf_bytes_n);
		r = snd_pcm_fmt_set_silence(fmt, silence_bufs[0],
						(unsigned int)buf_ufrs_n);
		if (r < 0)
			fatal("silence:interleaved:unable to fill with silence the buffer\n");
		pout("silence:interleaved:silence buffer filled with %u silence frames\n", buf_ufrs_n);
	} else if (access == SND_PCM_ACCESS_RW_NONINTERLEAVED
			 || access == SND_PCM_ACCESS_MMAP_NONINTERLEAVED) {
		ssize_t buf_bytes_n;
		long buf_samples_n;

		buf_samples_n = (long)buf_ufrs_n;
		buf_bytes_n = snd_pcm_samples_to_bytes(pcm, buf_samples_n);
		if (buf_bytes_n <= 0)
			fatal("silence:alsa:non interleaved:unable to get the pcm number of total bytes of all buffer samples\n");
		c = 0;
		loop {
			if (c == chans_n)
				break;
			silence_bufs[c] = malloc((size_t)buf_bytes_n);
			if (silence_bufs[c] == 0)
				fatal("silence:non interleaved:unable to allocate silence buffer %u of %d bytes\n", c, (int)buf_bytes_n);
			r = snd_pcm_fmt_set_silence(fmt, silence_bufs[c],
						(unsigned int)buf_samples_n);
			if (r < 0)
				fatal("silence:non interleaved:unable to fill with silence the buffer\n");
			if (print_info)
				pout("silence:non interleaved:buffer[%u] of %d bytes is allocated\n", c, (int)buf_bytes_n);
			++c;
		}
		if (print_info)
			pout("silence:non interleaved:allocated %u silence buffers for %u frames\n", chans_n, (unsigned int)buf_ufrs_n);
	} else
		fatal("silence:the pcm access type is not supported\n");
	snd_pcm_hw_params_free(hw_params);
}
static void ff_log_stdout(void *a, int b, const char *fmt, va_list ap)
{
	vprintf(fmt, ap);
}
struct ff_supported_fr_fmt_t {
	u8 *str;
	enum av_fr_fmt fmt;
};
/* this is the intersection of ff audio fr fmt and alsa pcm fmt */
static struct ff_supported_fr_fmt_t ff_supported_fr_fmts[] = {
	{"u8", AV_FR_FMT_U8},
	{"u8planar", AV_FR_FMT_U8P},
	{"s16", AV_FR_FMT_S16},
	{"s16planar", AV_FR_FMT_S16P},
	{"s32",  AV_FR_FMT_S32},
	{"s32planar", AV_FR_FMT_S32P},
	{"float32", AV_FR_FMT_FLT},
	{"float32planar", AV_FR_FMT_FLTP},
	{"float64", AV_FR_FMT_DBL},
	{"float64planar", AV_FR_FMT_DBLP},
	{0,0}
};
static void usage(void)
{
	struct ff_supported_fr_fmt_t *fmt;

	pout("\
npa [-p alsa pcm] [-v volume(0..100)] [-cb circular buffer slots]\n\
    [-fc override initial ffmpeg count of channels used to approximate the alsa\n\
          pcm configuration]\n\
    [-fr override initial ffmpeg rate(hz) used to approximate the alsa pcm\n\
          configuration]\n\
    [-ff override initial ffmpeg audio frame format used to approximate the alsa\n\
          pcm configuration, see below for a list]\n\
    [-h]\n\
    url\n\
\n\
the ffmpeg audio frame formats which intersect alsa pcm audio formats are:\n");

	fmt = ff_supported_fr_fmts;
	loop {
		if (fmt->str == 0)
			break;
		pout("\t%s\n", fmt->str);
		++fmt;
	}
}
static void opts_parse(int argc, u8 **args, u8 **url, u8 **pcm_str,
				double *initial_vol, unsigned int *cb_slots_n,
				int *override_initial_ff_chans_n,
				int *override_initial_ff_rate,
				enum av_fr_fmt *override_initial_ff_fr_fmt)
{
	int i;
	int url_idx;

	i = 1;
	url_idx = -1;
	loop {
		if (i == argc)
			break;
		if (strcmp("-p", args[i]) == 0) {
			if ((i + 1) == argc)
				fatal("-p:alsa pcm is missing\n");
			*pcm_str = args[i + 1];
			pout("-p:alsa pcm \"%s\"\n", *pcm_str);
			i += 2;
		} else if (strcmp("-v", args[i]) == 0) {
			unsigned long vol_ul;

			if ((i + 1) == argc)
				fatal("-v:initial volume option is missing\n");
			vol_ul = strtoul(args[i + 1], 0, 10);
			if (vol_ul < 0 || 100 < vol_ul)
				fatal("-v:invalid volume value %lu (0..100)\n", vol_ul);
			*initial_vol = (double)vol_ul / 100.0;
			pout("-v:using initial volume %f\n", *initial_vol);
			i += 2;
		} else if (strcmp("-cb", args[i]) == 0) {
			if ((i + 1) == argc)
				fatal("-cb:count of slots is missing\n");
			*cb_slots_n = (unsigned int)strtoul(args[i + 1], 0, 10);
			pout("-cb:using %u circular buffer slots\n",
								*cb_slots_n);
			i += 2;
		/*------------------------------------------------------------*/
		/* ff initial override for alsa pcm cfg  -- start */
		} else if (strcmp("-fr", args[i]) == 0) {
			if ((i + 1) == argc) {
				perr("-fr:override initial ffmpeg rate(hz) is missing\n");
				usage();
				exit(1);
			}
			*override_initial_ff_rate = (int)strtol(args[i + 1], 0,
									10);
			pout("-fr:override initial ffmpeg audio rate to %dHz used for alsa pcm configuration\n", *override_initial_ff_rate);
			i += 2;
		} else if (strcmp("-fc", args[i]) == 0) {
			if ((i + 1) == argc) {
				perr("-fc:override initial ffmpeg channel count is missing\n");
				usage();
				exit(1);
			}
			*override_initial_ff_chans_n = (int)strtol(args[i + 1],
									0, 10);
			pout("-fc:override initial ffmpeg count of channels to %d used for alsa pcm configuration\n", *override_initial_ff_chans_n);
			i += 2;
		} else if (strcmp("-ff", args[i]) == 0) {
			struct ff_supported_fr_fmt_t *fmt;

			if ((i + 1) == argc) {
				perr("-fc:override initial ffmpeg audio frame format is missing\n");
				usage();
				exit(1);
			}
			fmt = ff_supported_fr_fmts;
			loop {
				if  (fmt->str == 0) {
					perr("-ff:unknown ffmpeg audio frame format\n");
					usage();
					exit(1);
				}
				if (strcmp(fmt->str, args[i + 1]) == 0) {
					*override_initial_ff_fr_fmt = fmt->fmt;
					break;
				}
				++fmt;
			}
			pout("-ff:override initial ffmpeg audio frame format is %s\n", av_get_fr_fmt_name(*override_initial_ff_fr_fmt));
			i += 2;
		/* ff initial override for alsa pcm cfg  -- end */
		/*------------------------------------------------------------*/
		} else if (strcmp("-h", args[i]) == 0) {
			usage();
			exit(0);
		} else {
			url_idx = i;
			++i;
		}
	}
	if (url_idx == -1)
		fatal("missing url\n");
	*url = args[url_idx];
	pout("playing ####%s####\n", *url);
}
#define RESET_DONE 0
#define DONT_HOLD 0
static void cb_init_once(unsigned int cb_slots_n)
{
	unsigned int i;

	cb.pkts = malloc(cb_slots_n * sizeof(*cb.pkts));
	i = 0;
	loop {
		cb.pkts[i] = av_packet_alloc();
		if (cb.pkts[i] == 0)
			fatal("unable to allocate a packet reference for the circular buffer\n");
		if (i == cb_slots_n)
			break;
		++i;
	}
	cb.pkts_n = cb_slots_n;
	atomic_store(&cb.rd, 0);
	atomic_store(&cb.sd, 0);
	atomic_store(&cb.st_idx, -1);
	atomic_store(&cb.reset, RESET_DONE);
	atomic_store(&cb.hold, DONT_HOLD);
}
#undef RESET_DONE
#undef DONT_HOLD
static void filt_graph_init_once(void)
{
	filt_graph = 0;
	abufsrc_ctx = 0;
	abufsrc_filt = 0;
	memset(&abufsrc_key, 0, sizeof(abufsrc_key));
	vol_ctx = 0;
	vol_filt = 0;
	afmt_ctx = 0;
	afmt_filt = 0;
	abufsink_ctx = 0;
	abufsink_filt = 0;
	/* floating point strs are localized... */
	snprintf(double_zero_l10n_str, sizeof(double_zero_l10n_str), "%f", 0.);
	memset(&filt_frs, 0, sizeof(filt_frs));
	filt_frs.av = av_frs_alloc();
	if (filt_frs.av == 0)
		fatal("ffmpeg:unable to allocate a filtered frames structure\n");
	filt_frs.pcm_written_ufrs_n = 0;
}
static void dec_init_once(void)
{
	dec = 0;
	dec_ctx = 0;
	dec_frs.av = av_frs_alloc();
	if (dec_frs.av == 0)
		fatal("ffmpeg:unable to allocate a decoded frames structure\n");
	dec_frs.pushed_in_filt_graph = true;
	dec_frs.most_recent_ts = AV_NOPTS_VALUE;
}
static void rd_thd_init_once(void)
{
	rd_thd_pkt = av_packet_alloc();
	if (rd_thd_pkt == 0)
		fatal("ffmpeg:unable to allocate a packet for the read thread\n");
}
static void fmt_init_once(u8 *url)
{
	int r;

	fmt_ctx = 0;
	r = av_fmt_open_input(&fmt_ctx, url, NULL, NULL);
	if (r < 0)
		fatal("ffmpeg:unable to open \"%s\"\n", url);
	/* probe beyond the header, if any */
	r = av_fmt_find_st_info(fmt_ctx, 0);
	if (r < 0)
		fatal("ffmpeg:unable to probe \"%s\"\n", url);
	r = pthread_mutex_init(&fmt_ctx_mutex, 0);
	if (r != 0)
		fatal("unable to init the format context mutex\n");
}
static void pcm_init_once(u8 *pcm_str)
{
	int r;

	r = snd_output_stdio_attach(&pcm_pout, stdout, 0);
	if (r < 0)
		fatal("alsa:unable to attach stdout\n");
	r = snd_output_stdio_attach(&pcm_perr, stderr, 0);
	if (r < 0)
		fatal("alsa:unable to attach stderr\n");
	r = snd_pcm_open(&pcm_g, pcm_str, SND_PCM_ST_PLAYBACK,
							SND_PCM_NONBLOCK);
	if (r < 0) {
		if (r == -EAGAIN)
			fatal("alsa:\"%s\" pcm is already in use\n", pcm_str);
		else
			fatal("alsa:unable to open \"%s\" pcm for playback\n", pcm_str);
	}
}
static void init_once(u8 *url, u8 *pcm_str, unsigned int cb_slots_n)
{
	stdin_tty_cfg_modified = false;
	current_url = url;
	paused = false;

	sigs_init_once();
	stdin_tty_init_once();
	stdin_flags_init_once();
	stdout_init_once();
	input_state_init_once();
	cb_init_once(cb_slots_n);
	filt_graph_init_once();
	silence_bufs_init_once();
	dec_init_once();
	pcm_init_once(pcm_str);
	rd_thd_init_once();
	fmt_init_once(url);
	evt_init_once();
}
static int find_best_st(void)
{
	int r;

	r = av_find_best_st(fmt_ctx, AV_MEDIA_TYPE_AUDIO, -1, -1, 0, 0);
	if (r < 0)
		fatal("ffmpeg:no audio stream found\n");
	return r;
}
static void pcm_hw_chans_n_decide(snd_pcm_t *pcm,
		snd_pcm_hw_params_t *pcm_hw_params, unsigned int chans_n)
{
	int r;
	unsigned int chans_n_max;
	unsigned int chans_n_min;

	r = snd_pcm_hw_params_test_chans(pcm, pcm_hw_params, chans_n);
	if (r == 0) {
		r = snd_pcm_hw_params_set_chans(pcm, pcm_hw_params, chans_n);
		if (r != 0)
			fatal("alsa:unable to restrict pcm device to %u channels, count which was successfully tested\n", chans_n);
		pout("alsa:using %u channels\n", chans_n);
		return;
	}	
	pout("alsa:unable to use %u channels\n", chans_n);
	/* try to use the max chans n the pcm can */
	r = snd_pcm_hw_params_get_chans_max(pcm_hw_params, &chans_n_max);
	if (r != 0) 
		fatal("alsa:unable to get the maximum count of pcm device channels\n");
	r = snd_pcm_hw_params_test_chans(pcm, pcm_hw_params, chans_n_max);
	if (r == 0) {
		r = snd_pcm_hw_params_set_chans(pcm, pcm_hw_params,
								chans_n_max);
		if (r != 0)
			fatal("alsa:unable to restrict pcm device to %u channels, count which was successfully tested\n", chans_n_max);
		pout("alsa:using pcm maximum %u channels\n", chans_n_max);
		return;
	}
	/* ok... last try, the pcm dev min chans n */
	r = snd_pcm_hw_params_get_chans_min(pcm_hw_params, &chans_n_min);
	if (r != 0) 
		fatal("alsa:unable to get the minimum count of pcm device channels\n");
	r = snd_pcm_hw_params_test_chans(pcm, pcm_hw_params, chans_n_min);
	if (r == 0) {
		r = snd_pcm_hw_params_set_chans(pcm, pcm_hw_params,
								chans_n_min);
		if (r != 0)
			fatal("alsa:unable to restrict pcm device to %u channels, count which was successfully tested\n", chans_n_min);
		pout("alsa:using pcm device minimum %u channels\n", chans_n_min);
		return;
	}
	fatal("alsa:unable to find a suitable count of channels\n");
}
static void pcm_hw_rate_decide(snd_pcm_t *pcm,
			snd_pcm_hw_params_t *pcm_hw_params, unsigned int rate)
{
	int r;
	unsigned int rate_max;
	unsigned int rate_near;
	unsigned int rate_min;

	r = snd_pcm_hw_params_test_rate(pcm, pcm_hw_params, rate,
							SND_PCM_ST_PLAYBACK);
	if (r == 0) {
		r = snd_pcm_hw_params_set_rate(pcm, pcm_hw_params, rate,
							SND_PCM_ST_PLAYBACK);
		if (r != 0)
			fatal("alsa:unable to restrict pcm device to %uHz, which was successfully tested\n", rate);
		pout("alsa:using %uHz\n", rate);
		return;
	}	
	pout("alsa:unable to use %uHz\n", rate);
	/* try to use the max rate the pcm can */
	r = snd_pcm_hw_params_get_rate_max(pcm_hw_params, &rate_max,
							SND_PCM_ST_PLAYBACK);
	if (r != 0) 
		fatal("alsa:unable to get the maximum rate of pcm device\n");
	r = snd_pcm_hw_params_test_rate(pcm, pcm_hw_params, rate_max,
							SND_PCM_ST_PLAYBACK);
	if (r == 0) {
		r = snd_pcm_hw_params_set_rate(pcm, pcm_hw_params, rate_max,
							SND_PCM_ST_PLAYBACK);
		if (r != 0)
			fatal("alsa:unable to restrict pcm device to %uHz, which was successfully tested\n", rate_max);
		pout("alsa:using pcm device %uHz\n", rate_max);
		return;
	}
	/* try to use a rate "near" of what the pcm dev can */
	rate_near = rate;
	r = snd_pcm_hw_params_set_rate_near(pcm, pcm_hw_params, &rate_near,
							SND_PCM_ST_PLAYBACK);
	if (r == 0) {
		pout("alsa:using pcm device %uHz\n", rate_near);
		return;
	}
	/* even a "near" rate did failed... try the min */
	r = snd_pcm_hw_params_get_rate_min(pcm_hw_params, &rate_min,
							SND_PCM_ST_PLAYBACK);
	if (r != 0) 
		fatal("alsa:unable to get the minimum rate of pcm device\n");
	r = snd_pcm_hw_params_test_rate(pcm, pcm_hw_params, rate_min,
							SND_PCM_ST_PLAYBACK);
	if (r == 0) {
		r = snd_pcm_hw_params_set_rate(pcm, pcm_hw_params, rate_min,
							SND_PCM_ST_PLAYBACK);
		if (r != 0)
			fatal("alsa:unable to restrict pcm device to %uHz, which was successfully tested\n", rate_min);
		pout("alsa:using pcm device %uHz\n", rate_min);
		return;
	}
	fatal("alsa:unable to find a suitable rate\n");
}
static bool ff_fmt2pcm_layout_best_effort(enum av_fr_fmt ff_fmt,
		snd_pcm_fmt_t *alsa_fmt, snd_pcm_access_t *alsa_access)
{
	static u8 ff_fmt_str[STR_SZ];

	av_get_fr_fmt_str(ff_fmt_str, STR_SZ, ff_fmt);
	/* XXX: only classic non-mmap ones */
	switch (ff_fmt) {
	case AV_FR_FMT_U8:
		*alsa_fmt = SND_PCM_FMT_U8;
		*alsa_access = SND_PCM_ACCESS_RW_INTERLEAVED;
		break;
	case AV_FR_FMT_S16:
		*alsa_fmt = SND_PCM_FMT_S16;
		*alsa_access = SND_PCM_ACCESS_RW_INTERLEAVED;
		break;
	case AV_FR_FMT_S32:
		*alsa_fmt = SND_PCM_FMT_S32;
		*alsa_access = SND_PCM_ACCESS_RW_INTERLEAVED;
		break;
	case AV_FR_FMT_FLT:
		*alsa_fmt = SND_PCM_FMT_FLT;
		*alsa_access = SND_PCM_ACCESS_RW_INTERLEAVED;
		break;
	/* ff "planar" fmts are actually non interleaved fmts */
	case AV_FR_FMT_U8P:
		*alsa_fmt = SND_PCM_FMT_U8;
		*alsa_access = SND_PCM_ACCESS_RW_NONINTERLEAVED;
		break;
	case AV_FR_FMT_S16P:
		*alsa_fmt = SND_PCM_FMT_S16;
		*alsa_access = SND_PCM_ACCESS_RW_NONINTERLEAVED;
		break;
	case AV_FR_FMT_S32P:
		*alsa_fmt = SND_PCM_FMT_S32;
		*alsa_access = SND_PCM_ACCESS_RW_NONINTERLEAVED;
		break;
	case AV_FR_FMT_FLTP:
		*alsa_fmt = SND_PCM_FMT_FLT;
		*alsa_access = SND_PCM_ACCESS_RW_NONINTERLEAVED;
		break;
	default:
		pout("best effort:unable to wire ffmpeg sample format \"%sbits\" to alsa sample format, \n,", ff_fmt_str);
		return false;
	}
	pout("best effort:ffmpeg format \"%sbits\" (%u bytes) to alsa layout \"%s\" and access \"%s\"\n", ff_fmt_str, av_get_bytes_per_sample(ff_fmt), snd_pcm_fmt_desc(*alsa_fmt), snd_pcm_access_name(*alsa_access));
	return true;
}
static bool pcm_hw_fmt_decide_x(snd_pcm_t *pcm,
		snd_pcm_hw_params_t *pcm_hw_params, snd_pcm_fmt_t fmt)
{
	int r;

	r = snd_pcm_hw_params_test_fmt(pcm, pcm_hw_params, fmt);
	if (r != 0) 
		return false;
	r = snd_pcm_hw_params_set_fmt(pcm, pcm_hw_params, fmt);
	if (r != 0)
		fatal("alsa:unable to restrict pcm device to \"%s\", which was successfully tested\n", snd_pcm_fmt_desc(fmt));
	pout("alsa:using \"%s\" format\n", snd_pcm_fmt_desc(fmt));
	return true;
}
#define PCM_HW_FMT_DECIDE_X(fmt) pcm_hw_fmt_decide_x(pcm, pcm_hw_params, fmt)
static void pcm_hw_fmt_decide(snd_pcm_t *pcm,
			snd_pcm_hw_params_t *pcm_hw_params, bool force,
						 snd_pcm_fmt_t forced_fmt)
{
	int r;
	snd_pcm_fmt_t *fmt;

	if (force) {	
		r = snd_pcm_hw_params_test_fmt(pcm, pcm_hw_params, forced_fmt);
		if (r == 0) {
			r = snd_pcm_hw_params_set_fmt(pcm, pcm_hw_params,
								forced_fmt);
			if (r != 0)
				fatal("alsa:unable to restrict pcm device to \"%s\", which was successfully tested\n", snd_pcm_fmt_desc(forced_fmt));
			pout("alsa:using forced \"%s\" format\n", snd_pcm_fmt_desc(forced_fmt));
			return;
		}
	}
	/* then we try to select from the reasonable "best" to the lowest */
	/* prefer fmts we know supported by ff */
	if (PCM_HW_FMT_DECIDE_X(SND_PCM_FMT_FLT))
		return;
	if (PCM_HW_FMT_DECIDE_X(SND_PCM_FMT_S32))
		return;
	if (PCM_HW_FMT_DECIDE_X(SND_PCM_FMT_S16))
		return;
	if (PCM_HW_FMT_DECIDE_X(SND_PCM_FMT_U8))
		return;
	/*
	 * from here, at the time of writting, those fmts have no ff
 	 * wiring, but we are alsa centric here, validate that later
	 */
	if (PCM_HW_FMT_DECIDE_X(SND_PCM_FMT_U32))
		return;
	if (PCM_HW_FMT_DECIDE_X(SND_PCM_FMT_S24))
		return;
	if (PCM_HW_FMT_DECIDE_X(SND_PCM_FMT_U24))
		return;
	if (PCM_HW_FMT_DECIDE_X(SND_PCM_FMT_U16))
		return;
	if (PCM_HW_FMT_DECIDE_X(SND_PCM_FMT_S8))
		return;
	fatal("alsa:unable to find a suitable format\n");
}
#undef PCM_HW_FMT_DECIDE_X
static bool pcm_hw_access_decide_x(snd_pcm_t *pcm,
		snd_pcm_hw_params_t *pcm_hw_params, snd_pcm_access_t access)
{
	int r;

	r = snd_pcm_hw_params_test_access(pcm, pcm_hw_params, access);
	if (r != 0)
		return false;
	r = snd_pcm_hw_params_set_access(pcm, pcm_hw_params, access);
	if (r != 0)
		fatal("alsa:unable to restrict pcm device to \"%s\", which was successfully tested\n", snd_pcm_access_name(access));
	pout("alsa:using \"%s\" access\n", snd_pcm_access_name(access));
	return true;
}
#define PCM_HW_ACCESS_DECIDE_X(access) \
			pcm_hw_access_decide_x(pcm, pcm_hw_params, access)
/* XXX: only classic non-mmap ones */
static void pcm_hw_access_decide(snd_pcm_t *pcm,
				snd_pcm_hw_params_t *pcm_hw_params, bool force,
						snd_pcm_access_t forced_access)
{
	int r;
	snd_pcm_access_t access;

	if (force) {
		r = snd_pcm_hw_params_test_access(pcm, pcm_hw_params,
								forced_access);
		if (r == 0) {
			r = snd_pcm_hw_params_set_access(pcm, pcm_hw_params,
								forced_access);
			if (r != 0)
				fatal("alsa:unable to restrict pcm device to \"%s\", which was successfully tested\n", snd_pcm_access_name(forced_access));
			pout("alsa:using forced \"%s\" access\n", snd_pcm_access_name(forced_access));
			return;
		}
	}
	/* brute force */
	if (PCM_HW_ACCESS_DECIDE_X(SND_PCM_ACCESS_RW_INTERLEAVED))
		return;
	if (PCM_HW_ACCESS_DECIDE_X(SND_PCM_ACCESS_RW_NONINTERLEAVED))
		return;
	fatal("alsa:unable to find a suitable access\n");
}
#undef PCM_HW_ACCESS_DECIDE_X
/*
 * latency control: some audio bufs can be huge (tested on a pulseaudio with 10
 * secs audio buf). if we are careless, we will quickly fill this buf which is
 * worth a significant amount of time, hence will add huge latency to our
 * interactive audio filtering (vol...). in the case of the 10 secs pulseaudio
 * buf, it means if you want to mute the audio, it will happen 10 secs later.
 * we add lantency control by limiting the sz of the dev audio buf, in periods
 * n.
 * we choose roughly 0.25 secs, or roughly (rate / 4) frs.
 */
static void pcm_hw_buf_sz_cfg(snd_pcm_t *pcm,
					snd_pcm_hw_params_t *pcm_hw_params)
{
	int r;
	snd_pcm_ufrs_t latency_control_target_buf_ufrs_n;
	snd_pcm_ufrs_t latency_control_buf_ufrs_n;
	unsigned int rate;

	r = snd_pcm_hw_params_get_rate(pcm_hw_params, &rate, 0);
	if (r < 0) {
		warning("alsa:latency control:DISABLING LATENCY CONTROL:unable to get the decided rate from the current device parameters\n");
		return;
	}
	latency_control_target_buf_ufrs_n = (snd_pcm_ufrs_t)rate;
	latency_control_target_buf_ufrs_n /= 4;
	latency_control_buf_ufrs_n = latency_control_target_buf_ufrs_n;
	r = snd_pcm_hw_params_set_buf_size_near(pcm, pcm_hw_params,
						&latency_control_buf_ufrs_n);
	if (r < 0) {
		warning("alsa:latency control:DISABLING_LATENCY_CONTROL:unable to set the audio buffer size (count of frames) to %u periods for the current device parameters\n", latency_control_buf_ufrs_n);
		return;
	}
	pout("alsa:latency control:target buffer frame count is %u (~0.25 sec), got an audio buffer size set to %u frames\n", latency_control_target_buf_ufrs_n, latency_control_buf_ufrs_n);
}
static void pcm_cfg_sw(snd_pcm_t *pcm)
{
	int r;
	snd_pcm_sw_params_t *sw_params;

	pout("ALSA:SW_PARAMS START------------------------------------------------------------\n");
	r = snd_pcm_sw_params_malloc(&sw_params);
	if (r != 0)
		fatal("alsa:unable to allocate software parameters structure\n");
	r = snd_pcm_sw_params_current(pcm, sw_params);
	if (r != 0)
		fatal("alsa:unable to get current software parameters\n");
	r = snd_pcm_sw_params_set_period_event(pcm, sw_params, 1);
	if (r != 0)
		fatal("alsa:unable to enable period event\n");
	r = snd_pcm_sw_params(pcm, sw_params);
	if (r != 0)
		fatal("alsa:unable to install sotfware parameters\n");
	snd_pcm_sw_params_dump(sw_params, pcm_pout);
	snd_pcm_sw_params_free(sw_params);
	pout("ALSA:SW_PARAMS END--------------------------------------------------------------\n");
}
/*
 * this function will "decide" the pcm dev cfg:
 * the goal is to be the "closest" to the provided params,
 * the "gap" will have to "filled" with ff filts
 *
 * the "strategy" is a "fall-thru" (chans n then ... then ...) which
 * will "restrict" the pcm dev cfg further at each step
 *
 * we try to use a sensible restrict order regarding audio props
 */
static void pcm_cfg_hw_core(snd_pcm_t *pcm, snd_pcm_hw_params_t *pcm_hw_params,
				int chans_n, int rate, enum av_fr_fmt ff_fmt)
{
	int r;
	bool best_effort_wiring_success;
	snd_pcm_fmt_t fmt_from_best_effort;
	snd_pcm_access_t access_from_best_effort;

	/* the return value is from a first refine of the raw hw params */
	r = snd_pcm_hw_params_any(pcm, pcm_hw_params);
	if (r < 0)
		fatal("alsa:unable to populate the hardware parameters context\n");
	pcm_hw_chans_n_decide(pcm, pcm_hw_params, (unsigned int)chans_n);
	pcm_hw_rate_decide(pcm, pcm_hw_params, (unsigned int)rate);
	/* try our best */
	best_effort_wiring_success = ff_fmt2pcm_layout_best_effort(
		ff_fmt, &fmt_from_best_effort, &access_from_best_effort);
	pcm_hw_fmt_decide(pcm, pcm_hw_params, best_effort_wiring_success,
							fmt_from_best_effort);
	pcm_hw_access_decide(pcm, pcm_hw_params, best_effort_wiring_success,
						access_from_best_effort);
	pcm_hw_buf_sz_cfg(pcm, pcm_hw_params);
}
static void pcm_cfg_hw(snd_pcm_t *pcm, unsigned int chans_n, unsigned int rate,
							enum av_fr_fmt ff_fmt)
{
	int r;
	snd_pcm_access_t access;
	snd_pcm_hw_params_t *hw_params;

	pout("ALSA:HW_PARAMS START------------------------------------------------------------\n");
	r = snd_pcm_hw_params_malloc(&hw_params);
	if (r < 0)
		fatal("alsa:unable to allocate hardware parameters context\n");
	pcm_cfg_hw_core(pcm, hw_params, chans_n, rate, ff_fmt);
	r = snd_pcm_hw_params(pcm, hw_params);
	if (r != 0)
		fatal("alsa:unable to install the hardware parameters\n");
	r = snd_pcm_hw_params_current(pcm, hw_params);
	if (r != 0)
		fatal("alsa:unable to get current hardware parameters\n");
	snd_pcm_hw_params_dump(hw_params, pcm_pout);
	snd_pcm_hw_params_free(hw_params);
	pout("ALSA:HW_PARAMS END--------------------------------------------------------------\n");
}
static void pcm_cfg_epoll(snd_pcm_t *pcm)
{
	int r;

	r = snd_pcm_poll_descriptors_count(pcm);
	pout("alsa:have %d poll file descriptors\n", r);
	if ((r <= 0) || (r > PCM_POLLFDS_N_MAX))
		fatal("alsa:invalid count of alsa poll file descriptors\n");
	pcm_pollfds_n =(u8)r;
	memset(pcm_pollfds, 0, sizeof(pcm_pollfds));
	snd_pcm_poll_descriptors(pcm, pcm_pollfds,
						(unsigned int)pcm_pollfds_n);
}
static void pcm_cfg(snd_pcm_t *pcm, unsigned int chans_n, unsigned int rate,
							enum av_fr_fmt ff_fmt)
{
	pcm_cfg_hw(pcm, chans_n, rate, ff_fmt);
	pcm_cfg_sw(pcm);
	pcm_cfg_epoll(pcm); /* This must be done AFTER the sw params are set */
	pout("ALSA PCM DUMP START-------------------------------------------------------------\n");
	snd_pcm_dump(pcm, pcm_pout);
	pout("ALSA PCM DUMP END---------------------------------------------------------------\n");
}
static void evt_pcm_install(void)
{
	u8 i;

	i = 0;
	loop {
		struct epoll_event evt;
		int r;

		if (i == pcm_pollfds_n)
			break;
		evt.events = pcm_pollfds[i].events;
		evt.data.fd = pcm_pollfds[i].fd;
		r = epoll_ctl(ep_fd, EPOLL_CTL_ADD, pcm_pollfds[i].fd, &evt);
		if (r == -1)
			fatal("unable to add alsa poll file descriptor[%d]=%d to epoll file descriptor\n", i, pcm_pollfds[i].fd);
		++i;
	}
	pout("alsa:pcm events installed\n");
}
#define PRINT_INFO true
#define CHANS_N_NOT_OVERRIDDEN 0
#define RATE_NOT_OVERRIDDEN 0
#define FR_FMT_NOT_OVERRIDDEN AV_FR_FMT_NONE
static void prepare(int st_idx, double initial_vol, 
		int override_initial_ff_chans_n, int override_initial_ff_rate,
		enum av_fr_fmt override_initial_ff_audio_fr_fmt)
{
	enum av_fr_fmt dst_fmt;
	int initial_ff_chans_n;
	int initial_ff_rate;
	enum av_fr_fmt initial_ff_audio_fr_fmt;

	current_st_idx = st_idx;
	fmt_ctx_lock();
	dec_ctx_cfg(fmt_ctx->sts[st_idx]->codecpar);
	dec_frs.most_recent_ts = fmt_ctx->sts[st_idx]->start_time;
	cmd_info_data.fmt.duration = fmt_ctx->duration;
	cmd_info_data.fmt.m = fmt_ctx->duration_estimation_method;
	cmd_info_data.st.tb = fmt_ctx->sts[st_idx]->tb;
	cmd_info_data.st.id = fmt_ctx->sts[st_idx]->id;
	cmd_info_data.st.duration = fmt_ctx->sts[st_idx]->duration;
	fmt_ctx_unlock();

	if (override_initial_ff_chans_n == CHANS_N_NOT_OVERRIDDEN)
		initial_ff_chans_n = dec_ctx->ch_layout.nb_channels;
	else
		initial_ff_chans_n = override_initial_ff_chans_n;
	if (override_initial_ff_rate == RATE_NOT_OVERRIDDEN)
		initial_ff_rate = dec_ctx->fr_rate;
	else
		initial_ff_rate = override_initial_ff_rate;
	if (override_initial_ff_audio_fr_fmt == FR_FMT_NOT_OVERRIDDEN)
		initial_ff_audio_fr_fmt = dec_ctx->fr_fmt;
	else
		initial_ff_audio_fr_fmt = override_initial_ff_audio_fr_fmt;
	/* best effort configuration of the pcm based on initial ff params */
	pcm_cfg(pcm_g, initial_ff_chans_n, initial_ff_rate,
						initial_ff_audio_fr_fmt);
	silence_bufs_cfg(pcm_g, PRINT_INFO);
	evt_pcm_install();
	filt_frs.vol = initial_vol;
}
#undef PRINT_INFO
#undef CHANS_N_NOT_OVERRIDDEN
#undef RATE_NOT_OVERRIDDEN
#undef AUDIO_FR_FMT_NOT_OVERRIDDEN
#define HOLD 1
#define DONT_HOLD 0
static void seek_x(int64_t delta)
{
	int r;
	int64_t new_ts;
	av_st *st;
	av_rational st_tb;
	
	if (dec_frs.most_recent_ts == AV_NOPTS_VALUE)
		warning("unable to seek because no time stamp are currently available\n");
	fmt_ctx_lock();
	st = fmt_ctx->sts[current_st_idx];
	st_tb = st->tb;
	fmt_ctx_unlock();

	new_ts = dec_frs.most_recent_ts + delta * (int64_t)st_tb.den
							/ (int64_t)st_tb.num;
	/* rewind capping */
	pout("trying to seek to %"PRId64" stream time base units\n", new_ts);

	atomic_store(&cb.hold, HOLD);
	rd_thd_reset(current_st_idx);
	fmt_ctx_lock();
	r = av_seek_pkt(fmt_ctx, st->id, new_ts, 0);
	fmt_ctx_unlock();
	if (r < 0)
		warning("unable to seek to %"PRId64" stream time base units\n", new_ts);
	dec_frs.most_recent_ts = AV_NOPTS_VALUE;
	av_flush_bufs(dec_ctx);
	filt_flush();
	atomic_store(&cb.hold, DONT_HOLD);
}
#undef HOLD
#undef DONT_HOLD
static void cmd_rewind(void)
{
	pout("COMMAND:rewind\n");
	seek_x(-SEEK_DELTA);
}
static void cmd_rewind_big(void)
{
	pout("COMMAND:rewind big\n");
	seek_x(-SEEK_DELTA_BIG);
}
static void cmd_fastforward(void)
{
	pout("COMMAND:fastforward\n");
	seek_x(SEEK_DELTA);
}
static void cmd_fastforward_big(void)
{
	pout("COMMAND:fastforward big\n");
	seek_x(SEEK_DELTA_BIG);
}
static void cmd_pause(void)
{
	if (paused) {
		pout("COMMAND:unpause\n");
		paused = false;
	} else {
		pout("COMMAND:pause\n");
		paused = true;
	}
}
static void cmd_vol_up(void)
{
	int r;
	u8 vol_l10n_str[sizeof("xxx.xx")]; /* should be overkill */
	u8 response[STR_SZ];

	filt_frs.vol += VOL_DELTA;
	if (filt_frs.vol > 1.0)
		filt_frs.vol = 1.0;
	snprintf(vol_l10n_str, sizeof(vol_l10n_str), "%f", filt_frs.vol);
	pout("COMMAND:volume up to value %s(%s)\n", vol_l10n_str, filt_frs.muted ? "muted" : "unmuted");
	if (filt_frs.muted)
		return;
	r = av_filt_graph_send_cmd(filt_graph, "vol", "volume", vol_l10n_str,
						response, sizeof(response), 0);
	if (r < 0)
		warning("ffmpeg:volume context:unable to set the volume up to \"%s\":response from volume filter:\"%s\"\n", response);
}
static void cmd_vol_down(void)
{
	int r;
	u8 vol_l10n_str[sizeof("xxx.xx")]; /* should be overkill */
	u8 response[STR_SZ];

	filt_frs.vol -= VOL_DELTA;
	if (filt_frs.vol < 0.0)
		filt_frs.vol = 0.0;
	snprintf(vol_l10n_str, sizeof(vol_l10n_str), "%f", filt_frs.vol);
	pout("COMMAND:volume down to value %s(%s)\n", vol_l10n_str, filt_frs.muted ? "muted" : "unmuted");
	if (filt_frs.muted)
		return;
	r = av_filt_graph_send_cmd(filt_graph, "vol", "volume", vol_l10n_str,
						response, sizeof(response), 0);
	if (r < 0)
		warning("ffmpeg:volume context:unable to set the volume down to \"%s\":response from volume filter:\"%s\"\n", response);
}
static void cmd_vol_up_small(void)
{
	int r;
	u8 vol_l10n_str[sizeof("xxx.xx")]; /* should be overkill */
	u8 response[STR_SZ];

	filt_frs.vol += VOL_DELTA_SMALL;
	if (filt_frs.vol > 1.0)
		filt_frs.vol = 1.0;
	snprintf(vol_l10n_str, sizeof(vol_l10n_str), "%f", filt_frs.vol);
	pout("COMMAND:volume up to value %s(%s)\n", vol_l10n_str, filt_frs.muted ? "muted" : "unmuted");
	if (filt_frs.muted)
		return;
	r = av_filt_graph_send_cmd(filt_graph, "vol", "volume", vol_l10n_str,
						response, sizeof(response), 0);
	if (r < 0)
		warning("ffmpeg:volume context:unable to set the volume up to \"%s\":response from volume filter:\"%s\"\n", response);
}
static void cmd_vol_down_small(void)
{
	int r;
	u8 vol_l10n_str[sizeof("xxx.xx")]; /* should be overkill */
	u8 response[STR_SZ];

	filt_frs.vol -= VOL_DELTA_SMALL;
	if (filt_frs.vol < 0.0)
		filt_frs.vol = 0.0;
	snprintf(vol_l10n_str, sizeof(vol_l10n_str), "%f", filt_frs.vol);
	pout("COMMAND:volume down to value %s(%s)\n", vol_l10n_str, filt_frs.muted ? "muted" : "unmuted");
	if (filt_frs.muted)
		return;
	r = av_filt_graph_send_cmd(filt_graph, "vol", "volume", vol_l10n_str,
						response, sizeof(response), 0);
	if (r < 0)
		warning("ffmpeg:volume context:unable to set the volume down to \"%s\":response from volume filter:\"%s\"\n", response);
}
static void cmd_mute(void)
{
	int r;
	u8 vol_l10n_str[sizeof("xxx.xx")]; /* should be overkill */
	u8 response[STR_SZ];

	if (filt_frs.muted) {
		pout("COMMAND:unmuting\n");

		snprintf(vol_l10n_str, sizeof(vol_l10n_str), "%f",
								filt_frs.vol);
		r = av_filt_graph_send_cmd(filt_graph, "vol", "volume",
				vol_l10n_str, response, sizeof(response), 0);
		if (r < 0) {
			warning("ffmpeg:volume context:unable to mute the volume to 0:response from volume filter:%s\n", response);
		} else {
			filt_frs.muted = false;
		}
	} else {
		pout("COMMAND:muting\n");

		r = av_filt_graph_send_cmd(filt_graph, "vol", "volume",
			double_zero_l10n_str, response, sizeof(response), 0);
		if (r < 0) {
			warning("ffmpeg:volume context:unable to mute the volume to 0:response from volume filter:%s\n", response);
		} else {
			filt_frs.muted = true;
		}
	}
}
#define CHANS_N_NOT_OVERRIDDEN 0
#define RATE_NOT_OVERRIDDEN 0
#define FR_FMT_NOT_OVERRIDDEN AV_FR_FMT_NONE
int main(int argc, u8 **args)
{
	u8 *url;
	u8 *pcm_str;
	double initial_vol;
	unsigned int cb_slots_n;
	int best_st_idx;
	/* audio override -- start */
	/*
	 * we could have got direct parameters for alsa, but doing only an
	 * override triggers an autoconfiguration of any missing parameters
	 * based on initial codec params. In other words, the user is not
	 * required to provide all audio params, the code will try to fit the
	 * missing ones with the initial codec params, which is the default
	 * behavior.  
	 */
	int override_initial_ff_chans_n;
	int override_initial_ff_rate;
	enum av_fr_fmt override_initial_ff_audio_fr_fmt;
	/* audio override -- end */
	/* "turn on utf8" processing in used libs if any *AND* locale system */
	setlocale(LC_ALL, "");
	/* av_log_set_level(AV_LOG_VERBOSE); */
	/* av_log_set_level(AV_LOG_DEBUG); */
	url = 0;
	override_initial_ff_chans_n = CHANS_N_NOT_OVERRIDDEN;
	override_initial_ff_rate = RATE_NOT_OVERRIDDEN;
	override_initial_ff_audio_fr_fmt = FR_FMT_NOT_OVERRIDDEN;
	pcm_str = "default";
	initial_vol = 1.;
	cb_slots_n = 250;
	opts_parse(argc, args, &url, &pcm_str, &initial_vol, &cb_slots_n,
		&override_initial_ff_chans_n, &override_initial_ff_rate,
		&override_initial_ff_audio_fr_fmt);
	init_once(url, pcm_str, cb_slots_n);
	best_st_idx = find_best_st();
	rd_thd_start(best_st_idx);
	prepare(best_st_idx, initial_vol, override_initial_ff_chans_n,
		override_initial_ff_rate, override_initial_ff_audio_fr_fmt);
	wait_cb_filling();
	/* switch the ff log to stdout for metadata/etc dump */
	av_log_set_callback(ff_log_stdout);
	fmt_ctx_lock();
	av_dump_fmt(fmt_ctx, 0, url, 0);
	fmt_ctx_unlock();
	av_log_set_callback(av_log_default_callback);
	loop evts_loop();
	/* unreachable */
}
#undef CHANS_N_NOT_OVERRIDDEN
#undef RATE_NOT_OVERRIDDEN
#undef AUDIO_FR_FMT_NOT_OVERRIDDEN
/*----------------------------------------------------------------------------*/
/* alsa */
#undef snd_pcm_fmt_desc
#undef snd_pcm_fmt_set_silence
#undef SND_PCM_FMT_FLT
#undef SND_PCM_FMT_S16
#undef SND_PCM_FMT_S24
#undef SND_PCM_FMT_S32
#undef SND_PCM_FMT_S8
#undef snd_pcm_fmt_t
#undef SND_PCM_FMT_U16
#undef SND_PCM_FMT_U24
#undef SND_PCM_FMT_U32
#undef SND_PCM_FMT_U8
#undef SND_PCM_ST_PLAYBACK
#undef snd_pcm_frs_to_bytes
#undef snd_pcm_hw_params_get_chans
#undef snd_pcm_hw_params_get_chans_max
#undef snd_pcm_hw_params_get_chans_min
#undef snd_pcm_hw_params_get_buf_size
#undef snd_pcm_hw_params_get_fmt
#undef snd_pcm_hw_params_set_buf_size_near
#undef snd_pcm_hw_params_set_chans
#undef snd_pcm_hw_params_set_fmt
#undef snd_pcm_hw_params_test_chans
#undef snd_pcm_hw_params_test_fmt
#undef snd_pcm_sfrs_t
#undef snd_pcm_ufrs_t
/*----------------------------------------------------------------------------*/
/* ff */
#undef AV_AUDIO_FR_FMT_NONE
#undef av_bufsink_get_frs
#undef av_bufsrc_add_frs_flags
#undef AV_BUFSRC_FLAG_KEEP_REF
#undef AV_BUFSRC_FLAG_PUSH
#undef av_codec
#undef av_codec_ctx
#undef av_dump_fmt
#undef av_duration_estimation_method
#undef av_filt
#undef av_filt_ctx
#undef av_filt_get_by_name
#undef av_filt_graph
#undef av_filt_graph_alloc
#undef av_filt_graph_alloc_filt
#undef av_filt_graph_cfg
#undef av_filt_graph_dump
#undef av_filt_graph_free
#undef av_filt_graph_send_cmd
#undef av_filt_init_str
#undef av_filt_link
#undef av_find_best_st
#undef av_flush_bufs
#undef av_fmt_ctx
#undef AV_FMT_DURATION_FROM_BITRATE
#undef AV_FMT_DURATION_FROM_PTS
#undef AV_FMT_DURATION_FROM_ST
#undef av_fmt_find_st_info
#undef av_fmt_flush
#undef av_fmt_open_input
#undef av_fr_fmt_is_planar
#undef AV_FR_FMT_FLT
#undef AV_FR_FMT_FLTP
#undef AV_FR_FMT_S16
#undef AV_FR_FMT_S16P
#undef AV_FR_FMT_S32
#undef AV_FR_FMT_S32P
#undef AV_FR_FMT_U8P
#undef AV_FR_FMT_U8
#undef av_frs_alloc
#undef av_frs_set_silence
#undef av_frs_unref
#undef av_get_fr_fmt_name
#undef av_get_fr_fmt_str
#undef av_io_flush
#undef AV_MEDIA_TYPE_AUDIO
#undef av_pkt
#undef av_pkt_unref
#undef av_read_pkt
#undef av_rational
#undef av_seek_pkt
#undef av_receive_frs
#undef av_fr_fmt
#undef av_frs
#undef av_sd_pkt
#undef av_st
#undef format
#undef fr_fmt
#undef fr_rate
#undef frs_n
#undef st_idx
#undef sts
/*----------------------------------------------------------------------------*/
#if __GNUC__ > 4
	#undef atomic_u8
#else
	#undef atomic_u8
	#undef atomic_uint
	#undef atomic_int
	#undef atomic_load
	#undef atomic_store
#endif
/*----------------------------------------------------------------------------*/
#undef ARRAY_N
#undef esc_seq_sz
#undef f32
#undef INPUT_ESC_SEQ_TIMEOUT_SECS_N
#undef loop
#undef PCM_POLLFDS_N_MAX
#undef PKTS_N_MAX
#undef SEEK_DELTA
#undef SEEK_DELTA_BIG
#undef STR_SZ
#undef u16
#undef u32
#undef u8
#undef U8_MAX
#undef VOL_DELTA
#endif
